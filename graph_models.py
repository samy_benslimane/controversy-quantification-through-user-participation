#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 24 11:19:51 2022

@author: samy
"""

from sklearn.metrics import accuracy_score, log_loss
# from scipy.stats import entropy

from dgl.nn.pytorch import GATConv
from dgl.nn import GraphConv
from dgl.nn import SAGEConv
import dgl

import matplotlib.pyplot as plt

import torch.nn.functional as F
import torch.nn as nn
import torch

import numpy as np
import random
import copy
import math
import time
import os

import bert_processing as bp
import graph_processing as gp
import util as ut
import scipy.stats as stats


early_stop = 100 #20 None

def write_file(p,text):
    with open(p, 'a') as f:
        f.write(str(text))
        
    return 1
    

############################################ Training functions ################################################
"""
send random balanced idx users fro training
"""
def get_balanced_random_idx_users(x0, x1, epoch, is_test_set):
    if(is_test_set == True):
        if(len(x0) < len(x1)):
            min_lab = len(x0)
            r = np.array(random.sample(set(x1), min_lab))
            train_x = np.concatenate((r, x0), axis=0)
        elif(len(x0) > len(x1)):
            min_lab = len(x1)
            r = np.array(random.sample(set(x0), min_lab))
            train_x = np.concatenate((r, x1), axis=0)
        else:
            min_lab = len(x0)
            r = x0
            train_x = np.concatenate((x0, x1), axis=0)
    else:
        min_lab = len(x0)
        r = x0
        train_x = np.concatenate((x0, x1), axis=0) 
        
    if(epoch == 0):
        print(' >> total by for each label to train:', min_lab, '(', len(r), len(r)*2, len(train_x), '), is_test_set=',
              is_test_set)
        
    return train_x
        

"""
get the minimum number of nodes by graph
"""
def get_min_nodes_by_graph(dataset):
    min_nodes = None
    for G, _ in dataset:
        nbs = len(G.nodes())
        if(min_nodes is None or nbs < min_nodes):
            min_nodes = nbs
           
    print(' > min_nodes:', min_nodes)
    return min_nodes
        

"""
Return list of True, False. Size: nbs_nodes (with len(True) == min_nodes)
"""
def get_temp_train_mask(G, min_nodes_by_graph):
    train_mask = [False] * len(G.nodes())
    indexes = random.sample(range(len(G.nodes())), min_nodes_by_graph)
    
    for index in indexes:
        train_mask[index] = True
        
    return train_mask


"""
Training loop, semi-supervised model

            model = gm.train_loop(TRAIN_SET, TEST_SET, 
                                  only_text, model, 
                                  optimizer, epochs, device, batch_gnn, 
                                  is_edge_weighted, text_model, xp_path, model_name)
        #- fonction random qui renvoi les noeuds to train pour current epoch
        #- save score into 2 files: train_score.json & test_score.json        
"""
def train_loop(TRAIN_SET, TEST_SET, 
               only_text, model, 
               optimizer, epochs, device, batch_gnn, 
               is_edge_weighted, text_model, path, model_name):
    
    min_nodes_by_graph = get_min_nodes_by_graph(TRAIN_SET)
    
    # set parameters of EPOCHS LOOP
    final_model = copy.deepcopy(model)
    start_epoch = 0
    best_train_epoch = -1
    best_train_loss = None
    list_train_loss = []
    best_train_acc = None
    list_train_acc = []
    best_test_loss = -1 #depend on best train epoch
    best_test_acc = -1 #depend on best train epoch
    list_test_acc = []
    cpt_early_stop = 0
    runtime = 0
    
    fold_chkpt = path + ut.checkpoint_folder
    if(os.path.isdir(fold_chkpt) == False):
        os.mkdir(fold_chkpt)
    
    if(len(os.listdir(fold_chkpt)) > 0):
        file = os.listdir(fold_chkpt)[0]
        checkpoint_path = fold_chkpt + file
    
        if(os.path.exists(checkpoint_path)):
            print(' > found checkpoint!')
            chk = torch.load(checkpoint_path)
            model.load_state_dict(chk['model_state_dict'])
            optimizer.load_state_dict(chk['optimizer_state_dict'])
            start_epoch = chk['epoch']+1
            best_train_epoch = chk['best_train_epoch']
            best_train_loss = chk['best_train_loss']
            list_train_loss = chk['list_train_loss']
            best_train_acc = chk['best_train_acc']
            list_train_acc = chk['list_train_acc']
            best_test_loss = chk['best_test_loss']
            best_test_acc = chk['best_test_acc']
            list_test_acc = chk['list_test_acc']
            cpt_early_stop = chk['cpt_early_stop']
            runtime = chk['runtime']
            
            final_model = torch.load(path+'final_TEMP.pth')
    
    print(' >> early_stop=', early_stop)
    for e in range(start_epoch, epochs):
        print("------------------", e, "---------------------------")
        t_ep = time.time()
        ################# 1. TRAINING  #################
        model.train()
        total_loss = 0
        total_acc = 0
        total_train_x0 = 0
        total_train_x1 = 0
        for G, dict_node_features in TRAIN_SET:
            #1.1 PREPARE tHE GRAPH
            n_nodes = []
            features = []
            idx_none = []
            # for k, v in dict_node_features.items():
            #     print(v)
            #     n_nodes.append(k)
            #     features.append(v)
            #     if(v is None):
            #         idx_none.append(k)
            # if(text_model == False):
            #     features = [torch.tensor(f, dtype=torch.double) for f in features]
            for k in G.nodes():
                n_nodes.append(k)
                if(k in dict_node_features.keys()):
                    v = dict_node_features[k]
                else:
                    v = [0]
                features.append(v)
                if(v is None):
                    idx_none.append(k)
            if(text_model == False):
                features = [torch.tensor(f, dtype=torch.double) for f in features]
            
            labels = G.ndata['label']
            train_mask = get_temp_train_mask(G, min_nodes_by_graph)
                
            all_labels = labels[train_mask]
            all_labels = all_labels.cpu().detach().numpy()
            all_index = np.where(np.array(train_mask) == True)[0]
            
            if(only_text == True):
                temp_all_index = []
                temp_all_labels = []
                cpt=0
                for i in range(len(all_index)):
                    if(dict_node_features[all_index[i]] is not None):
                        temp_all_index.append(all_index[i])
                        temp_all_labels.append(all_labels[i])
                    else:
                        cpt+=1
                all_index = temp_all_index
                all_labels = temp_all_labels               
        
            x0 =np.array([all_index[i] for i in range(len(all_index)) if all_labels[i] == 0])
            x1 =np.array([all_index[i] for i in range(len(all_index)) if all_labels[i] == 1])
            total_train_x0 += len(x0)
            total_train_x1 += len(x1)
            
            
            #1.2 TRAIN THE MODEL
            optimizer.zero_grad()
            if(is_edge_weighted == True):
                _, h, _, logits = model(G, features, G.edata['weight'], device=device)
            else:
                _, h, _, logits = model(G, features, device=device)

            # Compute prediction
            preds = logits.argmax(1)
    
            # Compute loss
            loss = F.cross_entropy(logits[train_mask], labels[train_mask])
            
            # TRAIN acc
            train_acc = (preds[train_mask] == labels[train_mask]).float().mean()
            
            # Backward
            loss.backward()
            optimizer.step()
            
            total_loss += loss.item()
            total_acc += train_acc.item()
            
        # print('---- train preds -----')  # TODO delete
        # print(logits)
        # print('--------------------- \n')

        #1.3 COMPUTE LOSS
        epoch_loss = round(total_loss / len(TRAIN_SET), 5)
        epoch_acc = round(total_acc / len(TRAIN_SET), 5)
        
        #1.4 SAVE FINAL MODEL -- early stopping when train-loss increase --
        if(early_stop is None):
            final_model = copy.deepcopy(model)
        else:
            if(best_train_loss is not None and best_train_loss < epoch_loss):
                if(cpt_early_stop >= early_stop):
                    print(' > hop we stop, new_train_loss=', epoch_loss, 'best train loss=', best_train_loss, '==> cpt_early=', cpt_early_stop)
                    break
                else:
                    cpt_early_stop+=1
            else:
                print(' ! best model !')
                best_train_epoch = e
                best_train_loss = epoch_loss
                best_train_acc = epoch_acc
                final_model = copy.deepcopy(model)
                cpt_early_stop = 0
        
        #runtime
        train_epoch_runtime = time.time() - t_ep
        runtime += train_epoch_runtime
        
        ################# 2. EVALUATION  #################
        model.eval()
        all_test_loss = []
        all_test_preds = []
        all_test_labs = []
        total_test_x0 = 0
        total_test_x1 = 0
        for G, dict_node_features in TEST_SET:
            #2.1 PREPARE tHE GRAPH
            n_nodes = []
            features = []
            idx_none = []
            # for k, v in dict_node_features.items():
            #     n_nodes.append(k)
            #     features.append(v)
            #     if(v is None):
            #         idx_none.append(k)
            # if(text_model == False):
            #     features = [torch.tensor(f, dtype=torch.double) for f in features]
            for k in G.nodes():
                n_nodes.append(k)
                if(k in dict_node_features.keys()):
                    v = dict_node_features[k]
                else:
                    v = [0]
                features.append(v)
                if(v is None):
                    idx_none.append(k)
            if(text_model == False):
                features = [torch.tensor(f, dtype=torch.double) for f in features]
                
        
            labels = G.ndata['label']
            test_mask = [True] * len(G.nodes())
                
            all_labels = labels[test_mask]
            all_labels = all_labels.cpu().detach().numpy()
            all_index = np.where(np.array(test_mask) == True)[0]
            
            if(only_text == True):
                temp_all_index = []
                temp_all_labels = []
                cpt=0
                for i in range(len(all_index)):
                    if(dict_node_features[all_index[i]] is not None):
                        temp_all_index.append(all_index[i])
                        temp_all_labels.append(all_labels[i])
                    else:
                        cpt+=1
                all_index = temp_all_index
                all_labels = temp_all_labels               
        
            x0 =np.array([all_index[i] for i in range(len(all_index)) if all_labels[i] == 0])
            x1 =np.array([all_index[i] for i in range(len(all_index)) if all_labels[i] == 1])
            total_test_x0 += len(x0)
            total_test_x1 += len(x1)
            
            
            #2.2 GET MODEL PREDICTION
            with torch.no_grad():
                if(is_edge_weighted == True):
                    _, h, _, logits = model(G, features, G.edata['weight'], device=device)
                else:
                    _, h, _, logits = model(G, features, device=device)

            # Compute prediction
            preds = logits.argmax(1)
            proba = logits[:, 1]
            all_test_loss += proba[test_mask].tolist()
            all_test_preds += preds[test_mask].tolist()
            all_test_labs += labels[test_mask].tolist()
            
        # print('---- test preds -----') #TODO delete
        # print(all_test_preds)
        # logits.argmax(1)
        # print('--------------------- \n')
        
        #2.3 GET TEST ACCURACY
        test_acc = accuracy_score(all_test_labs, all_test_preds)
        test_loss = F.binary_cross_entropy(torch.tensor(all_test_loss, dtype=torch.float32), torch.tensor(all_test_labs, dtype=torch.float32))
        if (best_train_epoch == e):
            best_test_loss = test_loss.item()
            best_test_acc = test_acc
        

        ################# 3. SAVE CHKPT & MODEL & METADATA  #################        
        #3.1 SAVE LOSS & ACC train test sets
        list_train_loss.append(epoch_loss)
        list_train_acc.append(epoch_acc)
        list_test_acc.append(test_acc)
                
        #3.2 SAVE FINAL MODEL
        if(best_train_epoch == e):
            save_model(path+'final_TEMP.pth', final_model)
        
        #3.3 SAVE CHKPT MODEL
        #save checkpoint
        for old_file in os.listdir(fold_chkpt):
              os.remove(fold_chkpt+old_file)
             
        # save current epochs 
        checkpoint_file = fold_chkpt + str(e)+'_'+model_name 
        torch.save({
            'epoch': e,
            'model_state_dict': model.state_dict(),
            'optimizer_state_dict': optimizer.state_dict(),
            'best_train_epoch': best_train_epoch,
            'best_train_loss': best_train_loss,
            'list_train_loss': list_train_loss,
            'best_train_acc': best_train_acc,
            'list_train_acc': list_train_acc,
            'best_test_loss': best_test_loss,
            'best_test_acc': best_test_acc,
            'list_test_acc': list_test_acc,
            'cpt_early_stop': cpt_early_stop,
            'runtime': runtime
            }, checkpoint_file)
        
        #3.4 PRINT INFO ON EPOCHS
        if(e%1 == 0):
            print('In epoch {}, train loss: {:.3f} + train acc: {:.3f} || test acc: {:.3f}'.format(
                e, epoch_loss, epoch_acc, test_acc))
            print('           > BEST: In epoch {}, train_loss: {:.3f} + train_acc: {:.3f} || test_acc: {:.3f}, loss:{:.3f}'.format(
                best_train_epoch, best_train_loss, best_train_acc, best_test_acc, best_test_loss))
            
            print(' --> time :', round(((time.time() - t_ep)/60), 2), 'min --- ', total_train_x0, total_train_x1, total_test_x0, total_test_x1)
            print("------------------------------------------------- \n")

    # FINALISATION
    #save model
    print('\n --- Save final model ---')
    save_model(path+model_name, final_model)
    train_info = {'train_runtime': round(runtime/60,4),
                  'train_loss': best_train_loss,
                  'test_acc': best_test_acc,
                  'test_loss': best_test_loss}
    print('    -> Training info:', train_info)
    gp.write_json(path+'train_info.json', train_info)
    
    #plot         
    path = path + 'plot/'
    if(os.path.isdir(path) == False):
        os.mkdir(path)
    
    p_train_loss = path + "train_loss.png"
    plt.plot(range(len(list_train_loss)), list_train_loss)
    plt.savefig(fname = p_train_loss)
    plt.show()
    
    p_train_acc = path + "train_acc.png"
    plt.plot(range(len(list_train_acc)), list_train_acc)
    plt.savefig(fname = p_train_acc)
    plt.show()
    
    p_test_acc = path + "test_acc.png"
    plt.plot(range(len(list_test_acc)), list_test_acc)
    plt.savefig(fname = p_test_acc)
    plt.show()
                
    return final_model



"""
Predict result from trained model

1. predict by graph
2. print accuracy by graph, and save results in json file
3. print accuracy global, and save results in json file

embedding_file = current_path + type_set+'_predicted_nodes.pickle'
    {'pelosi': [node_ids, text_embeddings, node_embeddings, node_probas, node_predictions]}

json_file = current_path + type_set+'_results.pickle'
    {'TEST':
         'qc_score':
                'pelosi': {'proba': 0.81, 'prediction': 0.79, bayes: 1, 'accuracy': 0.78, 'topic_entropy': 0.3}
                ...
         'metrics':
                'accuracy': 0.45
                'set_entropy': xxx
    'TRAIN':
        ...
    }
"""
def predict_model(dataset, model, text_model, is_edge_weighted, device, current_path, type_set='TEST', is_timeline=False):
    print('||||||||||||||||||||||||||', type_set, ' PREDICTIONS ||||||||||||||||||||||||||')
    embedding_file = current_path + type_set+'_predictions.pickle'
    json_file = current_path + 'results.json'
    to_line = True
    data_objects_by_topic = {}
    results = {}
    
    #labels
    if(type_set == 'MY_DATA'):
        topic_labels = ut.my_data_labels
    else:
        topic_labels = ut.data_zarate_labels
    
    if(os.path.isfile(embedding_file) == True):
        data_objects_by_topic = gp.load_pickle(embedding_file)    
    if(os.path.isfile(json_file) == True):
        results = gp.load_json(json_file)  
        
    if(type_set in results.keys()):
        print(' > already computed, but still..')
        data_objects_by_topic = gp.load_pickle(embedding_file)
        # return results, data_objects_by_topic
    else:
        results[type_set] = {}
        
    if(is_timeline == False):
        sorted_topic = {k:topic_labels[k] for k in dataset.keys()}
    else:
        for k in dataset.keys():
            print(k)
            print('_'.join(k.split('_')[:-1]))
            print('***')
        sorted_topic = {k:topic_labels['_'.join(k.split('_')[:-1])] for k in dataset.keys()}
        
    sorted_topic = [k for k, v in sorted(sorted_topic.items(), key=lambda x: x[1])]
    list_all_probas = []
    list_all_predictions = []
    list_all_labels = []
    res_by_topics = {}
    print(" BON : ", len(sorted_topic), 'topics here \n\n')
    for topic in sorted_topic:
        # if(topic not in ut.data_zarate_split['TEST']): #TODO delete
        #     continue
        if(is_timeline == False):
            current_label = topic_labels[topic]
        else:
            current_label = topic_labels['_'.join(topic.split('_')[:-1])]
        if(topic in data_objects_by_topic.keys()):
            node_ids, text_embeddings, node_embeddings, node_probas, node_predictions = data_objects_by_topic[topic]
            node_labels = [current_label] * len(node_predictions)
        else:
            G, dict_node_features = dataset[topic]

            #1. GET PREDICTIONS
            n_nodes = []
            features = []
            for k, v in dict_node_features.items():
                n_nodes.append(k)
                features.append(v)
            if(text_model == False):
                features = [torch.tensor(f, dtype=torch.double) for f in features]
            
            t_mod = time.time()
            model.eval()
            with torch.no_grad():
                if(is_edge_weighted == True):
                    gnn_inputs, embeddings, _, probas = model(G, features, G.edata['weight'], device=device)
                else:
                    gnn_inputs, embeddings, _, probas = model(G, features, device=device)
            runtime = round(((time.time() - t_mod)/60), 2)
            
            node_ids = G.nodes().tolist()
            text_embeddings = gnn_inputs.cpu().detach().numpy()
            node_embeddings = embeddings.cpu().detach().numpy()
            node_probas = probas[:,1].tolist() #get prediction of user being controversial
            node_predictions = probas.argmax(1).tolist()
            node_labels = [current_label] * len(node_predictions)
            
            data_objects_by_topic[topic] = [node_ids, text_embeddings, node_embeddings, node_probas, node_predictions]
        
        #2. GET METRICS
        list_all_probas += node_probas
        list_all_predictions += node_predictions
        list_all_labels += node_labels
        
        #3. GET SCORES
        naive_bayes = gp.get_naive_score(node_probas)
        
        qc_probas = round(sum(node_probas)/len(node_probas) , 3) 
        qc_preds = round(sum(node_predictions)/len(node_predictions) , 3)
        median = np.median(node_probas)

        acc_topic = round(accuracy_score(node_labels, node_predictions), 3)
        topic_entropy = round(get_shannon_entropy(node_probas), 3)
        
        #4. PRINT
        if(current_label == 1 and to_line == True):
            print('\n')
            to_line=False
        print('----> ', topic, ': LAB=,', current_label, 'qc_probas (preds) =', qc_probas, '(', qc_preds, ')', '---> nb:', naive_bayes)
        
        #5. SAVE IN results topic info
        res_by_topics[topic] = {'proba': qc_probas,
                                'prediction': qc_preds,
                                'median': median,
                                'bayes': naive_bayes,
                                'topic_shannon_entropy': topic_entropy,
                                'accuracy': acc_topic,
                                'runtime': runtime}
        
    test_loss = F.binary_cross_entropy(torch.tensor(list_all_probas, dtype=torch.float32), torch.tensor(list_all_labels, dtype=torch.float32)) 
    print('>>> test loss:', test_loss)
    #6. SAVE in results dataset + SAVE IN JSON FILE dataset    
    results[type_set]['qc_score'] = res_by_topics
    results[type_set]['metric'] = {'accuracy': accuracy_score(list_all_labels, list_all_predictions),
                                   # 'set_cross_entropy': log_loss(list_all_labels, list_all_probas),
                                   'set_shannon_entropy': get_shannon_entropy(list_all_probas)}
    gp.write_json(json_file, results)
    
    #7. SAVE IN PICKLE FILE
    gp.pickle_save(embedding_file, data_objects_by_topic)
        
    return results, data_objects_by_topic


"""
Create and return a gnn model
input :
    + graph_model : type of gcn model
    + parameters : init model parameters
    
    + input == user feature
            + feat = node degree --> text_model = False, pre_trained_feat = False
            + feat = static embedding --> text_model = False, pre_trained_feat = True
    
    + input == tweet features
            + feat = embedding + train last layer --> text_model = ut.BASIC, all_layer_train = False
            + feat = embedding + train ALL layers --> text_model = ut.BASIC, all_layer_train = True
"""
def get_model(graph_model, text_model, pre_trained_feat, all_layer_train, only_text, layer_dimension, text_aggregator, default_features):
    model = CustomModel(layer_dimension, graph_model, text_model, pre_trained_feat, all_layer_train, only_text, text_aggregator, default_features)
    print('--- model layer')
    params = list(model.named_parameters())
    print([n for n, p in params])
    
    return model 


"""
get shannon entropy from node probabilities output of class 1
"""
def get_shannon_entropy(node_probas_class_1):    
    # Calcul des probabilités de classe 0 (compléments des probabilités de classe 1)
    # probabilities_class_0 = [1 - p for p in node_probas_class_1]
    
    # Calcul de l'entropie
    entropy = -sum(p * math.log2(p) for p in node_probas_class_1) / len(node_probas_class_1)
    
    return entropy

############################################### MODELS ####################################################

"""
model combining BERT and GNN
    + input == user feature
            (1) feat = node degree --> text_model = False, pre_trained_feat = False
            (2) feat = static embedding --> text_model = False, pre_trained_feat = True
    
    + input == tweet features
            (3) feat = embedding + train last layer --> text_model = ut.BASIC, all_layer_train = False
            (4) feat = embedding + train ALL layers --> text_model = ut.BASIC, all_layer_train = True
"""
class CustomModel(nn.Module):
    def __init__(self, layer_dimension, graph_model = False,
                 text_model = False, pre_trained_feat = False, all_layer_train = False, only_text = False,
                 text_aggregator = ut.MAX_AGGR, default_features = None):
        super(CustomModel, self).__init__()
        self.default_features = default_features
        
        self.only_text = only_text #(0) -> works for (2)(3)(4)
        
        #0. init parameters (1)(2)(3)(4)
        self.graph_model_type = graph_model
        
        self.text_model_type = text_model
        self.is_pre_trained_feat = pre_trained_feat
        self.is_all_layer_train = all_layer_train
        
        #1. Initialize BERT layer (4)
        self.bert_model = None
        if(text_model != False and all_layer_train == True):
            print(' > with bert!')
            self.bert_model = bp.get_model(text_model)
            
        #2. init embedding layer (3)(4)
        self.embedding_text_layer = nn.Linear(768, 768)
        self.dropout = nn.Dropout(0.2)
        
            
        #3. init aggregator (3)(4)
        self.text_aggregator = text_aggregator
        print('> text aggreg:', self.text_aggregator)
            
        #4. init size of input for gnn (1)(2)(3)(4)
        if(self.text_model_type == False and self.is_pre_trained_feat == False): #(1)
            in_feats = 1
        else: # (2)(3)(4)
            in_feats = 768
        print('     > (shape feature + layer_dimension):', in_feats, layer_dimension)
        
        if(self.text_model_type != False):
            self.embedding_layer = nn.Linear(768, 768)
        
        if(self.only_text == False):
            #5. Initialize GCN layers (1)(2)(3)(4)
            if(graph_model == ut.GRAPHSAGE):
                self.gnn_model = GraphSAGE(in_feats, layer_dimension)
            elif(graph_model == ut.GAT):
                self.gnn_model = GAT(in_feats, layer_dimension)
            elif(graph_model == ut.GCN):
                self.gnn_model = GCN(in_feats, layer_dimension)
            else:
                print('/!\ ', graph_model, ' is not a possible gnn model /!\ ')
        else:
            self.classifier_layer = nn.Linear(768, 2)


    """
    in_feat = list of features by user (tokenized_batch OR degree #### avant : dictionnary of features (tokenized batch, or list of user)
    """
    def forward(self, g, in_feat, edge_weight = None, device = None):
        # list_features_by_user = []
        # for k in range(len(in_feat)):
        #     list_features_by_user.append(in_feat[k])
        list_features_by_user = in_feat

        #1. get BERT features  (1)(2)(3)(4)
        if(self.text_model_type != False): #(3)(4)
            if(self.is_all_layer_train == True): #(4)
                bert_outputs = []
                for tokenized_batch in list_features_by_user: #in_feat by user
                    if(tokenized_batch is not None):
                        tokenized_batch = {k:v.type(torch.long).to(device) for k,v in tokenized_batch.items()}
                        kept_mask = bp.get_tokens_keep_mask(tokenized_batch, device)
                        
                        logits, tweet_embeddings = self.bert_model(tokenized_batch, kept_mask)
                        
                        tweet_embeddings = self.dropout(tweet_embeddings)
                        tweet_embeddings = self.embedding_layer(tweet_embeddings) ## extract the 1st token's embeddings
                        
                        #Aggregate (mean) embeddings by user
                        if(self.text_aggregator == ut.MEAN_AGGR):
                            user_embedding = tweet_embeddings.mean(dim=0).values
                        elif(self.text_aggregator == ut.MAX_AGGR):
                            user_embedding = tweet_embeddings.max(dim=0).values
                        bert_outputs.append(user_embedding)
                    else:
                        bert_outputs.append(torch.tensor(self.default_features, device=device))
                                        
                #stack and prepare for gnn model
                gnn_inputs = torch.stack(bert_outputs)
            else: # (3)
                bert_outputs = []
                for batch in list_features_by_user: #in_feat by user
                   if(batch is not None):
                        batch = torch.tensor(batch, device = device)
    
                        tweet_embeddings = self.dropout(batch)
                        tweet_embeddings = self.embedding_layer(tweet_embeddings) ## extract the 1st token's embeddings
                        #Aggregate (mean) embeddings by user
                        if(self.text_aggregator == ut.MEAN_AGGR):
                            user_embedding = tweet_embeddings.mean(dim=0)
                        elif(self.text_aggregator == ut.MAX_AGGR):
                            user_embedding = tweet_embeddings.max(dim=0).values
                        bert_outputs.append(user_embedding)
                   else:
                        bert_outputs.append(torch.tensor(self.default_features, device=device))
                        
                #stack and prepare for gnn model
                gnn_inputs = torch.stack(bert_outputs)
                           
        else: # (1)(2)
            if(self.is_pre_trained_feat == False): # (1)
                gnn_inputs = torch.tensor(list_features_by_user, device = device)
                gnn_inputs = gnn_inputs.view(gnn_inputs.size(0), 1)
            else: # (2)
                gnn_inputs = torch.stack(list_features_by_user)
                gnn_inputs = gnn_inputs.to(device)
        # print('3. shape_gnn_input:', gnn_inputs.shape)
        
        #2. GCN processing
        gnn_inputs = gnn_inputs.double()
        if(self.only_text == False):
            embeddings, gnn_outputs, predictions = self.gnn_model(g, gnn_inputs, edge_weight = None)
        else:
            embeddings = gnn_inputs
            gnn_outputs = self.classifier_layer(gnn_inputs)
            predictions = F.softmax(gnn_outputs, dim=1)
            
        return gnn_inputs, embeddings, gnn_outputs, predictions



"""
Build a two-layer GraphSAGE model
    - in_feats : Input feature size
    - h_feats : Output feature size
    - aggregator : aggregator used to aggregate neighbors
"""
class GraphSAGE(nn.Module):
    def __init__(self, in_feats, layer_dimension, num_class=2, aggregator = 'mean'):
        super(GraphSAGE, self).__init__()
        self.nbs_layer = len(layer_dimension)
        self.num_class = num_class
        
        if(self.nbs_layer == 0):
            self.pred_layer = SAGEConv(in_feats, num_class, aggregator)
        else:
            self.conv1 = SAGEConv(in_feats, layer_dimension[0], aggregator)
            
            if(self.nbs_layer > 1):
                self.conv2 = SAGEConv(layer_dimension[0], layer_dimension[1], aggregator)
                
            if(self.nbs_layer > 2):
                self.conv3 = SAGEConv(layer_dimension[1], layer_dimension[2], aggregator)
                
            self.pred_layer = SAGEConv(layer_dimension[-1], num_class, aggregator)
        
        print('-- nbs_layer:', self.nbs_layer+1, '| aggreg:', aggregator, '--')

    def forward(self, g, in_feat, edge_weight = None):
        if(self.nbs_layer == 0):
            preds = self.pred_layer(g, in_feat, edge_weight=edge_weight)
            return preds, preds, F.softmax(preds, dim=1)
        
        h = self.conv1(g, in_feat, edge_weight=edge_weight)
        if(self.nbs_layer > 1):
            h = F.relu(h)
            h = self.conv2(g, h, edge_weight=edge_weight)
            
            if(self.nbs_layer > 2):
                h = F.relu(h)
                h = self.conv3(g, h, edge_weight=edge_weight)
                
        x = F.relu(h)
        preds = self.pred_layer(g, x, edge_weight=edge_weight)
        
        return h, preds, F.softmax(preds, dim=1)


"""
Build a two-layer GAT model
    - in_feats : Input feature size
    - hidden_dim : hiddent dimension size
    - h_feats : Output feature size
    - num_head : anumber of multi-head attention of 1st layer
"""
class GAT(nn.Module):
    def __init__(self, in_feats, layer_dimension, num_class=2, num_heads=2):
        super(GAT, self).__init__()
        
        self.nbs_layer = len(layer_dimension)
        self.num_class = num_class
        
        
        if(self.nbs_layer == 0):
            self.pred_layer = GATConv(in_feats, num_class, 1, allow_zero_in_degree=True)
        else:
            if(self.nbs_layer == 1):
                self.conv1 = GATConv(in_feats, layer_dimension[0], 1, allow_zero_in_degree=True)
                num_heads = 1
            else:
                self.conv1 = GATConv(in_feats, layer_dimension[0], num_heads, allow_zero_in_degree=True)
                
            if(self.nbs_layer == 2):
                self.conv2 = GATConv(layer_dimension[0] * num_heads, layer_dimension[1], 1, allow_zero_in_degree=True)
                num_heads = 1
            else:
                if(self.nbs_layer > 1):
                    self.conv2 = GATConv(layer_dimension[0] * num_heads, layer_dimension[1], num_heads, allow_zero_in_degree=True)
                
            if(self.nbs_layer == 3):
                self.conv3 = GATConv(layer_dimension[1] * num_heads, layer_dimension[2], 1, allow_zero_in_degree=True)
                num_heads = 1

            self.pred_layer = GATConv(layer_dimension[-1] * num_heads, num_class, 1, allow_zero_in_degree=True)
        
        print('-- nbs_layer:', self.nbs_layer+1, '| num_heads_by_layer:', num_heads, '--')

    def forward(self, g, in_feat, edge_weight = None):
        if(self.nbs_layer == 0):
            preds = self.pred_layer(g, in_feat)
            preds = preds.squeeze()
            if(len(preds.shape) == 1):
                preds = preds.view(1, -1)    
            soft_preds = F.softmax(preds, dim=1)
            return preds, preds, soft_preds

        h = self.conv1(g, in_feat)
        
        if(self.nbs_layer > 1):
            h = h.view(-1, h.size(1) * h.size(2)) # (in_feat, num_heads, out_dim) -> (in_feat, num_heads * out_dim)
            h = F.elu(h)
            h = self.conv2(g, h)
            
            if(self.nbs_layer > 2):
                h = h.view(-1, h.size(1) * h.size(2)) # (in_feat, num_heads, out_dim) -> (in_feat, num_heads * out_dim)
                h = F.elu(h)
                h = self.conv3(g, h)
                
        embeddings = h.squeeze() # (in_feat, 1, out_dim) -> (in_feat, out_dim)
        
        h = h.view(-1, h.size(1) * h.size(2)) # (in_feat, num_heads, out_dim) -> (in_feat, num_heads * out_dim)
        h = F.elu(h)        
        preds = self.pred_layer(g, h)
        preds = preds.squeeze() # (in_feat, num_heads, out_dim) -> (in_feat, num_heads * out_dim)
        
        if(len(preds.shape) == 1):
            preds = preds.view(1, -1)
        
        soft_preds = F.softmax(preds, dim=1)
        
        return embeddings, preds, soft_preds
    

"""
Build a two-layer GCN model
    - in_feats : Input feature size
    - hidden_dim : hiddent dimension size
    - h_feats : Output feature size
"""
class GCN(nn.Module):
    def __init__(self, in_feats, layer_dimension, num_class=2):
        super(GCN, self).__init__()
        
        self.nbs_layer = len(layer_dimension)
        self.num_class = num_class
        
        self.drop = nn.Dropout(p=0.2)
        
        self.conv1 = GraphConv(in_feats, layer_dimension[0], norm='both', weight=True, bias=True, allow_zero_in_degree=True)
        
        if(self.nbs_layer > 1):
            self.conv2 = GraphConv(layer_dimension[0], layer_dimension[1], norm='both', weight=True, bias=True, allow_zero_in_degree=True)
        
        if(self.nbs_layer > 2):
            self.conv3 = GraphConv(layer_dimension[1], layer_dimension[2], norm='both', weight=True, bias=True, allow_zero_in_degree=True)

        self.pred_layer = GraphConv(layer_dimension[-1], num_class, norm='both', weight=True, bias=True, allow_zero_in_degree=True)

        print('-- nbs_layer:', self.nbs_layer+1, '| norm: both --')

    def forward(self, g, in_feat, edge_weight = None):
        h = self.conv1(g, in_feat, edge_weight=edge_weight)
        
        if(self.nbs_layer > 1):
            h = F.relu(h)
            h = self.conv2(g, h, edge_weight=edge_weight)
            
            if(self.nbs_layer > 2):
                h = F.relu(h)
                h = self.conv3(g, h, edge_weight=edge_weight)
               
        # print(h)
        h = self.drop(h)
        # print(h)
        x = F.relu(h)
        # print(x)
        preds = self.pred_layer(g, x, edge_weight=edge_weight)
        # print(preds)
        
        return h, preds, F.softmax(preds, dim=1)
          
    
############################################### OTHER ####################################################


"""
Save GNN model
"""
def save_model(model_path, model):
    torch.save(model, model_path)
    
    return 1

"""
Load GNN model
"""
def load_model(model_path):
    model = torch.load(model_path)
    
    return model


def get_list_user_manual_label(dict_node_label):
    dict_user_labelled = {}
    for idx, l in dict_node_label.items():
        if(l['label'] == 0 or l['label'] == 1):
            dict_user_labelled[idx] = l['label']
        
    #3. Transform into 2 list
    final_dict = {}
    labels = []
    for idx, l in dict_user_labelled.items():
        if(len([x for x in labels if x==l]) < 30):
            final_dict[idx] = l
            labels.append(l)
            
    return final_dict
    
    
"""
get test accuracy from manual labelling users
"""
def get_accuracy_test_from_manual_labelling(dict_node_prediction, dict_node_label):
    predictions = []
    labels = []
    for idx, l in dict_node_label.items():
            labels.append(l)
            predictions.append(dict_node_prediction[idx])
    
    acc = accuracy_score(labels, predictions)
    if(acc<0.5):
        acc = 1-acc
    
    return acc

"""
optimize learning rates
"""
def configure_optimizers(model, lr_gnn, lr_bert):
    params = list(model.named_parameters())
    def is_backbone(n, keyword): return keyword in n
    grouped_parameters = [
        {"params": [p for n, p in params if is_backbone(n, 'bert')], 'lr': lr_bert},
        # {"params": [p for n, p in params if (not is_backbone(n, 'bert') and not is_backbone(n, 'gnn_model'))], 'lr': lr_bert * 100},
        {"params": [p for n, p in params if is_backbone(n, 'gnn_model')], 'lr': lr_gnn}
    ]
    
    return grouped_parameters




"""
Predict result from trained model for sampled graph

1. predict by graph
2. print accuracy by graph, and save results in json file
3. print accuracy global, and save results in json file

embedding_file = current_path + type_set+'_predicted_nodes.pickle'
    {'pelosi': {0: [REAL_node_ids, node_probas, node_predictions]
                1: [REAL_node_ids, node_probas, node_predictions]},
     'thanksgiving': ...}

json_file = current_path + type_set+'_results.pickle'
    {'TEST':
            'pelosi': 0: {'usr_proba': 0.81, 'usr_prediction': 0.79, bayes: 1, 'model_accuracy': 0.78, 'usr_entropy': 0.3},
                      1: {'usr_proba': 0.81, 'usr_prediction': 0.79, bayes: 1, 'model_accuracy': 0.78, 'usr_entropy': 0.3},
                      ...
            'thanksgiving': ...
    'TRAIN':
        ...
    }
"""
def predict_model_by_sample(dataset, model, text_model, is_edge_weighted, device, param_sample, current_path, type_set='TEST', is_timeline=False):
    print('||||||||||||||||||||||||||', type_set, ' PREDICTIONS ||||||||||||||||||||||||||')
    embedding_file = current_path + type_set+'_predictions_sample_3.pickle'
    json_file = current_path + 'results_sample.json'
    to_line = True
    data_objects_by_topic = {}
    results = {}
    
    #parameters
    nbs_sample, prop_user_kept = param_sample
    
    #labels
    if(type_set == 'MY_DATA'):
        topic_labels = ut.my_data_labels
    else:
        topic_labels = ut.data_zarate_labels
    
    if(os.path.isfile(embedding_file) == True):
        data_objects_by_topic = gp.load_pickle(embedding_file)    
    if(os.path.isfile(json_file) == True):
        results = gp.load_json(json_file)  
        
    if(type_set not in results.keys()):
        results[type_set] = {}
        
    if(is_timeline == False):
        sorted_topic = {k:topic_labels[k] for k in dataset.keys()}
    else:
        for k in dataset.keys():
            print(k)
            print('_'.join(k.split('_')[:-1]))
            print('***')
        sorted_topic = {k:topic_labels['_'.join(k.split('_')[:-1])] for k in dataset.keys()}        
    sorted_topic = [k for k, v in sorted(sorted_topic.items(), key=lambda x: x[1])]
    
    print(' ::: current state :::')
    for topic in results[type_set]:
        print( '>', topic, ':', len(results[type_set][topic]), 'xps')
    print(' :::::::::::::::::::::')
    
    for topic in sorted_topic:
        if(is_timeline == False):
            current_label = topic_labels[topic]
        else:
            current_label = topic_labels['_'.join(topic.split('_')[:-1])]
            
        print('--------------------------', topic, current_label, '--------------------------')
        G, dict_node_features = dataset[topic]
        list_nodes = G.nodes().tolist()
        num_to_keep = int(len(list_nodes) * prop_user_kept)
        
        if(topic not in results[type_set].keys()):
            results[type_set][topic] = {}
        if(topic not in data_objects_by_topic.keys()):
            data_objects_by_topic[topic] = {}
            
        for n_xp in range(nbs_sample):
            n_xp = str(n_xp)
            if(n_xp not in results[type_set][topic].keys()):
                print('**',n_xp, '**')
                res_by_xp = {}
                
                #1. Get predictions
                if(n_xp in data_objects_by_topic[topic].keys()):
                    node_ids, node_probas, node_predictions = data_objects_by_topic[topic][n_xp]
                    runtime = -1
                else:
                    sampled_indices = random.sample(range(len(list_nodes)), num_to_keep)
                    sampled_indices.sort()
                    sampled_nodes = [list_nodes[i] for i in sampled_indices]      
                    
                    G_sampled = dgl.node_subgraph(G, sampled_nodes)
                    node_ids = G_sampled.ndata[dgl.NID].tolist()
                    features = []
                    for id_ in node_ids:
                        features.append(dict_node_features[id_])
                    if(text_model == False):
                        features = [torch.tensor(f, dtype=torch.double) for f in features]
        
                    t_mod = time.time()
                    model.eval()
                    with torch.no_grad():
                        if(is_edge_weighted == True):
                            _, _, _, probas = model(G_sampled, features, G_sampled.edata['weight'], device=device)
                        else:
                            _, _, _, probas = model(G_sampled, features, device=device)
                    runtime = round(((time.time() - t_mod)/60), 2)
                    
                    
                    node_probas = probas[:,1].tolist() #get prediction of user being controversial
                    node_predictions = probas.argmax(1).tolist()
                    
                    #save objects
                    data_objects_by_topic[topic][n_xp] = [node_ids, node_probas, node_predictions]
                    gp.pickle_save(embedding_file, data_objects_by_topic)
                    
                #2. Get scores
                node_labels = [current_label] * len(node_predictions)
                naive_bayes = gp.get_naive_score(node_probas)
                qc_probas = round(sum(node_probas)/len(node_probas) , 3) 
                qc_preds = round(sum(node_predictions)/len(node_predictions) , 3)
                median = np.median(node_probas)
                acc_topic = round(accuracy_score(node_labels, node_predictions), 3)
                topic_entropy = round(get_shannon_entropy(node_probas), 3)
            
                #4. PRINT
                if(current_label == 1 and to_line == True):
                    print('\n')
                    to_line=False
                print('  --> ', 'proba=', qc_probas, '  /  preds=', qc_preds)
            
                #5. SAVE IN results topic info
                res_by_xp = {'proba': qc_probas,
                            'prediction': qc_preds,
                            'median': median,
                            'bayes': naive_bayes,
                            'topic_shannon_entropy': topic_entropy,
                            'accuracy': acc_topic,
                            'runtime': runtime}
            
                results[type_set][topic][n_xp] = res_by_xp
                gp.write_json(json_file, results)
        print('\n')
        
    return results, data_objects_by_topic



"""
Training loop, semi-supervised model

            model = gm.train_loop(TRAIN_SET, TEST_SET, 
                                  only_text, model, 
                                  optimizer, epochs, device, batch_gnn, 
                                  is_edge_weighted, text_model, xp_path, model_name)
        #- fonction random qui renvoi les noeuds to train pour current epoch
        #- save score into 2 files: train_score.json & test_score.json    

TO ADD
    + verify we train on 30 topics (print(len(train)))  -----
        
    + if(type_sample==0):
        + save models at each 2 epoch  -----
        + at the end, delete all models after the best_epoch  -----
"""
def train_loop_sampled(TRAIN_SET, TEST_SET, type_sample, save_by_epoch,
               only_text, model, 
               optimizer, epochs, device, batch_gnn, 
               is_edge_weighted, text_model, path, model_name):
    
    if(os.path.isfile(path+model_name) == False):        
        min_nodes_by_graph = get_min_nodes_by_graph(TRAIN_SET)
        print(' > We train on', len(TRAIN_SET), ' topics !')
        
        # set parameters of EPOCHS LOOP
        final_model = copy.deepcopy(model)
        start_epoch = 0
        best_train_epoch = -1
        best_train_loss = None
        list_train_loss = []
        best_train_acc = None
        list_train_acc = []
        best_test_acc = 0 #depend on best train epoch
        list_test_acc = []
        cpt_early_stop = 0
        runtime = 0
        
        fold_chkpt = path + ut.checkpoint_folder
        if(os.path.isdir(fold_chkpt) == False):
            os.mkdir(fold_chkpt)
        fold_epoch_models = path + 'epochs/'
        if(os.path.isdir(fold_epoch_models) == False):
            os.mkdir(fold_epoch_models)
    
        if(len(os.listdir(fold_chkpt)) > 0):
            file = os.listdir(fold_chkpt)[0]
            checkpoint_path = fold_chkpt + file
        
            if(os.path.exists(checkpoint_path)):
                print(' > found checkpoint!')
                chk = torch.load(checkpoint_path)
                model.load_state_dict(chk['model_state_dict'])
                optimizer.load_state_dict(chk['optimizer_state_dict'])
                start_epoch = chk['epoch']+1
                best_train_epoch = chk['best_train_epoch']
                best_train_loss = chk['best_train_loss']
                list_train_loss = chk['list_train_loss']
                best_train_acc = chk['best_train_acc']
                list_train_acc = chk['list_train_acc']
                best_test_acc = chk['best_test_acc']
                list_test_acc = chk['list_test_acc']
                cpt_early_stop = chk['cpt_early_stop']
                runtime = chk['runtime']
                
                final_model = torch.load(path+'final_TEMP.pth')
        
        print(' >> early_stop=', early_stop)
        for e in range(start_epoch, epochs):
            print("------------------", e, "---------------------------")
            t_ep = time.time()
            ################# 1. TRAINING  #################
            model.train()
            total_loss = 0
            total_acc = 0
            total_train_x0 = 0
            total_train_x1 = 0
            for G, dict_node_features in TRAIN_SET:
                #1.1 PREPARE tHE GRAPH
                n_nodes = []
                features = []
                idx_none = []
                for k, v in dict_node_features.items():
                    n_nodes.append(k)
                    features.append(v)
                    if(v is None):
                        idx_none.append(k)
                if(text_model == False):
                    features = [torch.tensor(f, dtype=torch.double) for f in features]
            
                labels = G.ndata['label']
                train_mask = get_temp_train_mask(G, min_nodes_by_graph)
                    
                all_labels = labels[train_mask]
                all_labels = all_labels.cpu().detach().numpy()
                all_index = np.where(np.array(train_mask) == True)[0]
                
                if(only_text == True):
                    temp_all_index = []
                    temp_all_labels = []
                    cpt=0
                    for i in range(len(all_index)):
                        if(dict_node_features[all_index[i]] is not None):
                            temp_all_index.append(all_index[i])
                            temp_all_labels.append(all_labels[i])
                        else:
                            cpt+=1
                    all_index = temp_all_index
                    all_labels = temp_all_labels               
            
                x0 =np.array([all_index[i] for i in range(len(all_index)) if all_labels[i] == 0])
                x1 =np.array([all_index[i] for i in range(len(all_index)) if all_labels[i] == 1])
                total_train_x0 += len(x0)
                total_train_x1 += len(x1)
                
                
                #1.2 TRAIN THE MODEL
                optimizer.zero_grad()
                if(is_edge_weighted == True):
                    _, h, _, logits = model(G, features, G.edata['weight'], device=device)
                else:
                    _, h, _, logits = model(G, features, device=device)
    
                # Compute prediction
                preds = logits.argmax(1)
        
                # Compute loss
                loss = F.cross_entropy(logits[train_mask], labels[train_mask])
                
                # TRAIN acc
                train_acc = (preds[train_mask] == labels[train_mask]).float().mean()
                
                # Backward
                loss.backward()
                optimizer.step()
                
                total_loss += loss.item()
                total_acc += train_acc.item()
                
    
            #1.3 COMPUTE LOSS
            epoch_loss = round(total_loss / len(TRAIN_SET), 5)
            epoch_acc = round(total_acc / len(TRAIN_SET), 5)
            
            
            #1.4 save epoch model
            if(e%save_by_epoch == 0):
                save_model(fold_epoch_models+'model_'+str(e)+'_.pth', model)
                print(' saved!')
            
            #1.5 SAVE FINAL MODEL -- early stopping when train-loss increase --
            if(early_stop is None):
                final_model = copy.deepcopy(model)
            else:
                if(best_train_loss is not None and best_train_loss < epoch_loss):
                    if(cpt_early_stop >= early_stop):
                        print(' > hop we stop, new_train_loss=', epoch_loss, 'best train loss=', best_train_loss, '==> cpt_early=', cpt_early_stop)
                        break
                    else:
                        cpt_early_stop+=1
                else:
                    print(' ! best model !')
                    best_train_epoch = e
                    best_train_loss = epoch_loss
                    best_train_acc = epoch_acc
                    final_model = copy.deepcopy(model)
                    cpt_early_stop = 0
            
            #runtime
            train_epoch_runtime = time.time() - t_ep
            runtime += train_epoch_runtime
            
            ################# 2. EVALUATION  #################
            model.eval()
            all_test_preds = []
            all_test_labs = []
            total_test_x0 = 0
            total_test_x1 = 0
            for G, dict_node_features in TEST_SET:
                #2.1 PREPARE tHE GRAPH
                n_nodes = []
                features = []
                idx_none = []
                for k, v in dict_node_features.items():
                    n_nodes.append(k)
                    features.append(v)
                    if(v is None):
                        idx_none.append(k)
                if(text_model == False):
                    features = [torch.tensor(f, dtype=torch.double) for f in features]
            
                labels = G.ndata['label']
                test_mask = [True] * len(G.nodes())
                    
                all_labels = labels[test_mask]
                all_labels = all_labels.cpu().detach().numpy()
                all_index = np.where(np.array(test_mask) == True)[0]
                
                if(only_text == True):
                    temp_all_index = []
                    temp_all_labels = []
                    cpt=0
                    for i in range(len(all_index)):
                        if(dict_node_features[all_index[i]] is not None):
                            temp_all_index.append(all_index[i])
                            temp_all_labels.append(all_labels[i])
                        else:
                            cpt+=1
                    all_index = temp_all_index
                    all_labels = temp_all_labels               
            
                x0 =np.array([all_index[i] for i in range(len(all_index)) if all_labels[i] == 0])
                x1 =np.array([all_index[i] for i in range(len(all_index)) if all_labels[i] == 1])
                total_test_x0 += len(x0)
                total_test_x1 += len(x1)
                
                
                #2.2 GET MODEL PREDICTION
                with torch.no_grad():
                    if(is_edge_weighted == True):
                        _, h, _, logits = model(G, features, G.edata['weight'], device=device)
                    else:
                        _, h, _, logits = model(G, features, device=device)
    
                # Compute prediction
                preds = logits.argmax(1)
                all_test_preds += preds[test_mask].tolist()
                all_test_labs += labels[test_mask].tolist()
                
    
            #2.3 GET TEST ACCURACY
            test_acc = accuracy_score(all_test_labs, all_test_preds) 
            if (best_train_epoch == e):
                best_test_acc = test_acc
            
    
            ################# 3. SAVE CHKPT & MODEL & METADATA  #################        
            #3.1 SAVE LOSS & ACC train test sets
            list_train_loss.append(epoch_loss)
            list_train_acc.append(epoch_acc)
            list_test_acc.append(test_acc)
                    
            #3.2 SAVE FINAL MODEL
            if(best_train_epoch == e):
                save_model(path+'final_TEMP.pth', final_model)
            
            #3.3 SAVE CHKPT MODEL
            #save checkpoint
            for old_file in os.listdir(fold_chkpt):
                  os.remove(fold_chkpt+old_file)
                 
            # save current epochs 
            checkpoint_file = fold_chkpt + str(e)+'_'+model_name 
            torch.save({
                'epoch': e,
                'model_state_dict': model.state_dict(),
                'optimizer_state_dict': optimizer.state_dict(),
                'best_train_epoch': best_train_epoch,
                'best_train_loss': best_train_loss,
                'list_train_loss': list_train_loss,
                'best_train_acc': best_train_acc,
                'list_train_acc': list_train_acc,
                'best_test_acc': best_test_acc,
                'list_test_acc': list_test_acc,
                'cpt_early_stop': cpt_early_stop,
                'runtime': runtime
                }, checkpoint_file)
            
            #3.4 PRINT INFO ON EPOCHS
            if(e%1 == 0):
                print('In epoch {}, train loss: {:.3f} + train acc: {:.3f} || test acc: {:.3f}'.format(
                    e, epoch_loss, epoch_acc, test_acc))
                print('           > BEST: In epoch {}, train_loss: {:.3f} + train_acc: {:.3f} || test_acc: {:.3f}'.format(
                    best_train_epoch, best_train_loss, best_train_acc, best_test_acc))
                
                print(' --> time :', round(((time.time() - t_ep)/60), 2), 'min --- ', total_train_x0, total_train_x1, total_test_x0, total_test_x1)
                print("------------------------------------------------- \n")
    
        # FINALISATION
        #save model
        print('\n --- Save final model --- (epoch', best_train_epoch, ')')
        save_model(path+model_name, final_model)
        save_model(fold_epoch_models+'model_'+str(best_train_epoch)+'_OFF_.pth', final_model)
        
        train_info = {'train_runtime': round(runtime/60,4),
                      'train_loss': best_train_loss,
                      'test_acc': best_test_acc}
        print('    -> Training info:', train_info)
        gp.write_json(path+'train_info.json', train_info)
    
    
    # DELETE TOO MUCH MODELS
    print(' >>> DELETE FILES')
    delete, kept = delete_models_after_kept(fold_epoch_models)
    
    #plot         
    path = path + 'plot/'
    if(os.path.isdir(path) == False):
        os.mkdir(path)
    
    p_train_loss = path + "train_loss.png"
    plt.plot(range(len(list_train_loss)), list_train_loss)
    plt.savefig(fname = p_train_loss)
    plt.show()
    
    p_train_acc = path + "train_acc.png"
    plt.plot(range(len(list_train_acc)), list_train_acc)
    plt.savefig(fname = p_train_acc)
    plt.show()
    
    p_test_acc = path + "test_acc.png"
    plt.plot(range(len(list_test_acc)), list_test_acc)
    plt.savefig(fname = p_test_acc)
    plt.show()
                
    return final_model


"""
delete files not needed
"""
def delete_models_after_kept(folder_path):
    # Get the list of files in the folder
    files = os.listdir(folder_path)
    
    # Filter files to find the one with the "OFF" suffix
    kept_model_files = [file for file in files if len(file.split('_')) > 3]
    
    # Ensure that there is exactly one kept model file
    if len(kept_model_files) != 1:
        print("Could not find the kept model file or found multiple kept model files.")
        return 1
    
    # Get the name of the kept model file
    kept_model_name = kept_model_files[0]
    
    # Extract the epoch number from the kept model name
    kept_model_epoch = int(kept_model_name.split('_')[1])
    print(' > best kept epoch =', kept_model_epoch)
    
    # Iterate over the files and delete the ones after the kept model
    dele=0
    kept=0
    for file_name in files:
        curr_ep = int(file_name.split('_')[1])
        if(curr_ep > kept_model_epoch):
            file_path = os.path.join(folder_path, file_name)
            os.remove(file_path)
            dele+=1
        else:
            kept+=1
    print('we deleted', dele, 'models and kept', kept, 'models')
    
    return dele, kept




"""
    + save best user prediction in a file for the final_best_model

    + compute topic_qc_score at each epoch for each test topic
        + run predict_test ?
    + save topic_qc_score at each epoch for each test topic
        + {topic: [score1, score2, score3, ...]}
        + json file   
"""
def predict_epoch_model(dataset, type_sample, code_xp_saved_models_path, model, text_model, 
                        is_edge_weighted, device, current_path, type_set='TEST', is_timeline=False):
    print('||||||||||||||||||||||||||', type_set, ' PREDICTIONS |||||||||||||||||||||||||| \n')
    t1 = time.time()
    embedding_file = current_path + type_set+'_predictions.pickle'
    json_file = current_path + 'results.json'
    to_line = True
    data_objects_by_topic = {}
    results = {}
    
    #labels
    if(type_set == 'MY_DATA'):
        topic_labels = ut.my_data_labels
    else:
        topic_labels = ut.data_zarate_labels
    
    if(os.path.isfile(embedding_file) == True):
        data_objects_by_topic = gp.load_pickle(embedding_file)    
    if(os.path.isfile(json_file) == True):
        results = gp.load_json(json_file)  
        
    if(type_set in results.keys()):
        print(' > already computed, but still..')
        data_objects_by_topic = gp.load_pickle(embedding_file)
        # return results, data_objects_by_topic
    else:
        results[type_set] = {}
        
    if(is_timeline == False):
        sorted_topic = {k:topic_labels[k] for k in dataset.keys()}
    else:
        for k in dataset.keys():
            print(k)
            print('_'.join(k.split('_')[:-1]))
            print('***')
        sorted_topic = {k:topic_labels['_'.join(k.split('_')[:-1])] for k in dataset.keys()}
        
    sorted_topic = [k for k, v in sorted(sorted_topic.items(), key=lambda x: x[1])]
    
    print("--------------------- A. RES on best model ----------------------")
    list_all_probas = []
    list_all_predictions = []
    list_all_labels = []
    res_by_topics = {}
    for topic in sorted_topic:
        if(is_timeline == False):
            current_label = topic_labels[topic]
        else:
            current_label = topic_labels['_'.join(topic.split('_')[:-1])]    
            
        if(topic in data_objects_by_topic.keys()):
            node_ids, text_embeddings, node_embeddings, node_probas, node_predictions = data_objects_by_topic[topic]
            runtime = -1
        else:
            G, dict_node_features = dataset[topic]
            
            #1. GET PREDICTIONS
            n_nodes = []
            features = []
            for k, v in dict_node_features.items():
                n_nodes.append(k)
                features.append(v)
            if(text_model == False):
                features = [torch.tensor(f, dtype=torch.double) for f in features]
            
            t_mod = time.time()
            model.eval()
            with torch.no_grad():
                if(is_edge_weighted == True):
                    gnn_inputs, embeddings, _, probas = model(G, features, G.edata['weight'], device=device)
                else:
                    gnn_inputs, embeddings, _, probas = model(G, features, device=device)
            runtime = round(((time.time() - t_mod)/60), 2)
            
            node_ids = G.nodes().tolist()
            text_embeddings = gnn_inputs.cpu().detach().numpy()
            node_embeddings = embeddings.cpu().detach().numpy()
            node_probas = probas[:,1].tolist() #get prediction of user being controversial
            node_predictions = probas.argmax(1).tolist()
            
            data_objects_by_topic[topic] = [node_ids, text_embeddings, node_embeddings, node_probas, node_predictions]
        
        node_labels = [current_label] * len(node_predictions)
        
        #2. GET METRICS
        list_all_probas += node_probas
        list_all_predictions += node_predictions
        list_all_labels += node_labels
        
        #3. GET SCORES
        naive_bayes = gp.get_naive_score(node_probas)
        
        qc_probas = round(sum(node_probas)/len(node_probas) , 3) 
        qc_preds = round(sum(node_predictions)/len(node_predictions) , 3)
        median = np.median(node_probas)

        acc_topic = round(accuracy_score(node_labels, node_predictions), 3)
        topic_entropy = round(get_shannon_entropy(node_probas), 3)
        
        #4. PRINT
        if(current_label == 1 and to_line == True):
            print('\n')
            to_line=False
        print('----> ', topic, ': LAB=,', current_label, 'qc_probas (preds) =', qc_probas, '(', qc_preds, ')', '---> nb:', naive_bayes)
        
        #5. SAVE IN results topic info
        res_by_topics[topic] = {'proba': qc_probas,
                                'prediction': qc_preds,
                                'median': median,
                                'bayes': naive_bayes,
                                'topic_shannon_entropy': topic_entropy,
                                'accuracy': acc_topic,
                                'runtime': runtime}
        
        
    #6. SAVE in results dataset + SAVE IN JSON FILE dataset    
    results[type_set]['qc_score'] = res_by_topics
    results[type_set]['metric'] = {'accuracy': accuracy_score(list_all_labels, list_all_predictions),
                                   # 'set_cross_entropy': log_loss(list_all_labels, list_all_probas),
                                   'set_shannon_entropy': get_shannon_entropy(list_all_probas)}
    gp.write_json(json_file, results)
    
    #7. SAVE IN PICKLE FILE
    gp.pickle_save(embedding_file, data_objects_by_topic)
    
    print(' >>> AAA.',round(((time.time() - t1)/60)/60, 2), 'hours *********')
        
    ############
    print("--------------------- B. RES SAVE BY EPOCH ** TYPE_SAMPLE", type_sample,"** ----------------------")
    t1 = time.time()
    res_by_epoch = {}
    for t in dataset.keys():
        res_by_epoch[t] = {}
        
    epoch_file = current_path + 'results_epochs_by_topic_'+str(type_sample)+'.json'
    if(os.path.isfile(epoch_file) == True):
        res_by_epoch = gp.load_json(epoch_file)
        return results, data_objects_by_topic, res_by_epoch
        

    files = os.listdir(code_xp_saved_models_path+'epochs/')
    sorted_files = sorted(files, key=lambda x: int(x.split('_')[1]))
    
    # load it
    cpt=1
    for f in sorted_files:
        e = f.split('_')[1]
        add = ''
        if('_OFF_' in f):
            add = 'FINAL_MODEL'
        print('epoch:', e, '-->', cpt,' /', len(sorted_files), add)
        cpt+=1
        
        epoch_model = torch.load(code_xp_saved_models_path+'epochs/'+f)
        
        # run it for each test topic
        for topic in sorted_topic:
            G, dict_node_features = dataset[topic]
            if(is_timeline == False):
                current_label = topic_labels[topic]
            else:
                current_label = topic_labels['_'.join(topic.split('_')[:-1])]
            
            #1. GET PREDICTIONS
            n_nodes = []
            features = []
            for k, v in dict_node_features.items():
                n_nodes.append(k)
                features.append(v)
            if(text_model == False):
                features = [torch.tensor(f, dtype=torch.double) for f in features]
            
            t_mod = time.time()
            epoch_model.eval()
            with torch.no_grad():
                if(is_edge_weighted == True):
                    gnn_inputs, embeddings, _, probas = epoch_model(G, features, G.edata['weight'], device=device)
                else:
                    gnn_inputs, embeddings, _, probas = epoch_model(G, features, device=device)
            runtime = round(((time.time() - t_mod)/60), 2)
            
            node_ids = G.nodes().tolist()
            text_embeddings = gnn_inputs.cpu().detach().numpy()
            node_embeddings = embeddings.cpu().detach().numpy()
            node_probas = probas[:,1].tolist() #get prediction of user being controversial
            node_predictions = probas.argmax(1).tolist()
            node_labels = [current_label] * len(node_predictions)
            

            #3. GET SCORES
            naive_bayes = gp.get_naive_score(node_probas)
            
            qc_probas = round(sum(node_probas)/len(node_probas) , 3) 
            qc_preds = round(sum(node_predictions)/len(node_predictions) , 3)
            median = np.median(node_probas)
    
            acc_topic = round(accuracy_score(node_labels, node_predictions), 3)
            topic_entropy = round(get_shannon_entropy(node_probas), 3)
            
            #4. PRINT
            if(current_label == 1 and to_line == True):
                print('\n')
                to_line=False
            print('     ', topic , '   ', qc_probas, '   ', current_label)
            
            #5. SAVE IN results topic info
            result = {'proba': qc_probas,
                                    'prediction': qc_preds,
                                    'median': median,
                                    'bayes': naive_bayes,
                                    'topic_shannon_entropy': topic_entropy,
                                    'accuracy': acc_topic,
                                    'runtime': runtime}
            
            # append in dict
            res_by_epoch[topic][e] = result
    
    gp.write_json(epoch_file, res_by_epoch)
    print(' >>> BBB.',round(((time.time() - t1)/60)/60, 2), 'hours *********')

    return results, data_objects_by_topic, res_by_epoch



#-------------------------------------------------------------------------------------------------------------------------------------

def train_loop_sample_nosave(TRAIN_SET, TEST_SET, type_sample,
               only_text, model, 
               optimizer, epochs, device, batch_gnn, 
               is_edge_weighted, text_model, path, model_name):
    
    print("----------", type_sample, "----------")
    min_nodes_by_graph = get_min_nodes_by_graph(TRAIN_SET)
    
    # set parameters of EPOCHS LOOP
    final_model = copy.deepcopy(model)
    start_epoch = 0
    best_train_epoch = -1
    best_train_loss = None
    list_train_loss = []
    best_train_acc = None
    list_train_acc = []
    best_test_acc = 0 #depend on best train epoch
    list_test_acc = []
    cpt_early_stop = 0
    runtime = 0    
    dict_scores_by_epoch = {}
    for x in TEST_SET.keys():
        dict_scores_by_epoch[x] = {}

    
    fold_chkpt = path + ut.checkpoint_folder
    if(os.path.isdir(fold_chkpt) == False):
        os.mkdir(fold_chkpt)
    
    if(len(os.listdir(fold_chkpt)) > 0):
        file = os.listdir(fold_chkpt)[0]
        checkpoint_path = fold_chkpt + file
    
        if(os.path.exists(checkpoint_path)):
            print(' > found checkpoint!')
            chk = torch.load(checkpoint_path)
            model.load_state_dict(chk['model_state_dict'])
            optimizer.load_state_dict(chk['optimizer_state_dict'])
            start_epoch = chk['epoch']+1
            best_train_epoch = chk['best_train_epoch']
            best_train_loss = chk['best_train_loss']
            list_train_loss = chk['list_train_loss']
            best_train_acc = chk['best_train_acc']
            list_train_acc = chk['list_train_acc']
            best_test_acc = chk['best_test_acc']
            list_test_acc = chk['list_test_acc']
            cpt_early_stop = chk['cpt_early_stop']
            dict_scores_by_epoch = chk['dict_scores_by_epoch']
            runtime = chk['runtime']
            
            final_model = torch.load(path+'final_TEMP.pth')
    
    print(' >> early_stop=', early_stop)
    for e in range(start_epoch, epochs):
        print("------------------", e, "---------------------------")
        t_ep = time.time()
        ################# 1. TRAINING  #################
        model.train()
        total_loss = 0
        total_acc = 0
        total_train_x0 = 0
        total_train_x1 = 0
        for G, dict_node_features in TRAIN_SET:
            #1.1 PREPARE tHE GRAPH
            n_nodes = []
            features = []
            idx_none = []
            for k, v in dict_node_features.items():
                n_nodes.append(k)
                features.append(v)
                if(v is None):
                    idx_none.append(k)
            if(text_model == False):
                features = [torch.tensor(f, dtype=torch.double) for f in features]
        
            labels = G.ndata['label']
            train_mask = get_temp_train_mask(G, min_nodes_by_graph)
                
            all_labels = labels[train_mask]
            all_labels = all_labels.cpu().detach().numpy()
            all_index = np.where(np.array(train_mask) == True)[0]
            
            if(only_text == True):
                temp_all_index = []
                temp_all_labels = []
                cpt=0
                for i in range(len(all_index)):
                    if(dict_node_features[all_index[i]] is not None):
                        temp_all_index.append(all_index[i])
                        temp_all_labels.append(all_labels[i])
                    else:
                        cpt+=1
                all_index = temp_all_index
                all_labels = temp_all_labels               
        
            x0 =np.array([all_index[i] for i in range(len(all_index)) if all_labels[i] == 0])
            x1 =np.array([all_index[i] for i in range(len(all_index)) if all_labels[i] == 1])
            total_train_x0 += len(x0)
            total_train_x1 += len(x1)
            
            
            #1.2 TRAIN THE MODEL
            optimizer.zero_grad()
            if(is_edge_weighted == True):
                _, h, _, logits = model(G, features, G.edata['weight'], device=device)
            else:
                _, h, _, logits = model(G, features, device=device)

            # Compute prediction
            preds = logits.argmax(1)
    
            # Compute loss
            loss = F.cross_entropy(logits[train_mask], labels[train_mask])
            
            # TRAIN acc
            train_acc = (preds[train_mask] == labels[train_mask]).float().mean()
            
            # Backward
            loss.backward()
            optimizer.step()
            
            total_loss += loss.item()
            total_acc += train_acc.item()
            
        #1.3 COMPUTE LOSS
        epoch_loss = round(total_loss / len(TRAIN_SET), 5)
        epoch_acc = round(total_acc / len(TRAIN_SET), 5)
        
        #1.4 SAVE FINAL MODEL -- early stopping when train-loss increase --
        if(early_stop is None):
            final_model = copy.deepcopy(model)
        else:
            if(best_train_loss is not None and best_train_loss < epoch_loss):
                if(cpt_early_stop >= early_stop):
                    print(' > hop we stop, new_train_loss=', epoch_loss, 'best train loss=', best_train_loss, '==> cpt_early=', cpt_early_stop)
                    break
                else:
                    cpt_early_stop+=1
            else:
                print(' ! best model !')
                best_train_epoch = e
                best_train_loss = epoch_loss
                best_train_acc = epoch_acc
                final_model = copy.deepcopy(model)
                cpt_early_stop = 0
        
        #runtime
        train_epoch_runtime = time.time() - t_ep
        runtime += train_epoch_runtime
        
        ################# 2. EVALUATION  #################
        model.eval()
        all_test_preds = []
        all_test_labs = []
        total_test_x0 = 0
        total_test_x1 = 0
        # print('---len(TEST_SET):', len(TEST_SET))
        for topic, data in TEST_SET.items():
            G, dict_node_features = data
            #2.1 PREPARE tHE GRAPH
            n_nodes = []
            features = []
            idx_none = []
            for k, v in dict_node_features.items():
                n_nodes.append(k)
                features.append(v)
                if(v is None):
                    idx_none.append(k)
            if(text_model == False):
                features = [torch.tensor(f, dtype=torch.double) for f in features]
        
            labels = G.ndata['label']
            test_mask = [True] * len(G.nodes())
            # print('1/', len(test_mask), len(labels))
            # print('2/', len(features))
            all_labels = labels[test_mask]
            all_labels = labels
            all_labels = all_labels.cpu().detach().numpy()
            all_index = np.where(np.array(test_mask) == True)[0]
            
            if(only_text == True):
                temp_all_index = []
                temp_all_labels = []
                cpt=0
                for i in range(len(all_index)):
                    if(dict_node_features[all_index[i]] is not None):
                        temp_all_index.append(all_index[i])
                        temp_all_labels.append(all_labels[i])
                    else:
                        cpt+=1
                all_index = temp_all_index
                all_labels = temp_all_labels               
        
            x0 =np.array([all_index[i] for i in range(len(all_index)) if all_labels[i] == 0])
            x1 =np.array([all_index[i] for i in range(len(all_index)) if all_labels[i] == 1])
            total_test_x0 += len(x0)
            total_test_x1 += len(x1)
            
            
            #2.2 GET MODEL PREDICTION
            t_ep = time.time()
            with torch.no_grad():
                if(is_edge_weighted == True):
                    gnn_inputs, embeddings, _, probas = model(G, features, G.edata['weight'], device=device)
                else:
                    gnn_inputs, embeddings, _, probas = model(G, features, device=device)
            test_epoch_runtime = time.time() - t_ep
            
            #Get score QC
            current_label = ut.data_zarate_labels[topic]
            res_topic, dict_probas_by_id = get_prediction_score(G, probas, current_label, test_epoch_runtime)
            dict_scores_by_epoch[topic][e] = res_topic
            
            # Compute prediction
            # print('3/', len(probas))
            preds = probas.argmax(1)
            # print('4/', len(preds), len(test_mask))
            all_test_preds += preds[test_mask].tolist()
            all_test_labs += labels[test_mask].tolist()
        
        #2.3 GET TEST ACCURACY
        test_acc = accuracy_score(all_test_labs, all_test_preds) 
        if (best_train_epoch == e):
            best_test_acc = test_acc
        

        ################# 3. SAVE CHKPT & MODEL & METADATA  #################        
        #3.1 SAVE LOSS & ACC train test sets
        list_train_loss.append(epoch_loss)
        list_train_acc.append(epoch_acc)
        list_test_acc.append(test_acc)
                
        #3.2 SAVE FINAL MODEL
        if(best_train_epoch == e):
            save_model(path+'final_TEMP.pth', final_model)
        
        #3.3 SAVE CHKPT MODEL
        #save checkpoint
        for old_file in os.listdir(fold_chkpt):
              os.remove(fold_chkpt+old_file)
             
        # save current epochs 
        checkpoint_file = fold_chkpt + str(e)+'_'+model_name 
        torch.save({
            'epoch': e,
            'model_state_dict': model.state_dict(),
            'optimizer_state_dict': optimizer.state_dict(),
            'best_train_epoch': best_train_epoch,
            'best_train_loss': best_train_loss,
            'list_train_loss': list_train_loss,
            'best_train_acc': best_train_acc,
            'list_train_acc': list_train_acc,
            'best_test_acc': best_test_acc,
            'list_test_acc': list_test_acc,
            'cpt_early_stop': cpt_early_stop,
            'dict_scores_by_epoch': dict_scores_by_epoch,
            'runtime': runtime
            }, checkpoint_file)
        
        #3.4 PRINT INFO ON EPOCHS
        if(e%1 == 0):
            print('In epoch {}, train loss: {:.3f} + train acc: {:.3f} || test acc: {:.3f}'.format(
                e, epoch_loss, epoch_acc, test_acc))
            print('           > BEST: In epoch {}, train_loss: {:.3f} + train_acc: {:.3f} || test_acc: {:.3f}'.format(
                best_train_epoch, best_train_loss, best_train_acc, best_test_acc))
            
            print(' --> time :', round(((time.time() - t_ep)/60), 2), 'min --- ', total_train_x0, total_train_x1, total_test_x0, total_test_x1)
            print("------------------------------------------------- \n")

    # FINALISATION
    #save model
    print('\n --- Save final model ---')
    save_model(path+model_name, final_model)
    train_info = {'train_runtime': round(runtime/60,4),
                  'train_loss': best_train_loss,
                  'test_acc': best_test_acc}
    print('    -> Training info:', train_info)
    gp.write_json(path+'train_info.json', train_info)
    
    print('\n --- Save score at each epoch of test ---')
    dict_scores_file = path + 'dict_scores_by_epoch_'+str(type_sample)+'.json'
    dict_scores_by_epoch['BEST_EPOCH'] = best_train_epoch
    gp.write_json(dict_scores_file, dict_scores_by_epoch)
    
    
    #plot         
    path = path + 'plot/'
    if(os.path.isdir(path) == False):
        os.mkdir(path)
    
    p_train_loss = path + "train_loss.png"
    plt.plot(range(len(list_train_loss)), list_train_loss)
    plt.savefig(fname = p_train_loss)
    plt.show()
    
    p_train_acc = path + "train_acc.png"
    plt.plot(range(len(list_train_acc)), list_train_acc)
    plt.savefig(fname = p_train_acc)
    plt.show()
    
    p_test_acc = path + "test_acc.png"
    plt.plot(range(len(list_test_acc)), list_test_acc)
    plt.savefig(fname = p_test_acc)
    plt.show()
                
    return final_model

#---------------------------------------------------------------------------------------------------------------

def train_loop_sample_nosave_fold_epoch(TRAIN_SET, TEST_SET, type_sample,
               only_text, model, 
               optimizer, epochs, device, batch_gnn, 
               is_edge_weighted, text_model, path, model_name):
    
    print("----------", type_sample, "----------")
    print(len(TRAIN_SET), len(TEST_SET), '\n')
    min_nodes_by_graph = get_min_nodes_by_graph(TRAIN_SET)
    
    # set parameters of EPOCHS LOOP
    final_model = copy.deepcopy(model)
    start_epoch = 0
    best_train_epoch = -1
    best_train_loss = None
    list_train_loss = []
    best_train_acc = None
    list_train_acc = []
    best_test_acc = 0 #depend on best train epoch
    list_test_acc = []
    cpt_early_stop = 0
    runtime = 0    
    dict_scores_by_epoch = {}
    for x in TEST_SET.keys():
        dict_scores_by_epoch[x] = {}

    
    fold_chkpt = path + ut.checkpoint_folder
    if(os.path.isdir(fold_chkpt) == False):
        os.mkdir(fold_chkpt)
    
    if(len(os.listdir(fold_chkpt)) > 0):
        file = os.listdir(fold_chkpt)[0]
        checkpoint_path = fold_chkpt + file
    
        if(os.path.exists(checkpoint_path)):
            print(' > found checkpoint!')
            chk = torch.load(checkpoint_path)
            model.load_state_dict(chk['model_state_dict'])
            optimizer.load_state_dict(chk['optimizer_state_dict'])
            start_epoch = chk['epoch']+1
            best_train_epoch = chk['best_train_epoch']
            best_train_loss = chk['best_train_loss']
            list_train_loss = chk['list_train_loss']
            best_train_acc = chk['best_train_acc']
            list_train_acc = chk['list_train_acc']
            best_test_acc = chk['best_test_acc']
            list_test_acc = chk['list_test_acc']
            cpt_early_stop = chk['cpt_early_stop']
            dict_scores_by_epoch = chk['dict_scores_by_epoch']
            runtime = chk['runtime']
            
            final_model = torch.load(path+'final_TEMP.pth')
    
    print(' >> early_stop=', early_stop)
    for e in range(start_epoch, epochs):
        print("------------------", e, "---------------------------")
        t_ep = time.time()
        ################# 1. TRAINING  #################
        model.train()
        total_loss = 0
        total_acc = 0
        total_train_x0 = 0
        total_train_x1 = 0
        for G, dict_node_features in TRAIN_SET:
            #1.1 PREPARE tHE GRAPH
            n_nodes = []
            features = []
            idx_none = []
            for k, v in dict_node_features.items():
                n_nodes.append(k)
                features.append(v)
                if(v is None):
                    idx_none.append(k)
            if(text_model == False):
                features = [torch.tensor(f, dtype=torch.double) for f in features]
        
            labels = G.ndata['label']
            train_mask = get_temp_train_mask(G, min_nodes_by_graph)
                
            all_labels = labels[train_mask]
            all_labels = all_labels.cpu().detach().numpy()
            all_index = np.where(np.array(train_mask) == True)[0]
            
            if(only_text == True):
                temp_all_index = []
                temp_all_labels = []
                cpt=0
                for i in range(len(all_index)):
                    if(dict_node_features[all_index[i]] is not None):
                        temp_all_index.append(all_index[i])
                        temp_all_labels.append(all_labels[i])
                    else:
                        cpt+=1
                all_index = temp_all_index
                all_labels = temp_all_labels               
        
            x0 =np.array([all_index[i] for i in range(len(all_index)) if all_labels[i] == 0])
            x1 =np.array([all_index[i] for i in range(len(all_index)) if all_labels[i] == 1])
            total_train_x0 += len(x0)
            total_train_x1 += len(x1)
            
            
            #1.2 TRAIN THE MODEL
            optimizer.zero_grad()
            if(is_edge_weighted == True):
                _, h, _, logits = model(G, features, G.edata['weight'], device=device)
            else:
                _, h, _, logits = model(G, features, device=device)

            # Compute prediction
            preds = logits.argmax(1)
    
            # Compute loss
            loss = F.cross_entropy(logits[train_mask], labels[train_mask])
            
            # TRAIN acc
            train_acc = (preds[train_mask] == labels[train_mask]).float().mean()
            
            # Backward
            loss.backward()
            optimizer.step()
            
            total_loss += loss.item()
            total_acc += train_acc.item()
            
        #1.3 COMPUTE LOSS
        epoch_loss = round(total_loss / len(TRAIN_SET), 5)
        epoch_acc = round(total_acc / len(TRAIN_SET), 5)
        
        #1.4 SAVE FINAL MODEL -- early stopping when train-loss increase --
        if(early_stop is None):
            final_model = copy.deepcopy(model)
        else:
            if(best_train_loss is not None and best_train_loss < epoch_loss):
                if(cpt_early_stop >= early_stop):
                    print(' > hop we stop, new_train_loss=', epoch_loss, 'best train loss=', best_train_loss, '==> cpt_early=', cpt_early_stop)
                    break
                else:
                    cpt_early_stop+=1
            else:
                print(' ! best model !')
                best_train_epoch = e
                best_train_loss = epoch_loss
                best_train_acc = epoch_acc
                final_model = copy.deepcopy(model)
                cpt_early_stop = 0
        
        #runtime
        train_epoch_runtime = time.time() - t_ep
        runtime += train_epoch_runtime
        
        ################# 2. EVALUATION  #################
        model.eval()
        all_test_preds = []
        all_test_labs = []
        total_test_x0 = 0
        total_test_x1 = 0
        # print('---len(TEST_SET):', len(TEST_SET))
        dict_scores_by_epoch2 = {}
        for topic, data in TEST_SET.items():
            G, dict_node_features = data
            #2.1 PREPARE tHE GRAPH
            n_nodes = []
            features = []
            idx_none = []
            for k, v in dict_node_features.items():
                n_nodes.append(k)
                features.append(v)
                if(v is None):
                    idx_none.append(k)
            if(text_model == False):
                features = [torch.tensor(f, dtype=torch.double) for f in features]
        
            labels = G.ndata['label']
            test_mask = [True] * len(G.nodes())
            # print('1/', len(test_mask), len(labels))
            # print('2/', len(features))
            all_labels = labels[test_mask]
            all_labels = labels
            all_labels = all_labels.cpu().detach().numpy()
            all_index = np.where(np.array(test_mask) == True)[0]
            
            if(only_text == True):
                temp_all_index = []
                temp_all_labels = []
                cpt=0
                for i in range(len(all_index)):
                    if(dict_node_features[all_index[i]] is not None):
                        temp_all_index.append(all_index[i])
                        temp_all_labels.append(all_labels[i])
                    else:
                        cpt+=1
                all_index = temp_all_index
                all_labels = temp_all_labels               
        
            x0 =np.array([all_index[i] for i in range(len(all_index)) if all_labels[i] == 0])
            x1 =np.array([all_index[i] for i in range(len(all_index)) if all_labels[i] == 1])
            total_test_x0 += len(x0)
            total_test_x1 += len(x1)
            
            
            #2.2 GET MODEL PREDICTION
            t_ep = time.time()
            with torch.no_grad():
                if(is_edge_weighted == True):
                    gnn_inputs, embeddings, _, probas = model(G, features, G.edata['weight'], device=device)
                else:
                    gnn_inputs, embeddings, _, probas = model(G, features, device=device)
            test_epoch_runtime = time.time() - t_ep
            
            #Get score QC
            current_label = ut.data_zarate_labels[topic]
            res_topic, dict_probas_by_id = get_prediction_score(G, probas, current_label, test_epoch_runtime)
            dict_scores_by_epoch[topic][e] = res_topic
            dict_scores_by_epoch2[topic] = dict_probas_by_id
            
            # Compute prediction
            # print('3/', len(probas))
            preds = probas.argmax(1)
            # print('4/', len(preds), len(test_mask))
            all_test_preds += preds[test_mask].tolist()
            all_test_labs += labels[test_mask].tolist()
        
        #for each epoch save scores
        save_probas_by_epoch(dict_scores_by_epoch2, e, path)
        
        
        #2.3 GET TEST ACCURACY
        test_acc = accuracy_score(all_test_labs, all_test_preds) 
        if (best_train_epoch == e):
            best_test_acc = test_acc
        

        ################# 3. SAVE CHKPT & MODEL & METADATA  #################        
        #3.1 SAVE LOSS & ACC train test sets
        list_train_loss.append(epoch_loss)
        list_train_acc.append(epoch_acc)
        list_test_acc.append(test_acc)
                
        #3.2 SAVE FINAL MODEL
        if(best_train_epoch == e):
            save_model(path+'final_TEMP.pth', final_model)
        
        #3.3 SAVE CHKPT MODEL
        #save checkpoint
        for old_file in os.listdir(fold_chkpt):
              os.remove(fold_chkpt+old_file)
             
        # save current epochs 
        checkpoint_file = fold_chkpt + str(e)+'_'+model_name 
        torch.save({
            'epoch': e,
            'model_state_dict': model.state_dict(),
            'optimizer_state_dict': optimizer.state_dict(),
            'best_train_epoch': best_train_epoch,
            'best_train_loss': best_train_loss,
            'list_train_loss': list_train_loss,
            'best_train_acc': best_train_acc,
            'list_train_acc': list_train_acc,
            'best_test_acc': best_test_acc,
            'list_test_acc': list_test_acc,
            'cpt_early_stop': cpt_early_stop,
            'dict_scores_by_epoch': dict_scores_by_epoch,
            'runtime': runtime
            }, checkpoint_file)
        
        #3.4 PRINT INFO ON EPOCHS
        if(e%1 == 0):
            print('In epoch {}, train loss: {:.3f} + train acc: {:.3f} || test acc: {:.3f}'.format(
                e, epoch_loss, epoch_acc, test_acc))
            print('           > BEST: In epoch {}, train_loss: {:.3f} + train_acc: {:.3f} || test_acc: {:.3f}'.format(
                best_train_epoch, best_train_loss, best_train_acc, best_test_acc))
            
            print(' --> time :', round(((time.time() - t_ep)/60), 2), 'min --- ', total_train_x0, total_train_x1, total_test_x0, total_test_x1)
            print("------------------------------------------------- \n")

    # FINALISATION
    #save model
    print('\n --- Save final model ---')
    save_model(path+model_name, final_model)
    train_info = {'train_runtime': round(runtime/60,4),
                  'train_loss': best_train_loss,
                  'test_acc': best_test_acc}
    print('    -> Training info:', train_info)
    gp.write_json(path+'train_info.json', train_info)
    
    print('\n --- Save score at each epoch of test ---')
    dict_scores_file = path + 'dict_scores_by_epoch_'+str(type_sample)+'.json'
    dict_scores_by_epoch['BEST_EPOCH'] = best_train_epoch
    gp.write_json(dict_scores_file, dict_scores_by_epoch)
    
    
    #plot         
    path = path + 'plot/'
    if(os.path.isdir(path) == False):
        os.mkdir(path)
    
    p_train_loss = path + "train_loss.png"
    plt.plot(range(len(list_train_loss)), list_train_loss)
    plt.savefig(fname = p_train_loss)
    plt.show()
    
    p_train_acc = path + "train_acc.png"
    plt.plot(range(len(list_train_acc)), list_train_acc)
    plt.savefig(fname = p_train_acc)
    plt.show()
    
    p_test_acc = path + "test_acc.png"
    plt.plot(range(len(list_test_acc)), list_test_acc)
    plt.savefig(fname = p_test_acc)
    plt.show()
                
    return final_model

def get_corresponding_dict(G, dict_probas_by_id):
    new_dict = {}
    for id_, old_id_ in zip(G.nodes().tolist(), G.ndata[dgl.NID].tolist()):
        new_dict[old_id_] = dict_probas_by_id[id_]
        
    print(' > verif:', len(dict_probas_by_id), '=', len(new_dict))
    return new_dict


def save_probas_by_epoch(dict_scores_by_epoch2, e, path):
    
    fold_epoch = path + 'EPOCHS/'
    if(os.path.isdir(fold_epoch) == False):
        os.mkdir(fold_epoch)
        
    file_path = fold_epoch + str(e) + '.json'
    
    gp.write_json(file_path, dict_scores_by_epoch2)
        
    return 1

#---------------------------------------------------------------------------------------------------------------

def train_loop_sample_nosave_user_discarded(TRAIN_SET, TEST_SET, type_sample,
               only_text, model, 
               optimizer, epochs, device, batch_gnn, 
               is_edge_weighted, text_model, path, path0, model_name, type_user_to_discard, MAX_SPL):
    
    print(' \n <<< TRAIN USER DSC !!!! >>> \n')
    print("----------", type_sample, type_user_to_discard, "----------")
    dict_user_to_keep_for_score2 = {}
    if(type_sample==0):
        type_user_to_discard = 'BEST'
    
    print('step1: get user to keep')
    dict_user_to_keep_for_score = get_dict_user_to_keep(path0, list(TEST_SET.keys()), MAX_SPL)
    for k,v in dict_user_to_keep_for_score.items():
        if(type_sample==0):
            dict_user_to_keep_for_score2[k] = v['WORST']
        dict_user_to_keep_for_score[k] = v[type_user_to_discard]
        
        
    print('end step1')
    
    
    min_nodes_by_graph = get_min_nodes_by_graph(TRAIN_SET)
    
    # set parameters of EPOCHS LOOP
    final_model = copy.deepcopy(model)
    start_epoch = 0
    best_train_epoch = -1
    best_train_loss = None
    list_train_loss = []
    best_train_acc = None
    list_train_acc = []
    best_test_acc = 0 #depend on best train epoch
    list_test_acc = []
    cpt_early_stop = 0
    runtime = 0    
    dict_scores_by_epoch = {}
    dict_sampled_scored_by_epoch = {}
    dict_sampled_scored_by_epoch2 = {}
    for x in TEST_SET.keys():
        dict_scores_by_epoch[x] = {}
        dict_sampled_scored_by_epoch[x] = {}
        if(type_sample==0):
            dict_sampled_scored_by_epoch2[x] = {}
            
    
    fold_chkpt = path + ut.checkpoint_folder
    if(os.path.isdir(fold_chkpt) == False):
        os.mkdir(fold_chkpt)
    
    if(len(os.listdir(fold_chkpt)) > 0):
        file = os.listdir(fold_chkpt)[0]
        checkpoint_path = fold_chkpt + file
    
        if(os.path.exists(checkpoint_path)):
            print(' > found checkpoint!')
            chk = torch.load(checkpoint_path)
            model.load_state_dict(chk['model_state_dict'])
            optimizer.load_state_dict(chk['optimizer_state_dict'])
            start_epoch = chk['epoch']+1
            best_train_epoch = chk['best_train_epoch']
            best_train_loss = chk['best_train_loss']
            list_train_loss = chk['list_train_loss']
            best_train_acc = chk['best_train_acc']
            list_train_acc = chk['list_train_acc']
            best_test_acc = chk['best_test_acc']
            list_test_acc = chk['list_test_acc']
            cpt_early_stop = chk['cpt_early_stop']
            dict_scores_by_epoch = chk['dict_scores_by_epoch']
            dict_sampled_scored_by_epoch = chk['dict_sampled_scored_by_epoch']
            dict_sampled_scored_by_epoch2 = chk['dict_sampled_scored_by_epoch2']
            runtime = chk['runtime']
            
            final_model = torch.load(path+'final_TEMP.pth')
    
    print(' >> early_stop=', early_stop)
    for e in range(start_epoch, epochs):
        print("------------------", e, "---------------------------")
        t_ep = time.time()
        ################# 1. TRAINING  #################
        model.train()
        total_loss = 0
        total_acc = 0
        total_train_x0 = 0
        total_train_x1 = 0
        for G, dict_node_features in TRAIN_SET:
            #1.1 PREPARE tHE GRAPH
            n_nodes = []
            features = []
            idx_none = []
            for k, v in dict_node_features.items():
                n_nodes.append(k)
                features.append(v)
                if(v is None):
                    idx_none.append(k)
            if(text_model == False):
                features = [torch.tensor(f, dtype=torch.double) for f in features]
        
            labels = G.ndata['label']
            train_mask = get_temp_train_mask(G, min_nodes_by_graph)
                
            all_labels = labels[train_mask]
            all_labels = all_labels.cpu().detach().numpy()
            all_index = np.where(np.array(train_mask) == True)[0]
            
            if(only_text == True):
                temp_all_index = []
                temp_all_labels = []
                cpt=0
                for i in range(len(all_index)):
                    if(dict_node_features[all_index[i]] is not None):
                        temp_all_index.append(all_index[i])
                        temp_all_labels.append(all_labels[i])
                    else:
                        cpt+=1
                all_index = temp_all_index
                all_labels = temp_all_labels               
        
            x0 =np.array([all_index[i] for i in range(len(all_index)) if all_labels[i] == 0])
            x1 =np.array([all_index[i] for i in range(len(all_index)) if all_labels[i] == 1])
            total_train_x0 += len(x0)
            total_train_x1 += len(x1)
            
            
            #1.2 TRAIN THE MODEL
            optimizer.zero_grad()
            if(is_edge_weighted == True):
                _, h, _, logits = model(G, features, G.edata['weight'], device=device)
            else:
                _, h, _, logits = model(G, features, device=device)

            # Compute prediction
            preds = logits.argmax(1)
    
            # Compute loss
            loss = F.cross_entropy(logits[train_mask], labels[train_mask])
            
            # TRAIN acc
            train_acc = (preds[train_mask] == labels[train_mask]).float().mean()
            
            # Backward
            loss.backward()
            optimizer.step()
            
            total_loss += loss.item()
            total_acc += train_acc.item()
            
        #1.3 COMPUTE LOSS
        epoch_loss = round(total_loss / len(TRAIN_SET), 5)
        epoch_acc = round(total_acc / len(TRAIN_SET), 5)
        
        #1.4 SAVE FINAL MODEL -- early stopping when train-loss increase --
        if(early_stop is None):
            final_model = copy.deepcopy(model)
        else:
            if(best_train_loss is not None and best_train_loss < epoch_loss):
                if(cpt_early_stop >= early_stop):
                    print(' > hop we stop, new_train_loss=', epoch_loss, 'best train loss=', best_train_loss, '==> cpt_early=', cpt_early_stop)
                    break
                else:
                    cpt_early_stop+=1
            else:
                print(' ! best model !')
                best_train_epoch = e
                best_train_loss = epoch_loss
                best_train_acc = epoch_acc
                final_model = copy.deepcopy(model)
                cpt_early_stop = 0
        
        #runtime
        train_epoch_runtime = time.time() - t_ep
        runtime += train_epoch_runtime
        
        ################# 2. EVALUATION  #################
        model.eval()
        all_test_preds = []
        all_test_labs = []
        total_test_x0 = 0
        total_test_x1 = 0
        # print('---len(TEST_SET):', len(TEST_SET))
        for topic, data in TEST_SET.items():
            G, dict_node_features = data
            #2.1 PREPARE tHE GRAPH
            n_nodes = []
            features = []
            idx_none = []
            for k, v in dict_node_features.items():
                n_nodes.append(k)
                features.append(v)
                if(v is None):
                    idx_none.append(k)
            if(text_model == False):
                features = [torch.tensor(f, dtype=torch.double) for f in features]
        
            labels = G.ndata['label']
            test_mask = [True] * len(G.nodes())
            # print('1/', len(test_mask), len(labels))
            # print('2/', len(features))
            all_labels = labels[test_mask]
            all_labels = labels
            all_labels = all_labels.cpu().detach().numpy()
            all_index = np.where(np.array(test_mask) == True)[0]
            
            if(only_text == True):
                temp_all_index = []
                temp_all_labels = []
                cpt=0
                for i in range(len(all_index)):
                    if(dict_node_features[all_index[i]] is not None):
                        temp_all_index.append(all_index[i])
                        temp_all_labels.append(all_labels[i])
                    else:
                        cpt+=1
                all_index = temp_all_index
                all_labels = temp_all_labels               
        
            x0 =np.array([all_index[i] for i in range(len(all_index)) if all_labels[i] == 0])
            x1 =np.array([all_index[i] for i in range(len(all_index)) if all_labels[i] == 1])
            total_test_x0 += len(x0)
            total_test_x1 += len(x1)
            
            
            #2.2 GET MODEL PREDICTION
            t_ep = time.time()
            with torch.no_grad():
                if(is_edge_weighted == True):
                    gnn_inputs, embeddings, _, probas = model(G, features, G.edata['weight'], device=device)
                else:
                    gnn_inputs, embeddings, _, probas = model(G, features, device=device)
            test_epoch_runtime = time.time() - t_ep
            
            #Get score QC
            current_label = ut.data_zarate_labels[topic]
            res_topic, dict_probas_by_id = get_prediction_score(G, probas, current_label, test_epoch_runtime)
            dict_scores_by_epoch[topic][e] = res_topic
            dict_sampled_scored_by_epoch[topic][e] = get_score_by_sampled_graph(G, dict_probas_by_id, dict_user_to_keep_for_score[topic],
                                                                                current_label, test_epoch_runtime)
            if(type_sample == 0):
                dict_sampled_scored_by_epoch2[topic][e] = get_score_by_sampled_graph(G, dict_probas_by_id, dict_user_to_keep_for_score2[topic],
                                                                    current_label, test_epoch_runtime)
                
            
            # Compute prediction
            # print('3/', len(probas))
            preds = probas.argmax(1)
            # print('4/', len(preds), len(test_mask))
            all_test_preds += preds[test_mask].tolist()
            all_test_labs += labels[test_mask].tolist()
        
        #2.3 GET TEST ACCURACY
        test_acc = accuracy_score(all_test_labs, all_test_preds) 
        if (best_train_epoch == e):
            best_test_acc = test_acc
        

        ################# 3. SAVE CHKPT & MODEL & METADATA  #################        
        #3.1 SAVE LOSS & ACC train test sets
        list_train_loss.append(epoch_loss)
        list_train_acc.append(epoch_acc)
        list_test_acc.append(test_acc)
                
        #3.2 SAVE FINAL MODEL
        if(best_train_epoch == e):
            save_model(path+'final_TEMP.pth', final_model)
        
        #3.3 SAVE CHKPT MODEL
        #save checkpoint
        for old_file in os.listdir(fold_chkpt):
              os.remove(fold_chkpt+old_file)
             
        # save current epochs 
        checkpoint_file = fold_chkpt + str(e)+'_'+model_name 
        torch.save({
            'epoch': e,
            'model_state_dict': model.state_dict(),
            'optimizer_state_dict': optimizer.state_dict(),
            'best_train_epoch': best_train_epoch,
            'best_train_loss': best_train_loss,
            'list_train_loss': list_train_loss,
            'best_train_acc': best_train_acc,
            'list_train_acc': list_train_acc,
            'best_test_acc': best_test_acc,
            'list_test_acc': list_test_acc,
            'cpt_early_stop': cpt_early_stop,
            'dict_scores_by_epoch': dict_scores_by_epoch,
            'dict_sampled_scored_by_epoch': dict_sampled_scored_by_epoch,
            'dict_sampled_scored_by_epoch2': dict_sampled_scored_by_epoch2,
            'runtime': runtime
            }, checkpoint_file)
        
        #3.4 PRINT INFO ON EPOCHS
        if(e%1 == 0):
            print('In epoch {}, train loss: {:.3f} + train acc: {:.3f} || test acc: {:.3f}'.format(
                e, epoch_loss, epoch_acc, test_acc))
            print('           > BEST: In epoch {}, train_loss: {:.3f} + train_acc: {:.3f} || test_acc: {:.3f}'.format(
                best_train_epoch, best_train_loss, best_train_acc, best_test_acc))
            
            print(' --> time :', round(((time.time() - t_ep)/60), 2), 'min --- ', total_train_x0, total_train_x1, total_test_x0, total_test_x1)
            print("------------------------------------------------- \n")

    # FINALISATION
    #save model
    print('\n --- Save final model ---')
    save_model(path+model_name, final_model)
    train_info = {'train_runtime': round(runtime/60,4),
                  'train_loss': best_train_loss,
                  'test_acc': best_test_acc}
    print('    -> Training info:', train_info)
    gp.write_json(path+'train_info.json', train_info)
    
    print('\n --- Save score at each epoch of test ---')
    dict_scores_file = path + 'dict_scores_by_epoch_'+str(type_sample)+'.json'
    dict_scores_by_epoch['BEST_EPOCH'] = best_train_epoch
    gp.write_json(dict_scores_file, dict_scores_by_epoch)
    
    print('\n --- Save score at each epoch of PROBAS ---')
    dict_probs_file = path + 'dict_sampled_scored_by_epoch_'+str(type_sample)+'_'+str(type_user_to_discard)+'.json'
    dict_sampled_scored_by_epoch['BEST_EPOCH'] = best_train_epoch
    gp.write_json(dict_probs_file, dict_sampled_scored_by_epoch)
    
    if(type_sample == 0):
        print('\n --- DO WORST : Save score at each epoch of PROBAS ---')
        dict_probs_file = path + 'dict_sampled_scored_by_epoch_'+str(type_sample)+'_WORST.json'
        dict_sampled_scored_by_epoch['BEST_EPOCH'] = best_train_epoch
        gp.write_json(dict_probs_file, dict_sampled_scored_by_epoch2)    
    
    #plot         
    path = path + 'plot/'
    if(os.path.isdir(path) == False):
        os.mkdir(path)
    
    p_train_loss = path + "train_loss.png"
    plt.plot(range(len(list_train_loss)), list_train_loss)
    plt.savefig(fname = p_train_loss)
    plt.show()
    
    p_train_acc = path + "train_acc.png"
    plt.plot(range(len(list_train_acc)), list_train_acc)
    plt.savefig(fname = p_train_acc)
    plt.show()
    
    p_test_acc = path + "test_acc.png"
    plt.plot(range(len(list_test_acc)), list_test_acc)
    plt.savefig(fname = p_test_acc)
    plt.show()
                
    return final_model


def get_dict_user_to_keep(path0, all_topics, MAX_SPL):
    dict_user_to_keep_for_score = {}
    # code_xp = code_xp + '_0' #aaa     
    code_xp_saved_models_path = path0 + 'GRAPH_TEXT_B1_0/'
    node_off_file = code_xp_saved_models_path + 'TEST_predictions_OFF.pickle'
    node_off_results_by_topic = gp.load_pickle(node_off_file)

    path_to_save = path0 + 'dict_user_to_keep_for_score__'+str(MAX_SPL)+'.pickle'
    if(os.path.isfile(path_to_save) == False):
        for topic in all_topics:   
            if(topic not in dict_user_to_keep_for_score.keys()):
                dict_user_to_keep_for_score[topic] = {}
                    
            node_ids, _, _, node_probas, _ = node_off_results_by_topic[topic]
            dict_probs_by_id = {k:v for k,v in zip(node_ids, node_probas)}
            
            for user_to_discard in ['BEST', 'WORST']:
                if(user_to_discard not in dict_user_to_keep_for_score[topic].keys()):
                    dict_user_to_keep_for_score[topic][user_to_discard] = {}
                
                #### ADD THE SAMPLE PART
                if(user_to_discard == 'BEST'):
                    print('> best !')
                    sorted_list = sorted(dict_probs_by_id.items(), key = lambda x:x[1], reverse = True)
                elif(user_to_discard == 'WORST'):
                    print('> worst !')
                    sorted_list = sorted(dict_probs_by_id.items(), key = lambda x:x[1], reverse = False)
                    
                idx_to_remove = int(len(sorted_list) * (MAX_SPL/100))
                print(' > remove users from score', sorted_list[0] ,'to score', sorted_list[idx_to_remove])
                usr_to_keep = [x[0] for x in sorted_list[idx_to_remove:]]                
    
                dict_user_to_keep_for_score[topic][user_to_discard] = usr_to_keep
        gp.pickle_save(path_to_save, dict_user_to_keep_for_score)
    else:
        print(' > already saved!')
        dict_user_to_keep_for_score = gp.load_pickle(path_to_save)
        
    return dict_user_to_keep_for_score


def get_score_by_sampled_graph(OG, dict_probas_by_id, user_to_keep_for_score,
                               current_label, test_epoch_runtime, runtime=-1):
    node_probas = []
    for id_, old_id_ in zip(OG.nodes().tolist(), OG.ndata[dgl.NID].tolist()):
            if(old_id_ in user_to_keep_for_score):
                node_probas.append(dict_probas_by_id[id_])
    
    node_predictions = [0 if value < 0.5 else 1 for value in node_probas]
    node_labels = [current_label] * len(node_predictions)
    print(' % kept = ', round(len(node_probas)/len(dict_probas_by_id)*100, 1))
    
    #3. GET SCORES
    naive_bayes = gp.get_naive_score(node_probas)
    
    qc_probas = round(sum(node_probas)/len(node_probas) , 3) 
    qc_preds = round(sum(node_predictions)/len(node_predictions) , 3)
    median = np.median(node_probas)

    acc_topic = round(accuracy_score(node_labels, node_predictions), 3)
    topic_entropy = round(get_shannon_entropy(node_probas), 3)
        
    #5. SAVE IN results topic info
    result = {'proba': qc_probas,
                        'prediction': qc_preds,
                        'median': median,
                        'bayes': naive_bayes,
                        'topic_shannon_entropy': topic_entropy,
                        'accuracy': acc_topic,
                        'runtime': runtime}
    return result
    
    
        
#--------------------------------------------------------------------------------------------------------------


"""
return quantification scores
"""
def get_prediction_score(G, probas, current_label, runtime=-1):  
    node_probas = probas[:,1].tolist() #get prediction of user being controversial
    node_predictions = probas.argmax(1).tolist()
    node_labels = [current_label] * len(node_predictions)
    
    cpt=0
    dict_proba_by_id = {}
    for n_id in G.nodes().tolist():
        dict_proba_by_id[n_id] = node_probas[cpt]
        cpt+=1
    
    #3. GET SCORES
    naive_bayes = gp.get_naive_score(node_probas)
    
    qc_probas = round(sum(node_probas)/len(node_probas) , 3) 
    qc_preds = round(sum(node_predictions)/len(node_predictions) , 3)
    median = np.median(node_probas)

    acc_topic = round(accuracy_score(node_labels, node_predictions), 3)
    topic_entropy = round(get_shannon_entropy(node_probas), 3)
        
    #5. SAVE IN results topic info
    result = {'proba': qc_probas,
                        'prediction': qc_preds,
                        'median': median,
                        'bayes': naive_bayes,
                        'topic_shannon_entropy': topic_entropy,
                        'accuracy': acc_topic,
                        'runtime': runtime}
    
    # append in dict
    return result, dict_proba_by_id

# -------------------------------------------------------------------------------------------------------------

"""
list of test k

loss_json = {0:0.434, 1:0.547, ...}

qs_json = {0:{'pelosi': 0.954, 'thanksgiving':0.124}, ...}

"""
def predict_model_SUBGRAPHS(dataset, test_k, model, text_model, is_edge_weighted, device, current_path, type_set='TEST', type_topic='ALL', is_timeline=False, nbs_iter=5000):
    print('||||||||||||||||||||||||||', type_set, ' PREDICTIONS ||||||||||||||||||||||||||')
    if(type_topic == 'ALL'):
        json_qs = current_path + type_set+'_results_scores.json'
        json_loss = current_path + type_set+'_loss_prediction.json'
    else:
        json_qs = current_path + 'TOPIC_results_scores.json'
        json_loss = current_path + 'TOPIC_loss_prediction.json'

    results_qs_score = {}
    results_loss_score = {}
    
    #labels
    if(type_set == 'MY_DATA'):
        topic_labels = ut.my_data_labels
    else:
        topic_labels = ut.data_zarate_labels

    if(os.path.isfile(json_qs) == True):
        results_qs_score = gp.load_json(json_qs)  
    if(os.path.isfile(json_loss) == True):
        results_loss_score = gp.load_json(json_loss)  
        
        
    if(is_timeline == False):
        sorted_topic = {k:topic_labels[k] for k in dataset.keys()}
    else:
        for k in dataset.keys():
            print(k)
            print('_'.join(k.split('_')[:-1]))
            print('***')
        sorted_topic = {k:topic_labels['_'.join(k.split('_')[:-1])] for k in dataset.keys()}
    sorted_topic = [k for k, v in sorted(sorted_topic.items(), key=lambda x: x[1])]
    
    
    for current_k in test_k:
        t1 = time.time()
        print('####### k=', current_k)
        if(str(current_k) in results_loss_score.keys()):
            print('  > k already done !')
            continue
        if(str(current_k) not in results_qs_score.keys()):
            results_qs_score[str(current_k)] = {}
    
        dict_res_topics = {}
        list_all_probas = []
        list_all_labels = []    
        eee=0
        for curr_i in range(nbs_iter):
            #1. get random topic
            current_topic = random.choice(sorted_topic)
            topic_label = topic_labels[current_topic]
            G, dict_node_features = dataset[current_topic]
        
            #2. get random center node id
            center_node = random.choice(G.nodes().tolist())
        
            if(current_k == -1):
                current_g = get_1_random_subgraph(G, center_node, device)
            else:
                current_g = get_1_subgraph_from_k(G, center_node, current_k, device)
                
            if(eee%500==0):
                print('....', eee)
            eee+=1
        
            n_nodes = []
            features = []
            new_center_node = None
            for id_, old_id_ in zip(current_g.nodes().tolist(), current_g.ndata[dgl.NID].tolist()):
                n_nodes.append(id_)
                # --------------------------------------------------- #TODO
                # if(old_id_ not in dict_node_features.keys() or dict_node_features[old_id_] is None):
                #     features.append([0])
                # else:
                #     features.append(dict_node_features[old_id_])
                features.append(dict_node_features[old_id_])
                # ---------------------------------------------------

                if(center_node == old_id_):
                    new_center_node = id_
            if(text_model == False):
                features = [torch.tensor(f, dtype=torch.double) for f in features]

            model.eval()
            with torch.no_grad():
                if(is_edge_weighted == True):
                    _, _, _, probas = model(current_g, features, current_g.edata['weight'], device=device)
                else:
                    _, _, _, probas = model(current_g, features, device=device)
            probas = probas[:,1].tolist()
            
            
            if(current_topic not in dict_res_topics.keys()):
                dict_res_topics[current_topic] = []
            dict_res_topics[current_topic].append(probas[new_center_node])
            list_all_probas.append(probas[new_center_node])                            
            list_all_labels.append(topic_label)
        
        
        
        #2. COMPUTE QC SCORE
        for topic, probas in dict_res_topics.items():
            results_qs_score[str(current_k)][topic] = round(sum(probas)/len(probas) , 3) 
        gp.write_json(json_qs, results_qs_score)
            
    
        #3. COMPUTE loss k    if(os.path.isdir(xp_folder) == False):
        test_loss = F.binary_cross_entropy(torch.tensor(list_all_probas, dtype=torch.float32), torch.tensor(list_all_labels, dtype=torch.float32)) 
        print('    =========== test loss for k=', current_k, ' --> ',  test_loss, '=============')
        results_loss_score[str(current_k)] = test_loss.item()
        gp.write_json(json_loss, results_loss_score)
        
        print(' ***', round(((time.time() - t1)/60), 2), 'min ***')  
    
    print('-- END TEST --')
    return results_qs_score, results_loss_score 
        



def get_1_random_subgraph(G, center_node_id, device, poisson_lambda=2, iter_=1):
    
    list_subgraphs = []
    poisson_sample = stats.poisson.rvs(poisson_lambda, size=iter_)
    for k_value in poisson_sample:
        sg, _ = dgl.khop_out_subgraph(G, center_node_id, k=k_value, relabel_nodes=True)
        # sg = build_subgraph(G, center_node_id, k_value, device)
        list_subgraphs.append(sg)     
    
    return list_subgraphs[0]


def get_1_subgraph_from_k(G, center_node_id, k_value, device):
    if(k_value is None):
        subgraph = [G]                
    else:
        subgraph, _ = dgl.khop_out_subgraph(G, center_node_id, k=k_value, relabel_nodes=True)
        # subgraph = build_subgraph(G, center_node_id, k_value, device)
        
    return subgraph



"""
get random subgraphs for each user of size k, with k being randomly selected with a poisson distribution

new_test_set = {id_topic: { 'subgraphs': {id_iter:[center_node1, subgraphs, k_value],...},
                            'features': dict_nodes_features,
                            'label': topic_label 
                         }
               }
"""
def get_random_subgraphs(TEST_SET, path, device, poisson_lambda=2, nbs_iter=5000, type_topic='ALL'):
    new_path = '/'.join(path.split('/')[:-2])+'/subgraphs_storage/'
    if(os.path.isdir(new_path) == False):
        os.mkdir(new_path)
    filename = new_path+'test_'+type_topic+'random_subgraph_'+str(poisson_lambda)+'_'+str(nbs_iter)+'.pickle'
    t1 = time.time()
    print(' --- get subgraphs ---')

    if(os.path.isfile(filename) == True):
        print(' > test subgraphs already exist !')
        new_test_set = gp.load_pickle(filename)
        return new_test_set
    
    list_topic = list(TEST_SET.keys())
    
    new_test_set = {}
    nbs_labs = [0,0]
    nbs_k = {}
    for current_iter in range(nbs_iter):
        #1. random k_value
        current_k_value = stats.poisson.rvs(poisson_lambda, size=1)[0]
        if(current_k_value not in nbs_k.keys()):
            nbs_k[current_k_value] = 0
        nbs_k[current_k_value]+=1 
        
        #2. random graph
        current_topic = random.choice(list_topic)
        G, dict_node_features = TEST_SET[current_topic]
        
        current_topic_label = G.ndata['label'][0]
        nbs_labs[current_topic_label]+=1
        
        #3. random node
        current_center_node_id = random.choice(G.nodes().tolist())
        subgraph, _ = dgl.khop_out_subgraph(G, current_center_node_id, k=current_k_value, relabel_nodes=True)
        # subgraph = build_subgraph(G, current_center_node_id, current_k_value, device)
    
        if(current_topic not in new_test_set.keys()):
            new_test_set[current_topic] = {'subgraphs': {}, 'features': dict_node_features, 'label': current_topic_label}
        
        new_test_set[current_topic]['subgraphs'][current_iter] = [current_center_node_id, subgraph, current_k_value]
            
    print('             - labels repartition:', nbs_labs)
    print('             - k_value repartition:', nbs_k)
    
    print('---', round(((time.time() - t1)/60), 2), 'min ---') 
    
    gp.pickle_save(filename, new_test_set)
    
    return new_test_set



"""
get subgraphs for each user of size k

new_test_set = {id_topic: { 'subgraphs': {id_iter:[center_node1, subgraphs, k_value],...},
                            'features': dict_nodes_features,
                            'label': topic_label 
                         }
               }
"""
def get_subgraph_from_k(TEST_SET, k_value, path, device, nbs_iter=5000, type_topic='ALL'):
    new_path = '/'.join(path.split('/')[:-2])+'/subgraphs_storage/'
    if(os.path.isdir(new_path) == False):
        os.mkdir(new_path)
    filename = new_path+'test_'+type_topic+'_subgraph_'+str(k_value)+'_'+str(nbs_iter)+'.pickle'
    t1 = time.time()
    print(' --- get subgraphs ---')

    if(os.path.isfile(filename) == True):
        print(' > test subgraphs already exist !')
        new_test_set = gp.load_pickle(filename)
        return new_test_set
    
    list_topic = list(TEST_SET.keys())
    
    new_test_set = {}
    nbs_labs = [0,0]
    for current_iter in range(nbs_iter):
        current_topic = random.choice(list_topic)
        G, dict_node_features = TEST_SET[current_topic]
        current_topic_label = G.ndata['label'][0]
        
        nbs_labs[current_topic_label]+=1
        
        current_center_node_id = random.choice(G.nodes().tolist())
        
        if(k_value is None):
            subgraph = [G]                
        else:
            subgraph, _ = dgl.khop_out_subgraph(G, current_center_node_id, k=k_value, relabel_nodes=True)
            # subgraph = build_subgraph(G, center_node_id, k_value, device)   
        
        if(current_topic not in new_test_set.keys()):
            new_test_set[current_topic] = {'subgraphs': {}, 'features': dict_node_features, 'label': current_topic_label}
        
        new_test_set[current_topic]['subgraphs'][current_iter] = [current_center_node_id, subgraph, k_value]
            
            
    print('             >', k_value, ' - labels repartition:', nbs_labs)
    print('---', round(((time.time() - t1)/60), 2), 'min ---') 
    
    gp.pickle_save(filename, new_test_set)
    
    return new_test_set



# """
# create subgraph from central_node, of k levels
# """
# def build_subgraph(G, central_node, k, device):
#     kept_nodes = [torch.tensor([central_node], device=device)]
#     #1. get neighbors nodes at each hop k from central node
#     for i in range(k):
#         last_level = kept_nodes[-1]
        
#         next_level = [get_neighbors(G, n) for n in last_level]
#         next_level = torch.cat(next_level)
#         next_level = torch.unique(next_level)

#         kept_nodes.append(next_level)

#     #2. flat all levels & and get unique nodes
#     kept_nodes = torch.cat(kept_nodes)
#     kept_nodes = torch.unique(kept_nodes)
#     kept_nodes = kept_nodes.to(device)
    
#     # Create the subgraph
#     subgraph = G.subgraph(kept_nodes)

#     return subgraph    

# """
# get in and out neighbors
# """    
# def get_neighbors(G, node):
#     in_neighbors, _ = G.in_edges(node)
#     _, out_neighbors = G.out_edges(node)

#     neighbors = torch.concatenate((in_neighbors, out_neighbors))
#     neighbors = torch.unique(neighbors)

#     return neighbors


"""
train loop by sending subgraphs on train set
- 
- take random topic (1 time C, 1 time NC)
- take random user in it
- take random k

- max user iteration = 10k
- does it until loss train epoch does not move for 10 iter
"""
def train_loop_SUBGRAPHS(TRAIN_SET, TEST_SET, 
               only_text, model, 
               optimizer, epochs, device, batch_gnn, 
               is_edge_weighted, text_model, path, model_name, iter_by_ep = 100):
    
    epochs = 300
    
    # set parameters of EPOCHS LOOP
    final_model = copy.deepcopy(model)
    start_epoch = 0
    best_train_epoch = -1
    best_train_loss = None
    list_train_loss = []
    best_test_loss = -1 #depend on best train epoch
    best_test_acc = -1 #depend on best train epoch
    list_test_acc = []
    cpt_early_stop = 0
    runtime = 0
    
    fold_chkpt = path + ut.checkpoint_folder
    if(os.path.isdir(fold_chkpt) == False):
        os.mkdir(fold_chkpt)
    
    if(len(os.listdir(fold_chkpt)) > 0):
        file = os.listdir(fold_chkpt)[0]
        checkpoint_path = fold_chkpt + file
    
        if(os.path.exists(checkpoint_path)):
            print(' > found checkpoint!')
            chk = torch.load(checkpoint_path)
            model.load_state_dict(chk['model_state_dict'])
            optimizer.load_state_dict(chk['optimizer_state_dict'])
            start_epoch = chk['epoch']+1
            best_train_epoch = chk['best_train_epoch']
            best_train_loss = chk['best_train_loss']
            list_train_loss = chk['list_train_loss']
            best_test_loss = chk['best_test_loss']
            best_test_acc = chk['best_test_acc']
            list_test_acc = chk['list_test_acc']
            cpt_early_stop = chk['cpt_early_stop']
            
            runtime = chk['runtime']
            
            
            final_model = torch.load(path+'final_TEMP.pth')
    
    print(' >> early_stop=', early_stop)
    for e in range(start_epoch, epochs):
        print("------------------", e, "---------------------------")
        t_ep = time.time()

        is_contr_iter = 1
        list_logits = []
        list_labs = []

        good_labs = [0,0]
        ################# 1. TRAINING  #################
        for i in range(iter_by_ep):
            model.train()

            #1. select random topic
            rd_idx = random.randint(0, len(TRAIN_SET) - 1)
            while(TRAIN_SET[rd_idx][0].ndata['label'][0] != is_contr_iter):
                rd_idx = random.randint(0, len(TRAIN_SET) - 1)
            G, dict_node_features = TRAIN_SET[rd_idx]
            node_label = G.ndata['label'][0]
            good_labs[node_label]+=1
            if(is_contr_iter==1):
                is_contr_iter=0
            else:
                is_contr_iter=1
            
            #3. select random user
            center_node = random.choice(G.nodes().tolist())
            
            #4. select random k + create subgraph + dict_features
            current_g = get_1_random_subgraph(G, center_node, device)
            
            n_nodes = []
            features = []
            new_center_node = None
            for id_, old_id_ in zip(current_g.nodes().tolist(), current_g.ndata[dgl.NID].tolist()):
                n_nodes.append(id_)
                # --------------------------------------------------- #TODO
                # if(old_id_ not in dict_node_features.keys() or dict_node_features[old_id_] is None): #TODO
                #     features.append([0])
                # else:
                #     features.append(dict_node_features[old_id_])
                features.append(dict_node_features[old_id_])
                # ---------------------------------------------------
                if(center_node == old_id_):
                    new_center_node = id_
            if(text_model == False):
                features = [torch.tensor(f, dtype=torch.double) for f in features]
            
            #5. forward model
            optimizer.zero_grad()
            if(is_edge_weighted == True):
                _, h, _, logits = model(current_g, features, current_g.edata['weight'], device=device)
            else:
                _, h, _, logits = model(current_g, features, device=device)
    
            list_logits.append(logits[new_center_node])
            list_labs.append(node_label)

        #6. train only on the user selected

        list_logits = torch.stack(list_logits)
        list_logits = list_logits.cpu()
        list_labs = torch.Tensor(list_labs)
        list_labs = list_labs.long()
        loss = F.cross_entropy(list_logits, list_labs)
        loss.backward()
        optimizer.step()            
        
        #7. save model if it is better in train loss
        epoch_loss = loss.item()
        
        
        #1.4 SAVE FINAL MODEL -- early stopping when train-loss increase --
        if(early_stop is None):
            final_model = copy.deepcopy(model)
        else:
            if(best_train_loss is not None and best_train_loss < epoch_loss):
                if(cpt_early_stop >= early_stop):
                    print(' > hop we stop, new_train_loss=', epoch_loss, 'best train loss=', best_train_loss, '==> cpt_early=', cpt_early_stop)
                    break
                else:
                    cpt_early_stop+=1
            else:
                print(' ! best model !')
                best_train_epoch = e
                best_train_loss = epoch_loss
                final_model = copy.deepcopy(model)
                cpt_early_stop = 0
            
        ################# 2. EVALUATION  #################
        model.eval()
        all_test_loss = []
        all_test_preds = []
        all_test_labs = []
        total_test_x0 = 0
        total_test_x1 = 0
        for G, dict_node_features in TEST_SET:
            #2.1 PREPARE tHE GRAPH
            n_nodes = []
            features = []
            idx_none = []
            #------------------------------------------------ #TODO
            for k, v in dict_node_features.items(): 
                n_nodes.append(k)
                features.append(v)
                if(v is None):
                    idx_none.append(k)
            if(text_model == False):
                features = [torch.tensor(f, dtype=torch.double) for f in features]
            # for k in G.nodes(): 
            #     n_nodes.append(k)
            #     if(k in dict_node_features.keys()):
            #         v = dict_node_features[k]
            #     else:
            #         v = [0]
            #     features.append(v)
            #     if(v is None):
            #         idx_none.append(k)
            # if(text_model == False):
            #     features = [torch.tensor(f, dtype=torch.double) for f in features]
            #------------------------------------------------
                
        
            labels = G.ndata['label']
            test_mask = [True] * len(G.nodes())
                
            all_labels = labels[test_mask]
            all_labels = all_labels.cpu().detach().numpy()
            all_index = np.where(np.array(test_mask) == True)[0]
            
            if(only_text == True):
                temp_all_index = []
                temp_all_labels = []
                cpt=0
                for i in range(len(all_index)):
                    if(dict_node_features[all_index[i]] is not None):
                        temp_all_index.append(all_index[i])
                        temp_all_labels.append(all_labels[i])
                    else:
                        cpt+=1
                all_index = temp_all_index
                all_labels = temp_all_labels               
        
            x0 =np.array([all_index[i] for i in range(len(all_index)) if all_labels[i] == 0])
            x1 =np.array([all_index[i] for i in range(len(all_index)) if all_labels[i] == 1])
            total_test_x0 += len(x0)
            total_test_x1 += len(x1)
            
            
            #2.2 GET MODEL PREDICTION
            with torch.no_grad():
                if(is_edge_weighted == True):
                    _, h, _, logits = model(G, features, G.edata['weight'], device=device)
                else:
                    _, h, _, logits = model(G, features, device=device)

            # Compute prediction
            preds = logits.argmax(1)
            proba = logits[:, 1]
            all_test_loss += proba[test_mask].tolist()
            all_test_preds += preds[test_mask].tolist()
            all_test_labs += labels[test_mask].tolist()

        
        #2.3 GET TEST ACCURACY
        test_acc = accuracy_score(all_test_labs, all_test_preds)
        test_loss = F.binary_cross_entropy(torch.tensor(all_test_loss, dtype=torch.float32), torch.tensor(all_test_labs, dtype=torch.float32))
        if (best_train_epoch == e):
            best_test_loss = test_loss.item()
            best_test_acc = test_acc



        ################# 3. SAVE CHKPT & MODEL & METADATA  #################        
        #3.1 SAVE LOSS & ACC train test sets
        list_train_loss.append(epoch_loss)
        list_test_acc.append(test_acc)
                
        #3.2 SAVE FINAL MODEL
        if(best_train_epoch == e):
            save_model(path+'final_TEMP.pth', final_model)
        
        #3.3 SAVE CHKPT MODEL
        #save checkpoint
        for old_file in os.listdir(fold_chkpt):
              os.remove(fold_chkpt+old_file)
             
        # save current epochs 
        checkpoint_file = fold_chkpt + str(e)+'_'+model_name 
        torch.save({
            'epoch': e,
            'model_state_dict': model.state_dict(),
            'optimizer_state_dict': optimizer.state_dict(),
            'best_train_epoch': best_train_epoch,
            'best_train_loss': best_train_loss,
            'list_train_loss': list_train_loss,
            'best_test_loss': best_test_loss,
            'best_test_acc': best_test_acc,
            'list_test_acc': list_test_acc,
            'cpt_early_stop': cpt_early_stop,
            'is_contr_iter': is_contr_iter,
            'runtime': runtime
            }, checkpoint_file)
        
        #3.4 PRINT INFO ON EPOCHS
        if(e%1 == 0):
            print('In epoch {}, train loss: {:.3f} || test acc: {:.3f}, {}'.format(
                e, epoch_loss, test_acc, good_labs))
            print('           > BEST: In epoch {}, train_loss: {:.3f} || test_acc: {:.3f}, loss:{:.3f}, {}'.format(
                best_train_epoch, best_train_loss, best_test_acc, best_test_loss, good_labs))
            
            print(' --> time :', round(((time.time() - t_ep)/60), 2), 'min --- ', total_test_x0, total_test_x1)
            print("------------------------------------------------- \n")

    # FINALISATION
    #save model
    print('\n --- Save final model ---')
    save_model(path+model_name, final_model)
    train_info = {'train_runtime': round(runtime/60,4),
                  'train_loss': best_train_loss,
                  'test_acc': best_test_acc,
                  'test_loss': best_test_loss}
    print('    -> Training info:', train_info)
    gp.write_json(path+'train_info.json', train_info)
    
    #plot         
    path = path + 'plot/'
    if(os.path.isdir(path) == False):
        os.mkdir(path)
    
    p_train_loss = path + "train_loss.png"
    plt.plot(range(len(list_train_loss)), list_train_loss)
    plt.savefig(fname = p_train_loss)
    plt.show()
    
    p_test_acc = path + "test_acc.png"
    plt.plot(range(len(list_test_acc)), list_test_acc)
    plt.savefig(fname = p_test_acc)
    plt.show()
                
    return final_model
