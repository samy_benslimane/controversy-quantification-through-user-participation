#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 27 16:16:16 2023

@author: samy
"""


import torch.nn.functional as F
import torch.nn as nn
import torch

import numpy as np
import copy
import time
import os

import util as ut


from sklearn.metrics import classification_report, accuracy_score
from transformers import BertTokenizer, BertModel
from transformers import (AdamW, 
                          get_linear_schedule_with_warmup,
                          set_seed)

from torch.utils.data import WeightedRandomSampler
from torch.utils.data import DataLoader

from os import path
import html
import re
import pandas as pd



#-----------------------------------------------------------------------

"""
bert_fine_tuning with train & test set only
"""
def bert_fine_tuning(path_fine_tuned_model, train_tweets, test_tweets,
                     dict_tweet_label, dict_text,
                     topic, device, root, 
                     bert_epochs = 30,
                     bert_batches = 64,
                     max_length = 256,
                     lr_rate = 2e-5,
                     truncate = True,
                     discr_lr = True):
    
    # bert_epochs = 10
    print('<<<<<<<<<<<<<<<<<<<< TRAIN BERT MODEL FT >>>>>>>>>>>>>>>>>>>>>>>')
    print('******************************')
    print('       INIT PARAMETERS       ')
    print('+ topic : ', topic)
    print('+ size train/test : ', len(train_tweets), len(test_tweets))
    print('+ nbs tweets : ', len(dict_text))
    print('+ device : ', device)
    print('+ bert_epochs : ', bert_epochs)
    print('+ bert_batches : ', bert_batches)
    print('+ max_length : ', max_length)
    print('+ lr_rate : ', lr_rate)
    print('+ truncate : ', truncate)
    print('+ discr_lr : ', discr_lr)
    print('+ *early stoping*')
    print('******************************')  
    
    
    # path_fine_tuned_model  = ut.CURRENT_APP_FOLD + bert_m_folder + topic+'_'+task+'_'+features+'_BERT.pth'
    print(path_fine_tuned_model)
    
    start_time_1 = time.time()
    set_seed(123)
    
    # Loading tokenizer...
    if(os.path.isdir(ut.basic_tokenizer)==False):
        tokenizer = BertTokenizer.from_pretrained(ut.basic_hugging_face)
    else:
        tokenizer = BertTokenizer.from_pretrained(ut.basic_tokenizer)
    
    #... And load model...
    if(os.path.isdir(ut.BASIC_MODEL)==False):
        pretrained_bert_model = BertModel.from_pretrained(ut.basic_hugging_face)
    else:
        pretrained_bert_model = BertModel.from_pretrained(ut.BASIC_MODEL)        
        
    #delete previous fine-tuned model, because different split train/test
    if(path.exists(path_fine_tuned_model) == True):
        print('- /!\ a model fine-tuned was found & KEPT !')
        model = torch.load(path_fine_tuned_model)
        return model, tokenizer, None        
    
    # Get features

    model = Tweet_BERTModel(ut.basic_hugging_face, pretrained_bert_model)
    
    #... and put it on device
    if(torch.cuda.is_available() == False):
        print('- No gpu available !')
        model.to(torch.device("cpu"))
        device_ = 'cpu'
    else:
        first_gpu = 'cuda:'+str(device[0])
        model.to(torch.device(first_gpu)) #put the model on GPU(s) --for the backpropagation part--
        if(len(device) > 1):
            print('- Use of mutliple gpu !')
            model = nn.DataParallel(model, device_ids = device) #--for the forward part--
        device_ = ['cuda:'+d for d in device]
    
    
    ########################### DATA ##############################
    ###### Get data
    X, X_train, X_test, y, y_train, y_test = get_train_test_set(train_tweets, test_tweets, dict_tweet_label, dict_text)
    
    #df to list
    y_train = y_train['label'].tolist()
    y_test = y_test['label'].tolist()
    
    X_train = clean_text_v2(X_train[0].tolist())
    if(len(X_test) > 0):
        X_test = clean_text_v2(X_test[0].tolist())
    else:
        X_test = []

        
    ###### DATALOADERS
    print('#### 1.3.1 TOKENIZATION ####')
    
    # Train DATASET                        
    print('Dealing with Train...')
    train_dataset = TwitterDataset(X_train, y_train,
                                   tokenizer=tokenizer,
                                   max_sequence_len=max_length)
    
    print('Created `train_dataset` with', len(train_dataset), 'examples!')
    if(truncate == True):
        sampler = get_balanced_sampler(y_train)
        train_dataloader = DataLoader(train_dataset,
                                      sampler=sampler,
                                      batch_size=bert_batches)
    else:
        train_dataloader = DataLoader(train_dataset,                                      
                                      batch_size=bert_batches,
                                      shuffle=True)
    test_train_proportion(train_dataloader)
    
    
    # Test DATASET
    print('Dealing with Test...')
    test_dataset =  TwitterDataset(X=X_test, y=y_test,
                                   tokenizer=tokenizer, 
                                   max_sequence_len=max_length)
    print('Created `test_dataset` with', len(test_dataset), 'examples!')
    test_dataloader = DataLoader(test_dataset, batch_size=bert_batches, shuffle=False)
    
    # Test DATASET w/ balanced ds
    print('Dealing with test balanced...')
    test_dataset_balanced =  TwitterDataset(X=X_test, y=y_test,
                                   tokenizer=tokenizer, 
                                   max_sequence_len=max_length)
    print('Created `test_dataset_balanced` with', len(test_dataset_balanced), 'examples!')
    if(truncate == True and len(y_test) > 0):
        sampler = get_balanced_sampler(y_test)
        test_balanced_dataloader = DataLoader(test_dataset_balanced,
                                      sampler=sampler,
                                      batch_size=bert_batches, shuffle=False)
    else:
        print('--------- no truncate ------------')
        test_balanced_dataloader = DataLoader(test_dataset_balanced,                                      
                                      batch_size=bert_batches,
                                      shuffle=False)        
    
    #----------------------------------------------------------------------------------------------
    
    ######################### TRAINING ############################
    #optimizer
    print(' > add weight decay')
    if(discr_lr == True):
        grouped_parameters = configure_optimizers(model, lr_rate)
        optimizer = AdamW(grouped_parameters, lr = lr_rate, eps = 1e-8, weight_decay=0.05)
    else:
        optimizer = AdamW(model.parameters(), lr = lr_rate, eps = 1e-8, weight_decay=0.05)
        
    criterion = nn.CrossEntropyLoss()
    
    # Total number of training steps is number of batches * number of epochs.
    total_steps = len(train_dataset) * bert_epochs
    
    # Create the learning rate scheduler.
    scheduler = get_linear_schedule_with_warmup(optimizer, 
                                                num_warmup_steps = 0, # Default value in run_glue.py
                                                num_training_steps = total_steps)
    
    
    print('#### 1.3.4. START BERT TRAINING (learning rate :', lr_rate,') ####')
    train_loss_list = []
    test_loss_list = []
    test_b_loss_list = []
    
    train_acc_list = []
    test_acc_list = []
    test_b_acc_list = []
    
    start_epoch = 0
    fold_chkpt = root + ut.checkpoint_folder
    if(os.path.isdir(fold_chkpt) == False):
        os.mkdir(fold_chkpt)
        
    if(len(os.listdir(fold_chkpt)) > 0):
        file = os.listdir(fold_chkpt)[0]
        checkpoint_path = fold_chkpt + file
    
        if(os.path.exists(checkpoint_path)):
            print(' > find checkpoint!')
            chk = torch.load(checkpoint_path)
            
            model.load_state_dict(chk['model_state_dict'])
            optimizer.load_state_dict(chk['optimizer_state_dict'])
            start_epoch = chk['epoch']+1
            
            train_loss_list = chk['train_loss']
            test_loss_list = chk['test_loss']
            train_acc_list = chk['train_acc']
            test_acc_list = chk['test_acc']
        
    loss_last_epoch = 1000
    final_model = None
    for epoch in range(start_epoch, bert_epochs): # Loop through each epoch.
        print("--> EPOCH ", epoch, '/', bert_epochs-1)

        # Perform one full pass over the training set.
        train_labels, train_predict, train_loss = train(model, train_dataloader, criterion, optimizer, scheduler, device_)
        train_acc = accuracy_score(train_labels, train_predict)
    
        # Get prediction form model on test data. 
        # print('test on batches...')
        
        if(len(test_dataloader) > 0):
            test_labels, test_predict, test_loss = validation(model, test_dataloader, criterion, device_)
            test_acc = accuracy_score(test_labels, test_predict)
            test_labels_b, test_predict_b, test_loss_b = validation(model, test_balanced_dataloader, criterion, device_)
            test_acc_b = accuracy_score(test_labels_b, test_predict_b)
        else:
            test_loss = -1
            test_acc = -1
            test_loss_b = -1
            test_acc_b = -1
        
        if(loss_last_epoch < train_loss):
            print(' > hop we stop, new_train_loss=', train_loss)
            break
        else:
            loss_last_epoch = train_loss
            final_model = copy.deepcopy(model)


        # Print loss and accuracy values to see how training evolves.
        print('(train_loss, test_loss, trai_acc, test_acc) = ', train_loss, test_loss, train_acc, test_acc)
        train_loss_list.append(train_loss)
        test_loss_list.append(test_loss)
        test_b_loss_list.append(test_loss_b)
        
        train_acc_list.append(train_acc)
        test_acc_list.append(test_acc)
        test_b_acc_list.append(test_acc_b)
        # print('Train Loss : ', train_loss_list)
        
        #delete previous checkpoint
        for old_file in os.listdir(fold_chkpt):
              os.remove(fold_chkpt+old_file)
             
        # save current epochs 
        checkpoint = fold_chkpt + 'epoch_' + str(epoch) + '.pth'
        torch.save({
            'epoch': epoch,
            'model_state_dict': model.state_dict(),
            'optimizer_state_dict': optimizer.state_dict(),
            'train_loss': train_loss_list,
            'test_loss': test_loss_list,
            'test_b_loss': test_b_loss_list,
            'train_acc': train_acc_list,
            'test_acc': test_acc_list,
            'test_b_acc': test_b_acc_list,
            }, checkpoint)
    
    #### plot loss
    ut.plot_loss_bert(train_loss_list, test_loss_list, test_b_loss_list, root)
    ut.plot_acc_bert(train_acc_list, test_acc_list, test_b_acc_list, root)
    
    ######################## VALIDATION ###########################
    print('#### 1.3.5. START BERT TEST ####')
    
    # Get prediction form model on validation data. This is where you should use
    if(len(test_dataloader) > 0):
        true_labels, predictions_labels, avg_epoch_loss = validation(final_model, test_dataloader, criterion, device_)
  
    # Show the evaluation report.
    print(">>>> FINAL BERT MODEL RESULT <<<<")
    # Show the evaluation report.
    print(">>>> TRAIN (last epoch) <<<<")
    print(classification_report(train_labels, train_predict))
    
    if(len(test_dataloader) > 0):
        print(">>>> TEST <<<<")
        # Create the evaluation report.
        evaluation_report = classification_report(true_labels, predictions_labels)
        print(evaluation_report)
        
    print('--- runtime bert : ', round((time.time() - start_time_1)/60, 2), 'min')
    
    
    ##### Save model
    print('#### 1.3.6. SAVE model ####')
    torch.save(final_model, path_fine_tuned_model)
    print('- model saved! ', path_fine_tuned_model)
    
    #delete previous checkpoint
    for old_file in os.listdir(fold_chkpt):
         os.remove(fold_chkpt+old_file)
         
    
    train_report = classification_report(train_labels, train_predict, output_dict=True)
    test_report = None
    if(len(test_dataloader) > 0):
        test_report = classification_report(true_labels, predictions_labels,  output_dict=True)
        
    
    results = {'train': train_report, 'test': test_report}
        
    return final_model, tokenizer, results


"""
CONTROVERSY
class representing our model composed by a bert model + 1 more dense layer of 64 neurons + 1 softmax layer
Fine-tuning already done, here it is just a forward model
"""
class Tweet_BERTModel(nn.Module):
    """
    init function
    input : 
        - model_name_or_path : name of the pre-trained model
        - type_model : 'sentiment' or 'controversy'
        - pretrained_bert_model : pre-trained model il already loaded (default=None)
    """
    def __init__(self, model_name_or_path, pretrained_bert_model=None):
        super(Tweet_BERTModel, self).__init__()
        
        if(pretrained_bert_model is None):
            self.bert_model = BertModel.from_pretrained(model_name_or_path)
        else:
            self.bert_model = pretrained_bert_model
            
        self.dropout = nn.Dropout(0.2)
    
        self.embedding_layer = nn.Linear(768, 768)
        self.classifier_layer = nn.Linear(768, 2)
        
        
        
    """
    forward function
    input :
        - bert_ids : bert id of the sentence (from tokenization)
        - bert_mask : bert mask og the sentence (from tokenization)
    output :
        - out_ : prediction
        - out_embedding : embedding representation   
    """
    def forward(self, x, forward_2 = False):
        if(forward_2 == False):
            bert_ids = x[0]
            bert_mask = x[1]
                        
            #sequence_output, pooled_output = self.bert_model(bert_ids, attention_mask=bert_mask)
            out_bert = self.bert_model(bert_ids, attention_mask=bert_mask)
            sequence_output = out_bert['last_hidden_state']
            
            #Layer 1
            # sequence_output has the following shape: (batch_size, sequence_length, 768)
            seq = sequence_output[:,0,:].view(-1,768)
            seq = self.dropout(seq)
            seq = self.embedding_layer(seq) ## extract the 1st token's embeddings
            
            out_embedding = F.relu(seq)
        
            #Layer 2
            out_ = self.classifier_layer(out_embedding)
            out_ = F.softmax(out_, dim=1)
            
            return out_, out_embedding
        
        else:
            bert_ids = x[0]
            bert_mask = x[1]
                        
            #sequence_output, pooled_output = self.bert_model(bert_ids, attention_mask=bert_mask)
            out_bert = self.bert_model(bert_ids, attention_mask=bert_mask)
            sequence_output = out_bert['last_hidden_state']
            
            #Layer 1
            # sequence_output has the following shape: (batch_size, sequence_length, 768)
            seq = sequence_output[:,0,:].view(-1,768)
            seq = self.dropout(seq)
            seq = self.embedding_layer(seq) ## extract the 1st token's embeddings
            
            out_embedding = F.relu(seq)
        
            #Layer 2
            out_no_soft = self.classifier_layer(out_embedding)
            out_ = F.softmax(out_, dim=1)
            
            return out_, out_no_soft, out_embedding
        

"""
class which will transform (tokenized) the input text, and keep its corresponding label 
"""
class TwitterDataset():
    """
    init function
    input : 
        - tokenizer : Transformer type tokenizer used to process raw text into numbers.
        - y : Dictionary to encode any labels names into numbers. Keys map to labels names and Values map to number associated to those labels.
        - max_sequence_len : Value to indicate the maximum desired sequence to truncate or pad text sequences. If no value is passed it will used maximum sequence size
                             supported by the tokenizer and model.
    """
    def __init__(self, X, y, tokenizer, max_sequence_len=None):
        # Check max sequence length.
        max_sequence_len = tokenizer.max_len if max_sequence_len is None else max_sequence_len
        texts = X
        labels = y

        # Number of exmaples.
        self.n_examples = len(labels)
        print('max_sequence_len... ', max_sequence_len)
        
        # Use tokenizer on texts. This can take a while.
        if(len(texts) > 0):
            self.inputs = tokenizer(
                            text=texts,  # the sentence to be encoded
                            add_special_tokens=True,  # Add [CLS] and [SEP]
                            truncation=True,
                            max_length = max_sequence_len,  # maximum length of a sentence
                            pad_to_max_length=True,  # Add [PAD]s
                            return_attention_mask = True,  # Generate the attention mask
                            return_tensors = 'pt',  # ask the function to return PyTorch tensors
                        )    
    
            # Get maximum sequence length.
            self.sequence_len = self.inputs['input_ids'].shape[-1]
            print('Texts padded or truncated to', self.sequence_len, 'length!')
    
            # Add labels.
            self.inputs.update({'labels':torch.tensor(labels)})

        else:
            print(' > no sample inside!')
            self.inputs = {}

    def __len__(self):
        return self.n_examples

    def __getitem__(self, item):
        return {key: self.inputs[key][item] for key in self.inputs.keys()}
    
    
"""
clean text
"""
def clean_text_v2(inputs, is_list=True):
    if(is_list == True):
        new_inputs = []
        print(' > cleaning text!')
        for tweet in inputs:
            #remove retweet
            tweet = re.sub('(?<=^|(?<=[^a-zA-Z0-9-_\.]))@([A-Za-z0-9-_]+[A-Za-z0-9-_]+)', r'USER', tweet)
            
            #remove URL
            tweet = re.sub(r'http\S+', r'URL', tweet)
            
            tweet = html.unescape(tweet)     
            tweet = re.sub(r'\n', r'', tweet)
            new_inputs.append(tweet)
            
        return new_inputs
    
    
"""
get a sampler for having more balanced dataset for the dataloader
input : 
    - labels : list of label of the dataloader
output :
    - sampler : sampler which will sample the data
"""
def get_balanced_sampler(labels):
    lab_unique, counts = np.unique(labels, return_counts=True)
    
    class_w = [sum(counts)/c for c in counts]
    balanced_w = [class_w[e] for e in labels]
    
    sampler = WeightedRandomSampler(balanced_w, len(labels))
    
    return sampler


"""
test training proportion of the dataloader
input : 
    - train_dataloader : dataloader of training set
"""
def test_train_proportion(train_dataloader):
    lab0 = 0
    lab1 = 0
    for step, batch in enumerate(train_dataloader):
        list_lab =  batch['labels']
        for l in list_lab:
            if(l == 0):
                lab0+=1
            else:
                lab1+=1
                
    tot = lab0 + lab1
    print('--- PROPORTION CLASS DATALOADER ---')
    print('lab0 : ', round((lab0/tot)*100, 2), '% (', lab0 ,') / lab1 : ', round((lab1/tot)*100, 2), '% (', lab1 ,') --> TOT : ', tot)
   

################## MODEL FUNCTION ###################

"""
function used to fine-tuned our model
input : 
    - model : model to fine-tuned
    - dataloader : train dataloader
    - criterion : criterion to compute loss
    - optimizer_ : optimizer chosed
    - scheduler_ : scheduler 
    - device_ : device used
output : 
    - true_labels : real labels
    - predictions_labes : predicted labels
    - avg_epoch_loss : average loss by epoch during training
"""
def train(model, dataloader, criterion, optimizer, scheduler, device_):
    predictions_labels = []
    true_labels = []

    total_loss = 0
    model.train() 
    for step, batch in enumerate(dataloader):
        if(device_ != 'cpu'):
            batch = {k:v.type(torch.long).cuda(device_[0]) for k,v in batch.items()}     # move batch to device

        model.zero_grad() #init gradient at 0

        outputs, outputs_embedding = model([batch['input_ids'], batch['attention_mask']])
        loss = criterion(outputs, batch['labels'])
        
        loss.backward() #compute gradient
        torch.nn.utils.clip_grad_norm_(model.parameters(), 1.0)     #  prevent the "exploding gradients" problem.
        optimizer.step() #Apply gradient descent
        scheduler.step() #Update the learning rate.

        total_loss += loss.item() #sum up all loss       
        
        true_labels += batch['labels'].detach().cpu().numpy().flatten().tolist()  
        predictions_labels += outputs.detach().cpu().numpy().argmax(axis=-1).flatten().tolist() 

    avg_epoch_loss = total_loss / len(dataloader)
  
    return true_labels, predictions_labels, avg_epoch_loss


"""
function used to test our model
input : 
    - model : model to fine-tuned
    - dataloader : test dataloader
    - criterion : criterion to compute loss
    - device_ : device used
output : 
    - true_labels : real labels
    - predictions_labes : predicted labels
    - avg_epoch_loss : average loss by epoch during test
"""
def validation(model, dataloader, criterion, device_):
    predictions_labels = []
    true_labels = []

    total_loss = 0
    model.eval()
    for step, batch in enumerate(dataloader):
        if(device_ != 'cpu'):
            batch = {k:v.type(torch.long).cuda((device_[0])) for k,v in batch.items()} #move batch to device

        with torch.no_grad():
            outputs, outputs_embedding = model([batch['input_ids'], batch['attention_mask']])
               
            loss = criterion(outputs, batch['labels'])
            
            total_loss += loss.item()

            true_labels += batch['labels'].detach().cpu().numpy().flatten().tolist() #add original labels
            predictions_labels += outputs.detach().cpu().numpy().argmax(axis=-1).flatten().tolist()
            # embeddings += outputs_embedding.detach().cpu().numpy().tolist()

    avg_loss = total_loss / len(dataloader)

    return true_labels, predictions_labels, avg_loss



"""
predict from model
"""
def predict_model_TEXT_ALONE(path_model, dict_text, topic, 
                  device, bert_batches=64, max_length=256):
    
    ########################### get tokenizer and model ###########################
    if(os.path.isdir(ut.basic_tokenizer)==False):
        tokenizer = BertTokenizer.from_pretrained(ut.basic_hugging_face)
    else:
        tokenizer = BertTokenizer.from_pretrained(ut.basic_tokenizer) 
    
    model = torch.load(path_model)
    
    #... and put it on device
    if(torch.cuda.is_available() == False):
        print('- No gpu available !')
        model.to(torch.device("cpu"))
        device_ = 'cpu'
    else:
        first_gpu = 'cuda:'+str(device[0])
        model.to(torch.device(first_gpu)) #put the model on GPU(s) --for the backpropagation part--
        if(len(device) > 1):
            print('- Use of mutliple gpu !')
            model = nn.DataParallel(model, device_ids = device) #--for the forward part--
        device_ = ['cuda:'+d for d in device]
    
    
    ########################### DATA ##############################
      
    X = [v for k,v in dict_text.items()]

    X = clean_text_v2(X)
    false_y = [-1]*len(X)
    
    dataset =  TwitterDataset(X=X, y=false_y,
                              tokenizer=tokenizer, 
                              max_sequence_len=max_length)
    print('dataset contains', len(dataset), 'examples!')
    dataloader = DataLoader(dataset, batch_size=bert_batches, shuffle=False)    
    
    ########################### PREDICTION ##############################
    predicted_labels = []
    out_emb = []
    print(' > size dataloader (nbs batch):', len(dataloader))
    model.eval()
    for step, batch in enumerate(dataloader):
        if(device_ != 'cpu'):
            batch = {k:v.type(torch.long).cuda((device_[0])) for k,v in batch.items()} #move batch to device

        with torch.no_grad():       
            outputs, outputs_embedding = model([batch['input_ids'], batch['attention_mask']])
            
            predicted_labels += outputs.detach().cpu().numpy().argmax(axis=-1).flatten().tolist()
            out_emb += outputs_embedding.detach().cpu().numpy().tolist()
            
    dict_labels = {}
    dict_embedding = {}
    cpt=0
    for k,v in dict_text.items():
        dict_embedding[k] = out_emb[cpt]
        dict_labels[k] = predicted_labels[cpt]
        cpt+=1

    return dict_embedding, dict_labels



"""
output 
"""
def send_to_train(x, dict_label_by_tweet, dict_text):
    lab = dict_label_by_tweet[x['tweet_id']]
    feat = dict_text[x['tweet_id']]

    return [feat, feat, lab, lab]

"""
input :
    - train_tweets : list of train post
    - test_tweets : list of test post
    - dict_tweet_label : label for each post
    - dict_text : text for each post/comment
"""
def get_train_test_set(train_tweets, test_tweets, dict_tweet_label, dict_text):
    
    print(' -get train_test_set-')
    print('- dict_text (', len(dict_text.keys()),')')

                       
    print(' **sol fati2**')
    print('   *send train')
    df = pd.DataFrame({'tweet_id': train_tweets})
    df['temp_train'] = df.apply(lambda x: send_to_train(x, dict_tweet_label, dict_text), axis=1)
    df[['X', 'X_train', 'y', 'y_train']] = pd.DataFrame(df['temp_train'].tolist(), index= df.index)
    X = df[df["X"].isnull() == False]['X'].tolist()
    X_train = df[df["X_train"].isnull() == False]['X_train'].tolist()
    y = df[df["y"].isnull() == False]['y'].tolist()
    y_train = df[df["y_train"].isnull() == False]['y_train'].tolist()
    
    X_train_idx = df[df["X_train"].isnull() == False]['tweet_id'].tolist()
    
    print('   *send test')
    df = pd.DataFrame({'tweet_id': test_tweets})
    df['temp_test'] = df.apply(lambda x: send_to_train(x, dict_tweet_label, dict_text), axis=1)
    if(len(df) == 0):
        df = df.assign(X='', X_test='', y='', y_test='')
    else:
        df[['X', 'X_test', 'y', 'y_test']] = pd.DataFrame(df['temp_test'].tolist(), index= df.index)
    X += df[df["X"].isnull() == False]['X'].tolist()
    X_test = df[df["X_test"].isnull() == False]['X_test'].tolist()
    y += df[df["y"].isnull() == False]['y'].tolist()
    y_test = df[df["y_test"].isnull() == False]['y_test'].tolist()
    
    X_test_idx = df[df["X_test"].isnull() == False]['tweet_id'].tolist()
    
    pb=0    
    print('---problem in get train_test set (NO MEANING) : ', pb)
    
    print('  -- transform to DF no FN')
    X = pd.DataFrame(X)
    X_train = pd.DataFrame(X_train, index=X_train_idx)
    X_test = pd.DataFrame(X_test, index=X_test_idx)
    y = pd.DataFrame(y, columns=['label'])
    y_train = pd.DataFrame(y_train, columns=['label'], index=X_train_idx)
    y_test = pd.DataFrame(y_test, columns=['label'], index=X_test_idx)
    
    print(len(X), len(X_train), len(X_test), len(y), len(y_train), len(y_test))
    print('vs')
    print(len(dict_tweet_label), len(train_tweets), len(test_tweets))
    print('***')
    print(X_train.head(3))
    print(' <finish>')

    return X, X_train, X_test, y, y_train, y_test


"""
function used for creating discriminative learning rate
input :
    - model : the current model
    - the learning rate for bert layers
output :
    - grouped_parameters : the group parameters for each layers
"""
def configure_optimizers(model, lr):
    params = list(model.named_parameters())
    def is_backbone(n): return 'bert' in n
    grouped_parameters = [
        {"params": [p for n, p in params if is_backbone(n)], 'lr': lr},
        {"params": [p for n, p in params if not is_backbone(n)], 'lr': lr * 100},
    ]
    return grouped_parameters