#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun May  7 15:24:54 2023

@author: samy
"""
from sklearn.metrics import precision_score, recall_score, accuracy_score, f1_score
from sklearn.metrics import roc_auc_score
# from scipy.stats import entropy
from sklearn.metrics import mean_absolute_error

from scipy.special import logsumexp
import graph_processing as gp
import numpy as np
import math
import os
import pickle5 as pickle

import util as ut

data_zarate_labels = {'impeachment-5-10': 1,
                    'menciones-1-10enero': 1,
                    'mothersday': 0,
                    'menciones-20-27marzo': 1,
                    'area51': 0,
                    'OTDirecto20E': 0,
                    'bolsonaro27': 1,
                    'bolsonaro28': 1,
                    'VanduMuruganAJITH': 0,
                    'nintendo': 0,
                    'messicumple': 0,
                    'wrestlemania': 0,
                    'kingjacksonday': 0,
                    'kavanaugh06-08': 1,
                    'bolsonaro30': 1,
                    'notredam': 0,
                    'Thanksgiving': 0,
                    'halsey': 0,
                    'feliznatal': 0,
                    'kavanaugh16': 1,
                    'kavanaugh02-05': 1,
                    'EXODEUX': 0,
                    'bigil': 0,
                    'lula_moro_chats': 1,
                    'menciones-05-11abril': 1,
                    'LeadersDebate': 1,
                    'championsasia': 0,
                    'menciones05-11mayo': 1,
                    'pelosi': 1,
                    'SeungWooBirthday': 0,
                    'menciones-11-18marzo': 1}

"""
get naive score from log likehihood

x0 = log[P(x1==NC)] + ... + log[P(xn==NC)
x1 = log[P(x1==C)] + ... + log[P(xn==C)
SCORE = EXP[ x1 - logsumexp[x1;x0]]
"""
def get_naive_score(probs):
    x0 = 0
    for p in probs:
        x0 = x0 + np.log(1-p)
    x1 = 0
    for p in probs:
        x1 = x1 + np.log(p)
    naive_bayes = np.exp(x1 - logsumexp([x1, x0])) 
                          
    return naive_bayes

"""
get shannon entropy from node probabilities output of class 1
"""
def get_shannon_entropy(node_probas_class_1):    
    # Calcul des probabilités de classe 0 (compléments des probabilités de classe 1)
    # probabilities_class_0 = [1 - p for p in node_probas_class_1]
    
    # Calcul de l'entropie
    entropy = -sum(p * math.log2(p) for p in node_probas_class_1) / len(node_probas_class_1)
    
    return entropy

def load_pickle(path_file):
    if(os.path.isfile(path_file) == False):
        return False
    else:     
        with open(path_file, 'rb') as file:
            data = pickle.load(file)
    return data

# ------------------------------------------------ computes scores metric by topic -------------------------------------
# path = '/data/DEV_DATA/TWITTER API/other/data_complete_vaccine_chloro/data/data_objects/'
path = ut.CURRENT_DATA_FOLD + ut.object_folder

code_graph = 'connex' #'connex', 'not_connex'
all_xp = ['GRAPH_TEXT_B1'] #, 'GRAPH', 'GRAPH_TEXT_B1']
# all_xp = ['TEXT', 'GRAPH', 
#           'GRAPH_TEXT_B2f', 'GRAPH_TEXT_B2d', 'GRAPH_TEXT_B2e',
#           'GRAPH_TEXT_B2c', 'GRAPH_TEXT_B2', 'GRAPH_TEXT_B2b',
#           'GRAPH_TEXT_B1f', 'GRAPH_TEXT_B1d', 'GRAPH_TEXT_B1e',
#           'GRAPH_TEXT_B1c', 'GRAPH_TEXT_B1', 'GRAPH_TEXT_B1b']
print_perf_model_by_xp = False
print_qc_score_by_xp = True

for code_xp in all_xp:    
    path_xp = path + 'USR_QC_'+code_graph+'/ZARATE/RETWEET/'+code_xp+'/'
    print(path_xp)
    if(code_xp=='TEXT'):
        data_objects_by_topic = load_pickle(path_xp+'TEST_results_object.pickle')
    else:
        data_objects_by_topic = load_pickle(path_xp+'TEST_predictions.pickle')

    if(data_objects_by_topic == False):
        print('xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx')
        print('                    <<', code_xp, '>>')
        print('                /!\ xp prediction not found /!\ ')
        print('xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx \n')
        continue
    
    sorted_topics = {k:data_zarate_labels[k] for k in data_objects_by_topic.keys()}
    sorted_topics = {k: v for k, v in sorted(sorted_topics.items(), key=lambda item: item[1])}
    topics = list(sorted_topics.keys())
    
    list_all_probas = []
    list_all_predictions = []
    list_all_labels = []
    
    all_scores = {'naive_bayes': [],
                  'qc_preds': [],
                  'qc_med_probas': [],
                  'entropy_topic': [],
                  'acc_topic': [],
                  'labels': []}
        
    if(code_xp == 'TEXT'):
        for topic in topics:
            dict_scores_tweets, dict_score_users = data_objects_by_topic[topic]
            node_labels = [data_zarate_labels[topic]] * len(dict_score_users)
            node_probas = [v['user_score'][0] for k,v in dict_score_users.items()]
            node_predictions = [v['user_score'][1] for k,v in dict_score_users.items()]
            
            list_all_probas += node_probas
            list_all_predictions += node_predictions
            list_all_labels += node_labels
            
    else:
        for topic in topics:
            node_ids, text_embeddings, node_embeddings, node_probas, node_predictions = data_objects_by_topic[topic]
            node_labels = [data_zarate_labels[topic]] * len(node_probas)
         
            #3. GET SCORES
            naive_bayes = gp.get_naive_score(node_probas)
            qc_probas = round(sum(node_probas)/len(node_probas) , 3) 
            qc_preds = round(sum(node_predictions)/len(node_predictions) , 3)
            qc_med_probas = np.median(node_probas)
            
            acc_topic = accuracy_score([1]*len(node_predictions), node_predictions) 
            entropy_topic = get_shannon_entropy(node_probas)
            
            # print('******************', topic, '******************')
            # print('---- LABEL', data_zarate_labels[topic], '----')
            # print('    + naive_bayes:', naive_bayes)
            # print('    + avg (probas/preds):', qc_probas, '/', qc_preds)
            # print('    + median (probas):', qc_med_probas)
            # print('    + acc_topic:', acc_topic)
            # print('    + shannon_entropy_topic:', entropy_topic)
            # print('---------------- \n\n')
            
            all_scores['naive_bayes'].append(naive_bayes)
            all_scores['qc_preds'].append(qc_preds)
            all_scores['qc_med_probas'].append(round(qc_med_probas, 3))
            all_scores['acc_topic'].append(round(acc_topic, 3))
            all_scores['entropy_topic'].append(round(entropy_topic, 3))
            all_scores['labels'].append(data_zarate_labels[topic])
            list_all_probas += node_probas
            list_all_predictions += node_predictions
            list_all_labels += node_labels
        
        
    list_all_predictions = [int(x) for x in list_all_predictions]
    if(print_perf_model_by_xp == True):
        print('xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx')
        print('                    <<', code_xp, '>>')
        print(' ** entropy:', round(get_shannon_entropy(list_all_probas), 3)) #SHANNON entropy
        print(' ** precision:', round(precision_score(list_all_labels, list_all_predictions), 3))
        print(' ** recall:', round(recall_score(list_all_labels, list_all_predictions), 3))
        print(' ** accuracy:', round(accuracy_score(list_all_labels, list_all_predictions), 3))
        print(' ** f1-score:', round(f1_score(list_all_labels, list_all_predictions), 3))
        
        
    if(print_qc_score_by_xp == True):
        print('xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx')
        print('METRIC ON QUANTIFICATION SCORES, FROM TEST', len(topics), 'TOPICS')
        print('\n __________ \n')
        
        for type_score, scores  in all_scores.items():
            if(type_score !='labels'):
                labels = all_scores['labels']
                print('    +', type_score, '=', round(roc_auc_score(labels, scores), 3), '|', round(mean_absolute_error(labels, scores), 3))
        
    print('xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx \n')

