#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 16 15:08:40 2023

@author: samy
"""


import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import json

path = '/home/samy/Bureau/Development/Experiment/USR_QC/results/'
type_train = 'TRAIN protocol' #'TRAIN protocol' 'TRAIN classic'
type_sample = 'random' #'random' 'not random'

# rename_xp = {'GRAPH': 'GRAPH',
#              'TEXT': 'TEXT',
#              'B1': 'GRAPH_TEXT_B1',
#              'B1b': 'GRAPH_TEXT_B1b',
#              'B1c': 'GRAPH_TEXT_B1c',
#              'B2': 'GRAPH_TEXT_B2',
#              'B2b': 'GRAPH_TEXT_B2b',
#              'B2c': 'GRAPH_TEXT_B2c'}

rename_xp = {'GRAPH': 'GRAPH',
             'TEXT': 'TEXT',
             'B1': 'GAT_MEAN_2',
             'B1b': 'GAT_MEAN_3',
             'B1c': 'GAT_MEAN_1',
             'B2': 'GRAPHSAGE_MEAN_2',
             'B2b': 'GRAPHSAGE_MEAN_3',
             'B2c': 'GRAPHSAGE_MEAN_1'}

###### protocol #######
# xp_to_compare = ['GRAPH', 'TEXT', 'B1', 'B1b', 'B1c', 'B2', 'B2b', 'B2c']
xp_to_compare = ['GRAPH', 'B1', 'B1b', 'B1c', 'B2', 'B2b', 'B2c']
# xp_to_compare = ['B1', 'B1c', 'B1b', 'B2', 'B2b', 'B2c']

# k_to_compare = [0, 1, 2, 3, 4, 5]
k_to_compare = [0, 1, 2, 3, 4, 5]
# k_to_compare = [1, 2, 3, 4, 5]
#######################

###### classsic #######
# xp_to_compare = ['GRAPH', 'TEXT', 'B1', 'B1c', 'B2', 'B2b', 'B2c']
# xp_to_compare = ['GRAPH', 'B1', 'B1c', 'B2', 'B2b', 'B2c']
# xp_to_compare = ['GRAPH', 'B1', 'B1c']

# k_to_compare = [0, 1, 2, 3, 4, 5]
# k_to_compare = [0, 1, 2, 3, 4, 5]
# k_to_compare = [1, 2, 3, 4, 5]
######################

def load_json(filename):
    with open(filename) as f:
        res = json.load(f)
    return res

def create_plot(data, xp_to_compare):
    sns.set(style="whitegrid")  # Set the style of the plot
    
    for xp in xp_to_compare:
        data_temp = data[data['xp'] == xp]
        sns.lineplot(x='k_value', y='score', data=data_temp, label=xp)
    
    # Add labels and a legend
    plt.xlabel('k value')
    plt.ylabel('loss score')
    plt.legend(title='code xp', prop={'size': 8})
    # plt.legend(title='code xp', prop={'size': 8}, bbox_to_anchor=(0.3, 0.31))
    plt.xticks(k_to_compare)
    
    plt.savefig('loss_evolution.pdf') 
    
    # Show the plot
    plt.show()


if __name__ == "__main__":
    #1. get data
    print('1. get data')
    data_path = path + type_train + '/' + type_sample + '/'
    all_results = {}
    for xp in xp_to_compare:
        current_path = data_path + xp + '/'
        result = load_json(current_path+'TEST_loss_prediction.json')
        all_results[xp] = result
        
    #2. transform data
    print('2. transform data')
    df = pd.DataFrame(columns=['xp', 'k_value', 'score'])
    
    for current_xp, dict_res_by_k in all_results.items():
        if(current_xp == 'TEXT'):
            current_score = dict_res_by_k['0']
            for current_k in k_to_compare:
                df.loc[len(df)] = [current_xp, int(current_k), current_score]
        else:
            for current_k, current_score in dict_res_by_k.items():
                if(int(current_k) in k_to_compare):
                    df.loc[len(df)] = [current_xp, int(current_k), current_score]
    
    #2bis. rename
    for k,v in rename_xp.items():
        df['xp'] = df['xp'].replace(k,v)
    
    #3. plot results
    print('3. plot data')
    create_plot(df, df['xp'].unique().tolist())
    
    #4. print scores
    print('4. print scores')
    result = df.groupby('xp')['score'].mean().reset_index()
    result = result.sort_values(by='score', ascending=True)
    result['score'] = result['score'].round(3)
    
    print(result)
    
    