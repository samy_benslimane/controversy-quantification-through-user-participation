#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar  2 11:36:46 2021

@author: samy
"""

import time
import pytz

from datetime import datetime, timedelta
import matplotlib.pyplot as plt
import dateutil.parser
import numpy as np
import os


TYPE_SERVOR = 'jeanzay' #'local', 'gpu4', 'jeanzay'

scrath_disk = True
###################################################
################# PATH VARIABLE ###################
CURRENT_STATE_ = 'USR_QC/'

local = "/home/samy/Bureau/Development/Experiment/"+CURRENT_STATE_
gpu4 = "/home/samy/notebooks/"+CURRENT_STATE_

STORE = '/gpfsstore/rech/kdj/unn89uy/' + CURRENT_STATE_ #store real datasets
WORK = '/gpfswork/rech/kdj/unn89uy/'+ CURRENT_STATE_ #store outputs, dataset created, logs... 
HOME = '/linkhome/rech/genlir01/unn89uy/' + CURRENT_STATE_ #store code
SCRATCH = '/gpfsscratch/rech/kdj/unn89uy/' + CURRENT_STATE_ #cache for fast access --unused--


if(TYPE_SERVOR == 'local'):
    CURRENT_APP_FOLD = local
    #CURRENT_DATA_FOLD = "/home/samy/Bureau/Development/Experiment/Data collection/Twitter API/data/"
    CURRENT_DATA_FOLD = "/data/DEV_DATA/TWITTER API/other/data_complete_vaccine_chloro/data/"
elif(TYPE_SERVOR == 'gpu4'):
    CURRENT_APP_FOLD = gpu4
    CURRENT_DATA_FOLD = "/home/samy/notebooks/GNN_QC/data/"
else:
    CURRENT_APP_FOLD = WORK
    if(scrath_disk == True):
        CURRENT_DATA_FOLD = '/gpfsscratch/rech/kdj/unn89uy/GNN_QC/data/'
    else:
        CURRENT_DATA_FOLD = '/gpfsstore/rech/kdj/unn89uy/GNN_QC/data/'

CURRENT_CREDS = CURRENT_APP_FOLD + "twitter_api_key_academic.txt"
CURRENT_LOG_FOLD = CURRENT_APP_FOLD + "log/"
CURRENT_OUTPUT_FOLD = CURRENT_APP_FOLD + "output/"
CURRENT_OTHER_FOLD = CURRENT_APP_FOLD + "other/"

### FOLDERS
DATA_ZARATE = 'data_ZARATE/data_ZARATE_translated_v3/'
DATA_QC = 'data_QC/'
MY_DATA = 'my_data/'
rt_networks = 'retweet_networks/'
follow_networks = 'follow_networks/'
object_folder = 'data_objects/'
text_qc = 'text/'
text_qc_object = 'text_objects/'
checkpoint_folder = 'checkpoint/'


#my_data folders
tweet_folder = 'tweets/'
hashtag_folder = 'hashtags/'
request_folder = 'requests/'
user_folder = 'users/'

label_folder = 'data_objects/LABELS/'
community_folder = 'community_labels/'

#log file
ts = time.gmtime()
LOG_FILE = CURRENT_LOG_FOLD + 'log_run_' + str(ts[0]) + '_' + str(ts[1]) + '_' + str(ts[2]) + '_' + str(ts[3]) + '_' + str(ts[4]) + '_' + str(ts[5]) + '.txt'

#-------- About Text features ----------
HASHTAGS = 'hashtag'

BASIC = 'basic'
SENTIMENT = 'sentiment'
ARGUMENT = 'argument'
EMOTION = 'emotion'

bert_model_folder = 'bert_model/' 

# BASIC
basic_hugging_face = 'bert-base-uncased'
basic_tokenizer = CURRENT_APP_FOLD + bert_model_folder + "saved_BASIC_tokenizer"
BASIC_MODEL = CURRENT_APP_FOLD + bert_model_folder + "saved_BASIC_bert_model"

# SENTIMENT
sentiment_hugging_face = 'nlptown/bert-base-multilingual-uncased-sentiment' #"cardiffnlp/twitter-roberta-base-sentiment"
sentiment_tokenizer = CURRENT_APP_FOLD + bert_model_folder + "saved_SENTIMENT_tokenizer"
SENTIMENT_MODEL = CURRENT_APP_FOLD + bert_model_folder + "saved_SENTIMENT_bert_model"
SENTIMENT_model_classif = CURRENT_APP_FOLD + bert_model_folder + "saved_SENTIMENT_bert_classifier"

# ARGUMENT
argument_tokenizer = CURRENT_APP_FOLD + bert_model_folder + "saved_ARGUMENT_tokenizer"
ARGUMENT_MODEL = CURRENT_APP_FOLD + bert_model_folder + "saved_ARGUMENT_bert_model"
ARGUMENT_model_classif = CURRENT_APP_FOLD + bert_model_folder + "saved_ARGUMENT_bert_classifier"

# EMOTION
emotion_tokenizer = CURRENT_APP_FOLD + bert_model_folder + "saved_EMOTION_tokenizer"
EMOTION_MODEL = CURRENT_APP_FOLD + bert_model_folder + "saved_EMOTION_bert_model"
EMOTION_model_classif = CURRENT_APP_FOLD + bert_model_folder + "saved_EMOTION_bert_classifier"


###################################################
############### CONSTANT VATIABLE #################

CO_OCCURENCE = 'co_occurence'
COSINE_SIMILARITY = 'cosine_similarity'
WORD2VEC = 'word2vec'

COLORS = ['red','blue','green', 'yellow']

#---------- Twitter -----------

#TYPE OF GRAPHs
RETWEET = 'RETWEET'
QUOTE = 'QUOTE'
RT_Q = 'RT_QUOTE'
FOLLOW = 'FOLLOW'

#Tweet fields
tweet_fields = ['attachments','author_id','context_annotations','conversation_id','created_at',
                'entities','id','in_reply_to_user_id','public_metrics','possibly_sensitive','referenced_tweets','source','text', 'geo']
social_network = 'twitter'

#tweet type
retweeted_type = 'retweeted'
quoted_type = 'quoted'
replied_to_type = 'replied_to'

tweet_types = {RETWEET: [retweeted_type],
               QUOTE: [quoted_type],
               RT_Q: [retweeted_type, quoted_type]}

#state of request
CREATE = 'CREATE'
WIP = 'WIP'
FAILURE = 'FAILURE'
SUCCESS = 'SUCCESS'
DELETE = 'DELETE'

#default tweet
NO_TEXT = "[deleted]"

#dataset_split
train_ratio = 0.8
test_ratio = 0.2

nc_split = [0.7,0.1,0.2] #train, val, test

splitter = '<<;;>>'

#---------- Data TYPE -----------

mydata = 'my_data'
dataqc = 'data_qc'
dataqc_text = 'data_qc_with_text'
data_zarate = 'data_from_zarate'

#------------- GNN --------------

GCN = 'gcn'
GAT = 'gat'
GRAPHSAGE = 'graphsage'

#---------- Predictor -----------

DOT_PREDICTOR = 'dot_predictor'
MLP_PREDICTOR = 'mlp_predictor'

#----------- CLUSTER ------------
K_means = 'k_means'

#--------- REDUCE DIM ----------

TSNE ='t_sne'
UMAP = 'umap'

colormap = np.array(['r', 'g', 'b', 'y', 'c', 'm'])


#------ CONTROVERSY_SCORE -------

davies_bouldin = 'DB'
davies_bouldin_modified = 'DB_m'
all_metrics = 'all'


#-------- TEXT FEATURES ---------

#aggregators
MEAN_AGGR = 'MEAN'
MAX_AGGR = 'MAX'
SUM_AGGR = 'SUM'

#-------- TOPIC query ---------

my_data_no_text_topics = 'my_data_no_text_topics'
my_data_topics = 'my_data_topics'

data_qc_topics = 'data_qc_topics'

data_qc_T_topics = 'data_qc_retrieved_topics'
data_qc_T_topics_with_text = 'data_qc_retrieved_topics_with_text'

data_qc_zarate = 'data_qc_from_zarate'
data_qc_zarate_with_text = 'data_qc_from_zarate_with_text'

data_qc_retrieved_topics = 'data_qc_retrieved_topics'

##################################################
##################### DATA #######################

my_data_labels = {'covid_vaccine': 1,
                  'chloroquine': 1,
                  'cannabis_inm': 1,
                  'fasting_inm': 1,
                  'met_gala_2021': 0,
                  'sport_inm': 0,
                  'abortion': 1,
                  'impeachment_trump': 1,
                  'death_penalty': 1,
                  'gun_control': 1,
                  'vaccine_covid_child': 1,
                  'djokovic_AO': 1,
                  'kavanaughB1': 1,
                  'covid_janssen_moderna_or_or_or_or_pfizer_pfizer-biontech_vaccine': 1}

data_qc_labels = {'onedirection': 0,
                  'leadersdebate': 1,
                  'ukraine': 1,
                  'sxsw': 0,
                  'mothersday': 0,
                  'germanwings': 0, #in lot of language
                  'nepal': 0,
                  'indiana': 1,
                  'beefban': 1, #in indian
                  'ultralive': 0,
                  'netanyahu': 1,
                  'indiasdaughter': 1,
                  'baltimore': 1,
                  'nemtsov': 1,
                  'russia_march': 1, #in russian
                  'gunsense':1, #x
                  'ff':0, #x
                  'jurassicworld':0, #x
                  'nationalkissingday':0, #x
                  'wcw':0 #x
                  }

data_zarate_labels = {'impeachment-5-10': 1,
                    'menciones-1-10enero': 1,
                    # 'mothersday': 0,
                    'menciones-20-27marzo': 1,
                    'area51': 0,
                    'OTDirecto20E': 0,
                    'bolsonaro27': 1,
                    'bolsonaro28': 1,
                    'VanduMuruganAJITH': 0,
                    'nintendo': 0,
                    'messicumple': 0,
                    'wrestlemania': 0,
                    'kingjacksonday': 0,
                    'kavanaugh06-08': 1,
                    'bolsonaro30': 1,
                    'notredam': 0,
                    'Thanksgiving': 0,
                    'halsey': 0,
                    'feliznatal': 0,
                    'kavanaugh16': 1,
                    'kavanaugh02-05': 1,
                    'EXODEUX': 0,
                    'bigil': 0,
                    'lula_moro_chats': 1,
                    'menciones-05-11abril': 1,
                    'LeadersDebate': 1,
                    'championsasia': 0,
                    'menciones05-11mayo': 1,
                    'pelosi': 1,
                    'SeungWooBirthday': 0,
                    'menciones-11-18marzo': 1}


data_zarate_split = {'TRAIN': ['menciones-1-10enero',
                               'menciones-20-27marzo',
                               'menciones-05-11abril',
                               'menciones05-11mayo',
                               'menciones-11-18marzo',
                               'bolsonaro27',
                               'bolsonaro28',
                               'bolsonaro30',
                               'lula_moro_chats',
                               'impeachment-5-10',
                               'EXODEUX',
                               'notredam',
                               'bigil',
                               'VanduMuruganAJITH',
                               'championsasia',
                               'SeungWooBirthday',
                               'nintendo',
                               'halsey',
                               'kingjacksonday',
                               'OTDirecto20E'],
                     'TEST': ['kavanaugh16',
                              'kavanaugh02-05',
                              'kavanaugh06-08',
                              'pelosi',
                              'LeadersDebate',
                              'messicumple',
                              'feliznatal',
                              'area51',
                              'Thanksgiving',
                              'wrestlemania']}
# print(len(set([x for x in data_zarate_split['TRAIN'] if(data_zarate_labels[x]==0)])))





###################################################
################ DATA PARAMETERS ##################

uuid_admin = 'bc2f1d72-80af-4c19-b3ea-33ea78e7bda8'

request_to_topic = {'covid_vaccine': 'b26b9dbc-1c5a-4cd3-a3a6-c2f5a51ad0fc',
                    'chloroquine': '1aac5aa9-16ef-4aef-93e1-0b43ea7d03cb',
                    'kavanaughB1': 'e874fdb5-f032-4be2-97f2-2946b1376c57',
                    'covid_janssen_moderna_or_or_or_or_pfizer_pfizer-biontech_vaccine': '124e4728-fa81-4a36-a8d6-9c17d5402f9f'}

my_topic = ['covid_janssen_moderna_or_or_or_or_pfizer_pfizer-biontech_vaccine']

# topic_qc = ['beefban',
#             'russia_march',
#             'indiana',
#             'indiasdaughter',
#             'baltimore',
#             'nationalkissingday', #ok
#             'ff', #ok
#             'ukraine',
#             'nemtsov',
#             'sxsw', #ok
#             'gunsense',
#             'wcw', #ok
#             'netanyahu',
#             'ultralive', #ok
#             'onedirection', #ok
#             'jurassicworld', #ok
#             'germanwings', #ok
#             'leadersdebate',
#             'nepal', #ok
#             'mothersday'] #ok
topic_qc = ['sxsw']


# topic_qc_text = ['beefban',
#                  'indiana',
#                  'russia_march',
#                  'baltimore',
#                  'indiasdaughter',
#                  'netanyahu',
#                  'nemtsov',
#                  'ultralive',
#                  'ukraine',
#                  'onedirection',
#                  'sxsw',
#                  'germanwings',
#                  'leadersdebate',
#                  'nepal',
#                  'mothersday']
topic_qc_text = ['netanyahu', 'beefban', 'indiana', 'ultralive']


topic_qc_zarate = ['pelosi', 
                    'impeachment-5-10',
                    'menciones-1-10enero',
                    #'mothersday', #do not use it
                    'menciones-20-27marzo',
                    'area51', 
                    'OTDirecto20E',
                    'bolsonaro27',
                    'bolsonaro28', 
                    'VanduMuruganAJITH',
                    'nintendo',
                    'messicumple', 
                    'wrestlemania',
                    'kingjacksonday',
                    'kavanaugh06-08',
                    'bolsonaro30', 
                    'notredam',
                    'Thanksgiving',
                    'halsey', 
                    'feliznatal', 
                    'kavanaugh16',
                    'kavanaugh02-05',
                    'EXODEUX', 
                    'bigil', 
                    'lula_moro_chats',
                    'menciones-05-11abril',
                    'LeadersDebate',
                    'championsasia', 
                    'menciones05-11mayo',
                    'SeungWooBirthday',
                    'menciones-11-18marzo']

# #### Test for few topics ####
# topic_qc_zarate = ['pelosi', 'kavanaugh06-08', 'Thanksgiving', 'messicumple'] 

####Test in local####
# topic_qc_zarate = ['pelosi']


##########################################################################################################
############################################### FUNCTION #################################################

"""
compose the name for the file of the hashtags of the topic
"""
def compose_name_hashtag_of_topic(hashtags):
    name = hashtags.replace(' ', '')
    name = name.replace('#', '')
    name = name.upper()
                        
    return name

"""
get in the wanted format the from date and end_date
if no hours, minutes, seconds, every date will be set to midnight exactly (00:00:00)
return :
        - iso_from_date, ts_from_date, iso_end_date, ts_end_date
"""
def get_query_period(from_date, end_date):
    dtformat = '%Y-%m-%d' #'T%H:%M:%SZ'
    
    #handle end date
    if(end_date is None):
        dt_end_date = datetime.now(pytz.utc) - timedelta(seconds = 30)
    else:
        dt_end_date = datetime.strptime(end_date, dtformat)

    ts_end_date = datetime.timestamp(dt_end_date)
    iso_end_date = dt_end_date.isoformat('T').split('+')[0] + "Z"

    #handle from date    
    if(from_date is None):
        dt_from_date = dt_end_date - timedelta(days = 30)
    else:
        dt_from_date = datetime.strptime(from_date, dtformat)

    ts_from_date = datetime.timestamp(dt_from_date)
    iso_from_date = dt_from_date.isoformat('T').split('+')[0] + "Z"
        
    return iso_from_date, ts_from_date, iso_end_date, ts_end_date


"""
convert timestamp to iso format
"""
def from_timestamp_to_iso(timestamp):
     return datetime.fromtimestamp(timestamp).isoformat()+'Z'
         
"""
convert iso format to timestamp
"""
def from_iso_to_timestamp(iso):
    return datetime.timestamp(dateutil.parser.isoparse(iso))

"""
create a folder for xp running in parallel in jz
"""
def update_data_object_folder(code_xp):
    global object_folder
    
    xp_folder = CURRENT_DATA_FOLD + object_folder
    
    if(os.path.isdir(xp_folder) == False):
        os.mkdir(xp_folder)
        
    if(code_xp is not None):
        xp_folder = CURRENT_DATA_FOLD + object_folder + code_xp + '/'
        if(os.path.isdir(xp_folder) == False):
            os.mkdir(xp_folder)
    
        object_folder = object_folder + code_xp + '/'
    
    return 1  


"""
get all dates in between 2 date
"""
def all_dates(start_date, end_date, step=1):
    delta = timedelta(days=step)
    while start_date <= end_date:
        next_date = start_date + delta
        yield start_date, next_date
        start_date += delta
        
        

"""
plot the loss in train & validation set
input :
    - train_loss_list : list of train loss for each epoch
    - val_loss_list : list of val loss for each epoch
    - list_val_b_loss : list of balanced val loss for each epoch
output :
    - plot the pictures
"""
def plot_loss_bert(list_train_loss, list_val_loss, list_val_b_loss, root):
    print('--- plot loss bert model ---')
    
    fig1, ax1 = plt.subplots()

    #loss
    path_loss_plot = root + 'plot_bert_loss.png'
  
    ax1.plot(range(len(list_train_loss)), list_train_loss, label = 'Train loss')
    ax1.plot(range(len(list_val_loss)), list_val_loss, label = 'Validation loss')
    ax1.plot(range(len(list_val_b_loss)), list_val_b_loss, label = 'Balanced Validation loss')
    fig1.savefig(fname = path_loss_plot)
    
    #plt.show()
    plt.close(fig1)
    
    return 1


"""
ADDED
plot the accuracy in train in validation set
input :
    - list_train_acc : list of train acc for each epoch
    - list_val_acc : list of val acc for each epoch
    - list_val_b_acc : list of balanced val acc for each epoch
output :
    - plot the pictures
"""
def plot_acc_bert(list_train_acc, list_val_acc, list_val_b_acc, root):
    print('--- plot acc bert model ---')
    
    fig1, ax1 = plt.subplots()
    
    #accuracy
    path_acc_plot = root + 'plot_bert_acc.png'
    
    ax1.plot(range(len(list_train_acc)), list_train_acc, label = 'Train acc')
    ax1.plot(range(len(list_val_acc)), list_val_acc, label = 'Validation acc')
    ax1.plot(range(len(list_val_b_acc)), list_val_b_acc, label = 'Balanced Validation accuracy')
    
    fig1.savefig(fname = path_acc_plot)
    
    plt.close(fig1)
    
    #plt.show()
    
    return 1