#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 25 11:10:37 2021

@author: samy
"""

#LABEL_QC
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.preprocessing import MinMaxScaler
from sknetwork.clustering import Louvain

import bert_processing as bp
import util as ut

from sklearn.model_selection import train_test_split
from deep_translator import GoogleTranslator

from sklearn.cluster import KMeans, MeanShift
from sklearn.manifold import TSNE
from umap import UMAP


from matplotlib.pyplot import figure
import matplotlib.pyplot as plt
import plotly.express as px
# from fa2 import ForceAtlas2

from scipy import sparse
import networkx as nx
import pandas as pd
import numpy as np


import collections
import random
import shutil
import pickle5 as pickle # import pickle
import torch
#import metis
import time
import json
import html
import dgl
import csv
import os
import re

from datetime import datetime
import dateutil

from scipy.linalg import logm, expm
import scipy as sp
from scipy.special import logsumexp


TEST_LOCAL = False #TODO
id_graph_type = 3 #1= all nodes, 2=nodes with link, 3=nodes from largest connex component


#----
RT_MIN = True #nbs retweet minimum (min fixed  at 5) (LABEL_QC)
KEEP_ONLY_TWEETERS = False
REMOVE_USER_WITHOUT_TWEETS = False

bert_aggregator = 'max' #'mean'
###################################################################
####################### MAIN function #############################
###################################################################

"""
Return, for a specific request, for a specific type of graph, information of the user graph
"""
def create_user_info_graph(type_graph, request_id, period, to_connex, minimum_links):
    request = load_request(request_id)
    
    #1. get all tweets
    dict_data = get_tweets_by_request(request, type_graph, period) #create df tweets
    
    #2. get user graph information
    dict_tweets, dict_nodes, dict_edges, dict_edge_weights = get_nodes_and_edges(dict_data, type_graph, to_connex, minimum_links)
        
    print('\n      <<<< FINAL GRAPH PULLED : ', len(dict_nodes), ' Users -', len(dict_edges), ' Edges >>>> \n')     
    
    return dict_tweets, dict_nodes, dict_edges, dict_edge_weights


"""
Create user graph from data_QC topics (data retrieved in Garimella et al., 2018)
"""
def create_user_QC_graph(topic, type_graph, to_connex, minimum_links):
    dict_nodes, dict_edges = load_graph_QC(topic, type_graph)
    
    if(dict_nodes is None and dict_edges is None):
        return None, None, None

    #sort edges, source_id < dest_id        
    # dict_edges = sort_edges(dict_edges)

    #remove edges connected to itself + multi-RT    
    dict_edges, dict_edge_weights = remove_edges_auto_linked_and_duplicate(dict_edges, minimum_links)
      
    #remove nodes which are not connected
    if(id_graph_type == 1):
        print('  > ATTENTION !!, id_graph=1')
        dict_edges = add_self_connexion(dict_nodes, dict_edges)
    else:
        dict_nodes = remove_nodes_with_no_links(dict_nodes, dict_edges)
    
    #if we get only the largest component connex
    dict_nodes, dict_edges, dict_edge_weights = transform_graph_into_connex_graph(dict_nodes, dict_edges, dict_edge_weights, to_connex)
    
    print('\n      <<<< FINAL GRAPH PULLED : ', len(dict_nodes), ' Users -', len(dict_edges), ' Edges >>>> \n')     
    
    dict_edges = collections.OrderedDict(sorted(dict_edges.items())) #sort by timestamp

    return dict_nodes, dict_edges, dict_edge_weights

"""
Create user graph from textual nodes
"""
def create_user_QC_TEXT_graph(topic, type_graph, to_connex, minimum_links):
    path = ut.CURRENT_DATA_FOLD + ut.object_folder + '/' + topic + '/'
    if(os.path.isdir(path) == False):
        os.mkdir(path)
    path += type_graph + '/'
    if(os.path.isdir(path) == False):
        os.mkdir(path)
    path += 'dict_graph_temp.pickle'
    
    if(os.path.isfile(path) == True):
        print('<find dict graph temp>')
        dict_tweets, dict_nodes, dict_edges, dict_edge_weights = load_pickle(path)
        return dict_tweets, dict_nodes, dict_edges, dict_edge_weights
        
    dict_tweets, dict_nodes, dict_edges = load_graph_text_QC(topic, type_graph)
    
    if(dict_nodes is None and dict_edges is None):
        return None, None, None, None

    #sort edges, source_id < dest_id        
    print("no sort edges")
    # dict_edges = sort_edges(dict_edges)
    
    #remove edges connected to itself + multi-RT
    dict_edges, dict_edge_weights = remove_edges_auto_linked_and_duplicate(dict_edges, minimum_links)
      
    #remove nodes which are not connected
    dict_nodes = remove_nodes_with_no_links(dict_nodes, dict_edges)
    
    #remove tweeters who does not create at least one tweet
    if(KEEP_ONLY_TWEETERS == True):
        print('/!\ WE KEEP ONLY USERS WHO TWEETS AT LEAST ONCE + IN RT GRAPH /!\ ')
        dict_nodes, dict_edges, dict_edge_weights = remove_tweeters_with_no_tweets(dict_nodes, dict_edges, dict_edge_weights)

    #if we get only the largest component connex
    dict_nodes, dict_edges, dict_edge_weights = transform_graph_into_connex_graph(dict_nodes, dict_edges, dict_edge_weights, to_connex)

    ##sort by timestamp
    dict_edges = collections.OrderedDict(sorted(dict_edges.items())) 
    
    print('<save data>')
    pickle_save(path, (dict_tweets, dict_nodes, dict_edges, dict_edge_weights))

    print('\n      <<<< FINAL GRAPH PULLED : ', len(dict_nodes), ' Users -', len(dict_edges), ' Edges >>>> \n')        

    return dict_tweets, dict_nodes, dict_edges, dict_edge_weights


"""
create graph infos for topics coming from Zarate 
"""
def create_user_ZARATE_graph(path, topic, type_graph, to_connex, minimum_links):
    path += 'dict_graph_temp.pickle'
    
    if(os.path.isfile(path) == True):
        print('<find dict graph temp>')
        dict_tweets, dict_nodes, dict_edges, dict_edge_weights = load_pickle(path)
        return dict_tweets, dict_nodes, dict_edges, dict_edge_weights
    
    dict_tweets, dict_nodes, dict_edges = load_graph_Zarate(topic, type_graph)
      
    if(dict_nodes is None and dict_edges is None):
        return None, None, None, None

    #sort edges, source_id < dest_id        
    print("no sort edges")
    # dict_edges = sort_edges(dict_edges)
    
    #remove edges connected to itself + multi-RT
    dict_edges, dict_edge_weights = remove_edges_auto_linked_and_duplicate(dict_edges, minimum_links)
    
    #remove nodes which are not connected
    if(to_connex == False):
        print('!!! NO !!! NOT REMOVING NODES WITH NO LINKS')
    else:       
        #remove nodes which are not connected
        if(id_graph_type == 1):
            print('  > ATTENTION !!, id_graph=1')
            dict_edges = add_self_connexion(dict_nodes, dict_edges)
        else:
            print('REMOVING NODES WITH NO LINKS')
            dict_nodes = remove_nodes_with_no_links(dict_nodes, dict_edges)
 
            
    #remove tweeters who does not create at least one tweet
    if(KEEP_ONLY_TWEETERS == True):
        print('/!\ WE KEEP ONLY USERS WHO TWEETS AT LEAST ONCE + IN RT GRAPH /!\ ')
        dict_nodes, dict_edges, dict_edge_weights = remove_tweeters_with_no_tweets(dict_nodes, dict_edges, dict_edge_weights)

    #if we get only the largest component connex
    dict_nodes, dict_edges, dict_edge_weights = transform_graph_into_connex_graph(dict_nodes, dict_edges, dict_edge_weights, to_connex)
    ##sort by timestamp
    dict_edges = collections.OrderedDict(sorted(dict_edges.items())) 
    print('<save data>')
    pickle_save(path, (dict_tweets, dict_nodes, dict_edges, dict_edge_weights))

    print('\n      <<<< FINAL GRAPH PULLED : ', len(dict_nodes), ' Users -', len(dict_edges), ' Edges >>>> \n')        

    return dict_tweets, dict_nodes, dict_edges, dict_edge_weights

###################################################################
####################### Create object #############################
###################################################################

# """
# get position of nodes using a force atlas graph
# """
# def create_force_atlas_graph(topic, type_graph, type_text_model, dict_edges, dict_node_features, dict_node_label, dict_edge_weights):
#     #1. create network
#     G = create_graph_networkx(topic, type_graph, type_text_model, dict_edges, 
#                               dict_node_features, dict_node_label, with_feat_lab=False)
    
#     #2. run force atlas
#     path_file = ut.CURRENT_DATA_FOLD + ut.object_folder + '/' + topic + '/' + type_graph + '/'
#     filename = path_file + 'coordinates_force_atlas.pickle'
#     if(os.path.isfile(filename) == False):
#         forceatlas2 = ForceAtlas2(
#                         outboundAttractionDistribution=True,  # Dissuade hubs
#                         linLogMode=False,  # NOT IMPLEMENTED
#                         adjustSizes=False,  # Prevent overlap (NOT IMPLEMENTED)
#                         edgeWeightInfluence=1.0,
#                         jitterTolerance=1.0,  # Tolerance
#                         barnesHutOptimize=True,
#                         barnesHutTheta=1.2,
#                         multiThreaded=False,  # NOT IMPLEMENTED
#                         scalingRatio=2.0,
#                         strongGravityMode=False,
#                         gravity=1.0,
#                         verbose=True)
#         print('run force atlas 2...')
#         dict_force_atlas_coordinates = forceatlas2.forceatlas2_networkx_layout(G, pos=None, iterations=2000)
#         print('done')
        
#         pickle_save(filename, (dict_force_atlas_coordinates, dict_node_label, dict_edges, dict_edge_weights))
#     else:
#         print(' > coordinates forceAtlas already exists')
#         dict_force_atlas_coordinates,_,_,_ = load_pickle(filename)
    
#     return dict_force_atlas_coordinates

"""
Create networkx graph object
"""
def create_graph_networkx(topic, type_graph, type_text_model, dict_edges, 
                          dict_node_features, dict_node_label, with_feat_lab = True):
    type_object = 'networkx'
    
    path_file = ut.CURRENT_DATA_FOLD + ut.object_folder + '/' + topic + '/' + type_graph + '/'
    filename = type_object+'_'+str(type_text_model)+'.pickle'
    path_file += filename
    
    if(os.path.isfile(path_file) == True):
        print("-- *Graph G* networkx already created (", str(type_text_model), ") --")
        G = load_graph(topic, type_graph, type_text_model, type_object)
    else:      
        G = nx.Graph()
        if(with_feat_lab == True):
            list_nodes = [(user_id, {'feat': dict_node_features[user_id], 'label': dict_node_label[user_id]}) for user_id in dict_node_features.keys()]
        else:
            list_nodes = [user_id for user_id in dict_node_label.keys()]
            
        G.add_nodes_from(list_nodes)
        list_edges = [dict_edges[ts] for ts in dict_edges.keys()]
        G.add_edges_from(list_edges)
        
        save_data_in_pickle_file(topic, type_graph, filename, G)
        
    if(nx.is_connected(G) == True):
        print('  --- Yes! Graph connected: YES ---')
    else:
        print('  --- Graph connected: NO ---')
    return G

"""
create dgl Graph object for graph learning
"""
def create_dgl_graph(path_file, text_model, 
                     dict_edges, dict_edge_weights, topic_label,
                     device, is_edge_weighted, is_test_set, undirected = True):
    type_object = 'dgl'
    
    filename = type_object+'_'+str(text_model)+'.pickle'
    path_file += filename
    
    if(os.path.isfile(path_file) == True):
        print("-- *Graph G* dgl already created (", str(text_model), ") --")
        G = load_pickle(path_file)
    else:  
        ########## 1. Set edges
        src_ids = []
        dst_ids = []
        for k in dict_edges.keys():
            s,d = dict_edges[k]
            
            #1st sense
            src_ids.append(s)
            dst_ids.append(d)
            
        G = dgl.graph((src_ids, dst_ids))
        if(undirected == True):
            print(' >> set undirected')
            G = dgl.to_bidirected(G) #undirected graph
        G = dgl.add_self_loop(G)
        G = G.to(device)
        print('-- min_node :', min(G.nodes()).item(), ' / max_node:', max(G.nodes()).item(), ' / nb_edges:', len(G.edges()[0]))

        ########## 2. Set node features
        # nodes_with_no_features = []
        # n_nodes = []
        # f_nodes = []
        # for k, v in dict_node_features.items():
        #     n_nodes.append(k)
        #     f_nodes.append(v)
            
        #     if(has_no_features(v) == True):
        #         nodes_with_no_features.append(k)
        
        # feat = torch.tensor(f_nodes, dtype=torch.double, device=device)
        # G.nodes[n_nodes].data['feat'] = feat
        
        ########## 3. Set edge weights
        if(is_edge_weighted == True):
            print(" >> EDGE WEIGHT APPLIED HERE !")
            list_idx_edges, list_weight = get_list_edge_weight(G, dict_edge_weights)
            G.edges[list_idx_edges].data['weight'] = torch.tensor(list_weight, dtype=torch.double, device=device)
        else:
            print(" >> NO edge weight..")
        
        ########## 4. Set node labels
        n_nodes = [n.item() for n in G.nodes()]
        labels = [topic_label] * len(n_nodes)
        G.nodes[n_nodes].data['label'] = torch.tensor(labels, dtype=torch.long, device=device)

        pickle_save(path_file, G)

    return G

def has_no_features(features):
    if(len(features) == 1):
        return False
    
    for v in features:
        if(v!=1):
            return False
    
    return True

def get_list_edge_weight(G, dict_edge_weights, type_score='ratio'):
    print(' > type score for weight edges: ', type_score)
    list_idx_edges = []
    list_weight = []
    max_rt = max([v for _,v in dict_edge_weights.items()])
    
    for src, dst in zip(G.edges()[0], G.edges()[1]):
        edge_id = G.edge_ids(src, dst)
        
        if((src.item(),dst.item()) in dict_edge_weights.keys()):
            weight = dict_edge_weights[(src.item(),dst.item())]
        elif((dst.item(),src.item()) in dict_edge_weights.keys()):
            weight = dict_edge_weights[(dst.item(),src.item())]
        else:
            if(src.item() != dst.item()):
                print('WHAAAAAAAT edge does not exist : ', src, dst)
                weight = None
        
        if(type_score=='ratio'):
            if(src.item() == dst.item()):
                weight = 1
            else:
                weight = weight/(weight+1)
        else:
            if(src.item() == dst.item()):
                weight = max_rt+1
            
        # print(edge_id.item(), (src.item(),dst.item()), '=', weight)
        
        list_idx_edges.append(edge_id.item())
        list_weight.append(weight)
    
    return list_idx_edges, list_weight


"""
create dgl Graph object for graph learning
"""
def create_dgl_graph_nc(topic, type_graph, type_text_model, dict_edges, dict_node_label, train_mask, val_mask, test_mask, device):
    type_object = 'dgl'
    
    path_file = ut.CURRENT_DATA_FOLD + ut.object_folder + '/' + topic + '/' + type_graph + '/'
    filename = type_object+'_'+str(type_text_model)+'.pickle'
    path_file += filename
    
    if(os.path.isfile(path_file) == True):
        print("-- *Graph G* dgl already created (", str(type_text_model), ") --")
        G = load_graph(topic, type_graph, type_text_model, type_object)
    else:
        src_ids = []
        dst_ids = []
        for k in dict_edges.keys():
            s,d = dict_edges[k]

            #1st sense
            src_ids.append(s)
            dst_ids.append(d)
        
            # #2nd sense
            # src_ids.append(d)
            # dst_ids.append(s)
            
        G = dgl.graph((src_ids, dst_ids))
        G = dgl.to_bidirected(G) #undirected graph
        # G = dgl.add_self_loop(G)
        G = G.to(device)
        print('-- min_node :', min(G.nodes()).item(), ' / max_node:', max(G.nodes()).item(), ' / nb_edges:', len(G.edges()[0]))

        #features
        # n_nodes = []
        # f_nodes = []
        l_nodes = []

        # for k, v in dict_node_features.items():
        #     n_nodes.append(k)
        #     f_nodes.append(v) 
        #     l_nodes.append(dict_node_label[k])

        # feat = torch.tensor(f_nodes, dtype=torch.double, device=device)
        lab = torch.tensor(l_nodes, dtype=torch.long, device=device)
        train = torch.tensor(train_mask, device=device)
        val = torch.tensor(val_mask, device=device)
        test = torch.tensor(test_mask, device=device)
        
        # G.nodes[n_nodes].data['feat'] = feat
        G.ndata['label'] = lab
        G.ndata['train_mask'] = train
        G.ndata['val_mask'] = val
        G.ndata['test_mask'] = test
        
        save_data_in_pickle_file(topic, type_graph, filename, G)
        
    return G

"""
split graph into 2 partiions
"""
def create_communities(dict_edges, method='metis'):
    # if(ut.TYPE_SERVOR == 'local'): #bug in local
    #     list_n = []
    #     for k,v in dict_edges.items():
    #         list_n.append(v[0])
    #         list_n.append(v[1])
    #     list_n = list(set(list_n))
    #     dict_node_label = {}
    #     i=0
    #     for k in list_n:
    #         dict_node_label[k] = i%2 
    #         i+=1
    #     return dict_node_label
    

    list_edges = [dict_edges[ts] for ts in dict_edges.keys()]
    nodes, parts = graph_partitionning(list_edges, method=method, p=2)
    
    dict_node_label = {}
    for node, com in zip(nodes,parts):
       dict_node_label[node] = com
        
    return dict_node_label



"""
give an id starting from 0 for each node, and update the dict_node for tweets, and dict_edges w/ the corresponding ids
"""
def create_new_node_id(dict_nodes, dict_edges, dict_edge_weights):
    dict_user_to_id = {}
    dict_id_to_user = {}
    
    dict_new_nodes = {}
    dict_new_edges = {}
    dict_new_edge_weights = {}

    cpt=0
    for node in dict_nodes.keys():
        dict_user_to_id[node] = cpt
        dict_id_to_user[cpt] = node
        
        dict_new_nodes[cpt] = dict_nodes[node]
        cpt+=1
        
    for ts in dict_edges.keys():
        edge = dict_edges[ts]
        dict_new_edges[ts] = (dict_user_to_id[edge[0]], dict_user_to_id[edge[1]])
        
    for k,v in dict_edge_weights.items():
        new_key = (dict_user_to_id[k[0]], dict_user_to_id[k[1]])
        dict_new_edge_weights[new_key] = v
                                           
    return dict_user_to_id, dict_id_to_user, dict_new_nodes, dict_new_edges, dict_new_edge_weights



###################################################################
###################### Get function ###############################
###################################################################     

"""
load tweets information from our database
"""
def get_tweets_by_request(request, type_graph, period):
    start = None
    if(period[0] is not None):
        t = period[0]+'T00:00:00Z'
        start = from_iso_to_timestamp(t)
    end = None
    if(period[1] is not None):
        t = period[1]+'T00:00:00Z'
        end = from_iso_to_timestamp(t)

    dict_tweets = {}
    twt_deleted = 0
    for tweet_id in request['tweets']:
        tweet = load_tweet(tweet_id)
        
        if(tweet['creation'] is None):
            twt_deleted+=1
            continue
            
        # if(round((len(dict_tweets)/len(request['tweets']))*100)%10 == 0):
        #    print('lennn : ', round(((len(dict_tweets)/len(request['tweets'])))*100, 2))
        if(in_period(tweet, start, end) == True):
            if(is_original(tweet) == True): #original
                dict_tweets[tweet['id']] = tweet
            else:
                for type_ in ut.tweet_types[type_graph]:
                    if(type_ in tweet.keys()):                   
                        father = load_tweet(tweet[type_], is_a_father = True)
                        
                        if(father is None):
                            tweet.pop(type_, None)
                        else:
                            dict_tweets[father['id']] = father
                            
                        dict_tweets[tweet['id']] = tweet
            
                        if(TEST_LOCAL == True):
                            if(len(dict_tweets) > 300):
                                print('stooooooooop before : test')
                                return dict_tweets
    
    print('> Total problem w/ tweet_deleted:', twt_deleted)
    return dict_tweets

"""
return 2 dict, one with nodes and the other with edges.
#remove nodes with no links
output :
    - dict_nodes : {user_1: {id_tweet1: 'text',
                             id_tweet2: 'text'}, ...}
    - dict_edges : {timestamp1: (user1, user_2),
                    ...}
"""
def get_nodes_and_edges(dict_data, type_graph, to_connex, minimum_links):
    dict_tweets = {}
    dict_nodes = {}
    dict_edges = {}
        
    for tweet_id in dict_data.keys():
        tweet = dict_data[tweet_id]
        
        #if tweet original, we do not treat it (will be seen when the rt)
        if(is_original(tweet) == False):
            
            #1. add tweet
            if(tweet['user_id'] not in dict_nodes.keys()):
                dict_nodes[tweet['user_id']] = []
            
            for type_ in ut.tweet_types[type_graph]:
                if(type_ in tweet.keys()):
                    father = dict_data[tweet[type_]]
                    
                    #0. add tweet
                    dict_tweets[father['id']] = father['text']
                    
                    #1. add Father of the tweet
                    if(father['user_id'] not in dict_nodes.keys()):
                        dict_nodes[father['user_id']] = [father['id']]
                    else:
                        if(father['id'] not in dict_nodes[father['user_id']]):
                            dict_nodes[father['user_id']].append(father['id'])
                        
                    dict_edges[tweet['creation']] = (tweet['user_id'], father['user_id'])
        else:
            #0. add tweet
            dict_tweets[tweet_id] = tweet['text']
            
            #1. add user
            if(tweet['user_id'] not in dict_nodes.keys()):
                dict_nodes[tweet['user_id']] = [tweet_id]
            else:
                if(tweet_id not in dict_nodes[tweet['user_id']]):
                    dict_nodes[tweet['user_id']].append(tweet_id)
            
    cpt = 0
    for k,v in dict_nodes.items():
        if(len(v) == 0):
            dict_nodes[k] = None
            cpt+=1
    print('>> number of user who only RT : ', cpt)

    #sort edges, source_id < dest_id   
    # dict_edges = sort_edges(dict_edges)

    #remove edges connected to itself + multi-RT
    print('>> nbs edges before start removing: ', len(dict_edges))
    dict_edges, dict_edge_weights = remove_edges_auto_linked_and_duplicate(dict_edges, minimum_links)
            
    #remove nodes which are not connected
    dict_nodes = remove_nodes_with_no_links(dict_nodes, dict_edges)

    #remove tweeters who does not create at least one tweet
    if(KEEP_ONLY_TWEETERS == True):
        print('/!\ WE KEEP ONLY USERS WHO TWEETS AT LEAST ONCE + IN RT GRAPH /!\ ')
        dict_nodes, dict_edges, dict_edge_weights = remove_tweeters_with_no_tweets(dict_nodes, dict_edges, dict_edge_weights)
        
    #if we get only the largest component connex
    dict_nodes, dict_edges, dict_edge_weights = transform_graph_into_connex_graph(dict_nodes, dict_edges, dict_edge_weights, to_connex)
    
    dict_edges = collections.OrderedDict(sorted(dict_edges.items())) #sort by timestamp
    
    return dict_tweets, dict_nodes, dict_edges, dict_edge_weights

"""
sort edges by node order
"""
def sort_edges(dict_edges):
    new_dict_edges = {}
    for k in dict_edges.keys():
        a, b = dict_edges[k]
        
        if(a<b):
            new_dict_edges[k] = (a,b)
        else:
            new_dict_edges[k] = (b,a)
    
    return new_dict_edges
        

"""
remove tweeters who don't create at least one tweet
"""
def remove_tweeters_with_no_tweets(dict_nodes, dict_edges, dict_edge_weights):
    new_dict_nodes = {}
    new_dict_edges = {}
    nodes = []
    for ts, edge in dict_edges.items():
        if(dict_nodes[edge[0]] is not None and dict_nodes[edge[1]] is not None):
            new_dict_edges[ts] = edge
            nodes.append(edge[0])
            nodes.append(edge[1])
        else:
            del dict_edge_weights[edge]
    
    nodes = list(set(nodes))    
    for n in nodes:
        new_dict_nodes[n] = dict_nodes[n]
    
    print('>> Number of user removed : ', len(dict_nodes)-len(nodes),' | Number of user kept : ',len(nodes), 'kept')
    return new_dict_nodes, new_dict_edges, dict_edge_weights


"""
remove edges where a node is linked to itself
"""
def remove_edges_auto_linked_and_duplicate(dict_edges, minimum_links=1):
    dict_new_edges = {}
    temp = []
    dict_edge_weights = {}
    edge_removed = 0
    
    if(minimum_links == 1):
        print(' > normal count by edge (1)')
        for k in dict_edges.keys():
            edge = dict_edges[k]
            if(edge[0] != edge[1]):
                if(edge not in temp):
                    dict_new_edges[k] = edge
                    temp.append(edge)
                    
                    dict_edge_weights[edge] = 1
                else:
                    dict_edge_weights[edge] += 1
                    edge_removed += 1
    else:
        print(' > count by edge different (', minimum_links, ')')
        dict_edge_count = {}
        for k in dict_edges.keys():
            edge = dict_edges[k]
            sorted_edge = tuple(sorted(edge))
            
            if(edge[0] != edge[1]):
                if(sorted_edge in dict_edge_count.keys() and dict_edge_count[sorted_edge] >= minimum_links-1): #more than 1 times               
                    if(edge not in temp):
                        dict_new_edges[k] = edge
                        temp.append(edge)
                        dict_edge_weights[edge] = minimum_links
                    else:
                        dict_edge_weights[edge] += 1
                        edge_removed += 1
                    dict_edge_count[sorted_edge] += 1
                else:
                    if(edge in dict_edge_count.keys()):
                        dict_edge_count[sorted_edge] += 1
                    else:
                        dict_edge_count[sorted_edge] = 1
        print('>> number of edge deleted because < min_links : ', len([k for k,v in dict_edge_count.items() if v < minimum_links]))
    print('>> number of duplicate edges : ', edge_removed)

    return dict_new_edges, dict_edge_weights
"""
return dict of nodes but only nodes present on the graph (at least one edge)
"""
def remove_nodes_with_no_links(dict_nodes, dict_edges):
    real_nodes = []
    
    for ts in dict_edges.keys():
        edge = dict_edges[ts]
        
        if(edge[0] not in real_nodes):
            real_nodes.append(edge[0])
        if(edge[1] not in real_nodes):
            real_nodes.append(edge[1])
            
    old_nodes = dict_nodes.keys()
    
    nodes_with_no_edge = list(set(old_nodes) - set(real_nodes))
    
    for node in nodes_with_no_edge:
        dict_nodes.pop(node, None)

    print('>> number of nodes with no links : ', len(nodes_with_no_edge))
    return dict_nodes

"""
add self loop for every node, to put it on graph
"""
def add_self_connexion(dict_nodes, dict_edges):
    print(' -- add self connexion !!')
    max_key = max(k for k in dict_edges.keys()) + 1
    print(' > last key:', max_key)
    
    for k in dict_nodes.keys():
        dict_edges[max_key] = (k,k)
        
    return dict_edges


"""
if no features, get node degrees as features
"""
def get_user_node_features(filename_temp, text_model, dict_nodes, dict_new_edges, dict_tweets, 
                           text_aggregator, device, batch_bert):
    dict_node_features = {}

    #default features: node degree
    if(text_model == False):       
        print('  > features = node degree')
        for k,v in dict_new_edges.items():
            s,d = v
            if(s not in dict_node_features.keys()):
                dict_node_features[s] = [1]
            else:
                dict_node_features[s][0]+=1
                
            if(d not in dict_node_features.keys()):
                dict_node_features[d] = [1]
            else:
                dict_node_features[d][0]+=1
                
        return dict_node_features, None, None
                
    elif(text_model == ut.HASHTAGS):
        print('  > features = hashstags')       
        #1. get # by user + all hashtag
        dict_hashtag_by_user, all_hashtag, _, _ = get_hashtag_by_user(dict_nodes, dict_tweets, list(dict_nodes.keys()))
        
        #2. get one hot encoding vector
        dict_node_features, all_embeddings = get_one_hot_encoding_vector(dict_hashtag_by_user, all_hashtag)
        
    else:
        path_file = ut.CURRENT_DATA_FOLD + ut.DATA_QC + ut.text_qc_object + filename_temp
        
        #1. Get text embeddings
        if(os.path.isfile(path_file) == True):
            print('  > load text embeddings..', path_file)
            new_dict_tweets = load_pickle(path_file)
        else:
            default_no_text = ''
            default_text_embeddings, defaul_text_labels, defaul_text_labels_logits = bp.get_text_features([default_no_text], text_model, device, bert_aggregator, 
                                                                                    batch_bert)
            
            inputs = [v for k,v in dict_tweets.items()]
            inputs = clean_text_v2(inputs)            
            t = time.time()
            text_embeddings, text_labels, text_labels_logits = bp.get_text_features(inputs, text_model, device, bert_aggregator, 
                                                                                    batch_bert)
            print('-- bert runtime (get_class + get_emb) : ', round(((time.time() - t)/60)/60, 2), 'hours --') 
            
            cpt=0
            new_dict_tweets = {}
            new_dict_tweets['DEFAULT_TWEET'] = [default_no_text, default_text_embeddings[0], (defaul_text_labels[0], defaul_text_labels_logits[0])]
            for k,v in dict_tweets.items():
                new_dict_tweets[k] = [v, text_embeddings[cpt], (text_labels[cpt], text_labels_logits[cpt])]
                cpt+=1
                
            pickle_save(path_file, new_dict_tweets)
        print('Total embedding Tweets: ', len(new_dict_tweets))
        
        #2. Get user features
        print('-- Aggregator : ', text_aggregator, '--')
        default_features = new_dict_tweets['DEFAULT_TWEET'][1] #np.array([1.0] * len(new_dict_tweets[next(iter(new_dict_tweets))][1]))
        
        dict_node_features_aggregated = {}
        dict_node_features_stacked = {}
        for user_id, tweet_ids in dict_nodes.items():
            if tweet_ids is None:
                dict_node_features_aggregated[user_id] = default_features
                dict_node_features_stacked[user_id] = None
            else:
                tweet_emb = np.stack([new_dict_tweets[id_][1] for id_ in tweet_ids])
                dict_node_features_stacked[user_id] = tweet_emb
                if(text_aggregator == ut.MEAN_AGGR):
                    dict_node_features_aggregated[user_id] = np.mean(tweet_emb, axis=0)
                elif(text_aggregator == ut.MAX_AGGR):
                    dict_node_features_aggregated[user_id] = np.max(tweet_emb, axis=0)
    
    return dict_node_features_aggregated, dict_node_features_stacked, new_dict_tweets

"""
get features for our gnn model:
    - if text model: list of tweets processed ([] if no tweets)
    - ig False: degree
"""
def get_user_node_features_v2(dict_new_nodes, dict_new_edges, dict_tweets, type_model, connex = True, batch_bert=64):
    
    #default features: node degree
    print('  > features = node degree')
    dict_node_degree = {}
    for k,v in dict_new_edges.items():
        s,d = v
        if(s not in dict_node_degree.keys()):
            dict_node_degree[s] = [1]
        else:
            dict_node_degree[s][0]+=1
            
        if(d not in dict_node_degree.keys()):
            dict_node_degree[d] = [1]
        else:
            dict_node_degree[d][0]+=1
    if(connex == False):
        for k in dict_new_nodes.keys():
            if(k not in dict_node_degree.keys()):
                dict_node_degree[k] = 0
    
    #text features
    print('  > features = text inputs')
    default_no_text = ''
    default_no_text_token = bp.get_tokens_by_text([default_no_text], type_model, batch_bert)[0]

    clean_texts = clean_text_v2([v for k,v in dict_tweets.items()])
    clean_tokens = bp.get_tokens_by_text(clean_texts, type_model, batch_bert)
    
    cpt=0
    clean_dict_tweets = {}
    clean_dict_tokens = {}
    for k,_ in dict_tweets.items():
        clean_dict_tweets[k] = clean_texts[cpt]
        clean_dict_tokens[k] = clean_tokens[cpt]
        cpt+=1
        
    #b. split by users
    c=0
    dict_node_text = {}
    dict_node_tokens = {}
    for k,v in dict_new_nodes.items():
        if(v is None):   
            dict_node_text[k] = [default_no_text]
            dict_node_tokens[k] = [default_no_text_token]
        else:
            dict_node_text[k] = [clean_dict_tweets[i] for i in v]
            dict_node_tokens[k] = [clean_dict_tokens[i] for i in v]
            c+=1
    
    dict_node_tokens = reshape_feature(dict_node_tokens)
        
    return dict_node_text, dict_node_tokens, dict_node_degree


"""
reshape features
"""
def reshape_feature(dict_node_tokens):
    
    reshape_dict = {}
    for k,v in dict_node_tokens.items():
        input_ids = []
        token_type_ids = []
        attention_mask = []
        for tw in v:
            input_ids.append(tw['input_ids'])
            token_type_ids.append(tw['token_type_ids'])
            attention_mask.append(tw['attention_mask'])
        

        reshape_dict[k] = {'input_ids': torch.stack(input_ids, dim=0),
                'token_type_ids': torch.stack(token_type_ids, dim=0),
                'attention_mask': torch.stack(attention_mask, dim=0)}
                
    return reshape_dict

###################################################################
######################### Save data ###############################
###################################################################
"""
save in pickle file graph object
"""
def save_data_in_pickle_file(topic, type_graph, filename, data):
    path_file = ut.CURRENT_DATA_FOLD + ut.object_folder + '/'
    if(os.path.isdir(path_file) == False):
        os.mkdir(path_file)
        
    path_file += topic + '/'
    if(os.path.isdir(path_file) == False):
        os.mkdir(path_file)
        
    path_file+= type_graph + '/'
    if(os.path.isdir(path_file) == False):
        os.mkdir(path_file)
        
    path_file = path_file + filename
    
    pickle_save(path_file, data)

    return 1

"""
create the xp folder
"""
def create_xp_folder(topic, type_graph):
    path_file = ut.CURRENT_DATA_FOLD + ut.object_folder + '/'
    if(os.path.isdir(path_file) == False):
        os.mkdir(path_file)
        
    path_file += topic + '/'
    if(os.path.isdir(path_file) == False):
        os.mkdir(path_file)
        
    path_file+= type_graph + '/'
    if(os.path.isdir(path_file) == False):
        os.mkdir(path_file)
            
    return path_file
    
    
"""
save nodes and edges on csv files
"""
def save_giphi_files_comu(path_file, set_labels, dict_edges, type_label):
    
    #1. write node information     
    node_filename = path_file + 'nodes_'+type_label+'.csv'
    if(os.path.isfile(node_filename) == True):
        print("-- *Giphi node file* already created --")
    else:
        if(type_label == 'metis_louvain'):
            dict_node_label_metis, dict_node_label_louvain = set_labels
            with open(node_filename, mode='w') as nf:
                writer_node = csv.writer(nf, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                writer_node.writerow(['Id','Label', 'metis_label', 'louvain_label'])
            
                if(dict_node_label_metis is not None and dict_node_label_louvain is not None):
                    for user in dict_node_label_metis.keys():
                        writer_node.writerow([str(user), '', str(dict_node_label_metis[user]), str(dict_node_label_louvain[user])])           
        else:
            print(' > giphi all labels')
            dict_node_label_metis, dict_node_label_louvain, dict_node_label_predicted, dict_node_label_cluster = set_labels
            #1. write node information
            with open(node_filename, mode='w') as nf:
                writer_node = csv.writer(nf, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                writer_node.writerow(['Id','Label', 'metis_label', 'louvain_label', 'predicted_label', 'cluster_label'])
            
                if(dict_node_label_metis is not None and dict_node_label_louvain is not None):
                    for user in dict_node_label_metis.keys():
                        writer_node.writerow([str(user), '', str(dict_node_label_metis[user]), str(dict_node_label_louvain[user]),
                                              str(dict_node_label_predicted[user]), str(dict_node_label_cluster[user])])
                        
    #2. write edge information    
    edge_filename = path_file + 'edges.csv'
    if(os.path.isfile(edge_filename) == True):
        print("-- *Giphi edge file* already created --")
    else:
        with open(edge_filename, mode='w') as ef:
            writer_edge = csv.writer(ef, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            writer_edge.writerow(['Source','Target','Type', 'Id', 'Label']) 
            
            for ts in dict_edges.keys():
                edge = dict_edges[ts]
                writer_edge.writerow([str(edge[0]),str(edge[1]),'Undirected','',''])

    return 1

"""
save nodes and edges on csv files
"""
"""
save nodes and edges on csv files
"""
def save_giphi_files(path_file, topic, dict_node_label, dict_edges, to_add=''):
    #1. write node information
    node_filename = path_file + topic+'_nodes_'+to_add+'.csv'
    
    if(os.path.isfile(node_filename) == True):
        print("-- *Giphi files* already created --")
    else:
        with open(node_filename, mode='w') as nf:
            writer_node = csv.writer(nf, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            writer_node.writerow(['Id','Label','modularity_class'])
        
            if(dict_node_label is not None):
                for user in dict_node_label.keys():
                    writer_node.writerow([str(user), '', str(dict_node_label[user])])
                    
        
        edge_filename = path_file + topic+'_edges_'+to_add+'.csv'
        with open(edge_filename, mode='w') as ef:
            writer_edge = csv.writer(ef, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            writer_edge.writerow(['Source','Target','Type', 'Id', 'Label']) 
            
            for ts in dict_edges.keys():
                edge = dict_edges[ts]
                writer_edge.writerow([str(edge[0]),str(edge[1]),'Undirected','',''])
            
    return 1


"""
save data files for GNN application
"""
def save_gnn_files(request_id, type_graph, dict_node_features, dict_node_label, dict_edges, 
                   train_set, val_set, test_set, unknown_set, type_gnn = ut.GAT):
    print(" You chose to run with /!\ ", type_gnn, " /!\ You chose... Wisely.")
        
    if(type_gnn == ut.GAT):
        save_gat_files(request_id, type_graph, dict_node_features, dict_node_label, dict_edges,
                       train_set, val_set, test_set, unknown_set)
    
    return 1


"""
save data in our database, and copy-paste in GAT folder
    - adjacency_list.dict : {node_1: [node_related1, node_related2],
                             node_2: ...} -> size = number_of_nodes 
    - node_labels.npy : [label_node_1, label_node_2, ...] -> size = number_of_nodes 
    - node_features.csr : -> shape = (number_of_nodes, number_of_features)
"""
def save_gat_files(request_id, type_graph, dict_node_features, dict_node_label, dict_edges,
                   train_set, val_set, test_set, unknown_set):
    data_directory = ut.CURRENT_DATA_FOLD + ut.object_folder + '/' + request_id + '/' + type_graph + '/' + ut.GAT + '/'
    split_nodes_file = data_directory + 'node_idx_split.pickle'
    adj_file = data_directory + 'adjacency_list.dict'
    nl_file = data_directory + 'node_labels.npy'
    nf_file = data_directory + 'node_features.csr'

    if(os.path.isdir(data_directory) == True):
         print("-- *Gat files* already created --")
    else:
        os.mkdir(data_directory)
        
        ###O. save train_test_nodes
        pickle_save(split_nodes_file, (train_set, val_set, test_set, unknown_set))
        
        ###1. Adjacency_matrix
        print("-- adjacency matrix")
        dict_adj = {}
        for ts in dict_edges.keys():
            edge = dict_edges[ts]
            #add link for 1st node
            if(edge[0] not in dict_adj.keys()):
                dict_adj[edge[0]] = [edge[1]]
            else:
                if(edge[1] not in dict_adj[edge[0]]):
                    dict_adj[edge[0]].append(edge[1])
                    
            #add link for 2nd node
            if(edge[1] not in dict_adj.keys()):
                dict_adj[edge[1]] = [edge[0]]
            else:
                if(edge[0] not in dict_adj[edge[1]]):
                    dict_adj[edge[1]].append(edge[0])
                    
                    
        pickle_save(adj_file, dict_adj)
                
        ###2. Node label
        print("-- node label")
        labels = [dict_node_label[n] for n in dict_node_label.keys()]
        labels = np.array(labels)
        pickle_save(nl_file, labels)
        
        ###3. Node features
        print("-- node features")        
        feature_matrix = []
        for node in dict_node_label.keys():
            feature_matrix.append(dict_node_features[node])
        feature_matrix = np.stack(feature_matrix)
        feature_matrix = sparse.csr_matrix(feature_matrix)
        pickle_save(nf_file, feature_matrix)
    
    
    #copy-paste files
    print("-- copy-paste files")
    gnn_data_directory = ut.path_gnn[ut.GAT]['data_directory'] + ut.request_to_topic[request_id] + '/'
    if(os.path.isdir(gnn_data_directory)):
        shutil.rmtree(gnn_data_directory)
    os.mkdir(gnn_data_directory)
    
    copy_paste(split_nodes_file, gnn_data_directory + 'node_idx_split.pickle')
    copy_paste(adj_file, gnn_data_directory + 'adjacency_list.dict')
    copy_paste(nl_file, gnn_data_directory + 'node_labels.npy')
    copy_paste(nf_file, gnn_data_directory + 'node_features.csr')
    
    print("-- Finish saving files !")
    

"""
save as pickle file
"""
def pickle_save(path, data):
    with open(path, 'wb') as file:
        pickle.dump(data, file, protocol=pickle.HIGHEST_PROTOCOL)
        
    return 1

"""
write in json file
"""
def write_json(filename, res):
    with open(filename, 'w') as f:
        json.dump(res, f)
    return 1
        
    
###################################################################
######################## Load data ################################
###################################################################        
    
def load_request(request_id):
    request_path = ut.CURRENT_DATA_FOLD + ut.MY_DATA + ut.request_folder + request_id +'.json'
    
    if(os.path.isfile(request_path) == False):
        print(" /!\ Request not found !!! /!\ ")
        return None

    with open(request_path) as f:
        request = json.load(f)
        
    return request


def load_tweet(tweet_id, is_a_father = False):
    tweet_path = ut.CURRENT_DATA_FOLD + ut.MY_DATA + ut.tweet_folder + '/' + tweet_id +'.json'
    
    if(os.path.isfile(tweet_path) == False):
        if(is_a_father == False):
            print(" /!\ Tweet not found !!! /!\ ")
        return None
    
    with open(tweet_path) as f:
        tweet = json.load(f)
        
    return tweet


def load_topic_QC(type_graph):
    if(type_graph == ut.RETWEET):
        path = ut.CURRENT_DATA_FOLD + ut.DATA_QC + ut.rt_networks
    elif(type_graph == ut.FOLLOW):
        path = ut.CURRENT_DATA_FOLD + ut.DATA_QC + ut.follow_networks
        
    files = os.listdir(path)
    
    topics = [f.split('.')[0].split('_')[2] for f in files]
    
    return topics

def load_graph_QC(topic, type_graph):
    if(type_graph == ut.RETWEET):
        path = ut.CURRENT_DATA_FOLD + ut.DATA_QC + ut.rt_networks
    elif(type_graph == ut.FOLLOW):
        path = ut.CURRENT_DATA_FOLD + ut.DATA_QC + ut.follow_networks
    
    filename = [f for f in os.listdir(path) if topic in f]
    if(len(filename) == 0):
        print(' /!\ no', type_graph, 'file find for topic', topic, '/!\ ')
        return None, None
    
    filename = filename[0]
    path += filename
    
    list_nodes = set()
    dict_edges = {}
    cpt=0
    with open(path) as f:
        lines = f.readlines()
        for edge in lines:
            s,d = edge.split(',')[:2]
            
            list_nodes.add(s)
            list_nodes.add(d)
            dict_edges[cpt] = (s,d)

            cpt+=1
            
            if(TEST_LOCAL == True):
                if(len(dict_edges) > 300):
                    print('stooooooooop before : test')
                    break
            
    dict_nodes = {k:None for k in list(list_nodes)}
    
    return dict_nodes, dict_edges

"""
load textual data with graphs
"""
def load_graph_text_QC(topic, type_graph):
    text_path = ut.CURRENT_DATA_FOLD + ut.DATA_QC + ut.text_qc
    filename = [f for f in os.listdir(text_path) if topic in f and '.txt' in f]

    if(len(filename) == 0):
        print(' /!\ no', type_graph, 'file find for topic', topic, '/!\ ')
        return None, None, None
    
    filename = filename[0]
    text_path += filename
    
    df_edges, dict_tweets, tweets_by_user = read_text_tweets_qc(text_path)
    
    list_nodes = set()
    dict_edges = {}    
    for row in df_edges.itertuples():
        dict_edges[row[1]] = (row[2], row[3])
        list_nodes.add(row[2])
        list_nodes.add(row[3])

    #create dict_nodes with corresponding tweet ids  
    dict_nodes = {}
    for node in list(list_nodes):
        if(node in tweets_by_user.keys()):
            dict_nodes[node] = list(set((tweets_by_user[node])))
        else:
            dict_nodes[node] = None
        
    cpt=0
    for node in tweets_by_user.keys():
        if(node not in list(list_nodes)):
            cpt+=1
    print('>> Number of user: ', len(dict_nodes), '(', len(tweets_by_user), 'only retweet )')
    print('   ( Number of user not used [have tweets but not in the Graph] :', cpt, ')')

    return dict_tweets, dict_nodes, dict_edges


"""
load textual data with graph from Zarate
"""
def load_graph_Zarate(topic, type_graph):
    text_path = ut.CURRENT_DATA_FOLD + ut.DATA_ZARATE
    filename = [f for f in os.listdir(text_path) if topic in f and '.csv' in f]

    if(len(filename) == 0):
        print(' /!\ no', type_graph, 'file find for topic', topic, '/!\ ')
        return None, None, None
    
    filename = filename[0]
    text_path += filename
    print(text_path)
    df_edges, dict_tweets, tweets_by_user = read_text_zarate(text_path)
    list_nodes = set()
    dict_edges = {}
    cpt=0
    for row in df_edges.itertuples():
        dict_edges[cpt] = (row[2], row[3])
        list_nodes.add(row[2])
        list_nodes.add(row[3])
        cpt+=1
    
    #create dict_nodes with corresponding tweet ids  
    only_rt = 0
    dict_nodes = {}
    for node in list(list_nodes):
        if(node in tweets_by_user.keys()):
            dict_nodes[node] = list(set((tweets_by_user[node])))
        else:
            dict_nodes[node] = None
            only_rt+=1
    
    print('      **** test len before: ', len(dict_nodes))
    not_present_in_graph = 0
    if(id_graph_type == 1):
        print('> keep all users (even with no links)')
        for u in tweets_by_user.keys():
            if(u not in dict_nodes.keys()):
                dict_nodes[u] = list(set((tweets_by_user[u])))
                not_present_in_graph+=1
    print('      **** test len after: ', len(dict_nodes))
        
    cpt=0
    for node in tweets_by_user.keys():
        if(node not in list(list_nodes)):
            cpt+=1
        
    print('>> Number of user: ', len(dict_nodes), '(', only_rt, 'only retweet )')
    print('          -> user not_present_in_graph: ', not_present_in_graph)
    print('          -> cpt (user not present in the graph also):', cpt)

    return dict_tweets, dict_nodes, dict_edges


"""
read text csv zarate
"user_id", "status_id", "created_at", "text", "reply_to_user_id", "is_quote", "is_retweet", "retweet_status_id", "retweet_text", "retweet_created_at", "retweet_user_id", "text_translated"
"""
def read_text_zarate(path_file):
    df_edges = pd.DataFrame({'timestamp': [],
                              'user_id': [],
                              'user_id_original': []})    
    dict_tweets = {}    
    tweets_by_user = {}
    dataloader = load_data_csv_into_df(path_file)
    print('Total lines (file): ', len(dataloader))
    total_user_test = []
    cpt=0
    for index, item in dataloader.iterrows():
        #1. edges
        if(item[6] == True): #it is a retweet
            original_id = str(item[7])
            creator = str(item[10])
            content = str(item[11]) #8
            
            df_edges.loc[len(df_edges)] = [str(from_iso_to_timestamp(item[2])), str(item[0]), str(item[10])]
            
            total_user_test.append(str(item[0]))
            total_user_test.append(str(item[10]))
            
        else: #tweet
            original_id = str(item[1])
            creator = str(item[0])
            content = str(item[11]) #3
            
            total_user_test.append(str(item[0]))

        #2. tweet text
        dict_tweets[original_id] = content
        
        #3. tweet by user
        if(creator not in tweets_by_user.keys()):
            tweets_by_user[creator] = [original_id]
        else:
            tweets_by_user[creator].append(original_id)
        
        if(TEST_LOCAL == True):
            if(cpt==300):
                print('stooooooooop before : test')
                break
        cpt+=1
      
    print('Number of user should be: ', len(list(set(total_user_test))))
    print('Total lines (df): ', len(dataloader), 'vs cpt=', cpt)
    print('Total lines (RT): ', len(df_edges))
    return df_edges, dict_tweets, tweets_by_user



"""
read text csv zarate
"user_id", "status_id", "created_at", "text", "reply_to_user_id", "is_quote", "is_retweet", "retweet_status_id", "retweet_text", "retweet_created_at", "retweet_user_id", "text_translated"
"""
def read_text_zarate_tweet_for_every_user(topic): 
    text_path = ut.CURRENT_DATA_FOLD + ut.DATA_ZARATE
    filename = [f for f in os.listdir(text_path) if topic in f and '.csv' in f]
    if(len(filename) == 0):
        print(' /!\ no file find for topic', topic, '/!\ ')
        print(text_path)
        return None, None
    filename = filename[0]
    text_path += filename
    print(text_path)
    
    dict_tweets = {}    
    dict_tweets_by_all_user = {}
    
    dataloader = load_data_csv_into_df(text_path)
    print('Total lines (file): ', len(dataloader))
    cpt=0
    for index, item in dataloader.iterrows():
        #1. edges
        if(item[6] == True): #it is a retweet
            original_id = str(item[7])
            creator = str(item[10])
            retweeter = str(item[0])
            content = str(item[11]) #8
            
        else: #tweet
            original_id = str(item[1])
            creator = str(item[0])
            retweeter = None
            content = str(item[11]) #3
            
        #2. tweet text
        dict_tweets[original_id] = content
        
        #3. tweet for every user
        if(item[6] == True): #retweet
            if(retweeter not in dict_tweets_by_all_user.keys()): #retweeter
                dict_tweets_by_all_user[retweeter] = [original_id]
            else:
                dict_tweets_by_all_user[retweeter].append(original_id)
                
            if(creator not in dict_tweets_by_all_user.keys()): #creator
                dict_tweets_by_all_user[creator] = [original_id]
            else:
                dict_tweets_by_all_user[creator].append(original_id)
                
        else: #tweet
            if(creator not in dict_tweets_by_all_user.keys()): #creator
                dict_tweets_by_all_user[creator] = [original_id]
            else:
                dict_tweets_by_all_user[creator].append(original_id)
        

        if(TEST_LOCAL == True):
            if(cpt==300):
                print('stooooooooop before : test')
                break
        cpt+=1
      
    print('Total lines (df): ', len(dataloader), 'vs cpt=', cpt)
    
    return dict_tweets, dict_tweets_by_all_user



"""
read the content of the file : Tweets
'timestamp', 'tweet_id' 'user_id' 'text' 'tweet_id_rt' 'user_id_rt' 'text_rt'
"""
def read_text_tweets_qc(path_file):
    df_edges = pd.DataFrame({'timestamp': [],
                              'user_id': [],
                              'user_id_original': []})    
    dict_tweets = {}
    
    tweets_by_user = {}
    dataloader = load_data_csv(path_file)
    
    cpt=0
    for item in dataloader:
        #1. edges
        if(item[5] != 'None'): #retweet
            original_id = item[4]
            creator = item[5]
            content = item[6]
            
            df_edges.loc[len(df_edges)] = [item[0], item[2], item[5]]
            
        else: #tweet
            original_id = item[1]
            creator = item[2]
            content = item[3]
        
            
        #2. tweet text
        dict_tweets[original_id] = content
        
        #3. tweet by user
        if(creator not in tweets_by_user.keys()):
            tweets_by_user[creator] = [original_id]
        else:
            tweets_by_user[creator].append(original_id)
            
        if(TEST_LOCAL == True):
            if(cpt==300):
                print('stooooooooop before : test')
                break
        cpt+=1
        
    print('Total lines : ', len(df_edges))
    return df_edges, dict_tweets, tweets_by_user


"""
dataloader which yield line by line data
"""
def load_data_csv(path_file):
    print(path_file)
    with open(path_file, encoding='utf-8') as f:
        lines = f.readlines()
        print("Total lines pulled :", len(lines), '\n')
        i = 0
        for l in lines:
            items = l[:-1].split(ut.splitter)
            items = [str(get_timestamp_from_tweet_id(items[0]))] + items
                

            if(i%int((len(lines)/20))==0):
                print("    loading.. ", round((i/len(lines))*100, 2), '%')
            i+=1
            
            yield items

"""
load csv file as a dataframe
"""
def load_data_csv_into_df(path_file):
    t = {'user_id': str, 'status_id': str, 'reply_to_user_id': str, 'retweet_status_id': str, 'retweet_user_id': str}
    df = pd.read_csv(path_file, dtype=t)
    
    return df

"""
get the timestamp from the tweet id for data QC
"""
def get_timestamp_from_tweet_id(tweet_id):
    offset = 1288834974657
    tstamp = (int(tweet_id) >> 22) + offset
    
    #utcdttime = datetime.utcfromtimestamp(tstamp/1000)
    #print(str(tid) + " : " + str(tstamp) + " => " + str(utcdttime))
    
    return tstamp

"""
load graph networkx G
"""
def load_graph(topic, type_graph, type_text_model, type_object):
    path_file = ut.CURRENT_DATA_FOLD + ut.object_folder + '/' + topic + '/' + type_graph + '/'
    filename = type_object+'_'+str(type_text_model)+'.pickle'
    
    path_file += filename
    
    G = load_pickle(path_file)

    return G

"""
load pickle object
"""
def load_data_from_pickle_file(topic, type_graph, filename):
    path_file = ut.CURRENT_DATA_FOLD + ut.object_folder + '/' + topic + '/' + type_graph + '/'    
    path_file += filename
    
    if(os.path.isfile(path_file) == False):
        print(path_file, ': <not found>')
        return False
    else: 
        data = load_pickle(path_file)
            
        return data
    
def load_pickle(path_file):
    if(os.path.isfile(path_file) == False):
        print(path_file, ': <not found>')
        return False
    else:     
        with open(path_file, 'rb') as file:
            data = pickle.load(file)
    return data


###################################################################################
###################### clusterisation + reduce + vizu #############################
###################################################################################

"""
clusterise our embedded graph in 2 communities
return list of labels for each node
"""
def clusterisation(method, data, k=2):
    
    if(method == ut.K_means):
        method = KMeans(n_clusters=k, random_state=0).fit(data)
        
    clusters = method.predict(data)
    centers = method.cluster_centers_
    inertia = method.inertia_
    
    c1 = [c for c in clusters if(c==0)]
    c2 = [c for c in clusters if(c==1)]
    
    print("cluster size --> C1:", len(c1), ' | C2:', len(c2))

    return clusters, centers, inertia

"""
Reduce node embedding dimension for vizualisation
"""
def reduce_dimension(method, data, dim=2):
    if(method == ut.TSNE):
        X_embedded = TSNE(n_components=dim, init='random').fit_transform(data)
    if(method == ut.UMAP):
        umap_2d = UMAP(n_components=2, init='random', random_state=0)
        X_embedded = umap_2d.fit_transform(data)
        
    return X_embedded

"""
plot graph clustered
"""
def plot_graph(filename, title, reduced_node_embeddings, node_community, lib = 'plt'):
    if(lib == 'plt'):
        plot_plt_graph(filename, title, reduced_node_embeddings, node_community)
    elif(lib == 'px'):
        plot_px(filename, title, reduced_node_embeddings, node_community)
    elif(lib == 'bokeh'):
        plot_px(filename, title, reduced_node_embeddings, node_community)
    
    return 1

"""
plot scatter point with matplotlib 
"""
def plot_plt_graph(filename, title, reduced_node_embeddings, node_community):   
    figure(figsize=(12, 8), dpi=100)
        
    cmap = plt.get_cmap('hsv')
    colormap = cmap(np.linspace(0, 1.0, len(set(node_community))))

    plt.scatter(reduced_node_embeddings[:,0], reduced_node_embeddings[:,1],
                s=5, c=colormap[node_community])
    plt.title(title)    
    plt.savefig(filename)
    
    plt.show()
    
    return 1

"""
plot scatter point with plotly express
"""
def plot_px(filename, title, reduced_node_embeddings, node_community):
    file = filename
    
    if(reduced_node_embeddings.shape[1] == 2):
        fig = px.scatter(reduced_node_embeddings[:,0], reduced_node_embeddings[:,1], color=node_community)
        fig.show()
    elif(reduced_node_embeddings.shape[1] == 3):
        df_embeddings = pd.DataFrame(data=reduced_node_embeddings, columns=["d1", "d2", "d3"])
        fig = px.scatter_3d(df_embeddings, x='d1', y='d2', z='d3', color=node_community,
            title = title,  opacity=0.7)
        fig.show()
    else:
        print('impossible to plot', reduced_node_embeddings.shape[1], 'd points...')
        
    return file


"""
plot interactive graph with text onclick
"""
def plot_bokeh(filename, title, reduced_node_embeddings, node_community):
    pass

###################################################################
########################## Other ##################################
###################################################################   

"""
partitionning of the graph
output :
    - parts : [node1,node2,node3] [node4,node5,node6]
"""
def graph_partitionning(list_edges, method='metis', p=2):
    if(method == 'metis'):
        print('    ***start metis***')
        G=nx.Graph()
        G.add_edges_from(list_edges)
            
        (_, parts) = (-100,-100) #metis.part_graph(G, p)
        
        part0 = 0
        part1 = 0
        for p in parts:
            if(p==0):
                part0+=1
            else:
                part1+=1
        
        print("//stats partitionning//")
        print("com1 : (", part0, ")+ com2 (", part1 , ") = ", part0+part1)
        print("///////////////////////")
    elif(method == 'louvain'):
        print('    ***start louvain***')
        G=nx.Graph()
        G.add_edges_from(list_edges)
        louvain = Louvain()
        parts = louvain.fit_transform(nx.to_scipy_sparse_matrix(G))
        print("//stats partitionning//")
        stats = np.unique(parts, return_counts=True)
        print("there is", len(stats[0]), "labels ---> stats for label 0, 1 and 2", stats[0][:3], stats[1][:3])
        print("///////////////////////")

    return G.nodes, parts

"""
split nodes into 3 list : train, val, test and unknown
"""
def split_train_test_nodes(dict_node_label):
    nodes = list(dict_node_label.keys())
    
    com1 = [x for x in nodes if dict_node_label[x]==0]
    com2 = [x for x in nodes if dict_node_label[x]==1]
    
    size_min = min(len(com1), len(com2))
    train_indice = [0,int(size_min*ut.train_size)]
    test_indice = [int(size_min*ut.train_size), int(size_min*(ut.train_size+ut.test_size))]
    val_indice =  [int(size_min*(ut.train_size+ut.test_size)), int(size_min*(ut.train_size+ut.test_size+ut.val_size))]
    unknown_indice = int(size_min*(ut.train_size+ut.test_size+ut.val_size))
    
    train_set = com1[train_indice[0]:train_indice[1]] + com2[train_indice[0]:train_indice[1]]
    train_idx = [nodes.index(i) for i in train_set]

    
    test_set = com1[test_indice[0]:test_indice[1]] + com2[test_indice[0]:test_indice[1]]
    test_idx = [nodes.index(i) for i in test_set]
    
    val_set = com1[val_indice[0]:val_indice[1]] + com2[val_indice[0]:val_indice[1]]
    val_idx = [nodes.index(i) for i in val_set]

    unknown_set = com1[unknown_indice:] + com2[unknown_indice:]
    unknown_idx = [nodes.index(i) for i in unknown_set]  
    
    print('------ compare : ', len(nodes),'==', len(train_idx)+len(test_idx)+len(val_idx)+len(unknown_idx))
    
    print('- nbs train nodes : ', len(train_set))
    print('- nbs test nodes : ', len(test_set))
    print('- nbs val nodes : ', len(val_set))
    print('- nbs unknown nodes : ', len(unknown_set))
    print('- Total : ', len(train_set)+len(test_set)+len(unknown_set)+len(val_set))

    return train_idx, val_idx, test_idx, unknown_idx


"""
return True if the tweet is an origin tweet
"""
def is_original(tweet):
    if(ut.retweeted_type not in tweet.keys() and ut.quoted_type not in tweet.keys() and ut.replied_to_type not in tweet.keys()): #original
        return True
    return False

"""
return True if tweet has been created in good period
"""
def in_period(tweet, start, end):
    if(start is None or tweet['creation'] >= start):
        if(end is None or tweet['creation'] <= end):
            return True
    return False

def get_type_tweet(tweet):
    if(is_original(tweet) == True):
        return 'original'
    else:
        if(ut.retweeted_type in tweet.keys()):
            return ut.retweeted_type
        elif(ut.quoted_type in tweet.keys()):
            return ut.quoted_type
        else:
            return ut.replied_to_type
        

"""
cpy-paste files
"""
def copy_paste(from_xxx, to_xxx):
    shutil.copy(from_xxx, to_xxx)
    
"""
translate text into english
"""
def translate_texts(inputs): 
    print(' > we translate !')
    translated_txt = GoogleTranslator(source='auto', target='en').translate_batch(inputs)
    
    return translated_txt

"""
replace :
    - @username -> _user_
    - https://link.fr -> _link_
    - #text -> text (?)
    - remove 'RT'
    - decode code
"""
def clean_text(inputs):
    new_inputs = []
    for tweet in inputs:
        tweet = re.sub("@[^\s]+","_user_",tweet) #replace @user citation by _user_
        tweet = re.sub(r"(?:\@|http?\://|https?\://|www)\S+", "_link_", tweet) #Replace http links by _link_
        # tweet = ''.join(c for c in tweet if c not in emoji.UNICODE_EMOJI) #Remove Emojis
        # tweet = tweet.replace("#", "").replace("_", " ") #Remove hashtag sign but keep the text
        # tweet = " ".join(w for w in nltk.wordpunct_tokenize(tweet) if w.lower() in words or not w.isalpha())
        tweet = html.unescape(tweet) #uncode the code
        
        new_inputs.append(tweet)
    
    return new_inputs

"""
Remove the URL linkes
Remove the "RT", "&", "<", ">", and "@" symbles
Remove non-ascii words
Remove # (or the whole hashtag)
Remove extra spaces
Insert spaces between punctuation marks
Strip leading and ending spaces
Convert all words to lower-case
"""
def clean_text_v2(inputs):
    new_inputs = []
    for tweet in inputs:
        #remove retweet
        tweet = re.sub('^RT @[^\s]+:', r'', tweet)
        
        #remove URL
        tweet = re.sub(r'http\S+', r'', tweet)
        
        # remove @
        tweet = re.sub(r'@[^\s]+',r'', tweet)
        
        #remove non-ascii words and characters
        # tweet = [''.join([i if ord(i) < 128 else '' for i in text]) for text in tweet]
        # tweet = ''.join(tweet)
        tweet = re.sub(r'_[\S]?',r'', tweet)
        tweet = re.sub(r'\n',r' ', tweet)
        
        #remove #
        tweet = re.sub(r'#([a-zA-Z0-9_]{1,50})',r'', tweet)
        #tweet = re.sub(r'#', '', tweet)
        
        # #remove &, < and >
        # tweet = tweet.replace(r'&amp;?',r'and')
        # tweet = tweet.replace(r'&lt;',r'<')
        # tweet = tweet.replace(r'&gt;',r'>')
        
        # # remove extra space
        # tweet = re.sub(r'\s\s+', r' ', tweet)
        
        # # insert space between punctuation marks
        # tweet = re.sub(r'([\w\d]+)([^\w\d ]+)', r'\1 \2', tweet)
        # tweet = re.sub(r'([^\w\d ]+)([\w\d]+)', r'\1 \2', tweet)
        
        # lower case and strip white spaces at both ends
        tweet = tweet.lower()
        tweet = tweet.strip()
        
        tweet = html.unescape(tweet)
        
        new_inputs.append(tweet)
    
    return new_inputs

###########################################################################################

"""
get node label
"""
def get_user_node_label(label_method, topic, type_graph):
    path = ut.CURRENT_DATA_FOLD + ut.label_folder + label_method + '/' + topic + '/' + type_graph + '/'
    filename = path + topic+'_labels_'+label_method+'.json'
    
    with open(filename) as f:
        labels = json.load(f)
    
    # all_labels = list(set([v for k,v in labels.items()])).sort()
    # l0 = all_labels[len(all_labels)-1]
    # l1 = all_labels[len(all_labels)-2]
    l0=1
    l1=2    
    
    count_labels = {}
    real_labels = {}
    cpt=0
    for k,v in labels.items():
        if(v in count_labels.keys()):
            count_labels[v] += 1
        else:
            count_labels[v] = 1
        
        if(v==l0):
            real_labels[k] = 0
        elif(v==l1):
            real_labels[k] = 1
        else:
            cpt+=1
                
    print(' > COUNT_LABELS_SAMPLE:', count_labels)
    print(' > we founded', len(labels), 'labels, but we kept', len(real_labels), 'and removed', cpt, ' users (because of mean-shif clustering)')
        
    return real_labels
        
"""
split dataset
"""
def split_dataset(dict_node_label_sample, is_test_set, is_balanced=True):
    if(is_balanced == True):  
        print(' ... balancing ...')
        x0 = []
        x1 = []
        y0 = []
        y1 = []
        for k,v in dict_node_label_sample.items():
            if(v==0):
                x0.append(k)
                y0.append(v)
            elif(v==1):
                x1.append(k)
                y1.append(v)
            
        print(' > x0 :', len(x0), 'vs x1 :', len(x1), 'elements <')
        
        if(len(x0) > len(x1)):       
            while(len(x0) != len(x1)):
                idx = random.randint(0,len(x0)-1)
                x0.pop(idx)
                y0.pop(idx)
        elif(len(x1) > len(x0)):
            while(len(x1) != len(x0)):
                idx = random.randint(0,len(x1)-1)
                x1.pop(idx)
                y1.pop(idx)
        print('...')
        print(' > x0 :', len(x0), 'vs x1 :', len(x1), 'elements <')
    
        X = x0 + x1
        y = y0 + y1
        
    else:
        print(' ... no balanced, we will do it at each epoch ...')
        X = []
        y = []
        rm=0
        for k,v in dict_node_label_sample.items():
            if(v==0 or v==1):
                X.append(k)
                y.append(v)
            else:
                rm+=1
        print(' /!\ rm other label =', rm)
           
                   
    if(is_test_set == False):
        print(' > no test set !')
        X_train = X
        X_test = []
        y_train = y
        y_test = []
    else:
        print(' > yess test set !')
        X_train, X_test, y_train, y_test = train_test_split(X, y,
                                                    stratify=y, 
                                                    train_size=0.8)
    
    print(len(X_train),len(X))
    print('------- info split before normalizing -------')
    print('train set: ', round(len(X_train)/len(X), 2), '(ratio) -->  y0: ', 
          round(len([y for y in y_train if y==0])/len(y_train), 2), '(', len([y for y in y_train if y==0]),')',
          ' /  y1: ', round(len([y for y in y_train if y==1])/len(y_train), 2), '(', len([y for y in y_train if y==1]),')')
    
    if(is_test_set == True):
        print('test set: ', round(len(X_test)/len(X),2), ' -->  y0: ',
              round(len([y for y in y_test if y==0])/len(y_test), 2), '(', len([y for y in y_test if y==0]),')',
              ' /  y1: ', round(len([y for y in y_test if y==1])/len(y_test), 2), '(', len([y for y in y_test if y==1]),')')
    else:
        print('No test set -> test size : ', round(len(X_test)))

    return X_train, X_test
    

"""
save in json format user node prediction
"""
def save_object_for_qc_score(node_ids, node_predictions, dict_coordinates, dict_user_to_id,
                                    dict_new_edges, dict_new_edge_weights,
                                    dict_node_label_metis, dict_node_label_sample, path):
    dict_preds = {}
    for n, pred in zip(node_ids, node_predictions):
        dict_preds[n] = pred
        
    dict_new_node_label_sample = {}
    for user, lab in dict_node_label_sample.items():
        if(user in dict_user_to_id):
            dict_new_node_label_sample[dict_user_to_id[user]] = lab
        else:
            print('WTTTTF problem w/ final save object for qc score')
    obj = (dict_coordinates, dict_preds, dict_new_edges, dict_new_edge_weights, dict_node_label_metis, dict_new_node_label_sample)
    
    pickle_save(path, obj)
        
    return 1


"""
convert timestamp to iso format
"""
def from_timestamp_to_iso(timestamp):
     return datetime.fromtimestamp(timestamp).isoformat()+'Z'
         
"""
convert iso format to timestamp
"""
def from_iso_to_timestamp(iso):
    return datetime.timestamp(dateutil.parser.isoparse(iso))


"""
transform the graph into a connex one (if not)
"""
def transform_graph_into_connex_graph(dict_nodes, dict_edges, dict_edge_weights, to_connex = True):
    list_edges = [v for k,v in dict_edges.items() if(v[0] is not None and v[1] is not None)]
    print(' >>> pb None nodes: ', len(dict_edges)-len(list_edges), '/', len(dict_edges))
    G = nx.Graph()
    G.add_edges_from(list_edges)
    print('    > number of components:', len([len(x) for x in nx.connected_components(G)]))
    
    if(to_connex == False):
        print('/!\ WE EVEN TAKE NON-CONNEX COMPONENT /!\ ')
        return dict_nodes, dict_edges, dict_edge_weights
    
    print(' /!\ WE KEEP ONLY THE LARGEST CONNEX COMPONENT cc /!\ ')

    is_connected = nx.is_connected(G)   
    if(is_connected == True):
        print(' > the graph is ALREADY connected')
        return dict_nodes, dict_edges, dict_edge_weights
                
    else:
        print(' > the graph is NOT connected')
        largest_cc = max(nx.connected_components(G), key=len)

        print('    > number of components:', len([len(x) for x in nx.connected_components(G)]),
              ' --- biggest component:', len(largest_cc), '(DELETE ', len(dict_nodes)-len(largest_cc), 'nodes)')
    
        dict_edges_cc =  {}
        for ts, e in dict_edges.items():
            if(e[0] in largest_cc and e[1] in largest_cc):
                dict_edges_cc[ts] = e
                
        #keep connected edges and nodes info
        dict_nodes_cc = {}
        for n in largest_cc:
            dict_nodes_cc[n] = dict_nodes[n]
        dict_edge_weights_cc = {}
        for ts, e in dict_edges_cc.items():
            dict_edge_weights_cc[e] = dict_edge_weights[e]
            
        #verify the new connectivity
        test_G = nx.Graph()
        test_G.add_edges_from([v for k,v in dict_edges_cc.items()])
        print('    > new graph is connected:', nx.is_connected(test_G), 'with', len(dict_edges_cc), 'edges')
        print('>> number of nodes deleted: ', len(dict_nodes)-len(largest_cc), ', edges deleted: ', len(dict_edges)-len(dict_edges_cc))
            
        
        return dict_nodes_cc, dict_edges_cc, dict_edge_weights_cc
    
    
"""
get # from the tweets
"""
def get_hashtag_by_user(dict_nodes, dict_tweets, list_user_kept):
    all_hashtag = []
    dict_hashtag_by_user = {}
    dict_text_by_user = {}
    dict_count_by_hashtag = {}
    for user in list_user_kept:
        tweet_ids = dict_nodes[user]
        list_h = []
        texts = []
        if(tweet_ids is not None):
            for id_ in tweet_ids:
                list_h.append(get_hashtag(dict_tweets[id_]))
                texts.append(dict_tweets[id_])
            
        list_h = [item for sublist in list_h for item in sublist]
        
        all_hashtag.append(list(set(list_h)))        
        dict_hashtag_by_user[user] = count_frequency(list_h)
        dict_text_by_user[user] = texts
        
    all_hashtag = [item for sublist in all_hashtag for item in sublist]
    
    for h in all_hashtag:
        if(h not in dict_count_by_hashtag.keys()):
            dict_count_by_hashtag[h] = 0
        dict_count_by_hashtag[h] += 1
    
    return dict_hashtag_by_user, list(set(all_hashtag)), dict_text_by_user, dict_count_by_hashtag

    
"""
create dict of frequencies
"""
def count_frequency(my_list): 
    freq = {}
    for item in my_list:
        if (item in freq):
            freq[item] += 1
        else:
            freq[item] = 1
            
    return freq

"""
clean text to get proper hashtags from raw text
for car in [',', '.', '\'', '@', ':', 'http']
"""
def get_hashtag(tweet):
    alphabet = [x for x in 'abcdefghijklmnopqrstuvwxyz0123456789_']
    special_char = ['http']
    
    h = []
    txt = tweet.split(' ')
    for t in txt:
        if('#' in t):
            i=0            
            for x in t.split('#'):
                if(i!=0):
                    new_x = x.lower()
                    for car in x.lower():
                        if(car not in alphabet):
                            new_x = new_x.split(car)[0]
                    for car in special_char:
                        if(len(new_x.split(car)) != 1):
                            new_x = new_x.split(car)[0]
                    
                    h.append('#'+new_x)
                i+=1
    return h
    

"""
return dict of one-hot encoded vector
"""
def get_one_hot_encoding_vector(dict_hashtag_by_user, all_hashtag):
    dict_feat = {}
    all_emb = []
    
    for k, dict_freq in dict_hashtag_by_user.items():
        total_freq = sum([j for _,j in dict_freq.items()])
        all_user_h = list(dict_freq.keys())
        feat = []
        for word in all_hashtag:
            if(word in all_user_h):
                feat.append(round(dict_freq[word]/total_freq, 3))
            elif(word == k): #case of user RT: we suppose that user must rt itself (as we work on similarity)
                feat.append(1)
            else:
                feat.append(0)
        dict_feat[k] = feat
        all_emb.append(feat)
    
    return dict_feat, np.stack(all_emb)



"""
load json file
"""
def load_json(filename):
    with open(filename) as f:
        res = json.load(f)
    return res

#-----------------------------------------------------------------------------------------------------------------
#--------------------------------------------------- LABEL_QC ----------------------------------------------------
"""
list of best hashtags for a topic
"""
def get_best_hashtags(dict_nodes, dict_tweets, q=0.7, print_log=False):
    list_best_h = []
    _, _, _, dict_count_by_hashtag = get_hashtag_by_user(dict_nodes, dict_tweets, list(dict_nodes.keys()))
    total_users_with_h = len([k for k,v in dict_nodes.items() if v is not None])
    for k,v in dict_count_by_hashtag.items():
        prop = v/total_users_with_h
        if(prop > q):
            print('    >>', k, 'with prop=', round(prop, 3), '(',v,'/',total_users_with_h,')')
            list_best_h.append(k)
    print(' > FINAL BEST # TO REMOVE (q=', q, ') :', list_best_h)

    return list_best_h

"""
list of hashtag with low frequencies 
"""
def get_hashtags_with_low_freq(dict_nodes, dict_tweets, q=0.5, print_log=False):
    _, _, _, dict_count_by_hashtag = get_hashtag_by_user(dict_nodes, dict_tweets, list(dict_nodes.keys()))
    list_freq = [v for k,v in dict_count_by_hashtag.items()]
    limit_h = np.quantile(list_freq, q)
    
    list_h_to_keep = [k for k,v in dict_count_by_hashtag.items() if v <= limit_h]
    print(' > FINAL LOW # freq (q=', q, ', so freq(#)<', limit_h, ') :', len(list_h_to_keep), '/',
          len(list_freq), 'hashtags.') #, list_h_to_keep)
    
    return list_h_to_keep


"""
return list of user to keep
"""

"""
return list of user to keep
"""
def keep_most_rt_users(dict_rt_by_users, top_K, dict_nodes, dict_tweets, keep_only_usr_with_h = True, 
                       remove_users_from_hashtag = False, keep_user_with_atleastone_h_from_low_freq_quartile = False):
    
    if('.' in str(top_K)): #quantile
        l = [v for _,v in dict_rt_by_users.items()]
        limit = np.quantile(l, top_K)
        lh = [v for _,v in dict_rt_by_users.items() if v>=limit]
        print(' > Quartile', top_K, ', keep users w/ at least', limit, 'RT (total of', len(lh), '/', len(l), 'users)')
    
    
    print(' > parameter with h: ', keep_only_usr_with_h, remove_users_from_hashtag, keep_user_with_atleastone_h_from_low_freq_quartile)
    ordered_dict = {k: v for k, v in sorted(dict_rt_by_users.items(), key=lambda item: item[1], reverse=True)}
    
    
    #1. get best # to remove
    list_of_h_to_remove = []
    if(remove_users_from_hashtag == True):
        print('remove main # from #list')
        list_of_h_to_remove = get_best_hashtags(dict_nodes, dict_tweets, print_log=True)
  
    #2. keep only tweets with h on this list
    list_h_to_keep = None
    if(keep_user_with_atleastone_h_from_low_freq_quartile == True):
        print('keep low # freq from #list')        
        list_h_to_keep = get_hashtags_with_low_freq(dict_nodes, dict_tweets, print_log=False)
        
    #3. get top_k users
    list_user_kept = []
    on_quartile=0
    cpt=0
    least = 1000000
    for k,v in ordered_dict.items():
        if('.' in str(top_K) and v < limit):
            break
        
        if((k in dict_rt_by_users.keys() and dict_rt_by_users[k] >= 5) or RT_MIN == False):
            if(keep_only_usr_with_h == False):
                list_user_kept.append(k)
                least = v
                cpt+=1
                
            else:
                _, h, _, _ = get_hashtag_by_user(dict_nodes, dict_tweets, [k])
            
                if(remove_users_from_hashtag == True):
                    h = [x for x in h if x not in list_of_h_to_remove]
                    
                go = True
                if(keep_user_with_atleastone_h_from_low_freq_quartile == True):
                    go = False
                    for low_h in list_h_to_keep:
                        if(low_h in h):
                            go = True
                    
                if((remove_users_from_hashtag==True and len(h)>= 3) or 
                   (keep_user_with_atleastone_h_from_low_freq_quartile==True and go==True) or
                   (remove_users_from_hashtag == False and keep_user_with_atleastone_h_from_low_freq_quartile == False and len(h)>=1)):
                    list_user_kept.append(k)
                    least = v
                    cpt+=1
            
            if('.' not in str(top_K) and cpt == top_K):
                break
            on_quartile+=1

        
    print(' > least_rt_user has ', least, ' RT, we kept', len(list_user_kept), 'users')
    print('     >> stats:', cpt,'/', on_quartile)

    return list_user_kept


"""
get rt by user 
(a,b) -> a retweet b

"""
def get_rt_by_user(dict_edges):
    dict_rt_by_user = {}
    
    for k, e in dict_edges.items():
        
        if(e[1] not in dict_rt_by_user.keys()):
            dict_rt_by_user[e[1]] = 0
        dict_rt_by_user[e[1]] += 1
    
    return dict_rt_by_user

# """
# return list of user to keep
# """
# def keep_most_rt_users_v1(dict_rt_by_users, top_K, dict_nodes, dict_tweets, keep_only_usr_with_h):
#     if('.' in str(top_K)): #quantile
#         l = [v for _,v in dict_rt_by_users.items()]
#         limit = np.quantile(l, top_K)
#         print(' > Quartile', top_K, ', keep users w/ at least', limit, 'RT')
    
    
#     print('keep_only_usr_with_h: ', keep_only_usr_with_h)
#     ordered_dict = {k: v for k, v in sorted(dict_rt_by_users.items(), key=lambda item: item[1], reverse=True)}
    
#     list_user_kept = []
#     cpt=0
#     for k,v in ordered_dict.items():
#         if('.' in str(top_K) and v < limit):
#             break
        
#         if(keep_only_usr_with_h == False):
#             list_user_kept.append(k)
#             cpt+=1
#         else:
#             _, h, _, _ = get_hashtag_by_user(dict_nodes, dict_tweets, [k])
#             if(len(h)>1):
#                 list_user_kept.append(k)
#                 cpt+=1 
        
#         if('.' not in str(top_K) and cpt == top_K):
#             break
        
#     print(' > least_rt_user has ', v, ' RT, we kept', len(list_user_kept), 'users')

#     return list_user_kept

"""
return list of user to keep (most #)
"""
def keep_most_post_users(top_K, dict_nodes, dict_tweets, dict_rt_by_users, keep_only_usr_with_h):
    dict_nbs_tweets_by_usr = {}
    for k,v in dict_nodes.items():
        if(v is not None):
            dict_nbs_tweets_by_usr[k] = len(v)
        else:
            dict_nbs_tweets_by_usr[k] = 0
            
    ordered_dict = {k: v for k, v in sorted(dict_nbs_tweets_by_usr.items(), key=lambda item: item[1], reverse=True)}
    
    list_user_kept = []
    cpt=0
    max_post_tweets = -1
    least=1000000
    for k,v in ordered_dict.items():
        if(cpt==0):
            max_post_tweets = v
            
        if((k in dict_rt_by_users.keys() and dict_rt_by_users[k] >= 5) or RT_MIN == False):
            if(keep_only_usr_with_h == False):
                list_user_kept.append(k)
                cpt+=1
                least = (v, dict_rt_by_users[k])
            else:
                _, h, _, _ = get_hashtag_by_user(dict_nodes, dict_tweets, [k])
                if(len(h)>=1):
                    list_user_kept.append(k)
                    cpt+=1
                    least = v
            if(cpt==top_K):
                break    

    print(' > least_post_user has ', least, 'tweets posted, where max_post_tweets has', max_post_tweets)
    return list_user_kept


        
    

"""
return dict of user who retweeted

input:
    + type_link: pred (retweeted), succ (retweeter), both (pred & succ)
"""
def get_rt_user_by_user(dict_edges, dict_tweets, list_user_kept, no_nodes_alone,
                        type_link='pred', between_them = True, connected_to_itself = True, weighted_features = 0):
    print(' > between them: ', between_them)
    print(' > weighted_features: ', weighted_features)
    print(' > connected_to_itself:', connected_to_itself)
    print(' > no_nodes_alone:', no_nodes_alone)
    
    all_user_involved = []
    dict_RT_by_user = {}
    dict_count_by_user = {}    
    
    for ts, edge in dict_edges.items():
        if(type_link == 'pred' or type_link == 'both'):
            if(edge[1] in list_user_kept):
                if(between_them == False or weighted_features > 0 or edge[0] in list_user_kept):
                    if(edge[1] in dict_RT_by_user):
                        dict_RT_by_user[edge[1]].append(edge[0])
                    else:
                        dict_RT_by_user[edge[1]] = [edge[0]]
                    all_user_involved.append(edge[0])
                    
                if(connected_to_itself == True):
                    if(edge[1] in dict_RT_by_user):
                        if(edge[1] not in dict_RT_by_user[edge[1]]):
                            dict_RT_by_user[edge[1]].append(edge[1])
                    else:
                        dict_RT_by_user[edge[1]] = [edge[1]]
                    all_user_involved.append(edge[1])               
                
        if(type_link == 'succ' or type_link == 'both'):
            if(edge[0] in list_user_kept):
                if(between_them == False or weighted_features > 0 or edge[1] in list_user_kept):
                    if(edge[0] in dict_RT_by_user):
                        dict_RT_by_user[edge[0]].append(edge[1])
                    else:
                        dict_RT_by_user[edge[0]] = [edge[1]]
                    all_user_involved.append(edge[1])
                    
                if(connected_to_itself == True):
                    if(edge[0] in dict_RT_by_user):
                        if(edge[0] not in dict_RT_by_user[edge[0]]):
                            dict_RT_by_user[edge[0]].append(edge[0])
                    else:
                        dict_RT_by_user[edge[0]] = [edge[0]]
                    all_user_involved.append(edge[0])   
    
        
    for h in all_user_involved:
        if(h not in dict_count_by_user.keys()):
            dict_count_by_user[h] = 0
        dict_count_by_user[h] += 1
    all_user_involved = list(set(all_user_involved))
        
    dict_features = {}
    no_nodes = 0
    for k,v in dict_RT_by_user.items():
        if(connected_to_itself == False):
            if(k not in v):
                v.append(k)
              
        if(no_nodes_alone == True and len(v) == 1):
            no_nodes += 1
        else:
            dict_features[k] = count_frequency(v)

        
    print(' >> WE WILL LABELIZE', len(dict_features),'USERS (instead of', len(list_user_kept), ') -> we poped', no_nodes, 'users')
    print(' >> total features :', len(all_user_involved))
    return dict_features, all_user_involved, dict_count_by_user


"""
return dict of one-hot encoded vector
"""
def create_one_hot_encoding(dict_features_by_user, all_features, dict_nodes, weighted_features):
    dict_feat = {}
    all_emb = []
    
    print('weighted features:', weighted_features)
    
    if(weighted_features == 2):
        ratio=0.1
        topk_features = []
        temp_features = []
        
        print('  len(all_features) =', len(all_features))

        for i in all_features:
            if(i in dict_features_by_user.keys()):
                topk_features.append(i)
            else:
                temp_features.append(i)
                
        print('  len(topk_features) = ', len(topk_features))
        print('  len(features) = ', len(temp_features))
        print('   > ratio to add --> ', int(len(temp_features)*ratio))
        
        temp2_features = random.sample(set(temp_features), int(len(temp_features)*ratio))        
        print('  len(temp2_features) =', len(temp2_features))
        
        all_features = topk_features + temp2_features
        print('  len(all_features) =', len(all_features))


    for k, dict_freq in dict_features_by_user.items():
        total_freq = sum([j for _,j in dict_freq.items()])
        all_user_h = list(dict_freq.keys())
        feat = []
        for u in all_features:
            if(u in all_user_h):
                value = round(dict_freq[u]/total_freq, 3)
            elif(u == k): #case of user RT: we suppose that user must rt itself (as we work on similarity)
                value = 1
            else:
                value = 0
                
            #if we weight features
            if(weighted_features > 0):
                w = 1
                if(weighted_features < 3):
                    if(dict_nodes[u] is not None):
                        w = len(dict_nodes[u]) + 1
                else:
                    if(u in dict_features_by_user.keys()):
                        w = 2
                        
                value = w*value
                
            feat.append(value)
            
            
        dict_feat[k] = feat
        all_emb.append(feat)
    
    return dict_feat, np.stack(all_emb)


def compute_cos_similarity(embeddings):
    similarities = cosine_similarity(embeddings)
    
    return similarities

def cluster(data, method='k_means'):
    print('------', method, '-------')
    if(method=='mean_shift'):
        labels = MeanShift().fit_predict(data)
    elif(method=='k_means'):
        labels = KMeans(n_clusters=2, random_state=0).fit_predict(data)
        
    n_lab = len(list(set(labels)))
    for k in range(n_lab):
        if(k<3):
            print(' > label', k,':', len([x for x in labels if x==k]))
    print('...')
    print(' >>> Total label = ', n_lab)
    print('-----------------')

    return labels

def scale_data(data, scale_type='min_max'):
    scaled_data = None
    if(scale_type == 'min_max'):
        scaler = MinMaxScaler(feature_range=(-1,1))
        scaled_data = scaler.fit_transform(data)
        
    return scaled_data


"""
save data 
"""
def save_json(dict_labels_by_user, label_path):
    with open(label_path, 'w') as f:
      json.dump(dict_labels_by_user, f)
      
    labels = [v for k,v in dict_labels_by_user.items()]
    n_lab = len(list(set(labels)))
    for k in range(n_lab):
        print(' > label', k,':', len([x for x in labels if x==k]))
    print(' >>> Total label = ', len(labels))
    
    return 1 
        
    
def get_rt_user_by_user_maximilien(dict_edges, dict_tweets, list_user_kept, 
                        type_link='pred', between_them = True, connected_to_itself = True):
    print('----- MAXXX VERSION -----')
    print(' > between them: ', between_them)
    print(' > connected_to_itself:', connected_to_itself)
    
    all_user_involved = list_user_kept
    dict_RT_by_user = {}
    dict_count_by_user = {}    
    
    for ts, edge in dict_edges.items():
        #edge[1]
        if(edge[0] in list_user_kept):
            if(edge[1] in dict_RT_by_user):
                dict_RT_by_user[edge[1]].append(edge[0])
            else:
                dict_RT_by_user[edge[1]] = [edge[0]]
        else:
            if(edge[1] not in dict_RT_by_user):
                dict_RT_by_user[edge[1]] = []
        
        #edge[0]
        if(edge[1] in list_user_kept):
            if(edge[0] in dict_RT_by_user):
                dict_RT_by_user[edge[0]].append(edge[1])
            else:
                dict_RT_by_user[edge[0]] = [edge[1]]     
        else:
            if(edge[0] not in dict_RT_by_user):
                dict_RT_by_user[edge[0]] = []
                
        #itself 
        if(connected_to_itself == True):
            if(edge[0] in list_user_kept):
                if(edge[0] in dict_RT_by_user):
                    if(edge[0] not in dict_RT_by_user[edge[0]]):
                        dict_RT_by_user[edge[0]].append(edge[0])
                else:
                    dict_RT_by_user[edge[0]] = [edge[0]]       
               
            if(edge[1] in list_user_kept):
                if(edge[1] in dict_RT_by_user):
                    if(edge[1] not in dict_RT_by_user[edge[1]]):
                        dict_RT_by_user[edge[1]].append(edge[1])
                else:
                    dict_RT_by_user[edge[1]] = [edge[1]]            
                
               
    for h in all_user_involved:
        if(h not in dict_count_by_user.keys()):
            dict_count_by_user[h] = 0
        dict_count_by_user[h] += 1
        
    for k,v in dict_RT_by_user.items():
        dict_RT_by_user[k] = count_frequency(dict_RT_by_user[k])
        
    print(' >> WE WILL LABELIZE', len(dict_RT_by_user),'USERS (instead of', len(list_user_kept), ') : max version')
    return dict_RT_by_user, list(set(all_user_involved)), dict_count_by_user


def create_nx_graph(dict_edges, dict_edge_weights, type_graph, list_user_kept):
    G = nx.DiGraph()
    if(type_graph == 'whole_graph'):
        print('use all connected nodes')
        for ts, edge in dict_edges.items():
            if(edge[0] in list_user_kept or edge[1] in list_user_kept):
                G.add_edge(edge[0], edge[1], weight=dict_edge_weights[edge])
                
    else:
        print('use only top-k nodes')
        for ts, edge in dict_edges.items():
            if(edge[0] in list_user_kept and edge[1] in list_user_kept):
                G.add_edge(edge[0], edge[1], weight=dict_edge_weights[edge])
            elif(edge[0] in list_user_kept):
                G.add_edge(edge[0], edge[0], weight=1)
            elif(edge[1] in list_user_kept):
                G.add_edge(edge[1], edge[1], weight=1)                
        
    print(' > total nodes:', len(G.nodes()), '| total edges:', len(G.edges()), 'vs', len(dict_edges))
    print(' > len(list_user_kept):', len(list_user_kept))
    return G

"""
compute similarity

type_similarity = 'sim_rank', 'panther'
type_graph = 'whole_graph', 'top_k' only

importance_factor : how much we use both side for nodes

"""
def compute_nx_similarity(dict_edges, dict_edge_weights, list_user_kept, type_similarity, no_nodes_alone, type_graph = 'topk'):
    print(' > type graph:', type_graph, '- no_nodes_alone:', no_nodes_alone)
    
    #1. create graph nx
    G = create_nx_graph(dict_edges, dict_edge_weights, type_graph, list_user_kept)
    
    #2. compute similarity
    print('-2-')
    if(type_similarity == 'simrank'):
        importance_factor = 0.5
        print(' >> similarity:', type_graph, ' + importance_factor:', importance_factor)
        dict_sim = nx.simrank_similarity(G, importance_factor=importance_factor)
    elif(type_similarity == 'panther'):
        pass
        
    #3. get sim only for top-k
    print('-3-')
    dict_sim_top_k = {}
    if(type_graph == 'whole_graph'):
        for u in list_user_kept:
            temp_v = dict_sim[u]
            new_v = {}
            for k in list_user_kept:
                new_v[k] = temp_v[k]
            dict_sim_top_k[u] = new_v
    else:
        dict_sim_top_k = dict_sim
        
        
    if(no_nodes_alone == True):
        final_dict_sim = {}
        alone = 0
        for k,v in dict_sim_top_k.items():
            cpt = len([k1 for k1,v1 in v.items() if v1 > 0.0])
            if(cpt>1):
                final_dict_sim[k] = v
            else:
                alone+=1
        print(' > ALONE nodes:', alone)
    else:
        final_dict_sim = dict_sim_top_k
        
    #5. get list embeddings
    print('-4-')
    embeddings = []
    for k,v in final_dict_sim.items():
        e = [j for _,j in v.items()]
        embeddings.append(e)
    
    return G, dict_sim, final_dict_sim, embeddings
    

"""
propagate the user label to each tweet
"""
def get_label_to_tweet(dict_new_nodes, dict_labs, dict_tweets):
    dict_text = {}
    dict_label_by_tweet = {}
    for usr, tweet_list in dict_new_nodes.items():
        if(tweet_list is not None):
            for tweet in tweet_list:
                dict_label_by_tweet[tweet] = dict_labs[usr]
                dict_text[tweet] = dict_tweets[tweet]
                
    return dict_label_by_tweet, dict_text
    
"""
aggregate embedding and prediction by usr
"""
def aggregate_tweets_by_usr(dict_tweets_by_all_new_user, dict_tweet_embeddings, dict_tweet_label_predicted, text_aggregator):
    dict_node_embeddings = {}
    dict_node_label_predicted = {}
    for usr, list_tweets in dict_tweets_by_all_new_user.items():
        e = [dict_tweet_embeddings[t] for t in list_tweets]
        e = np.stack(e)
        dict_node_embeddings[usr] = np.max(e, axis=0)
        
        l = [dict_tweet_label_predicted[t] for t in list_tweets]
        dict_node_label_predicted[usr] = int(round(sum(l)/len(l)))
        
    return dict_node_embeddings, dict_node_label_predicted


"""
get naive score from log likehihood

x0 = log[P(x1==NC)] + ... + log[P(xn==NC)
x1 = log[P(x1==C)] + ... + log[P(xn==C)
SCORE = EXP[ x1 - logsumexp[x1;x0]]
"""
def get_naive_score(probs):
    x0 = 0
    for p in probs:
        x0 = x0 + np.log(1-p)
    x1 = 0
    for p in probs:
        x1 = x1 + np.log(p)
    naive_bayes = np.exp(x1 - logsumexp([x1, x0])) 
                          
    return naive_bayes
        
def get_diameter(TEST_SET):
    for topic, v in TEST_SET.items():
        G, _ = v
        
        distances = dgl.shortest_dist(G, root=0)
        diameter = torch.max(distances)
        
        print(topic, ':', diameter)
        
    return 1
    
    
    
    
    
    