from operator import itemgetter

import numpy as np
import scipy.sparse as sp
import time;


"""
convert graph G to directed graph
"""
def convertIntoDirected(G):
	for edge in G.edges():
		node1 = edge[0]
		node2 = edge[1]
		G.add_edge(node2,node1)
	return G

"""
get the top-k highest degree nodes for each
G = graph (nx)
k = top-k (int)
community = 0 or 1
"""
def getNodesFromLabelsWithHighestDegree(G,k,community, dict_left, dict_right): # first take the nodes with the highest degree according to the "flag" and then take the top $k$
	random_nodes = {}
	dict_degrees = {}
	for node in G.nodes():
		dict_degrees[node] = G.degree(node)
	sorted_dict = sorted(dict_degrees.items(), key=itemgetter(1), reverse=True) # sorts nodes by degrees
    
	if(community==0):
		count = 0
		for i in sorted_dict:
			if(count>k):
				break
			if(i[0] not in dict_left.keys()):
				continue
			random_nodes[i[0]] = i[1]
			count += 1
	elif(community==1):
		count = 0
		for i in sorted_dict:
			if(count>k):
				break
			if(i[0] not in dict_right.keys()):
				continue
			random_nodes[i[0]] = i[1]
			count += 1
	else:
		count = 0
		for i in sorted_dict:
			if(count>k/2):
				break
			if(i[0] not in dict_left.keys()):
				continue
			random_nodes[i[0]] = i[1]
			count += 1
		count = 0
		for i in sorted_dict:
			if(count>k/2):
				break
			if(i[0] not in dict_right.keys()):
				continue
			random_nodes[i[0]] = i[1]
			count += 1

	return random_nodes


def get_model(G,corenode,tol=10**-5,save_xi=True):
    #G: Graph to calculate opinions. The nodes have an attribute "ideo" with their ideology, set to 0 for all listeners, 1 and -1 for the elite.
    #corenode: Nodes that belong to the seed (Identifiers from the Graph G)
    #tol is the threshold for convergence. It will evaluate the difference between opinions at time t and t+1
    #save_xi: boolean to save results

    N=len(G.nodes())
    
    #Get de adjacency matrix
    Aij = sp.lil_matrix((N,N))
    for o,d in G.edges():
        Aij[o,d]=1

    #Build the vectors with users opinions
    v_current= []
    v_new = []
    # dict_nodes = {};
    for nodo in G.nodes():
        # dict_nodes[G.nodes[nodo]['label']] = G.nodes[nodo]['ideo'];
        v_current.append(G.nodes[nodo]['ideo'])
        v_new.append(0.0)

    v_current = 1.*np.array(v_current)
    v_new = 1.*np.array(v_new)
    
    notconverged=len(v_current)
    times = 0;
    
    #Do as many times as required for convergence
    while notconverged > 0:
        times=times+1
        t=time.time()
        
        #for all nodes apart from corenode, calculate opinion as average of neighbors
        for j in np.setdiff1d(range(len(v_current)),corenode):
            nodosin=Aij.getrow(j).nonzero()[1]
            if len(nodosin) > 0:
                v_new[j]= np.mean(v_current[nodosin])
            else:
                v_new[j]=v_current[j]
       
        #update opinion
        for j in corenode:
            v_new[j]=v_current[j]

        diff=np.abs(v_current-v_new)
        notconverged=len(diff[diff>tol])
        v_current=v_new.copy()
        
        print('t', times, ':', round(((time.time() - t)/60), 2), 'min')
	
    return v_current;     

def GetPolarizationIndex(ideos):
    #Input: Vector with individuals Xi
    #Output: Polarization index, Area Difference, Normalized Pole Distance
    D=[]#POLE DISTANCE
    AP=[]#POSSITIVE AREA
    AN=[]#NEGATIVE AREA
    cgp=[]#POSSITIVE GRAVITY CENTER
    cgn=[]#NEGATIVE GRAVITY CENTER
    
    ideos.sort()
    hist,edg=np.histogram(ideos,np.linspace(-1,1.1,50))
    edg=edg[:len(edg)-1]
    AP=sum(hist[edg>0])
    AN=sum(hist[edg<0])
    AP0=1.*AP/(AP+AN)
    AN0=1.*AN/(AP+AN)
    cgp=sum(hist[edg>0]*edg[edg>0])/sum(hist[edg>0])
    cgn=sum(hist[edg<0]*edg[edg<0])/sum(hist[edg<0])
    D=cgp-cgn
    p1= 0.5*D*(1.-1.*abs(AP0-AN0))#polarization index (d)
    DA=1.*abs(AP0-AN0)/(AP0+AN0)#Areas Difference (delta A)
    D=0.5*D#Normalized Pole Distance
    
    MBLB = (1-DA) * p1
    
    return p1,DA,D,MBLB