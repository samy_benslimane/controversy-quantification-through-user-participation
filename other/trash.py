#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug  9 10:51:33 2023

@author: samy
"""

def train_loop_sample_nosave(TRAIN_SET, TEST_SET, type_sample,
               only_text, model, 
               optimizer, epochs, device, batch_gnn, 
               is_edge_weighted, text_model, path, model_name):
    
    print("----------", type_sample, "----------")
    min_nodes_by_graph = get_min_nodes_by_graph(TRAIN_SET)
    
    # set parameters of EPOCHS LOOP
    final_model = copy.deepcopy(model)
    start_epoch = 0
    best_train_epoch = -1
    best_train_loss = None
    list_train_loss = []
    best_train_acc = None
    list_train_acc = []
    best_test_acc = 0 #depend on best train epoch
    list_test_acc = []
    cpt_early_stop = 0
    runtime = 0    
    dict_scores_by_epoch = {}
    dict_all_probs_by_epoch = {}
    for x in TEST_SET.keys():
        dict_scores_by_epoch[x] = {}
        dict_all_probs_by_epoch[x] = {}

    
    fold_chkpt = path + ut.checkpoint_folder
    if(os.path.isdir(fold_chkpt) == False):
        os.mkdir(fold_chkpt)
    
    if(len(os.listdir(fold_chkpt)) > 0):
        file = os.listdir(fold_chkpt)[0]
        checkpoint_path = fold_chkpt + file
    
        if(os.path.exists(checkpoint_path)):
            print(' > found checkpoint!')
            chk = torch.load(checkpoint_path)
            model.load_state_dict(chk['model_state_dict'])
            optimizer.load_state_dict(chk['optimizer_state_dict'])
            start_epoch = chk['epoch']+1
            best_train_epoch = chk['best_train_epoch']
            best_train_loss = chk['best_train_loss']
            list_train_loss = chk['list_train_loss']
            best_train_acc = chk['best_train_acc']
            list_train_acc = chk['list_train_acc']
            best_test_acc = chk['best_test_acc']
            list_test_acc = chk['list_test_acc']
            cpt_early_stop = chk['cpt_early_stop']
            dict_scores_by_epoch = chk['dict_scores_by_epoch']
            dict_all_probs_by_epoch = chk['dict_all_probs_by_epoch']
            runtime = chk['runtime']
            
            final_model = torch.load(path+'final_TEMP.pth')
    
    print(' >> early_stop=', early_stop)
    for e in range(start_epoch, epochs):
        print("------------------", e, "---------------------------")
        t_ep = time.time()
        ################# 1. TRAINING  #################
        model.train()
        total_loss = 0
        total_acc = 0
        total_train_x0 = 0
        total_train_x1 = 0
        for G, dict_node_features in TRAIN_SET:
            #1.1 PREPARE tHE GRAPH
            n_nodes = []
            features = []
            idx_none = []
            for k, v in dict_node_features.items():
                n_nodes.append(k)
                features.append(v)
                if(v is None):
                    idx_none.append(k)
            if(text_model == False):
                features = [torch.tensor(f, dtype=torch.double) for f in features]
        
            labels = G.ndata['label']
            train_mask = get_temp_train_mask(G, min_nodes_by_graph)
                
            all_labels = labels[train_mask]
            all_labels = all_labels.cpu().detach().numpy()
            all_index = np.where(np.array(train_mask) == True)[0]
            
            if(only_text == True):
                temp_all_index = []
                temp_all_labels = []
                cpt=0
                for i in range(len(all_index)):
                    if(dict_node_features[all_index[i]] is not None):
                        temp_all_index.append(all_index[i])
                        temp_all_labels.append(all_labels[i])
                    else:
                        cpt+=1
                all_index = temp_all_index
                all_labels = temp_all_labels               
        
            x0 =np.array([all_index[i] for i in range(len(all_index)) if all_labels[i] == 0])
            x1 =np.array([all_index[i] for i in range(len(all_index)) if all_labels[i] == 1])
            total_train_x0 += len(x0)
            total_train_x1 += len(x1)
            
            
            #1.2 TRAIN THE MODEL
            optimizer.zero_grad()
            if(is_edge_weighted == True):
                _, h, _, logits = model(G, features, G.edata['weight'], device=device)
            else:
                _, h, _, logits = model(G, features, device=device)

            # Compute prediction
            preds = logits.argmax(1)
    
            # Compute loss
            loss = F.cross_entropy(logits[train_mask], labels[train_mask])
            
            # TRAIN acc
            train_acc = (preds[train_mask] == labels[train_mask]).float().mean()
            
            # Backward
            loss.backward()
            optimizer.step()
            
            total_loss += loss.item()
            total_acc += train_acc.item()
            
        #1.3 COMPUTE LOSS
        epoch_loss = round(total_loss / len(TRAIN_SET), 5)
        epoch_acc = round(total_acc / len(TRAIN_SET), 5)
        
        #1.4 SAVE FINAL MODEL -- early stopping when train-loss increase --
        if(early_stop is None):
            final_model = copy.deepcopy(model)
        else:
            if(best_train_loss is not None and best_train_loss < epoch_loss):
                if(cpt_early_stop >= early_stop):
                    print(' > hop we stop, new_train_loss=', epoch_loss, 'best train loss=', best_train_loss, '==> cpt_early=', cpt_early_stop)
                    break
                else:
                    cpt_early_stop+=1
            else:
                print(' ! best model !')
                best_train_epoch = e
                best_train_loss = epoch_loss
                best_train_acc = epoch_acc
                final_model = copy.deepcopy(model)
                cpt_early_stop = 0
        
        #runtime
        train_epoch_runtime = time.time() - t_ep
        runtime += train_epoch_runtime
        
        ################# 2. EVALUATION  #################
        model.eval()
        all_test_preds = []
        all_test_labs = []
        total_test_x0 = 0
        total_test_x1 = 0
        # print('---len(TEST_SET):', len(TEST_SET))
        for topic, data in TEST_SET.items():
            G, dict_node_features = data
            #2.1 PREPARE tHE GRAPH
            n_nodes = []
            features = []
            idx_none = []
            for k, v in dict_node_features.items():
                n_nodes.append(k)
                features.append(v)
                if(v is None):
                    idx_none.append(k)
            if(text_model == False):
                features = [torch.tensor(f, dtype=torch.double) for f in features]
        
            labels = G.ndata['label']
            test_mask = [True] * len(G.nodes())
            # print('1/', len(test_mask), len(labels))
            # print('2/', len(features))
            all_labels = labels[test_mask]
            all_labels = labels
            all_labels = all_labels.cpu().detach().numpy()
            all_index = np.where(np.array(test_mask) == True)[0]
            
            if(only_text == True):
                temp_all_index = []
                temp_all_labels = []
                cpt=0
                for i in range(len(all_index)):
                    if(dict_node_features[all_index[i]] is not None):
                        temp_all_index.append(all_index[i])
                        temp_all_labels.append(all_labels[i])
                    else:
                        cpt+=1
                all_index = temp_all_index
                all_labels = temp_all_labels               
        
            x0 =np.array([all_index[i] for i in range(len(all_index)) if all_labels[i] == 0])
            x1 =np.array([all_index[i] for i in range(len(all_index)) if all_labels[i] == 1])
            total_test_x0 += len(x0)
            total_test_x1 += len(x1)
            
            
            #2.2 GET MODEL PREDICTION
            t_ep = time.time()
            with torch.no_grad():
                if(is_edge_weighted == True):
                    gnn_inputs, embeddings, _, probas = model(G, features, G.edata['weight'], device=device)
                else:
                    gnn_inputs, embeddings, _, probas = model(G, features, device=device)
            test_epoch_runtime = time.time() - t_ep
            
            #Get score QC
            current_label = ut.data_zarate_labels[topic]
            res_topic, dict_probas_by_id = get_prediction_score(G, probas, current_label, test_epoch_runtime)
            dict_scores_by_epoch[topic][e] = res_topic
            dict_all_probs_by_epoch[topic][e] = dict_probas_by_id
            
            # Compute prediction
            # print('3/', len(probas))
            preds = probas.argmax(1)
            # print('4/', len(preds), len(test_mask))
            all_test_preds += preds[test_mask].tolist()
            all_test_labs += labels[test_mask].tolist()
        
        #2.3 GET TEST ACCURACY
        test_acc = accuracy_score(all_test_labs, all_test_preds) 
        if (best_train_epoch == e):
            best_test_acc = test_acc
        

        ################# 3. SAVE CHKPT & MODEL & METADATA  #################        
        #3.1 SAVE LOSS & ACC train test sets
        list_train_loss.append(epoch_loss)
        list_train_acc.append(epoch_acc)
        list_test_acc.append(test_acc)
                
        #3.2 SAVE FINAL MODEL
        if(best_train_epoch == e):
            save_model(path+'final_TEMP.pth', final_model)
        
        #3.3 SAVE CHKPT MODEL
        #save checkpoint
        for old_file in os.listdir(fold_chkpt):
              os.remove(fold_chkpt+old_file)
             
        # save current epochs 
        checkpoint_file = fold_chkpt + str(e)+'_'+model_name 
        torch.save({
            'epoch': e,
            'model_state_dict': model.state_dict(),
            'optimizer_state_dict': optimizer.state_dict(),
            'best_train_epoch': best_train_epoch,
            'best_train_loss': best_train_loss,
            'list_train_loss': list_train_loss,
            'best_train_acc': best_train_acc,
            'list_train_acc': list_train_acc,
            'best_test_acc': best_test_acc,
            'list_test_acc': list_test_acc,
            'cpt_early_stop': cpt_early_stop,
            'dict_scores_by_epoch': dict_scores_by_epoch,
            'dict_all_probs_by_epoch': dict_all_probs_by_epoch,
            'runtime': runtime
            }, checkpoint_file)
        
        #3.4 PRINT INFO ON EPOCHS
        if(e%1 == 0):
            print('In epoch {}, train loss: {:.3f} + train acc: {:.3f} || test acc: {:.3f}'.format(
                e, epoch_loss, epoch_acc, test_acc))
            print('           > BEST: In epoch {}, train_loss: {:.3f} + train_acc: {:.3f} || test_acc: {:.3f}'.format(
                best_train_epoch, best_train_loss, best_train_acc, best_test_acc))
            
            print(' --> time :', round(((time.time() - t_ep)/60), 2), 'min --- ', total_train_x0, total_train_x1, total_test_x0, total_test_x1)
            print("------------------------------------------------- \n")

    # FINALISATION
    #save model
    print('\n --- Save final model ---')
    save_model(path+model_name, final_model)
    train_info = {'train_runtime': round(runtime/60,4),
                  'train_loss': best_train_loss,
                  'test_acc': best_test_acc}
    print('    -> Training info:', train_info)
    gp.write_json(path+'train_info.json', train_info)
    
    print('\n --- Save score at each epoch of test ---')
    dict_scores_file = path + 'dict_scores_by_epoch_'+str(type_sample)+'.json'
    dict_scores_by_epoch['BEST_EPOCH'] = best_train_epoch
    gp.write_json(dict_scores_file, dict_scores_by_epoch)
    
    print('\n --- Save score at each epoch of PROBAS ---')
    dict_probs_file = path + 'dict_all_probs_by_epoch_'+str(type_sample)+'.json'
    dict_all_probs_by_epoch['BEST_EPOCH'] = best_train_epoch
    gp.write_json(dict_probs_file, dict_all_probs_by_epoch)    
    
    #plot         
    path = path + 'plot/'
    if(os.path.isdir(path) == False):
        os.mkdir(path)
    
    p_train_loss = path + "train_loss.png"
    plt.plot(range(len(list_train_loss)), list_train_loss)
    plt.savefig(fname = p_train_loss)
    plt.show()
    
    p_train_acc = path + "train_acc.png"
    plt.plot(range(len(list_train_acc)), list_train_acc)
    plt.savefig(fname = p_train_acc)
    plt.show()
    
    p_test_acc = path + "test_acc.png"
    plt.plot(range(len(list_test_acc)), list_test_acc)
    plt.savefig(fname = p_test_acc)
    plt.show()
                
    return final_model