#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 14 17:01:01 2022

@author: samy
"""
import random
import pickle
import json
import csv
import re
import html 

type_graph = 'RETWEET'

label_method = 'LC_8A_no_alone_nodes'
topic = 'pelosi' #'kavanaugh06-08' 'pelosi'

type_nodes = '' #'NOT_ALONE'

root = '/data/DEV_DATA/TWITTER API/other/data_complete_vaccine_chloro/data/data_objects/LABELS/'

path_2 = root+label_method+'/'+topic+'/RETWEET/'

def clean_text_v2(inputs):
    new_inputs = []
    for tweet in inputs:        
        #remove URL
        tweet = re.sub(r'http\S+', r'', tweet)        
        # remove @
        tweet = re.sub(r'@[^\s]+',r'', tweet)
        
        tweet = re.sub(r'_[\S]?',r'', tweet)
        tweet = re.sub(r'\n',r' ', tweet)
        
        #remove #
        tweet = re.sub(r'#([a-zA-Z0-9_]{1,50})',r'', tweet)

        tweet = tweet.strip()
        
        tweet = html.unescape(tweet)
        
        new_inputs.append(tweet)
    
    return new_inputs

def print_random_tweets_by_label(dict_usr_by_label, dict_tweets, dict_nodes):
    file = path_2 + 'tweets_by_comu.txt'
    
    with open(file, 'w') as f:
        for k,v in dict_usr_by_label.items():
            f.write('************************** '+str(k)+' ************************** \n')
            done = []
            while(len(done) < 10):
                usr = random.choice(v)
                tweets = dict_nodes[usr]
                one_tweet = random.choice(tweets)
                
                if(one_tweet in done):
                    continue
                else:
                    content = dict_tweets[one_tweet]
                    f.write(content)
                    f.write('\n -------------- \n')
                    done.append(one_tweet)
                    
            f.write('************************************************************* \n\n')
            
    return 1

def print_stats_graph_by_label(dict_usr_by_label, dict_edges):
    file = path_2 + 'stats_graph_by_comu.txt'
    preds = {}
    succ = {}
    
    for k, v in dict_edges.items():
        if(v[1] not in preds):
            preds[v[1]] = 0
        preds[v[1]] += 1
            
        if(v[0] not in succ):
            succ[v[0]] = 0
        succ[v[0]] += 1
        
    
    with open(file, 'w') as f:
        for k,v in dict_usr_by_label.items():
            f.write('************************** '+str(k)+' ************************** \n')
            tot_preds = 0
            nbs_usr_preds = 0
            min_max_preds = [None, None]
            
            tot_succ = 0
            nbs_usr_succ = 0
            min_max_succ = [None, None]

            for usr in v:
                if(usr in preds):
                    tot_preds += preds[usr]
                    if(min_max_preds[0] is None or preds[usr] < min_max_preds[0]):
                        min_max_preds[0] = preds[usr]
                    if(min_max_preds[1] is None or preds[usr] > min_max_preds[1]):
                        min_max_preds[1] = preds[usr]
                    nbs_usr_preds+=1
                    
                if(usr in succ):
                    tot_succ += succ[usr]
                    if(min_max_succ[0] is None or succ[usr] < min_max_succ[0]):
                        min_max_succ[0] = succ[usr]
                    if(min_max_succ[1] is None or succ[usr] > min_max_succ[1]):
                        min_max_succ[1] = succ[usr]
                    nbs_usr_succ+=1
            
            mean_preds = round(tot_preds / len(v), 2)
            mean_succ = round(tot_succ / len(v), 2)
            
            f.write(' > total usr  : '+str(len(v))+' \n')
            f.write('------- preds ------- \n')
            f.write(' > usr with preds : '+str(nbs_usr_preds)+' \n')
            f.write(' > mean : '+str(mean_preds)+' \n')
            f.write(' > min-max : '+str(min_max_preds)+' \n')
            
            f.write('------- succ ------- \n')
            f.write(' > usr with succ : '+str(nbs_usr_succ)+' \n')
            f.write(' > mean : '+str(mean_succ)+' \n')
            f.write(' > min-max : '+str(min_max_succ)+' \n')
            
            f.write('************************************************************* \n\n')

    return 1

def print_stats_graph_inside_top_k(dict_usr_by_label, dict_features_by_user):
    file = path_2 + 'stats_graph_inside_topk.txt'        
    
    new_dict_usr_by_label = {}
    
    with open(file, 'w') as f:
        for k,v in dict_usr_by_label.items():
            f.write('************************** '+str(k)+' ************************** \n')
            degree_by_usr = []
            
            for usr in v:   
                degree_by_usr.append(len(dict_features_by_user[usr]))
                
                if(len(dict_features_by_user[usr]) != 1):
                    new_dict_usr_by_label[usr] = k
                
            f.write(' > NBS USER : '+str(len(degree_by_usr))+' \n')
            f.write(' > total degree : '+str(sum(degree_by_usr))+' \n')
            f.write(' > min degree : '+str(min(degree_by_usr))+' \n')
            f.write(' > mean degree : '+str(sum(degree_by_usr)/len(degree_by_usr))+' \n')
            f.write(' > max degree : '+str(max(degree_by_usr))+' \n')
            f.write(' > nbs_1 degree (itself) :'+str(len([r for r in degree_by_usr if r==1]))+' \n')
                
            f.write('************************************************************* \n\n')
            
    return new_dict_usr_by_label
    

def load_pickle(path_file):
    with open(path_file, 'rb') as file:
        data = pickle.load(file)
        
    return data

def get_user_node_label():
    path = '/data/DEV_DATA/TWITTER API/other/data_complete_vaccine_chloro/data/data_objects/LABELS/'+label_method+'/'+topic+'/RETWEET/'
    
    if(type_nodes == 'NOT_ALONE'):
        filename = path + topic+'_labels_'+label_method+'_WITHOUT_ALONE_NODES.json'
    else:
        filename = path + topic+'_labels_'+label_method+'.json'
    
    with open(filename) as f:
        dict_labels = json.load(f)
        
    labels = [v for k,v in dict_labels.items()]
    n_lab = len(list(set(labels)))
    for k in range(n_lab):
        print(' > label', k,':', len([x for x in labels if x==k]))
    print(' >>> Total label = ', len(labels))
    print('----------')
    
    return dict_labels

def save_json(new_dict_node_sample):
    path = '/data/DEV_DATA/TWITTER API/other/data_complete_vaccine_chloro/data/data_objects/LABELS/'+label_method+'/'+topic+'/RETWEET/'
    filename = path + topic+'_labels_'+label_method+'_WITHOUT_ALONE_NODES.json'

    with open(filename, 'w') as f:
        json.dump(new_dict_node_sample, f)
        
    labels = [v for k,v in new_dict_node_sample.items()]
    n_lab = len(list(set(labels)))
    for k in range(n_lab):
        print(' > label', k,':', len([x for x in labels if x==k]))
    print(' >>> Total label = ', len(labels))
    print('----------')
        
    return 1


def save_giphi_files(dict_node_label, dict_nodes, dict_edges):   
    
    add = ''
    if(type_nodes == 'NOT_ALONE'):
        add = '_NOT_ALONE'
    
    #1. write node information
    node_filename = path_2 + 'nodes_'+label_method+add+'.csv'
    
    with open(node_filename, mode='w') as nf:
        writer_node = csv.writer(nf, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        writer_node.writerow(['Id','Label','modularity_class'])
    
        if(dict_node_label is not None):
            for user in dict_node_label.keys():
                writer_node.writerow([str(user), '', str(dict_node_label[user])])
                
    
    edge_filename = path_2 + 'edges_'+label_method+add+'.csv'
    with open(edge_filename, mode='w') as ef:
        writer_edge = csv.writer(ef, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        writer_edge.writerow(['Source','Target','Type', 'Id', 'Label']) 
        
        for ts in dict_edges.keys():
            edge = dict_edges[ts]
            writer_edge.writerow([str(edge[0]),str(edge[1]),'Undirected','',''])
            
    return 1


####################### 1. save giphi files with label from LABEL_QC #######################

if __name__ == "__main__":
    filename = root+'load_fast_graph_'+topic+'_True_1.pickle'
    dict_tweets, dict_nodes, dict_edges, dict_edge_weights, dict_rt_by_users = load_pickle(filename)
    # res = load_pickle(filename)
    # dict_nodes, dict_edges, dict_new_nodes, dict_new_edges, dict_user_to_id, dict_id_to_user, dict_new_nodes, dict_node_label_metis, _, dict_node_features, dict_new_edge_weights = res
    dict_node_label_sample = get_user_node_label()

    dict_new_lab = {}
    for k,v in dict_nodes.items():
        if(k in dict_node_label_sample.keys()):
            dict_new_lab[k] = dict_node_label_sample[k]
        else:
            dict_new_lab[k] = 100
            
    save_giphi_files(dict_new_lab, dict_nodes, dict_edges)
    
    
####################### 3. print nbs neighbors here for comu in label_QC ++ create json file with no nodes alone #######################

# if __name__ == "__main__":
#     # filename = path_2+'data.pickle'
#     # res = load_pickle(filename)
#     # dict_tweets, dict_nodes, dict_edges, dict_edge_weights, dict_rt_by_users, list_user_kept, dict_features_by_user, all_uniq_features, dict_text_by_user, dict_feat, embeddings, similarity, reduced_similarity, dict_reduced_embedding_by_user, dict_scaled_reduced_embedding_by_user, dict_labels_by_user, dict_count_by_features = res
#     dict_node_label_sample = get_user_node_label(type_nodes)
    
#     dict_usr_by_label = {}
#     for k, v in dict_node_label_sample.items():
#         if(v not in dict_usr_by_label):
#             dict_usr_by_label[v] = []
#         dict_usr_by_label[v].append(k)
        
#     new_dict_node_sample = print_stats_graph_inside_top_k(dict_usr_by_label, dict_features_by_user)
    
#     save_json(new_dict_node_sample)
        
    
    
    
####################### 2. print text from comu in label_QC #######################

# if __name__ == "__main__":
#     filename = path_2+'data.pickle'
#     res = load_pickle(filename)
#     dict_tweets, dict_nodes, dict_edges, dict_edge_weights, dict_rt_by_users, list_user_kept, dict_features_by_user, all_uniq_features, dict_text_by_user, dict_feat, embeddings, similarity, reduced_similarity, dict_reduced_embedding_by_user, dict_scaled_reduced_embedding_by_user, dict_labels_by_user, dict_count_by_features = res
#     dict_node_label_sample = get_user_node_label()
    
#     dict_usr_by_label = {}
#     for k, v in dict_node_label_sample.items():
#         if(v not in dict_usr_by_label):
#             dict_usr_by_label[v] = []
#         dict_usr_by_label[v].append(k)
        
#     print_random_tweets_by_label(dict_usr_by_label, dict_tweets, dict_nodes)
    
#     print('end')
        
    
####################### 3. print Stats neigbors from comu in label_QC #######################


# if __name__ == "__main__":
#     filename = path_2+'data.pickle'
#     res = load_pickle(filename)
#     dict_tweets, dict_nodes, dict_edges, dict_edge_weights, dict_rt_by_users, list_user_kept, dict_features_by_user, all_uniq_features, dict_text_by_user, dict_feat, embeddings, similarity, reduced_similarity, dict_reduced_embedding_by_user, dict_scaled_reduced_embedding_by_user, dict_labels_by_user, dict_count_by_features = res
#     dict_node_label_sample = get_user_node_label()
    
#     dict_usr_by_label = {}
#     for k, v in dict_node_label_sample.items():
#         if(v not in dict_usr_by_label):
#             dict_usr_by_label[v] = []
#         dict_usr_by_label[v].append(k)
        
#     print_stats_graph_by_label(dict_usr_by_label, dict_edges)
    
#     print('end')
                
      