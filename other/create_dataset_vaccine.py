#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan  9 14:12:30 2023

@author: samy
"""

import pandas as pd
import pickle
import csv


"""
load pickle data file
"""
def load_pickle(path_file):
    with open(path_file, 'rb') as file:
        data = pickle.load(file)
    return data

"""
write on a csv file
"""
def create_csv_file(df, filename):    
    with open(filename, mode='w') as nf:
        writer_node = csv.writer(nf, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        writer_node.writerow(list(df.columns))
    
        for k, item in df.iterrows():
            writer_node.writerow(item)
    return 1


topic = 'chloroquine'
columns = ['topic', 'user_id', 'tweet_id', 'clean_text', 'metis_label']

to_path = '/data/DEV_DATA/TWITTER API/other/data_complete_vaccine_chloro/data/my_data/LIWC/'+topic+'_LIWC.csv'

path_data = '/data/DEV_DATA/TWITTER API/other/data_complete_vaccine_chloro/data/data_objects/S_1A/'+topic+'/RETWEET/data.pickle'
dict_tweets, dict_nodes, _, _, _, _, dict_id_to_user, _, dict_node_label_metis, _, _, _ = load_pickle(path_data)

dict_node_label = {}
for k,v in dict_node_label_metis.items():
    dict_node_label[dict_id_to_user[k]] = v

dict_user_by_tweet = {}
for k,v in dict_nodes.items():
    if(v is not None):
        for tweet_id in v:
            dict_user_by_tweet[tweet_id] = k


df = pd.DataFrame([], columns=columns)

print('2')
for tweet_id, text in dict_tweets.items():
    if(tweet_id in dict_user_by_tweet.keys()):
        usr_id = dict_user_by_tweet[tweet_id]
        data = [topic, usr_id, tweet_id, text, dict_node_label[usr_id]]
        df.loc[len(df)] = data
        
create_csv_file(df, to_path)