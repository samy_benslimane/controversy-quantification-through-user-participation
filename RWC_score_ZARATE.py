import networkx as nx
import random,sys, numpy
from operator import itemgetter
import copy


"""
Create a nx weighted graph G from dict
"""
def create_graph_G(dict_edges, dict_edge_weights):
    G = nx.Graph()
    if(dict_edge_weights is None):
        print('Create graph w/ weights')
        list_edges = [v for k,v in dict_edges.items()]
        G.add_edges_from([list_edges])
    else:
        print('Create graph with edges, no weights')
        for e, w in dict_edge_weights.items():
            G.add_edge(e[0], e[1], weight=w)
            
    return G
    
"""
create 2 communities dict and list
"""
def separate_communites(dict_node_label):
    dict_right = {}
    right = []
    dict_left = {}
    left = []
    
    for n,v in dict_node_label.items():
        if(v==0):
            dict_right[n] = 1
            right.append(n)
        elif(v==1):
            dict_left[n] = 1
            left.append(n)
        else:
            print('WTFFF problem node label=',v)
    
    return dict_right, right, dict_left, left

#--------------------------------------------------------

"""
Get k random nodes from G
"""
def getRandomNodes(G,k): # parameter k = number of random nodes to generate
	nodes = G.nodes()
	random_nodes = {}
	for i in range(k):
		random_num = random.randint(0,len(nodes)-1)
		random_nodes[nodes[random_num]] = 1
	return random_nodes

"""
Get random nodes from either comu1, comu2 or equally from both side.
"""
def getRandomNodesFromLabels(G,k,flag,left,right): 
	random_nodes = []
	random_nodes1 = {}
	if(flag=="left"):
		for i in range(k):
			random_num = random.randint(0,len(left)-1)
			random_nodes.append(left[random_num])
	elif(flag=="right"):
		for i in range(k):
			random_num = random.randint(0,len(right)-1)
			random_nodes.append(right[random_num])
	else:
		for i in range(k/2):
			random_num = random.randint(0,len(left)-1)
			random_nodes.append(left[random_num])
		for i in range(k/2):
			random_num = random.randint(0,len(right)-1)
			random_nodes.append(right[random_num])
	for ele in random_nodes:
		random_nodes1[ele] = 1
        
	return random_nodes1

"""
Get nodes
- First, we take the nodes with the highest degree according to the "flag" (left, right)
- Second, we take the top $k$ nodes
"""
def getNodesFromLabelsWithHighestDegree(G,k,flag,dict_left,dict_right):
	random_nodes = {}
	dict_degrees = {}
	for node in G.nodes():
		dict_degrees[node] = G.degree(node)
	sorted_dict = sorted(dict_degrees.items(), key=itemgetter(1), reverse=True) # sorts nodes by degrees
#	sorted_dict = sorted_dict[:k]
	if(flag=="left"):
		count = 0
		for i in sorted_dict:
			if(count>k):
				break
			if(i[0] not in dict_left):
				continue
			random_nodes[i[0]] = i[1]
			count += 1
	elif(flag=="right"):
		count = 0
		for i in sorted_dict:
			if(count>k):
				break
			if(i[0] not in dict_right):
				continue
			random_nodes[i[0]] = i[1]
			count += 1
	else:
		count = 0
		for i in sorted_dict:
			if(count>k/2):
				break
			if(i[0] not in dict_left):
				continue
			random_nodes[i[0]] = i[1]
			count += 1
		count = 0
		for i in sorted_dict:
			if(count>k/2):
				break
			if(i[0] not in dict_right):
				continue
			random_nodes[i[0]] = i[1]
			count += 1

	return random_nodes

"""
return the side in which we ended ("left" node or a "right" node)
"""
def performRandomWalk(G,starting_node,user_nodes_side1,user_nodes_side2):
    dict_nodes = {} # contains unique nodes seen till now
    num_edges = len(G.edges())
    step_count = 0
#	total_other_nodes = len(user_nodes.keys())
    flag = 0
    side = ""
    max_stop = num_edges*10
    max_times = 10000
    
    last_visited = 0
    times = 0
    while(flag!=1):
        # print("starting from ", starting_node, "num nodes visited ", len(dict_nodes.keys()), " out of ", len(G.nodes()))
        neighbors = list(G.neighbors(starting_node))
        random_num = random.randint(0,len(neighbors)-1)
        starting_node = neighbors[random_num]
        dict_nodes[starting_node] = 1
        step_count += 1
        # print(step_count, '/', num_edges**2)
        if(starting_node in user_nodes_side1):
            side = "left"
            flag = 1
        if(starting_node in user_nodes_side2):
            side = "right"
            flag = 1

        # # if stuck
        # if(len(dict_nodes.keys()) == last_visited):
        #     if(times > max_times):
        #         break
        #     else:
        #         times+=1
        # else:
        #     last_visited = len(dict_nodes.keys())
        #     times = 0
        if(step_count>max_stop):
            break

    return side

# """
# Return the number of steps taken before reaching *ALL* node from the set of user nodes. 
# Difference from the above method is that we should reach all nodes, instead of just any one of them.
# """
# def performRandomWalkFull(G,starting_node,user_nodes): 
# 	dict_nodes = {} # contains unique nodes seen till now
# # 	nodes = G.nodes()
# 	num_edges = len(G.edges())
# 	step_count = 0
# 	total_other_nodes = len(user_nodes.keys())
# 	dict_already_seen_nodes = {}
# 	flag = 0

# 	while(flag!=1):
# 		# print "starting from ", starting_node, "num nodes visited ", len(dict_nodes.keys()), " out of ", len(nodes)
# 		neighbors = G.neighbors(starting_node)
# 		random_num = random.randint(0,len(neighbors)-1)
# 		starting_node = neighbors[random_num]
# 		dict_nodes[starting_node] = 1
# 		step_count += 1
# 		if(starting_node in user_nodes):
# 			dict_already_seen_nodes[starting_node] = 1
# 			if(len(dict_already_seen_nodes.keys())==total_other_nodes):
# 				flag = 1
# 		if(step_count>num_edges**2): # if stuck
# 			break
# 		if(step_count%100000==0):
# 			print(step_count, "steps reached")
            
# 	return step_count

"""
get dict nodes
"""
def getDict(nodes_list):
	dict_nodes = {}
	for node in nodes_list:
		dict_nodes[node] = 1
	return dict_nodes

"""
return random user_id not already called
"""
def get_random_node_unused(length, user_used):
    list_num_user = list(range(length))
        
    for u in user_used:
        list_num_user.remove(u)
        
    random_num = random.randint(0,len(list_num_user)-1)
    while list_num_user[random_num] in user_used:
        random_num = random.randint(0,len(list_num_user)-1)
    
    # random_num = random.randint(0,length-1)
    # while random_num in user_used:
    #     random_num = random.randint(0,length-1)       
        
    user_used.append(list_num_user[random_num])
    return list_num_user[random_num], user_used
    

"""
COMPUTE RWC SCORE
"""
def compute_rwc_score(G, dict_right, right, dict_left, left): 
    left_left = 0 # start_end
    left_right = 0
    right_right = 0
    right_left = 0
    no_find_side = 0
    
    total_rw = 20
    nbs_repetitions = 200
    percent = 0.1 # 0.1 0.5 1
    
    left_percent = int(percent*len(dict_left.keys()));
    right_percent = int(percent*len(dict_right.keys()));
    
    print('***** param rw *****')
    print('nbs rw: ', total_rw)
    print('nbs repetitions: ', nbs_repetitions)
    
    print('left_percent: ', left_percent)
    print('right_percent: ', right_percent)
    print('**** ********** ****')

    print('Start randomWalk...')
    
    print('  <<< random to random >>>')
    for j in range(1,total_rw+1):
        user_nodes_left = getRandomNodesFromLabels(G,left_percent,"left", left, right)
        user_nodes_right = getRandomNodesFromLabels(G,right_percent,"right", left, right)
        
        #1. Start left side 
        user_nodes_left_list = list(user_nodes_left.keys())
        if(j==1):
            print(' > len user_node_left: ', len(user_nodes_left_list))
        user_used = []
        for i in range(nbs_repetitions-1):
            if(len(user_nodes_left_list) == len(user_used)):
                user_used = []
            
            no, user_used = get_random_node_unused(len(user_nodes_left_list), user_used)
            node = user_nodes_left_list[no]
            other_nodes = user_nodes_left_list[:no] + user_nodes_left_list[no+1:]
            other_nodes_dict = getDict(other_nodes)
            side = performRandomWalk(G,node,other_nodes_dict,user_nodes_right)       
            if(side=="left"):
                left_left += 1 
            elif(side=="right"):
                left_right += 1
            else: # side == ""
                no_find_side += 1
                
        #1. Start Right side 
        user_nodes_right_list = list(user_nodes_right.keys())
        if(j==1):
            print(' > len user_node_right: ', len(user_nodes_right_list))
        user_used = [] 
        for i in range(nbs_repetitions-1): # for no in range(len(user_nodes_right_list)-1): 
            if(len(user_nodes_right_list) == len(user_used)):
                user_used = [] 
            
            no, user_used = get_random_node_unused(len(user_nodes_right_list), user_used)
            node = user_nodes_right_list[no]
            other_nodes = user_nodes_right_list[:no] + user_nodes_right_list[no+1:]
            other_nodes_dict = getDict(other_nodes)
            side = performRandomWalk(G,node,user_nodes_left,other_nodes_dict)
            if(side=="left"):
                right_left += 1
            elif(side=="right"):
                right_right += 1
            else: # side == ""
                no_find_side += 1
                
        # print('run... ', j , '/', total_rw)
        if(j%2 == 0):
            print('loading... ', round((j/total_rw)*100, 1), '% (', j, ')')

                
    print('...End randomWalk')


    total_run = no_find_side+left_left+left_right+right_right+right_left
    print("left -> left", left_left)
    print("left -> right", left_right)
    print("right -> right", right_right)
    print("right -> left", right_left)
    print("no find side (got stuck) : ", no_find_side)
    print('       (', round((no_find_side/total_run)*100, 2), '%)')
    print("Total: ", total_run)
    
    e1 = left_left*1.0/(left_left+right_left)
    e2 = left_right*1.0/(left_right+right_right)
    e3 = right_left*1.0/(left_left+right_left)
    e4 = right_right*1.0/(left_right+right_right)
    
    rwc_score = e1*e4 - e2*e3
    
    print("\n******************** -- % seed nodes " + str(percent) + " *********************")
    print('from left: ', e1, e2)
    print('from right: ', e3, e4)
    print("\n >> RWC_SCORE = ", rwc_score)

    """
    p_e1 = dict_degree[file2]
    p_e2 = 1 - p_e1
    
    e1_x = (e1*p_e1)/0.5
    e2_x = (e2*p_e2)/0.5
    e3_x = (e3*p_e1)/0.5
    e4_x = (e4*p_e2)/0.5
    
    print "--------------------------------------"
    
    print e1_x, e3_x
    print e2_x, e4_x
    
    print e1_x*e4_x - e2_x*e3_x
    """
    
    return rwc_score
