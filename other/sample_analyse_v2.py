#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 16 14:51:46 2023

@author: samy
"""

import matplotlib.pyplot as plt
import graph_processing as gp
import seaborn as sns
import numpy as np
import pandas as pd
import json
import os

def load_json(filename):
    with open(filename) as f:
        res = json.load(f)
    return res

data_zarate_labels = {'impeachment-5-10': 1,
                    'menciones-1-10enero': 1,
                    # 'mothersday': 0,
                    'menciones-20-27marzo': 1,
                    'area51': 0,
                    'OTDirecto20E': 0,
                    'bolsonaro27': 1,
                    'bolsonaro28': 1,
                    'VanduMuruganAJITH': 0,
                    'nintendo': 0,
                    'messicumple': 0,
                    'wrestlemania': 0,
                    'kingjacksonday': 0,
                    'kavanaugh06-08': 1,
                    'bolsonaro30': 1,
                    'notredam': 0,
                    'Thanksgiving': 0,
                    'halsey': 0,
                    'feliznatal': 0,
                    'kavanaugh16': 1,
                    'kavanaugh02-05': 1,
                    'EXODEUX': 0,
                    'bigil': 0,
                    'lula_moro_chats': 1,
                    'menciones-05-11abril': 1,
                    'LeadersDebate': 1,
                    'championsasia': 0,
                    'menciones05-11mayo': 1,
                    'pelosi': 1,
                    'SeungWooBirthday': 0,
                    'menciones-11-18marzo': 1}

####################### TODO
# For 1, 5, 10, 20:

#     - HOW DIFFICULT IT IS (TRAIN==30 topics) --> model = GRAPH_TEXT_B1_0
#         - one plot by topic -SCORE=avg-
#     		   + one line corresponds to % removed
#     		   + add parameter smooth=True (avg of the last 5 run)
            
#         - print mean(SCORE) for all epoch
        
#         - print(air sous la courbe)
        
# ************ 
    
#     - CORECTNESS (TRAIN==20 topics) --> model = GRAPH_TEXT_B1
#         - See if perf last model gets down when removing best
#         - See if perf last model gets up when removing worst
#         - Both if perf last model gets up when removing random

# ************ 
        
#     - ROBUSTNESS (TRAIN==20 topics) --> model = GRAPH_TEXT_B1
#         - how much topics bascule to the bad side

# path = '/data/DEV_DATA/TWITTER API/other/data_complete_vaccine_chloro/data/data_objects/USR_QC_connex/ZARATE/RETWEET/'
# sample_path = path + 'SAMPLE/'
# colors = ['red', 'blue', 'green', 'orange', 'purple', 'grey']
# score_to_keep = 'proba'
# #-----------
# TYPE_TRAIN = 'TRAIN_20' # 'TRAIN_20', 'TRAIN_30'
# list_type_user_discard = ['BEST', 'WORST', 'RANDOM'] # 'BEST', 'WORST', 'RANDOM'
# type_sample = [0, 10, 20, 50]

# topics = ['area51'] # 'kavanaugh16', 'area51'
# #-----------


# #------------------------------------------- DIFFICULTY -----------------------------------

# ###### PLOT 

# ######### by topic : 5 lines for 5 type_sample
# for type_user_discard in list_type_user_discard:
#     print('==================', type_user_discard, '==================')
#     for topic in topics:
#         print('----------', topic, '----------')
#         res_epoch_by_sample = {}
#         for ts in type_sample:
#             if(ts == 0):
#                 curr_path = sample_path + TYPE_TRAIN + '/' + str(ts) + '/'
#             else:
#                 curr_path = sample_path + TYPE_TRAIN + '/' + type_user_discard + '/' + str(ts) + '/'
            
#             filename = curr_path+'results_epochs_by_topic_'+str(ts)+'.json'
#             if(os.path.isfile(filename) == False):
#                 print(' >', ts, type_user_discard, 'does not exist !')
#                 continue
            
#             results_by_topic_by_epoch = gp.load_json(filename)
#             res_epoch_by_sample[ts] = results_by_topic_by_epoch[topic]
            
    
#         # Plotting the lines
#         cpt=0
#         avg_score_by_sample = {}
#         for ts, dict_score in res_epoch_by_sample.items():
#             list_score = [v[score_to_keep] for k,v in dict_score.items()]
#             x = [i*2 for i in list(range(len(list_score)))]
            
#             avg_score_by_sample[ts] =  [round(sum(list_score) / len(list_score), 3), list_score[-1]]
            
#             plt.plot(x, list_score, color=colors[cpt], label='sample -'+str(ts)+' | '+type_user_discard)
#             cpt+=1
        
#         # Adding a legend
#         plt.legend()
        
#         # Displaying the plot
#         plt.show()
        
#         for ts, score in avg_score_by_sample.items():
#             print('--', ts, ':', score[0], ' (', score[1] ,')')
#     print('\n')

    
    
########## by type_sample : 2 plot (1 avec les 5C; 1 avec les 5NC)


#------------------------------------------- COHERANCE -----------------------------------

# base_model = 0
# TYPE_TRAIN = 'TRAIN_20'
# type_sample_to_compare = [20, 50]
# type_user_discard = ['WORST'] # add 'BEST', 'WORST', 'RANDOM'


# print('******************** 0 vs', type_sample_to_compare, '********************')
# # Get data    
# results = {}
# for ts in [base_model]+type_sample_to_compare:
#     for type_user in type_user_discard:
#         if(ts == base_model):
#             curr_path = sample_path + TYPE_TRAIN + '/' + str(ts) + '/'
#         else:
#             curr_path = sample_path + TYPE_TRAIN + '/' + type_user + '/' + str(ts) + '/'
        
#         res = gp.load_json(curr_path+'results.json')
#         res = res['TEST']['qc_score']
#         topics = res.keys()
        
#         if(ts not in results.keys()):
#             results[ts] = {}
#         results[ts][type_user] = {k:v[score] for k,v in res.items()}                   
 
# # Print data
# for topic in topics:
#     print('----------', topic, '----------')
#     for ts in type_sample_to_compare:
#         for type_user in type_user_discard:
#             default = results[base_model][type_user][topic]
#             sample = results[ts][type_user][topic]
#             if(type_user == 'BEST'):
#                 value = default > sample
#             elif(type_user == 'WORST'):
#                 value = default > sample
#             elif(type_user == 'RANDOM'):
#                 value = default > sample #should be both
                
#             print('**', ts, '/', type_user, ':', default, 'TO', sample, '-->', value)


# ===========================================================================================================================
# =============================================== TRAIN ALL topics every time ===============================================
            
    
# path = '/data/DEV_DATA/TWITTER API/other/data_complete_vaccine_chloro/data/data_objects/USR_QC_connex/ZARATE/RETWEET/'
# sample_path = path + 'SAMPLE/'
# colors = ['red', 'blue', 'green', 'orange', 'purple', 'grey']
# score_to_keep = 'proba'
# #-----------
# TYPE_TRAIN = 'TRAIN_30' # 'TRAIN_20', 'TRAIN_30', 'TRAIN_30_cut'
# list_type_user_discard = ['BEST', 'WORST']#['BEST', 'WORST'] # ['BEST_WORST'] #, 'WORST', 'RANDOM'] # 'BEST', 'WORST', 'RANDOM'
# type_sample = [0,25] #2 by 2

# # topics = ['pelosi', 'kavanaugh16', 'area51', 'messicumple'] # 'pelosi', 'kavanaugh16', 'area51', 'pelosi', 'messicumple'
# to_plot = True
# #-----------


# #------------------------------------------- DIFFICULTY -----------------------------------

# ##### PLOT 

# ####### by topic : 5 lines for 5 type_sample
# all_scores = {} # topic : {sampled_best_avg:v, sampled_worst_avg:v, normal_avg:v}
# for type_user_discard in list_type_user_discard:
#     print('==================', type_user_discard, '==================')
#     for topic, label in data_zarate_labels.items():
#         # print('----------', topic, '----------')
#         res_epoch_by_sample = {}
#         for ts in type_sample:
#             if(ts == 0):
#                 curr_path = sample_path + TYPE_TRAIN + '/' + str(ts) + '/'
#             else:
#                 curr_path = sample_path + TYPE_TRAIN + '/' + type_user_discard + '/' + str(ts) + '/'
            
#             filename = curr_path+'dict_scores_by_epoch_'+str(ts)+'.json'
#             if(os.path.isfile(filename) == False):
#                 print(' >', ts, type_user_discard, 'does not exist !')
#                 continue
            
#             results_by_topic_by_epoch = gp.load_json(filename)
#             res_epoch_by_sample[ts] = results_by_topic_by_epoch[topic]
            
    
#         # Plotting the lines
#         cpt=0
#         avg_score_by_sample = {}
#         for ts, dict_score in res_epoch_by_sample.items():
#             list_score = [v[score_to_keep] for k,v in dict_score.items()]
#             x = [i*2 for i in list(range(len(list_score)))]    
#             avg_score_by_sample[ts] =  [round(sum(list_score) / len(list_score), 3), list_score[-1]]    
#             if(to_plot==True):
#                 plt.plot(x, list_score, color=colors[cpt], label='sample -'+str(ts)+' | '+type_user_discard)
#             cpt+=1
#         if(to_plot==True):
#             # Adding topic as title
#             plt.title(topic)

#             # Adding a legend
#             plt.legend()
#             # Displaying the plot
#             plt.show()
        
#         if(topic not in all_scores.keys()):
#             all_scores[topic] = {}
        
#         if(type_user_discard == 'BEST'):
#             all_scores[topic]['sampled_best_avg'] = avg_score_by_sample[type_sample[1]][0]
#         if(type_user_discard == 'WORST'):
#             all_scores[topic]['sampled_worst_avg'] = avg_score_by_sample[type_sample[1]][0]
#         if(type_user_discard == 'BEST_WORST'):
#             if(label == 1):
#                 all_scores[topic]['sampled_best_avg'] = avg_score_by_sample[type_sample[1]][0]
#             if(label == 0):
#                 all_scores[topic]['sampled_worst_avg'] = avg_score_by_sample[type_sample[1]][0]
#         all_scores[topic]['normal_avg'] = avg_score_by_sample[type_sample[0]][0]
            
        
        
        
#         # for ts, score in avg_score_by_sample.items():
#         #     print('--', ts, ':', score[0], ' (', score[1] ,')')
            
#     print('\n')

    
# print('*** Get accuracy (avg_by_epoch) ***')
    

# # pour 15 BEST (les C) et 15 WORST (les NC)
# first = True
# correct = []
# dict_sorted_topic = dict(sorted(data_zarate_labels.items(), key=lambda item: item[1]))
# for topic, label in dict_sorted_topic.items():
#     scores = all_scores[topic]
#     if(label == 1):
#         if(first == True):
#             print("\n")
#             first=False
#         sc = scores['sampled_best_avg'] 
#         res = scores['sampled_best_avg'] <= scores['normal_avg']
#         print('>',label,'--', topic, ':', sc, '<', scores['normal_avg'], '==', res)

#     elif(label == 0):
#         sc = scores['sampled_worst_avg'] 
#         res = scores['sampled_worst_avg'] >= scores['normal_avg']
#         print('>',label,'--', topic, ':', sc, '>', scores['normal_avg'], '==', res)
#     correct.append(res)
    
    
# print('\n >> FINAL ACC (30) = ', len([c for c in correct if(c==True)])/len(correct), len([c for c in correct if(c==True)]), '/', len(correct))

# # # **************

# # # pour 30 BEST et 30 WORST
# # if('BEST_WORST' not in list_type_user_discard):
# #     correct = []
# #     for topic, label in data_zarate_labels.items():
# #         scores = all_scores[topic]
# #         res = scores['sampled_best_avg'] <= scores['normal_avg']
# #         correct.append(res)
# #         res = scores['sampled_worst_avg'] >= scores['normal_avg']
# #     correct.append(res)
# #     print('\n >> FINAL ACC (60) = ', len([c for c in correct if(c==True)])/len(correct), len([c for c in correct if(c==True)]), '/', len(correct))
    
            
    

    
########## by type_sample : 2 plot (1 avec les 5C; 1 avec les 5NC)


#------------------------------------------- COHERANCE -----------------------------------

# base_model = 0
# TYPE_TRAIN = 'TRAIN_30'
# type_sample_to_compare = [10, 20, 50]
# type_user_discard = ['WORST'] # add 'BEST', 'WORST', 'RANDOM'

# only_show_correct_label = True

# print('******************** 0 vs', type_sample_to_compare, '********************')
# # Get data    
# results = {}
# for ts in [base_model]+type_sample_to_compare:
#     for type_user in type_user_discard:
#         if(ts == base_model):
#             curr_path = sample_path + TYPE_TRAIN + '/' + str(ts) + '/'
#         else:
#             curr_path = sample_path + TYPE_TRAIN + '/' + type_user + '/' + str(ts) + '/'
        
#         res = gp.load_json(curr_path+'results.json')
#         res = res['TEST']['qc_score']
#         topics = res.keys()
        
#         if(ts not in results.keys()):
#             results[ts] = {}
#         results[ts][type_user] = {k:v[score_to_keep] for k,v in res.items()}                   
 
# # Print data
# for topic in topics:
#     print('----------', topic, '----------')
#     for ts in type_sample_to_compare:
#         for type_user in type_user_discard:
#             if(((data_zarate_labels[topic] == 1 and type_user == 'BEST') or
#                 (data_zarate_labels[topic] == 0 and type_user == 'WORST') or
#                 (type_user == 'RANDOM')) or only_show_correct_label == False):
#                 default = results[base_model][type_user][topic]
#                 sample = results[ts][type_user][topic]
#                 if(type_user == 'BEST'):
#                     value = default > sample
#                 elif(type_user == 'WORST'):
#                     value = default > sample
#                 elif(type_user == 'RANDOM'):
#                     value = default > sample #should be both
                    
#                 print('**', ts, '/', type_user, ':', default, 'TO', sample, '-->', value)
    
    
    


#--------------------------- AVG OF ALL TOPICS, BY XP, BY LABEL

path = '/data/DEV_DATA/TWITTER API/other/data_complete_vaccine_chloro/data/data_objects/USR_QC_connex/ZARATE/RETWEET/'
sample_path = path + 'SAMPLE/'
colors = ['red', 'blue', 'green', 'orange', 'purple', 'grey']
score_to_keep = 'proba'
#-----------
TYPE_TRAIN = 'TRAIN_30' # 'TRAIN_20', 'TRAIN_30', 'TRAIN_30_cut'
list_type_user_discard = ['BEST', 'WORST']#['BEST', 'WORST'] # ['BEST_WORST'] #, 'WORST', 'RANDOM'] # 'BEST', 'WORST', 'RANDOM'
type_sample = [0,5,10,15,20] #2 by 2
to_plot=False

TYPE_TO_SEE = 'REAL_SCORE' # 'AVG_TRAINING', 'REAL_SCORE'
#-----------

##### PLOT 

####### by topic : 5 lines for 5 type_sample

#### 1. GET ALL SCORES
# BEST : pelosi : 0 : score
print('step1.')
all_scores = {}
for type_user_discard in list_type_user_discard:
    if(type_user_discard not in all_scores.keys()):
        all_scores[type_user_discard] = {}
    
    print('==================', type_user_discard, '==================')
    for topic, label in data_zarate_labels.items():
        if(topic not in all_scores[type_user_discard].keys()):
            all_scores[type_user_discard][topic] = {}
        
        res_epoch_by_sample = {}
        for ts in type_sample:            
            curr_path = sample_path + TYPE_TRAIN + '/'
            
            if(ts==0):
                filename = curr_path+'dict_scores_by_epoch_'+str(ts)+'.json'
            else:
                filename = curr_path+'dict_scores_by_epoch_'+str(ts)+'_'+str(type_user_discard)+'.json'
                
            if(os.path.isfile(filename) == False):
                print(filename)
                print(' >', ts, type_user_discard, 'does not exist !')
                continue
            
            results_by_topic_by_epoch = gp.load_json(filename)
            res_epoch_by_sample[ts] = results_by_topic_by_epoch[topic]
            
    
        # Plotting the lines
        cpt=0
        avg_score_by_sample = {}
        for ts, dict_score in res_epoch_by_sample.items():
            list_score = [v[score_to_keep] for k,v in dict_score.items()]
            # avg_score_by_sample[ts] =  [round(sum(list_score) / len(list_score), 3), list_score[-1]]
            if(topic not in all_scores[type_user_discard][topic].keys()):
                if(TYPE_TO_SEE=='AVG_TRAINING'):
                    all_scores[type_user_discard][topic][ts] = round(sum(list_score) / len(list_score), 3)
                elif(TYPE_TO_SEE=='REAL_SCORE'):
                    all_scores[type_user_discard][topic][ts] = list_score[-1]
            else:
                print('wtff')
            cpt+=1


#### 2. AGGREGATE BY ELEMENT
# BEST : 0 : avg(15topics)
print('step2.')
new_scores = {}
for type_user_discard, d1 in all_scores.items():
    if(type_user_discard not in new_scores.keys()):
        new_scores[type_user_discard] = {}
        
    for topic, d2 in d1.items():
        label = data_zarate_labels[topic]
        for ts, score in d2.items():
            if(ts not in new_scores[type_user_discard].keys()):
                new_scores[type_user_discard][ts] = []
            if(label == 0 and type_user_discard == 'WORST'): #and topic in topic_to_look):
                new_scores[type_user_discard][ts].append(score)
            elif(label == 1 and type_user_discard == 'BEST'): # and topic in topic_to_look):
                new_scores[type_user_discard][ts].append(score)

    
    #averaging
    for ts, scores in new_scores[type_user_discard].items():
        scores = np.array(scores)
        new_scores[type_user_discard][ts] = [np.mean(scores), np.std(scores)] 

#### 3. PLOT BY XP & LABEL
print('step3.')
x = []
y0 = []
yerr0 = []
y1 = []
yerr1 = []
for type_user_discard, d1 in new_scores.items(): 
    x = list(d1.keys())
    for ts, sc in d1.items():
        if(type_user_discard == 'WORST'):
            y0.append(sc[0])
            yerr0.append(sc[1])
        elif(type_user_discard == 'BEST'):
            y1.append(sc[0])
            yerr1.append(sc[1])
            

#plot normal          
# for type_user_discard in list_type_user_discard:
#     if(type_user_discard == 'WORST'):
#         # plt.plot(x, y0, color=colors[0], label='NC - WORST')
#         plt.errorbar(x, y0, yerr0, color=colors[0], linestyle='-', marker='^')
#     if(type_user_discard == 'BEST'):
#         plt.errorbar(x, y1, yerr1, color=colors[1], linestyle='-', marker='^')
#         # plt.plot(x, y1, color=colors[1], label='C - BEST')
# plt.title('plot progressions of samples')
# plt.legend()
# plt.show()

#plot seaborn
# Création du DataFrame
data0 = pd.DataFrame({'x': x, 'y': y0, 'ci': yerr0})
data1 = pd.DataFrame({'x': x, 'y': y1, 'ci': yerr1})


# Création du plot avec Seaborn
palette = sns.color_palette("pastel")
sns.set_style('whitegrid')
plt.figure(figsize=(10, 6))
sns.lineplot(x='x', y='y', data=data0, marker='o', label='Controversial topics')

plt.fill_between(data0['x'], data0['y'] - data0['ci'], data0['y'] + data0['ci'], alpha=0.3, color=palette[0])
# plt.errorbar(x, y0, yerr=yerr0, fmt='none', capsize=4, color='grey', label='Error Margin')

sns.lineplot(x='x', y='y', data=data1, marker='o', label='Non-controversial topics')
plt.fill_between(data1['x'], data1['y'] - data1['ci'], data1['y'] + data1['ci'], alpha=0.3, color=palette[1])
# plt.errorbar(x, y1, yerr=yerr1, fmt='none', capsize=4, color='grey')


plt.xlabel('% removed users')
if(TYPE_TO_SEE=='AVG_TRAINING'):
    plt.ylabel('Average training epoch USR_QS by topic')
elif(TYPE_TO_SEE=='REAL_SCORE'):
    plt.ylabel('AVG USR_QS score by topic')
plt.legend()
plt.show()


