#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 21 14:55:02 2023

@author: samy
"""

import RWC_score_ZARATE as rwc
import dipole_GARIMELLA as dg

from sklearn.metrics import silhouette_score, davies_bouldin_score

import numpy as np 
import pickle
import json
import time
import os

import networkx as nx
# from scipy.stats import entropy
from scipy.spatial import distance


"""
save as pickle file
"""
def pickle_save(path, data):
    with open(path, 'wb') as file:
        pickle.dump(data, file, protocol=pickle.HIGHEST_PROTOCOL)
    return 1

"""
write in json file
"""
def write_json(filename, res):
    with open(filename, 'w') as f:
        json.dump(res, f)
    return 1

"""
load json file
"""
def load_json(filename):
    with open(filename) as f:
        res = json.load(f)
    return res

#-----------------------------------------------------------------

"""
get baseline score of graphs

+ type_score:['rwc_score', 'betweeness', 'dipole']

"""
def get_baseline_score(key, dict_edges, dict_edge_weights, dict_node_label, result_file, type_scores=['rwc_score']):
    for type_score in type_scores:
        print('--', type_score, '--')
        results = {}
        if(os.path.isfile(result_file) == True):
            results = load_json(result_file)
        
        if(key in results.keys() and type_score in results[key]):
            print('<<', type_score, 'for', key, 'already computed !! >>')
            print(type_score, ':', results[key][type_score])
        else:
            t = time.time()
            if(type_score == 'rwc_score'):
                print('1. get graph G')
                G = rwc.create_graph_G(dict_edges, dict_edge_weights)
                print('2. get commu')
                dict_right, right, dict_left, left = rwc.separate_communites(dict_node_label)
                print('3. get rwc score')
                score = rwc.compute_rwc_score(G, dict_right, right, dict_left, left)
                
            elif(type_score == 'betweeness'):
                score = compute_edge_betweeness(dict_edges, dict_node_label)
                
            elif(type_score == 'dipole_GAR'):
                score = compute_dipole_moment_GAR(dict_edges, dict_node_label)
                
            elif(type_score == 'dipole_GPT'):
                score = compute_dipole_moment_GPT(dict_edges, dict_node_label)
                
            runtime = round(((time.time() - t)/60), 2)
            print('  >> time score', type_score, runtime, 'min, score = ', score)
            print(" \n +++++++++++++++++++++++++++++++++++++++++++++++++++++ \n")

            if(key not in results.keys()):
               results[key] = {}
            results[key][type_score] = [score, runtime]
            
            write_json(result_file, results)
        
    return results


"""
GET Quantification score from embeddings of our users
    + type_score : ['silhouette_score', 'CS_zarate', 'CS_zarate_ALL', 'DB_index']
    
    + format results : d = {'2018-09-09': {'rwc_score'=0.25, 'metis_label': {'sil_score' = 0.5, 'CS_zarate' = 0.7}}}

"""
def get_score(dict_node_embeddings, dict_node_label, dict_edges, type_score):
    score = -1000
    t = time.time()
    if(type_score == 'silhouette_score'):
        score = get_silhouette_score(dict_node_embeddings, dict_node_label)
    elif(type_score == 'CS_zarate'):
        score = get_CS_zarate_score(dict_node_embeddings, dict_node_label, dict_edges, only_central_users = True)
    elif(type_score == 'CS_zarate_ALL'):
        score = get_CS_zarate_score(dict_node_embeddings, dict_node_label, dict_edges, only_central_users = False)
    elif(type_score == 'DB_index'):
        score = get_db_index(dict_node_embeddings, dict_node_label)
        
    runtime = round(((time.time() - t)/60), 2)
    print('  >> time score', type_score, runtime, 'min, score = ', score)
    
    return [score, runtime]


"""
Get silhouette score
"""
def get_silhouette_score(dict_node_embeddings, dict_node_label, metric='cosine'):
    X = []
    labels = []
    i=0
    for k,v in dict_node_label.items():
        X.append(np.asarray(dict_node_embeddings[k]))
        labels.append(v)
        i+=1
        
    sil_score = silhouette_score(np.stack(X), np.stack(labels), metric=metric)
    print('--> sil_score:', sil_score)
    
    return sil_score

"""
Get Davies–Bouldin index
"""
def get_db_index(dict_node_embeddings, dict_node_label):
    X = []
    labels = []
    i=0
    for k,v in dict_node_label.items():
        X.append(np.asarray(dict_node_embeddings[k]))
        labels.append(v)
        i+=1

    db_index = davies_bouldin_score(np.stack(X), np.stack(labels))
    print('--> db_index:', db_index)
    
    return db_index
    

"""
get controversy score
"""
def get_CS_zarate_score(dict_node_embeddings, dict_node_label, dict_edges, only_central_users = True):
    if(only_central_users == True):
        print(' > top 30% central users')      
        G=nx.Graph()
        G.add_edges_from([v for _, v in dict_edges.items()])
        users = hits(G, len(dict_node_embeddings))
    else:
        print(' > all users')
        users = dict_node_embeddings.keys()
    
    tweetsc1 = []
    tweetsc2 = []
    X = []
    for k in users:
        emb = dict_node_embeddings[k]
        if(dict_node_label[k] == 0):
            tweetsc1.append(emb)
        elif(dict_node_label[k] == 1):
            tweetsc2.append(emb)

        X.append(emb)    
    X = np.stack(tweetsc1)
    c1 = np.stack(tweetsc1)
    c2 = np.stack(tweetsc2)
    
    # calculate centroids
    cent1 = c1.mean(axis=0)
    cent2 = c2.mean(axis=0)
    cent0 = X.mean(axis=0)
    
    v = np.cov(X.T)
    SS0 = 0
    for row in X:
        #SS0 = SS0 + distance.mahalanobis(row,cent0,v)
        SS0 = SS0 + distance.cosine(row,cent0)
        #SS0 = SS0 + distance.euclidean(row,cent0)
        #SS0 = SS0 + distance.cityblock(row,cent0)
    v = np.cov(c1.T)
    SS1 = 0
    for row in c1:
        #SS1 = SS1 + distance.mahalanobis(row,cent1,v)
        SS1 = SS1 + distance.cosine(row,cent1)
        #SS1 = SS1 + distance.euclidean(row,cent1)
        #SS1 = SS1 + distance.cityblock(row,cent1)
    v = np.cov(c2.T)
    SS2 = 0
    for row in c2:
        #SS2 = SS2 + distance.mahalanobis(row,cent2,v)
        SS2 = SS2 + distance.cosine(row,cent2)
        #SS2 = SS2 + distance.euclidean(row,cent2)
        #SS2 = SS2 + distance.cityblock(row,cent2)
        
    # Controversy index
    CS_zarate = (SS1+SS2)/SS0
    print('--> CS_ZARATE:', CS_zarate)
    
    return CS_zarate

"""
run hits algorithm
"""
def hits(G, nbs_users):
    hubs, authorities = nx.hits(G, max_iter = 1000, tol=1e-01, normalized = True)
    
    central_users_hubs = dict(sorted(hubs.items(), key=lambda item: item[1], reverse=True))
    central_users_hubs = list(central_users_hubs.keys())[:int(nbs_users * 0.3)]
    
    central_users_authority = dict(sorted(authorities.items(), key=lambda item: item[1], reverse=True))
    central_users_authority = list(central_users_authority.keys())[:int(nbs_users * 0.3)]

    users = central_users_hubs+central_users_authority
    users = list(set(users))
    print(' > len HITS: hubs/auth/total', len(central_users_hubs), len(central_users_authority), len(users))
    return users

#--------------------------------------------------------------------------------------------------

"""
compute betweeness score [BASELINE]
"""
def compute_edge_betweeness(dict_edges, dict_node_label):   
    t = time.time()
    #Step1: compute edge betweeness
    print('step1.')
    G=nx.Graph()
    G.add_edges_from([v for _, v in dict_edges.items()])
    dict_edgebetweenness = nx.edge_betweenness_centrality(G,normalized=True)
    print('  >>', round(((time.time() - t)/60), 2), 'min')
    
    print('step2.')
    t = time.time()
    c0 = [k for k,v in dict_node_label.items() if(v==0)]
    c1 = [k for k,v in dict_node_label.items() if(v==1)]
    
    eb_list = []
    
    for u0 in c0:
    	for u1 in c1:
    		if(G.has_edge(u0,u1) or G.has_edge(u1,u0)):
    			if((u0,u1) in dict_edgebetweenness):
    				edge_betweenness = dict_edgebetweenness[(u0,u1)]
    				eb_list.append(edge_betweenness)
    			else:
    				edge_betweenness = dict_edgebetweenness[(u1,u0)]
    				eb_list.append(edge_betweenness)    
    eb_list1 = np.asarray(eb_list)      
    print("length of cut", len(eb_list))
    print("length of cut/num edges", len(eb_list)*1.0/len(G.edges()))    
    print('  >>', round(((time.time() - t)/60), 2), 'min')
   
    print('step3.')
    t = time.time()
    eb_list_all = []
    for edge, btwness in dict_edgebetweenness.items():
    	eb_list_all.append(btwness)
    eb_list_all1 = np.asarray(eb_list_all)
    
    # print(entropy(eb_list,eb_list_all))
    score = np.median(eb_list1)/np.median(eb_list_all1)
    print('  >>', round(((time.time() - t)/60), 2), 'min')
    
    # eb_list2 = []
    # for eb in eb_list:
    # 	eb_list2.append(eb/np.max(eb_list1))
    # eb_list2 = np.asarray(eb_list2)
    # print("Mean edge betweenness on the cut", np.mean(eb_list2), "Median", np.median(eb_list2), "\n")

    return score
    
#-------------------------------------------------------
"""
compute dipole moment score [BASELINE]
"""
def compute_dipole_moment_GAR(dict_edges, dict_node_label):   
    #step0: prepare data
    G=nx.Graph()
    G.add_edges_from([v for _, v in dict_edges.items()])
    
    dict_c0 = {k:1 for k,v in dict_node_label.items() if(v==0)}
    dict_c1 = {k:1 for k,v in dict_node_label.items() if(v==1)}
    
    #step1: get G, and highest degree nodes for each community
    G = convertIntoDirected(G)
    
    seed_nodes_num = int(len(G.nodes())*0.05); # seed nodes percenatage = 5 percent
    print(" > selecting ", seed_nodes_num, " seed nodes from each side")
    
    left_seed_nodes = dg.getNodesFromLabelsWithHighestDegree(G,seed_nodes_num, 0, dict_c0, dict_c1)
    right_seed_nodes = dg.getNodesFromLabelsWithHighestDegree(G,seed_nodes_num, 1, dict_c0, dict_c1)
    
    #step2: Get top-k user label of each commu
    for node in G.nodes():
        if(node in left_seed_nodes.keys()):
            l=1
        elif(node in right_seed_nodes.keys()):
            l=-1
        else:
            l=0
        G.nodes[node]['ideo'] = l

    #step3: compute dipole score
    ideos = nx.get_node_attributes(G,'ideo');
    corenode = [];
    
    for key in ideos.keys():
    	if(ideos[key]==1 or ideos[key]==-1):
    		corenode.append(key);
    
    v_current = dg.get_model(G,corenode)
    p1,DA,D, dipole = dg.GetPolarizationIndex(v_current)
    
    print("dipole scores:", p1, DA, D, dipole)

    return dipole
    


"""
compute dipole moment score [BASELINE]
"""
def compute_dipole_moment_GPT(dict_edges, dict_node_label):   
    #step0: prepare graph G
    G=nx.Graph()
    G.add_edges_from([v for _, v in dict_edges.items()])
    G = convertIntoDirected(G)

    #step1: get the highest degree nodes for each community
    top_k = int(len(G.nodes())*0.05); # seed nodes percenatage = 5 percent
    print(" > selecting ", top_k, " seed nodes from each side")
    
    community_1 = []
    community_2 = []
    for node in G.nodes():
        if dict_node_label[node] == 0:
            community_1.append((node, G.degree(node)))
        else:
            community_2.append((node, G.degree(node)))
    community_1 = sorted(community_1, key=lambda x: x[1], reverse=True)[:top_k]
    community_2 = sorted(community_2, key=lambda x: x[1], reverse=True)[:top_k]
        
    #step2: calculer center of the mass
    # Calculate the center of mass of community 1
    com_1 = np.array([0.0, 0.0])
    for node in community_1:
        pos = np.array(list(nx.spring_layout(G, k=0.1, seed=42)[node[0]]))
        com_1 += node[1] * pos
    com_1 /= sum([node[1] for node in community_1])
    
    # Calculate the center of mass of community 2
    com_2 = np.array([0.0, 0.0])
    for node in community_2:
        pos = np.array(list(nx.spring_layout(G, k=0.1, seed=42)[node[0]]))
        com_2 += node[1] * pos
    com_2 /= sum([node[1] for node in community_2])
    
    #step3: calculer center of the mass
    dipole_moment = np.linalg.norm(com_1 - com_2)

    return dipole_moment


"""
convert graph G to directed graph
"""
def convertIntoDirected(G):
    for edge in G.edges():
        node1 = edge[0] 
        node2 = edge[1] 
        G.add_edge(node2,node1) 
    return G



def get_rwc_score_for_sampled_graph(dict_edges, dict_edge_weights, dict_node_label):
    print('1. get graph G')
    G = rwc.create_graph_G(dict_edges, dict_edge_weights)
    print('2. get commu')
    dict_right, right, dict_left, left = rwc.separate_communites(dict_node_label)
    print('3. get rwc score')
    score = rwc.compute_rwc_score(G, dict_right, right, dict_left, left)
    
    return score