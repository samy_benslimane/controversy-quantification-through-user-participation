#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 17 15:09:33 2021

@author: samy
"""

#!/usr/bin/env python
# coding: utf-8

from transformers import AutoModel, AutoModelForSequenceClassification, AutoTokenizer
import torch.nn.functional as F

from torch.utils.data import DataLoader
from torch import nn
import torch

from tqdm import tqdm
import numpy as np
import util as ut

import copy
import os



"""
Get text features from list of texts
"""
def get_tokens_by_text(inputs, type_model, batch_bert=64):
    
    tokenizer = get_tokenizer(type_model)
    
    #create dataloader
    dataloader = DataLoader(inputs, batch_size=batch_bert, shuffle=False)
    
    #Get prediction
    tokens = []
    for batch in tqdm(dataloader, total=len(dataloader)):
        tokenized_batch = tokenizer(text=batch, 
                                    return_tensors = 'pt',
                                    add_special_tokens=True,  # Add [CLS] 101 and [SEP] 102
                                    max_length = 256,  # maximum length of a sentence
                                    pad_to_max_length=True,  # Add [PAD]s
                                    padding='max_length')

        for cpt in range(len(tokenized_batch['input_ids'])):
            temp = {'input_ids': tokenized_batch['input_ids'][cpt],
                    'token_type_ids': tokenized_batch['token_type_ids'][cpt],
                    'attention_mask': tokenized_batch['attention_mask'][cpt]}
            tokens.append(temp)
        
    return tokens



"""
Get text features from list of texts
"""
def get_text_features(inputs, type_model, device, bert_aggregator, batch_bert=64):
    print('-- bert_aggregator:',bert_aggregator, '--')
    
    tokenizer = get_tokenizer(type_model)
    model = get_model(type_model, bert_aggregator)
    model = model.to(device)
    
    #create dataloader
    dataloader = DataLoader(inputs, batch_size= batch_bert, shuffle=False)
    
    #Get prediction
    text_labels_logits = None
    text_embeddings, text_labels = [], []
    for batch in tqdm(dataloader, total=len(dataloader)):
        tokenized_batch = tokenizer(text=batch, 
                                    return_tensors = 'pt',
                                    add_special_tokens=True,  # Add [CLS] 101 and [SEP] 102
                                    max_length = 256,  # maximum length of a sentence
                                    pad_to_max_length=True,  # Add [PAD]s
                                    padding='max_length')

        tokenized_batch = {k:v.type(torch.long).to(device) for k,v in tokenized_batch.items()}
        kept_mask = get_tokens_keep_mask(tokenized_batch, device)
        
        with torch.no_grad():
            logits, embedding = model(tokenized_batch, kept_mask)
            text_embeddings += embedding.tolist()
            if(logits is not None):
                text_labels += logits.argmax(axis=-1).flatten().tolist()
                if(text_labels_logits is None):
                    text_labels_logits = logits
                else:
                    text_labels_logits = np.concatenate((text_labels_logits, logits), axis=0)

    if(logits is None):
        text_labels = [None] * len(text_embeddings)
        text_labels_logits = [None] * len(text_embeddings)
    else:
        text_labels_logits = text_labels_logits.tolist()
    
    return text_embeddings, text_labels, text_labels_logits

"""
get only the mask for token wanted
"""
def get_tokens_keep_mask(inputs, device):    
    batch_size = inputs['input_ids'].shape[0]
    
    kept_mask = None
    for s in range(batch_size):       
        new_mask = copy.deepcopy(inputs['input_ids'][s])
        new_mask[new_mask==101] = 0
        new_mask[new_mask==102] = 0
        new_mask[new_mask != 0] = 1
        
        mask = new_mask > 0
        mask = mask.view(1,-1)
            
        if(kept_mask is None):
            kept_mask = copy.deepcopy(mask)
        else:
            kept_mask = torch.cat((kept_mask, mask), 0)
       
    return kept_mask

###################################################### Get objects #######################################################


"""
Get tokenizer corresponding to the model
"""
def get_tokenizer(type_model):
    print(' < get tokenizer: ', type_model, '>')
    
    if(type_model == ut.BASIC):
        model_name = ut.basic_hugging_face
        tokenizer_path = ut.basic_tokenizer
    elif(type_model == ut.SENTIMENT):
        model_name = ut.sentiment_hugging_face
        tokenizer_path = ut.sentiment_tokenizer
    elif(type_model == ut.ARGUMENT):
        model_name = ut.argument_hugging_face
        tokenizer_path = ut.argument_tokenizer
    elif(type_model == ut.EMOTION):
        model_name = ut.emotion_hugging_face
        tokenizer_path = ut.emotion_tokenizer
    
    if(os.path.isdir(tokenizer_path)==True):
        print('read tokenizer>>')
        tokenizer = AutoTokenizer.from_pretrained(tokenizer_path)
    else:
        print('upload tokenizer>')
        tokenizer = AutoTokenizer.from_pretrained(model_name)
        
    return tokenizer

"""
get model corresponding to the task wanted
"""
def get_model(type_model, bert_aggregator = ut.MAX_AGGR):
    print(' < get model: ', type_model, '>')

    if(type_model == ut.BASIC):
        classifier = None
        if(os.path.isdir(ut.BASIC_MODEL)==False):
            print('upload model>>')
            pretrained_bert_model = AutoModel.from_pretrained(ut.basic_hugging_face, output_hidden_states=True, output_attentions=True)
        else:
            print('read model>>')
            pretrained_bert_model = AutoModel.from_pretrained(ut.BASIC_MODEL)
                    
    elif(type_model == ut.SENTIMENT):
        if(os.path.isdir(ut.SENTIMENT_MODEL)==False):
            print('upload model>>')
            pretrained_bert_model = AutoModel.from_pretrained(ut.sentiment_hugging_face, output_hidden_states=True, output_attentions=True)
            classifier = AutoModelForSequenceClassification.from_pretrained(ut.sentiment_hugging_face)
        else:
            print('read model>>')
            pretrained_bert_model = AutoModel.from_pretrained(ut.SENTIMENT_MODEL)
            classifier = AutoModelForSequenceClassification.from_pretrained(ut.SENTIMENT_model_classif)
            
    
    elif(type_model == ut.ARGUMENT):
        pass
    
    elif(type_model == ut.EMOTION):
        pass
    
    model = BERT_model(type_model, pretrained_bert_model, classifier, bert_aggregator)
    return model


###################################################### CLASS #######################################################
   
class BERT_model(nn.Module):
    """
    init function
    input : 
        - type_model : type of model used
        - pretrained_bert_model : pre-trained model
        - classifier : classifier bert (None if BASIC pre_trained model used)
    """
    def __init__(self, type_model, pretrained_bert_model, classifier, bert_aggregator, 
                 CLS_embedding = True, get_classification = False):
        super(BERT_model, self).__init__()        
        
        self.type_model = type_model
        
        self.bert_model = pretrained_bert_model
        
        # self.embedding_layer = nn.Linear(768, 768)
        # self.dropout = nn.Dropout(0.2)
        
        self.classifier = classifier
        
        self.bert_aggregator = bert_aggregator
        self.get_classification = get_classification
        self.CLS_embedding = CLS_embedding
        
        
    """
    forward function
    input :
        - bert_ids : bert id of the sentence (from tokenization)
        - bert_mask : bert mask of the sentence (from tokenization)
    output :
        - out_ : prediction
        - out_embedding : embedding representation   
    """
    def forward(self, inputs, kept_mask):
        out_bert = self.bert_model(**inputs)        
        sequence_output = out_bert['last_hidden_state']

        if(self.CLS_embedding == True):
            text_embedding = sequence_output[:,0,:].view(-1,768) ## extract 1st [CLS] token's 
        else:
            text_embedding = self.get_embeddings(sequence_output, kept_mask)
        # attention_output = out_bert['attentions']
        # attention_layers = np.array([l.numpy() for l in attention_output])
        
        # text_embedding = self.dropout(text_embedding)
        # text_embedding = self.embedding_layer(text_embedding) ## extract the 1st token's embeddings
        # text_embedding = F.relu(text_embedding)

        logits = None
        # if(self.classifier is not None and self.get_classification == True):
        #     out_classifier = self.classifier(**inputs)
        #     logits = out_classifier['logits'].detach().cpu().numpy()
        
        return logits, text_embedding
    
    """
    return the mean embeddings of tokens kept
    """
    def get_embeddings(self, sequence_output, kept_mask):
        list_emb = None
        for emb, mask in zip(sequence_output, kept_mask):
            if(self.bert_aggregator == 'mean'):
                mean_emb = emb[mask].detach().cpu().numpy().mean(axis=0).reshape(1,-1)
            elif(self.bert_aggregator == 'max'):
                mean_emb = emb[mask].detach().cpu().numpy().max(axis=0).reshape(1,-1)
            
            if(list_emb is None):
                list_emb = copy.deepcopy(mean_emb)
            else:
                list_emb = np.append(list_emb, mean_emb, axis=0)
                
        return list_emb
    
    
#################################################### FUNCTIONS #####################################################
 
"""
download bert models and tokenizer to be used without internet
"""
def download_model_and_tokenizer(type_model):
    #1. Get model
    if(type_model == ut.BASIC):
        model_path = ut.BASIC_MODEL
        classifier = None
        if(os.path.isdir(model_path)==False):
            print('upload tokenizer>')
            pretrained_bert_model = AutoModel.from_pretrained(ut.basic_hugging_face, output_hidden_states=True,
                                                              output_attentions=True)
        else:
            print('read tokenizer>')
            pretrained_bert_model = AutoModel.from_pretrained(ut.BASIC_MODEL)
                
    elif(type_model == ut.SENTIMENT):
        model_path = ut.SENTIMENT_MODEL
        classifier_path = ut.SENTIMENT_model_classif
        if(os.path.isdir(model_path)==False):
            print('upload model>')
            pretrained_bert_model = AutoModel.from_pretrained(ut.sentiment_hugging_face, output_hidden_states=True, output_attentions=True)
            classifier = AutoModelForSequenceClassification.from_pretrained(ut.sentiment_hugging_face)
        else:
            print('read model>')
            pretrained_bert_model = AutoModel.from_pretrained(ut.SENTIMENT_MODEL)
            classifier = AutoModelForSequenceClassification.from_pretrained(ut.SENTIMENT_model_classif)
    
    
    #2. Get tokenizer
    tokenizer = get_tokenizer(type_model)
    if(type_model == ut.BASIC):
        tokenizer_path = ut.basic_tokenizer
    elif(type_model == ut.SENTIMENT):
        tokenizer_path = ut.sentiment_tokenizer
    else:
        print('WTF problem')
        
        
    #3. Save model & tokenizer
    if(os.path.isdir(model_path) == False):
        tokenizer.save_pretrained(tokenizer_path)
        print(' -> Tokenizer saved !')
    else:
        print(' -> Tokenizer already saved...')
     
    if(os.path.isdir(model_path) == False):
        pretrained_bert_model.save_pretrained(model_path)
        print(' -> Model saved !')
    else:
        print(' -> Model already saved...')
        
    if(type_model != ut.BASIC):
        if(os.path.isdir(classifier_path) == False):
            classifier.save_pretrained(classifier_path)
            print(' -> Classifier saved !')
        else:
            print(' -> Classifier already saved...')
            
    return 1