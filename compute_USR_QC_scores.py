#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug  7 19:34:31 2023

@author: samy
"""

import graph_processing as gp
import util as ut

import torch
import time
import dgl
import os

code_graph = 'connex' #'connex', 'not_connex' #TODO
code_xp = 'GRAPH_TEXT_B1' #TODO

keep_all_users = False

#----------------------------------------------- VARIABLE --------------------------------------------------
only_text = False  #(0) --> works with (2) (3) (4) #if we do not apply gnn


list_reduction_method = [ut.UMAP, ut.TSNE] #[ut.TSNE, ut.UMAP]
device = torch.device('cuda:0') #'cuda:0', 'cpu'
type_graph = ut.RETWEET
type_score = ['rwc_score'] #, 'dipole_GAR'] #, 'dipole_GPT', 'betweeness'

get_baseline_score = True #rwc_score
get_plot = False

#--------------- SHELL VARIABLE ----------------

#---- type_graph of xp ----
is_undirected = True
to_connex = True #TODO
minimum_links = 1
#--------------------------

#---- type_model_gnn ----
graph_model = 'gat' #'gat', 'gcn', 'graphsage' #TODO
is_edge_weighted = True
#------------------------

#---- ############################# XP parameters ############################# ----
text_model = ut.BASIC # ut.BASIC, ut.SENTIMENT, ut.ARGUMENT, ut.EMOTION, ut.HASHTAGS, False #TODO
## only if text_model == False
pre_trained_feat = False #(1) (2) #TODO
## only if text_model is NOT False
all_layer_train = False #(3) (4) #TODO
#---- ############################# ---------------- ############################# ----
####### ---------------- ############################# ----

TO_LOOK_AT = [0, 5, 10, 15, 20]
MAX_SPL = 20
BOTH_TYPE_SAMPLE = ['BEST', 'WORST']


#------------------------------------- ------------ ---- ------------ ----------------------------- --------

inputs = (ut.data_zarate, 'ZARATE', None, None, 1)

"""
optimize computing section
"""
def get_all_result_dict(res_file, dataset_by_topic, dict_user_to_keep_for_score, xp_path, all_topics, xp, usr_discard):
    t1 = time.time()
    results_by_topics = {}       
    if(os.path.isfile(res_file) == True):
        results_by_topics = gp.load_json(res_file)

    xp_path = xp_path + 'EPOCHS/'
    file_list = [(int(f.split('.')[0]), f) for f in os.listdir(xp_path)]
    file_list.sort()
    for e, f in file_list: 
        # print('--epoch', e,'--')
        filename = os.path.join(xp_path, f)
        dict_probas_by_id = gp.load_json(filename)
        
        t3 = time.time()
        if(e==0):
            print('nbs topics:', len(all_topics))
        for topic in all_topics:
            if(e==0):
                print(' >> topic:', topic)
            if(topic in results_by_topics.keys() and e in results_by_topics[topic]):
                continue
            
            G, _ = dataset_by_topic[topic]
            user_to_keep = dict_user_to_keep_for_score[topic][usr_discard]
            dict_proba_topic = dict_probas_by_id[topic]
            
            # t2 = time.time()
            node_probas = get_probas_user_kept(G, dict_proba_topic, user_to_keep, e, xp)
            # print('2b.', topic, ':', round(((time.time() - t2)/60), 2), 'min ---') 
            
            if(topic not in results_by_topics.keys()):
                results_by_topics[topic] = {}
            results_by_topics[topic][e] = get_prediction_score(node_probas, runtime=-1)
            
        gp.write_json(res_file, results_by_topics)
        
        # print(' <', round(((time.time() - t3)/60), 2), 'min>') 
        
    print('--- XP time:', round(((time.time() - t1)/60), 2), 'min --- \n\n') 
    gp.write_json(res_file, results_by_topics)
    
    return results_by_topics


"""
get probas
"""    
def get_probas_user_kept(G, dict_probas_by_id, user_to_keep, e, percent):
    node_probas = []
    set_user_to_keep = set(user_to_keep)
    
    #1. verif
    if(e == 0):
        print('verif1:', len(dict_probas_by_id.keys()), len(G.nodes().tolist()))
        print('verif2:', len(set_user_to_keep), len(user_to_keep))
        print('    >', round((len(user_to_keep)/len(dict_probas_by_id))*100, 2), '% kept')
          
    #2. get probas
    if(percent == 0):
        for id_ in G.nodes().tolist():
            if(keep_all_users == True or id_ in set_user_to_keep):
                node_probas.append(dict_probas_by_id[str(id_)])
    else:       
        for id_, old_id_ in zip(G.nodes().tolist(), G.ndata[dgl.NID].tolist()):
            if(keep_all_users == True or old_id_ in set_user_to_keep):
                node_probas.append(dict_probas_by_id[str(id_)])
        
    return node_probas

"""
return quantification scores
"""
def get_prediction_score(node_probas, runtime=-1):  
    qc_probas = round(sum(node_probas)/len(node_probas) , 3) 
    result = {'proba': qc_probas,
              'runtime': runtime}   
    return result
    


# """
# get score for each topic
# """
# def get_result_dict(G, user_to_keep, xp_path, topic, percent):
#     xp_path = xp_path + 'EPOCHS/'
#     file_list = [f for f in os.listdir(xp_path)] # if os.path.isfile()]
    
#     dict_score_by_epoch = {}
#     for f in file_list:
#         e = int(f.split('.')[0])
#         print('--epoch', e,'--')
#         filename = os.path.join(xp_path, f)
#         dict_probas_by_id = gp.load_json(filename)
#         dict_probas_by_id = dict_probas_by_id[topic]
        
#         node_probas = get_probas_user_kept(G, dict_probas_by_id, user_to_keep, e, percent)

#         dict_score_by_epoch[e] = get_prediction_score(node_probas, runtime=-1)
        
#     return dict_score_by_epoch
    





"""
GNN_QC semi-supervised for my data
type_topics = (ut.mydata, topic, from_date, to_date, step)
"""
if __name__ == "__main__":
    print('    >>>>>>>>>>>>>>>>>>>>>>>>   HELLO SAMPLE <<<<<<<<<<<<<<<<<<<<<<<<< \n')
    t0 = time.time()  
    ut.update_data_object_folder('USR_QC'+'_'+code_graph)
    period=(inputs[2], inputs[3])
    data_type = inputs[0]
    general_topic = inputs[1]
    step = inputs[4]
    path = gp.create_xp_folder(general_topic, type_graph)
    data_object_path = path + 'temp_topic_objects/'
    if(os.path.isdir(data_object_path) == False):
        os.mkdir(data_object_path)
    if(general_topic == 'ZARATE'):
        all_topics = ut.topic_qc_zarate
    
    code_xp_saved_models_path = path + code_xp +'_0/' 
    node_off_file = code_xp_saved_models_path + 'TEST_predictions.pickle'
    
    
    #1. GET THE x% BEST and the x% WORST
    print('****************** STEP 1 : GET BEST/WORST USER IDs *******************')
    dict_user_to_keep_for_score = {}
    node_off_results_by_topic = gp.load_pickle(node_off_file)

    path_to_save = path + 'dict_user_to_keep_for_score__'+str(MAX_SPL)+'.pickle'
    if(os.path.isfile(path_to_save) == False):
        for topic in all_topics:
            path_file = data_object_path + str(topic) +'/' + 'dgl_basic.pickle'        
            G = gp.load_pickle(path_file)
    
            if(topic not in dict_user_to_keep_for_score.keys()):
                dict_user_to_keep_for_score[topic] = {}
                    
            node_ids, _, _, node_probas, _ = node_off_results_by_topic[topic]
            dict_probs_by_id = {k:v for k,v in zip(node_ids, node_probas)}
            
            for user_to_discard in BOTH_TYPE_SAMPLE:
                if(user_to_discard not in dict_user_to_keep_for_score[topic].keys()):
                    dict_user_to_keep_for_score[topic][user_to_discard] = {}
                
                #### ADD THE SAMPLE PART
                ####### aaa
                if(user_to_discard == 'BEST'):
                    print('> best !')
                    sorted_list = sorted(dict_probs_by_id.items(), key = lambda x:x[1], reverse = True)
                elif(user_to_discard == 'WORST'):
                    print('> worst !')
                    sorted_list = sorted(dict_probs_by_id.items(), key = lambda x:x[1], reverse = False)
                    
                idx_to_remove = int(len(sorted_list) * (MAX_SPL/100))
                print(' > remove users from score', sorted_list[0] ,'to score', sorted_list[idx_to_remove])
                usr_to_keep = [x[0] for x in sorted_list[idx_to_remove:]]
                
                OG = dgl.node_subgraph(G, usr_to_keep)            
                OG_dict_ids = {}
                for id_, old_id_ in zip(OG.nodes().tolist(), OG.ndata[dgl.NID].tolist()):
                    OG_dict_ids[id_] = old_id_
                        
                print(' >>>', len(G.nodes()), '!=', len(OG.nodes()))
                dict_user_to_keep_for_score[topic][user_to_discard] = usr_to_keep 
                #{'user_to_keep': usr_to_keep, 'dict_old_by_new_id': OG_dict_ids}
                
        gp.pickle_save(path_to_save, dict_user_to_keep_for_score)
    else:
        print(' > already done')
        dict_user_to_keep_for_score = gp.load_pickle(path_to_save)
    
    
    print('****************** STEP 2 : GET scores for each xp *******************')
    print(' > SHALL WE KEEP ALL USERS ? ', keep_all_users)
    #res = one file per type_sample x user_discard
    res_path = path + 'RESULTS/'
    if(os.path.isdir(res_path) == False):
        os.mkdir(res_path)
        
    # For each xp
    for xp in TO_LOOK_AT:
        for usr_discard in BOTH_TYPE_SAMPLE:                       
            print('---------', xp, usr_discard, '---------')
            new_dict_scores = {}    
            
            if(xp==0 and usr_discard == 'WORST'):
                print(' > do xp 0 only once !')
                continue
            
            print(' > go on !')
            #1. create files
            if(xp==0):
                res_file = res_path + 'dict_scores_by_epoch_'+str(xp)+'.json'
                xp_path = path + code_xp +'_'+str(xp)+'/'
            else:
                res_file = res_path + 'dict_scores_by_epoch_'+str(xp)+'_'+usr_discard+'.json'
                xp_path = path + code_xp +'_'+str(xp)+'___'+ usr_discard +'/'
                
                
            #2. get graph G for each topic, dependending on the xp
            dataset_file = path + 'dataset_basic_False_False_MEAN'
            if(xp == 0):
                dataset_file += '.pickle'
            else:
                dataset_file = dataset_file+'_'+str(xp)+'_'+str(usr_discard)+'.pickle'        
            dataset_by_topic = gp.load_pickle(dataset_file)  
            
            #3. get results
            results_by_topics = get_all_result_dict(res_file, dataset_by_topic, dict_user_to_keep_for_score, xp_path, all_topics, xp, usr_discard)
            
            

            
    print(' > finish !!! ')
                
                
            
