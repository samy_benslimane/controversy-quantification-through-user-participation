# Controversy Quantification through user participation

In this paper, we quantify controversy on topics from the perspective of user polarization around
communities with diverse viewpoints and opinions within social networks.
We propose a user-based approach to quantify controversy, using both structural and textual information through GNN layers, resulting from the three contributions:  

+ **Controversy quantification**. We propose a theoretical model to quantify the controversy according to the users. This score is based on the expected probability of a user participating in a controversial topic. 

+ **Empirical estimation of the quantification score**. We propose a method for estimating this score empirically.  We estimate the conditional probabilities of users participating in a controversial topic. We show that minimizing the loss function of this estimator is equivalent to optimizing our score. To estimate this probability for each user, we propose a model based on GNNs, exploiting both textual and structural information of the graph.

## paper link 

(the paper has been submitted to the NAACL 2024 conference)

## code

(code will be available after publication of the paper)
