#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 20 16:33:55 2023

@author: samy
"""


import graph_processing as gp

import pandas as pd

import pickle

def pickle_save(path, data):
    with open(path, 'wb') as file:
        pickle.dump(data, file, protocol=pickle.HIGHEST_PROTOCOL)
    return 1

topic = 'pelosi' #QC_score = 0.833
xp = 'GRAPH_TEXT_B1'


path = '/data/DEV_DATA/TWITTER API/other/data_complete_vaccine_chloro/data/data_objects/USR_QC_connex/ZARATE/RETWEET/'+xp+'/'
data_objects_by_topic = gp.load_pickle(path+'TEST_predictions.pickle')


n_ids, _, _, probas, preds = data_objects_by_topic[topic]
dict_node_l = {}
dict_node_preds = {}
for n, p, pre in zip(n_ids, probas, preds):
    if(p > 0.7):
        l = 2
    elif(p > 0.3):
        l = 1
    else:
        l = 0
    dict_node_l[n] = l
    dict_node_preds[n] = pre
    
    
a = gp.load_pickle(path+'data_object_'+topic+'.pickle')
dict_tweets, _, _, dict_nodes, _, _, _, _, _, _, _, _, _, _, _ = a

dict_nodes_text = {k:[dict_tweets[w] for w in v] for k,v in dict_nodes.items() if(v is not None)}
dict_nodes_preds = {k:v for k,v in dict_node_preds.items() if(k in dict_nodes_text.keys())}
df1 = pd.DataFrame(dict_nodes_text.items())
df2 = pd.DataFrame(dict_nodes_preds.items())
df1.columns = ['key', 'text']
df2.columns = ['key', 'label']
df = pd.merge(df1, df2, on='key')

pickle_save(path+'df_prediction_text.pickle', df)



# """
# list of test k

# loss_json = {0:0.434, 1:0.547, ...}

# qs_json = {0:{'pelosi': 0.954, 'thanksgiving':0.124}, ...}

# """
def predict_model_SUBGRAPHS(dataset, test_k, model, text_model, is_edge_weighted, device, current_path, type_set='TEST', is_timeline=False):
    print('||||||||||||||||||||||||||', type_set, ' PREDICTIONS ||||||||||||||||||||||||||')

    json_qs = current_path + 'results_scores.json'
    json_loss = current_path + type_set+'_loss_prediction.pickle'

    results_qs_score = {}
    results_loss_score = {}
    
    #labels
    if(type_set == 'MY_DATA'):
        topic_labels = ut.my_data_labels
    else:
        topic_labels = ut.data_zarate_labels

    if(os.path.isfile(json_qs) == True):
        results_qs_score = gp.load_json(json_qs)  
    if(os.path.isfile(json_loss) == True):
        results_loss_score = gp.load_json(json_loss)  
        
        
    if(is_timeline == False):
        sorted_topic = {k:topic_labels[k] for k in dataset.keys()}
    else:
        for k in dataset.keys():
            print(k)
            print('_'.join(k.split('_')[:-1]))
            print('***')
        sorted_topic = {k:topic_labels['_'.join(k.split('_')[:-1])] for k in dataset.keys()}
    sorted_topic = [k for k, v in sorted(sorted_topic.items(), key=lambda x: x[1])]
    
    
    for current_k in test_k:
        t1 = time.time()
        print('####### k=', current_k)
        if(str(current_k) in results_loss_score.keys()):
            print('  > k already done !')
            continue
        if(str(current_k) not in results_qs_score.keys()):
            results_qs_score[str(current_k)] = {}
    
        list_all_probas = []
        list_all_labels = []
        
        if(current_k is not None):
            if(current_k == -1):
                dict_subgraphs = get_random_subgraphs(dataset, current_path, device)
            else:
                dict_subgraphs = get_subgraph_from_k(dataset, current_k, current_path, device)
        
        for topic in sorted_topic:
            t2 = time.time()
            print('   --- topic=', topic)
            if(is_timeline == False):
                current_label = topic_labels[topic]
            else:
                current_label = topic_labels['_'.join(topic.split('_')[:-1])]
            G, dict_node_features = dataset[topic]
    
            #1. GET PREDICTIONS
            if(current_k is None):
                n_nodes = []
                features = []
                for k, v in dict_node_features.items():
                    n_nodes.append(k)
                    features.append(v)
                if(text_model == False):
                    features = [torch.tensor(f, dtype=torch.double) for f in features]
                
                model.eval()
                with torch.no_grad():
                    if(is_edge_weighted == True):
                        _, _, _, probas = model(G, features, G.edata['weight'], device=device)
                    else:
                        _, _, _, probas = model(G, features, device=device)
                
                node_probas = probas[:,1].tolist() #get prediction of user being controversial
                node_labels = [current_label] * len(node_probas)
            
            else:
                node_probas = []
                node_labels = []
                for id_topic, val1 in dict_subgraphs.items():
                    dict_g_by_nodes, dict_node_features, topic_label = val1
                    
                    for center_node, list_g in dict_g_by_nodes.items():
                        for current_g in list_g: 
                            n_nodes = []
                            features = []
                            new_center_node = None
                            for id_, old_id_ in zip(current_g.nodes().tolist(), current_g.ndata[dgl.NID].tolist()):
                                n_nodes.append(id_)
                                features.append(dict_node_features[old_id_])
                                if(center_node == old_id_):
                                    new_center_node = id_
                            if(text_model == False):
                                features = [torch.tensor(f, dtype=torch.double) for f in features]
                            
                            model.eval()
                            with torch.no_grad():
                                if(is_edge_weighted == True):
                                    _, _, _, probas = model(current_g, features, current_g.edata['weight'], device=device)
                                else:
                                    _, _, _, probas = model(current_g, features, device=device)
                            probas = probas[:,1].tolist()
                            
                            node_probas.append(probas[new_center_node])                            
                            node_labels.append(topic_label)

            #2. GET QC score
            results_qs_score[str(current_k)][topic] = round(sum(node_probas)/len(node_probas) , 3) 
            
            list_all_probas += node_probas
            list_all_labels += node_labels
            
            print(' ***', round(((time.time() - t2)/60), 2), 'min ***')   


        #3. COMPUTE loss k
        test_loss = F.binary_cross_entropy(torch.tensor(list_all_probas, dtype=torch.float32), torch.tensor(list_all_labels, dtype=torch.float32)) 
        print('    =========== test loss for k=', current_k, ' --> ',  test_loss, '=============')
        
        #4. SAVE IN JSON FILE results    
        results_qs_score[str(current_k)] = test_loss
        gp.write_json(json_loss, results_loss_score)
        
        gp.write_json(json_qs, results_qs_score)
        
        print(' ***', round(((time.time() - t1)/60), 2), 'min ***')  

    print('-- END TEST --')
    return results_qs_score, results_loss_score



"""
input :
    - test_k = 0,1,..., None
    
    + set the k parameter
    
    + add final evaluation on subgraphs only (after training)
    + add final evaluation on predict_function, to test different k
    
"""
# def train_loop_SUBGRAPHS(TRAIN_SET, TEST_SET, test_k,
#                            only_text, model, 
#                            optimizer, epochs, device, batch_gnn, 
#                            is_edge_weighted, text_model, path, model_name):
                
#     min_nodes_by_graph = get_min_nodes_by_graph(TRAIN_SET)
    
    
#     list_subgraph_test = get_subgraph_from_k(TEST_SET, test_k, path)
    
    
#     # set parameters of EPOCHS LOOP
#     final_model = copy.deepcopy(model)
#     start_epoch = 0
#     best_train_epoch = -1
#     best_train_loss = None
#     list_train_loss = []
#     best_train_acc = None
#     list_train_acc = []
#     best_test_acc = 0 #depend on best train epoch
#     list_test_acc = []
#     cpt_early_stop = 0
#     runtime = 0
    
#     fold_chkpt = path + ut.checkpoint_folder
#     if(os.path.isdir(fold_chkpt) == False):
#         os.mkdir(fold_chkpt)
    
#     if(len(os.listdir(fold_chkpt)) > 0):
#         file = os.listdir(fold_chkpt)[0]
#         checkpoint_path = fold_chkpt + file
    
#         if(os.path.exists(checkpoint_path)):
#             print(' > found checkpoint!')
#             chk = torch.load(checkpoint_path)
#             model.load_state_dict(chk['model_state_dict'])
#             optimizer.load_state_dict(chk['optimizer_state_dict'])
#             start_epoch = chk['epoch']+1
#             best_train_epoch = chk['best_train_epoch']
#             best_train_loss = chk['best_train_loss']
#             list_train_loss = chk['list_train_loss']
#             best_train_acc = chk['best_train_acc']
#             list_train_acc = chk['list_train_acc']
#             best_test_acc = chk['best_test_acc']
#             list_test_acc = chk['list_test_acc']
#             cpt_early_stop = chk['cpt_early_stop']
#             runtime = chk['runtime']
            
#             final_model = torch.load(path+'final_TEMP.pth')
    
#     print(' >> early_stop=', early_stop)
#     for e in range(start_epoch, epochs):
#         print("------------------", e, "---------------------------")
#         t_ep = time.time()
#         ################# 1. TRAINING  #################
#         model.train()
#         total_loss = 0
#         total_acc = 0
#         total_train_x0 = 0
#         total_train_x1 = 0
#         for G, dict_node_features in TRAIN_SET:
#             #1.1 PREPARE tHE GRAPH
#             n_nodes = []
#             features = []
#             idx_none = []
#             for k, v in dict_node_features.items():
#                 n_nodes.append(k)
#                 features.append(v)
#                 if(v is None):
#                     idx_none.append(k)
#             if(text_model == False):
#                 features = [torch.tensor(f, dtype=torch.double) for f in features]
        
#             labels = G.ndata['label']
#             train_mask = get_temp_train_mask(G, min_nodes_by_graph)
                
#             all_labels = labels[train_mask]
#             all_labels = all_labels.cpu().detach().numpy()
#             all_index = np.where(np.array(train_mask) == True)[0]
            
#             if(only_text == True):
#                 temp_all_index = []
#                 temp_all_labels = []
#                 cpt=0
#                 for i in range(len(all_index)):
#                     if(dict_node_features[all_index[i]] is not None):
#                         temp_all_index.append(all_index[i])
#                         temp_all_labels.append(all_labels[i])
#                     else:
#                         cpt+=1
#                 all_index = temp_all_index
#                 all_labels = temp_all_labels               
        
#             x0 =np.array([all_index[i] for i in range(len(all_index)) if all_labels[i] == 0])
#             x1 =np.array([all_index[i] for i in range(len(all_index)) if all_labels[i] == 1])
#             total_train_x0 += len(x0)
#             total_train_x1 += len(x1)
            
            
#             #1.2 TRAIN THE MODEL
#             optimizer.zero_grad()
#             if(is_edge_weighted == True):
#                 _, h, _, logits = model(G, features, G.edata['weight'], device=device)
#             else:
#                 _, h, _, logits = model(G, features, device=device)

#             # Compute prediction
#             preds = logits.argmax(1)
    
#             # Compute loss
#             loss = F.cross_entropy(logits[train_mask], labels[train_mask])
            
#             # TRAIN acc
#             train_acc = (preds[train_mask] == labels[train_mask]).float().mean()
            
#             # Backward
#             loss.backward()
#             optimizer.step()
            
#             total_loss += loss.item()
#             total_acc += train_acc.item()
            
#         # print('---- train preds -----')  # TODO delete
#         # print(logits)
#         # print('--------------------- \n')

#         #1.3 COMPUTE LOSS
#         epoch_loss = round(total_loss / len(TRAIN_SET), 5)
#         epoch_acc = round(total_acc / len(TRAIN_SET), 5)
        
#         #1.4 SAVE FINAL MODEL -- early stopping when train-loss increase --
#         if(early_stop is None):
#             final_model = copy.deepcopy(model)
#         else:
#             if(best_train_loss is not None and best_train_loss < epoch_loss):
#                 if(cpt_early_stop >= early_stop):
#                     print(' > hop we stop, new_train_loss=', epoch_loss, 'best train loss=', best_train_loss, '==> cpt_early=', cpt_early_stop)
#                     break
#                 else:
#                     cpt_early_stop+=1
#             else:
#                 print(' ! best model !')
#                 best_train_epoch = e
#                 best_train_loss = epoch_loss
#                 best_train_acc = epoch_acc
#                 final_model = copy.deepcopy(model)
#                 cpt_early_stop = 0
        
#         #runtime
#         train_epoch_runtime = time.time() - t_ep
#         runtime += train_epoch_runtime
        
#         ################# 2. EVALUATION  #################
#         model.eval()
#         all_test_preds = []
#         all_test_labs = []
#         total_test_x0 = 0
#         total_test_x1 = 0
#         for G, dict_node_features in TEST_SET:
#             #2.1 PREPARE tHE GRAPH
#             n_nodes = []
#             features = []
#             idx_none = []
#             for k, v in dict_node_features.items():
#                 n_nodes.append(k)
#                 features.append(v)
#                 if(v is None):
#                     idx_none.append(k)
#             if(text_model == False):
#                 features = [torch.tensor(f, dtype=torch.double) for f in features]
        
#             labels = G.ndata['label']
#             test_mask = [True] * len(G.nodes())
                
#             all_labels = labels[test_mask]
#             all_labels = all_labels.cpu().detach().numpy()
#             all_index = np.where(np.array(test_mask) == True)[0]
            
#             if(only_text == True):
#                 temp_all_index = []
#                 temp_all_labels = []
#                 cpt=0
#                 for i in range(len(all_index)):
#                     if(dict_node_features[all_index[i]] is not None):
#                         temp_all_index.append(all_index[i])
#                         temp_all_labels.append(all_labels[i])
#                     else:
#                         cpt+=1
#                 all_index = temp_all_index
#                 all_labels = temp_all_labels               
        
#             x0 =np.array([all_index[i] for i in range(len(all_index)) if all_labels[i] == 0])
#             x1 =np.array([all_index[i] for i in range(len(all_index)) if all_labels[i] == 1])
#             total_test_x0 += len(x0)
#             total_test_x1 += len(x1)
            
            
#             #2.2 GET MODEL PREDICTION
#             with torch.no_grad():
#                 if(is_edge_weighted == True):
#                     _, h, _, logits = model(G, features, G.edata['weight'], device=device)
#                 else:
#                     _, h, _, logits = model(G, features, device=device)

#             # Compute prediction
#             preds = logits.argmax(1)
#             all_test_preds += preds[test_mask].tolist()
#             all_test_labs += labels[test_mask].tolist()
            
#         # print('---- test preds -----') #TODO delete
#         # print(all_test_preds)
#         # logits.argmax(1)
#         # print('--------------------- \n')
        
#         #2.3 GET TEST ACCURACY
#         test_acc = accuracy_score(all_test_labs, all_test_preds) 
#         if (best_train_epoch == e):
#             best_test_acc = test_acc
        

#         ################# 3. SAVE CHKPT & MODEL & METADATA  #################        
#         #3.1 SAVE LOSS & ACC train test sets
#         list_train_loss.append(epoch_loss)
#         list_train_acc.append(epoch_acc)
#         list_test_acc.append(test_acc)
                
#         #3.2 SAVE FINAL MODEL
#         if(best_train_epoch == e):
#             save_model(path+'final_TEMP.pth', final_model)
        
#         #3.3 SAVE CHKPT MODEL
#         #save checkpoint
#         for old_file in os.listdir(fold_chkpt):
#               os.remove(fold_chkpt+old_file)
             
#         # save current epochs 
#         checkpoint_file = fold_chkpt + str(e)+'_'+model_name 
#         torch.save({
#             'epoch': e,
#             'model_state_dict': model.state_dict(),
#             'optimizer_state_dict': optimizer.state_dict(),
#             'best_train_epoch': best_train_epoch,
#             'best_train_loss': best_train_loss,
#             'list_train_loss': list_train_loss,
#             'best_train_acc': best_train_acc,
#             'list_train_acc': list_train_acc,
#             'best_test_acc': best_test_acc,
#             'list_test_acc': list_test_acc,
#             'cpt_early_stop': cpt_early_stop,
#             'runtime': runtime
#             }, checkpoint_file)
        
#         #3.4 PRINT INFO ON EPOCHS
#         if(e%1 == 0):
#             print('In epoch {}, train loss: {:.3f} + train acc: {:.3f} || test acc: {:.3f}'.format(
#                 e, epoch_loss, epoch_acc, test_acc))
#             print('           > BEST: In epoch {}, train_loss: {:.3f} + train_acc: {:.3f} || test_acc: {:.3f}'.format(
#                 best_train_epoch, best_train_loss, best_train_acc, best_test_acc))
            
#             print(' --> time :', round(((time.time() - t_ep)/60), 2), 'min --- ', total_train_x0, total_train_x1, total_test_x0, total_test_x1)
#             print("------------------------------------------------- \n")

#     # FINALISATION
#     #save model
#     print('\n --- Save final model ---')
#     save_model(path+model_name, final_model)
#     train_info = {'train_runtime': round(runtime/60,4),
#                   'train_loss': best_train_loss,
#                   'test_acc': best_test_acc}
#     print('    -> Training info:', train_info)
#     gp.write_json(path+'train_info.json', train_info)
    
#     #plot         
#     path = path + 'plot/'
#     if(os.path.isdir(path) == False):
#         os.mkdir(path)
    
#     p_train_loss = path + "train_loss.png"
#     plt.plot(range(len(list_train_loss)), list_train_loss)
#     plt.savefig(fname = p_train_loss)
#     plt.show()
    
#     p_train_acc = path + "train_acc.png"
#     plt.plot(range(len(list_train_acc)), list_train_acc)
#     plt.savefig(fname = p_train_acc)
#     plt.show()
    
#     p_test_acc = path + "test_acc.png"
#     plt.plot(range(len(list_test_acc)), list_test_acc)
#     plt.savefig(fname = p_test_acc)
#     plt.show()
                
#     return final_model





#------------------------------------------------------------------------------ ALL IN THE SAME TIME


def predict_model_SUBGRAPHS(dataset, test_k, model, text_model, is_edge_weighted, device, current_path, type_set='TEST', type_topic='ALL', is_timeline=False, nbs_iter=5000):
    print('||||||||||||||||||||||||||', type_set, ' PREDICTIONS ||||||||||||||||||||||||||')
    if(type_topic == 'ALL'):
        json_qs = current_path + type_set+'_results_scores.json'
        json_loss = current_path + type_set+'_loss_prediction.json'
    else:
        json_qs = current_path + 'TOPIC_results_scores.json'
        json_loss = current_path + 'TOPIC_loss_prediction.json'

    results_qs_score = {}
    results_loss_score = {}
    
    #labels
    if(type_set == 'MY_DATA'):
        topic_labels = ut.my_data_labels
    else:
        topic_labels = ut.data_zarate_labels

    if(os.path.isfile(json_qs) == True):
        results_qs_score = gp.load_json(json_qs)  
    if(os.path.isfile(json_loss) == True):
        results_loss_score = gp.load_json(json_loss)  
        
        
    if(is_timeline == False):
        sorted_topic = {k:topic_labels[k] for k in dataset.keys()}
    else:
        for k in dataset.keys():
            print(k)
            print('_'.join(k.split('_')[:-1]))
            print('***')
        sorted_topic = {k:topic_labels['_'.join(k.split('_')[:-1])] for k in dataset.keys()}
    sorted_topic = [k for k, v in sorted(sorted_topic.items(), key=lambda x: x[1])]
    
    
    for current_k in test_k:
        t1 = time.time()
        print('####### k=', current_k)
        if(str(current_k) in results_loss_score.keys()):
            print('  > k already done !')
            continue
        if(str(current_k) not in results_qs_score.keys()):
            results_qs_score[str(current_k)] = {}
    
        list_all_probas = []
        list_all_labels = []
        
        if(current_k is None):
            for topic in sorted_topic:
                t2 = time.time()
                # print('   --- topic=', topic)
                if(is_timeline == False):
                    current_label = topic_labels[topic]
                else:
                    current_label = topic_labels['_'.join(topic.split('_')[:-1])]
                G, dict_node_features = dataset[topic]
        
                #1. GET PREDICTIONS
                if(current_k is None):
                    n_nodes = []
                    features = []
                    for k, v in dict_node_features.items():
                        n_nodes.append(k)
                        features.append(v)
                    if(text_model == False):
                        features = [torch.tensor(f, dtype=torch.double) for f in features]
                    
                    model.eval()
                    with torch.no_grad():
                        if(is_edge_weighted == True):
                            _, _, _, probas = model(G, features, G.edata['weight'], device=device)
                        else:
                            _, _, _, probas = model(G, features, device=device)
                    
                    node_probas = probas[:,1].tolist() #get prediction of user being controversial
                    node_labels = [current_label] * len(node_probas)
                    
                #2. GET QC score
                results_qs_score[str(current_k)][topic] = round(sum(node_probas)/len(node_probas) , 3)
                
                list_all_probas += node_probas
                list_all_labels += node_labels
                    
        else:
            if(current_k == -1):
                dict_of_all_subgraphs = get_random_subgraphs(dataset, current_path, device, nbs_iter=nbs_iter, type_topic=type_topic)
            else:
                dict_of_all_subgraphs = get_subgraph_from_k(dataset, current_k, current_path, device, nbs_iter=nbs_iter, type_topic=type_topic)
                
            dict_res_topics = {}
            cpt=0
            for topic in dict_of_all_subgraphs.keys():
                t2 = time.time()
                print('   --- topic=', topic)
                dict_res_topics[topic] = []
                dict_subgraphs = dict_of_all_subgraphs[topic]['subgraphs']
                dict_node_features = dict_of_all_subgraphs[topic]['features']
                topic_label = dict_of_all_subgraphs[topic]['label']
    
                for _, values in dict_subgraphs.items():
                    center_node, current_g, curr_k_value = values
                    
                    n_nodes = []
                    features = []
                    new_center_node = None
                    for id_, old_id_ in zip(current_g.nodes().tolist(), current_g.ndata[dgl.NID].tolist()):
                        n_nodes.append(id_)
                        if(dict_node_features[old_id_] is None): #TODO
                            features.append([0])
                        if(center_node == old_id_):
                            new_center_node = id_
                    if(text_model == False):
                        features = [torch.tensor(f, dtype=torch.double) for f in features]
        
                    model.eval()
                    with torch.no_grad():
                        if(is_edge_weighted == True):
                            _, _, _, probas = model(current_g, features, current_g.edata['weight'], device=device)
                        else:
                            _, _, _, probas = model(current_g, features, device=device)
                    probas = probas[:,1].tolist()
                    
                    dict_res_topics[topic].append(probas[new_center_node])
                    list_all_probas.append(probas[new_center_node])                            
                    list_all_labels.append(topic_label)
                    
                    if(cpt%500 == 0):
                        print('     ...', round((cpt/nbs_iter)*100, 2), '%')
                    cpt+=1
                            

                #2. GET QC score
                results_qs_score[str(current_k)][topic] = round(sum(dict_res_topics[topic])/len(dict_res_topics[topic]) , 3) 
                
                print(' ***', round(((time.time() - t2)/60), 2), 'min ***')   
    
    
            #3. COMPUTE loss k    if(os.path.isdir(xp_folder) == False):
            test_loss = F.binary_cross_entropy(torch.tensor(list_all_probas, dtype=torch.float32), torch.tensor(list_all_labels, dtype=torch.float32)) 
            print('    =========== test loss for k=', current_k, ' --> ',  test_loss, '=============')
            
            
            #4. SAVE IN JSON FILE results    
            gp.write_json(json_qs, results_qs_score)

            results_loss_score[str(current_k)] = test_loss.item()
            gp.write_json(json_loss, results_loss_score)
            
            
            print(' ***', round(((time.time() - t1)/60), 2), 'min ***')  
    
    print('-- END TEST --')
    return results_qs_score, results_loss_score

#------------------------------------------------------------------------------ ONE BY ONE


"""
list of test k

loss_json = {0:0.434, 1:0.547, ...}

qs_json = {0:{'pelosi': 0.954, 'thanksgiving':0.124}, ...}

"""
def predict_model_SUBGRAPHS(dataset, test_k, model, text_model, is_edge_weighted, device, current_path, type_set='TEST', is_timeline=False):
    print('||||||||||||||||||||||||||', type_set, ' PREDICTIONS ||||||||||||||||||||||||||')

    json_qs = current_path + 'results_scores.json'
    json_loss = current_path + type_set+'_loss_prediction.pickle'

    results_qs_score = {}
    results_loss_score = {}
    
    #labels
    if(type_set == 'MY_DATA'):
        topic_labels = ut.my_data_labels
    else:
        topic_labels = ut.data_zarate_labels

    if(os.path.isfile(json_qs) == True):
        results_qs_score = gp.load_json(json_qs)  
    if(os.path.isfile(json_loss) == True):
        results_loss_score = gp.load_json(json_loss)  
        
        
    if(is_timeline == False):
        sorted_topic = {k:topic_labels[k] for k in dataset.keys()}
    else:
        for k in dataset.keys():
            print(k)
            print('_'.join(k.split('_')[:-1]))
            print('***')
        sorted_topic = {k:topic_labels['_'.join(k.split('_')[:-1])] for k in dataset.keys()}
    sorted_topic = [k for k, v in sorted(sorted_topic.items(), key=lambda x: x[1])]
    
    
    for current_k in test_k:
        t1 = time.time()
        print('####### k=', current_k)
        if(str(current_k) in results_loss_score.keys()):
            print('  > k already done !')
            continue
        if(str(current_k) not in results_qs_score.keys()):
            results_qs_score[str(current_k)] = {}
    
        list_all_probas = []
        list_all_labels = []
        
        for topic in sorted_topic:
            t2 = time.time()
            print('   --- topic=', topic)
            if(is_timeline == False):
                current_label = topic_labels[topic]
            else:
                current_label = topic_labels['_'.join(topic.split('_')[:-1])]
            G, dict_node_features = dataset[topic]
    
            #1. GET PREDICTIONS
            if(current_k is None):
                n_nodes = []
                features = []
                for k, v in dict_node_features.items():
                    n_nodes.append(k)
                    features.append(v)
                if(text_model == False):
                    features = [torch.tensor(f, dtype=torch.double) for f in features]
                
                model.eval()
                with torch.no_grad():
                    if(is_edge_weighted == True):
                        _, _, _, probas = model(G, features, G.edata['weight'], device=device)
                    else:
                        _, _, _, probas = model(G, features, device=device)
                
                node_probas = probas[:,1].tolist() #get prediction of user being controversial
                node_labels = [current_label] * len(node_probas)
            
            else:
                node_probas = []
                node_labels = []
                topic_label = G.ndata['label'][0]
                                        
                eee=0
                for center_node in G.nodes().tolist():
                    if(current_k == -1):
                        list_g = get_1_random_subgraph(G, center_node, device)
                    else:
                        list_g = get_1_subgraph_from_k(G, center_node, current_k, device)
                    for current_g in list_g: 
                        if(eee%1000==0):
                            print('....', eee)
                        eee+=1
                        
                        n_nodes = []
                        features = []
                        new_center_node = None
                        # print('----------------------------------')
                        # print('\n')
                        # print('c.', current_g.nodes().tolist())
                        for id_, old_id_ in zip(current_g.nodes().tolist(), current_g.ndata[dgl.NID].tolist()):
                            n_nodes.append(id_)
                            features.append(dict_node_features[old_id_])
                            if(center_node == old_id_):
                                new_center_node = id_
                                # print('\n\n    ----->', center_node, new_center_node)
                        if(text_model == False):
                            features = [torch.tensor(f, dtype=torch.double) for f in features]
                        model.eval()
                        with torch.no_grad():
                            if(is_edge_weighted == True):
                                # print('\n d. (here we have none and tensors):: ', len(features))
                                # print('\n e.', type(features[0]), type(features[-1]))
                                _, _, _, probas = model(current_g, features, current_g.edata['weight'], device=device)
                            else:
                                _, _, _, probas = model(current_g, features, device=device)
                        probas = probas[:,1].tolist()
                        
                        node_probas.append(probas[new_center_node])     
                        # print('----------------------------------- \n\n')

                        node_labels.append(topic_label)

            #2. GET QC score
            results_qs_score[str(current_k)][topic] = round(sum(node_probas)/len(node_probas) , 3) 
            
            list_all_probas += node_probas
            list_all_labels += node_labels
            
            print(' ***', round(((time.time() - t2)/60), 2), 'min ***')   


        #3. COMPUTE loss k
        test_loss = F.binary_cross_entropy(torch.tensor(list_all_probas, dtype=torch.float32), torch.tensor(list_all_labels, dtype=torch.float32)) 
        print('    =========== test loss for k=', current_k, ' --> ',  test_loss, '=============')
        
        #4. SAVE IN JSON FILE results    
        results_qs_score[str(current_k)] = test_loss
        gp.write_json(json_loss, results_loss_score)
        
        gp.write_json(json_qs, results_qs_score)
        
        print(' ***', round(((time.time() - t1)/60), 2), 'min ***')  

    print('-- END TEST --')
    return results_qs_score, results_loss_score




"""
get random subgraphs for each user of size k, with k being randomly selected with a poisson distribution

new_test_set = {id_topic: [{center_node1: [subgraphs]},
                           dict_nodes_features,
                           topic_label]}
"""
def get_random_subgraphs(TEST_SET, path, device, poisson_lambda=2, iter_=3):
    filename = path+'test_random_subgraph_'+str(poisson_lambda)+'_'+str(iter_)+'.pickle'
    t1 = time.time()
    print(' --- get subgraphs ---')


    filename+='.pickle'
    if(os.path.isfile(filename) == True):
        print(' > test subgraphs already exist !')
        new_test_set = gp.load_pickle(filename)
        return new_test_set

    idx_topic = 0
    new_test_set = {}
    for k,v in TEST_SET.items():
        G, dict_node_features = v
        
        topic_label = G.ndata['label'][0]
        print('             >', k, 'label:', topic_label)
        
        dict_subgraphs = {}
        e=0
        for center_node_id in G.nodes().tolist():
            list_subgraphs = []
            poisson_sample = stats.poisson.rvs(poisson_lambda, size=iter_)
            for k_value in poisson_sample:
                if(e%100==0):
                    print('....', e)
                e+=1
                sg, _ = dgl.khop_out_subgraph(G, center_node_id, k=k_value, relabel_nodes=True)
                # sg = build_subgraph(G, center_node_id, k_value, device)
                list_subgraphs.append(sg)
                
            dict_subgraphs[center_node_id] = list_subgraphs
        new_test_set[idx_topic] = [dict_subgraphs, dict_node_features, topic_label]
        idx_topic+=1
    
    print('---', round(((time.time() - t1)/60), 2), 'min ---') 
    
    gp.pickle_save(filename, new_test_set)
    
    return new_test_set



"""
get subgraphs for each user of size k

new_test_set = {id_topic: [{center_node1: subgraphs},
                           dict_nodes_features,
                           topic_label]}
"""
def get_subgraph_from_k(TEST_SET, k_value, path, device):
    filename = path+'test_subgraph_'+str(k_value)+'.pickle'
    t1 = time.time()
    print(' --- get subgraphs ---')


    filename+='.pickle'
    if(os.path.isfile(filename) == True):
        print(' > test subgraphs already exist !')
        new_test_set = gp.load_json(filename)
        return new_test_set
    
    idx_topic = 0
    new_test_set = {}
    for k,v in TEST_SET.items():
        G, dict_node_features = v
        topic_label = G.ndata['label'][0]
        print('             >', k, 'label:', topic_label)
        
        dict_subgraphs = {}
        for center_node_id in G.nodes().tolist():
            if(k_value is None):
                subgraph = [G]                
            else:
                subgraph, _ = dgl.khop_out_subgraph(G, center_node_id, k=k_value, relabel_nodes=True)
                # subgraph = build_subgraph(G, center_node_id, k_value, device)
            dict_subgraphs[center_node_id] = [subgraph]
            
            
        new_test_set[idx_topic] = [dict_subgraphs, dict_node_features, topic_label]
        idx_topic+=1
    
    print('---', round(((time.time() - t1)/60), 2), 'min ---') 
    
    gp.pickle_save(filename, new_test_set)
    
    return new_test_set




def get_1_random_subgraph(G, center_node_id, device, poisson_lambda=2, iter_=3):
    
    list_subgraphs = []
    poisson_sample = stats.poisson.rvs(poisson_lambda, size=iter_)
    for k_value in poisson_sample:
        sg, _ = dgl.khop_out_subgraph(G, center_node_id, k=k_value, relabel_nodes=True)
        # sg = build_subgraph(G, center_node_id, k_value, device)
        list_subgraphs.append(sg)     
    
    return list_subgraphs


def get_1_subgraph_from_k(G, center_node_id, k_value, device):
    if(k_value is None):
        subgraph = [G]                
    else:
        subgraph, _ = dgl.khop_out_subgraph(G, center_node_id, k=k_value, relabel_nodes=True)
        # subgraph = build_subgraph(G, center_node_id, k_value, device)
        
    return [subgraph]