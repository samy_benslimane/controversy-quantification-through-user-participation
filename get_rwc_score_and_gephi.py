#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 29 14:26:09 2023

@author: samy
"""



import graph_processing as gp
import quantification_score as qs
import util as ut

import collections
import random
import torch
import time
import copy
import dgl
import os

code_graph = 'connex' #'connex', 'not_connex' #TODO
code_xp = 'GRAPH_TEXT_B1' #TODO

list_type_samples = [20] # 0 | 1, 5, 10, (15), 20, 50 # number of nodes to remove during training #TODO
list_type_user_to_discard = ['BEST', 'WORST']
TOPICS_TO_LOOK = ['kavanaugh16', 'area51']


#----------------------------------------------- VARIABLE --------------------------------------------------
only_text = False  #(0) --> works with (2) (3) (4) #if we do not apply gnn


list_reduction_method = [ut.UMAP, ut.TSNE] #[ut.TSNE, ut.UMAP]
device = torch.device('cuda:0') #'cuda:0', 'cpu'
type_graph = ut.RETWEET
type_score = ['rwc_score'] #, 'dipole_GAR'] #, 'dipole_GPT', 'betweeness'

get_baseline_score = True #rwc_score
get_plot = False

#--------------- SHELL VARIABLE ----------------

#---- type_graph of xp ----
is_undirected = True
to_connex = True #TODO
minimum_links = 1
#--------------------------

#---- type_model_gnn ----
graph_model = 'gat' #'gat', 'gcn', 'graphsage' #TODO
is_edge_weighted = True
#------------------------

#---- ############################# XP parameters ############################# ----
text_model = ut.BASIC # ut.BASIC, ut.SENTIMENT, ut.ARGUMENT, ut.EMOTION, ut.HASHTAGS, False #TODO
## only if text_model == False
pre_trained_feat = False #(1) (2) #TODO
## only if text_model is NOT False
all_layer_train = False #(3) (4) #TODO
#---- ############################# ---------------- ############################# ----

#---- ############################# MODEL parameters ############################# ----
#----FULL Model Parameters ----
is_test_set = False
epochs = 300 #30 #30

#---- Model graph Parameters ----
layer_dimension = [192] # [] = 1-layer, [192] = 2-layer, [192,192] = 3-layer #TODO
learning_rate_gnn = 0.001
weight_decay = 0.05
batch_gnn = 64

#---- Model bert Parameters ----
text_aggregator = ut.MEAN_AGGR  # ut.MAX_AGGR ut.MEAN_AGGR #TODO
learning_rate_bert = 2e-5
batch_bert = 64
#---- ############################# ---------------- ############################# ----

#------------------------------------- ------------ ---- ------------ ----------------------------- --------

inputs = (ut.data_zarate, 'ZARATE', None, None, 1)

"""
GNN_QC semi-supervised for my data
type_topics = (ut.mydata, topic, from_date, to_date, step)
"""
if __name__ == "__main__":
    print('    >>>>>>>>>>>>>>>>>>>>>>>>     Welcome to USR_QC training from SB  <<<<<<<<<<<<<<<<<<<<<<<<< \n')

    t0 = time.time()  
    ut.update_data_object_folder('USR_QC'+'_'+code_graph)
    period=(inputs[2], inputs[3])
    data_type = inputs[0]
    general_topic = inputs[1]
    step = inputs[4]
    path = gp.create_xp_folder(general_topic, type_graph)
    data_object_path = path + 'temp_topic_objects/'
    if(os.path.isdir(data_object_path) == False):
        os.mkdir(data_object_path)
    if(general_topic == 'ZARATE'):
        all_topics = ut.topic_qc_zarate
        
    code_xp_saved_models_path = path + code_xp +'_0/' #aaa 
    node_off_file = code_xp_saved_models_path + 'TEST_predictions.pickle'

#----------------------------------------------------------------------------
    print(' \n\n\n\n >>>>>>> I- Start get data : ')

    DICT_BY_SAMPLE_BY_DISCARDUSER = {}
    for type_sample in list_type_samples:
        for type_user_to_discard in list_type_user_to_discard:
            dataset_by_topic = None
            print('\n\n**********************', type_sample, type_user_to_discard, '***********************')
            code_xp = 'GRAPH_TEXT_B1'
            if(type_sample == 0): #aaa
                code_xp = code_xp + '_0' #aaa
            else: #aaa
                code_xp = code_xp + '_' + str(type_sample) + '___' + type_user_to_discard #aaa
                
            print('CODE_XP : ', code_xp)
            print('type_sample : ', type_sample) #aaa
            print('type_user_to_discard : ', type_user_to_discard) #aaa
            print('general_topic : ', general_topic)
            print('to_connex : ', to_connex)
            print('is_undirected : ', is_undirected)
            print('minimum_links : ', minimum_links)
            print('epochs : ', epochs)
            # print('label_method : ', label_method)
            print('gnn_method : ', graph_model)
            print('   + layer_dim : ', layer_dimension)
            print('   + is_test_set : ', is_test_set)
            print('   + are edges weight :', is_edge_weighted)
            print('   + learning_rate_gnn : ', learning_rate_gnn)
            print('text_model : ', text_model, pre_trained_feat, all_layer_train)
            print('   + text_aggregator : ', text_aggregator)
            print('   + batch_bert : ', batch_bert)
            print('   + learning_rate_bert : ', learning_rate_bert)
            print('batch_gnn : ', batch_gnn)
            print('device :', device)
            print('****************   *   ****   *   ******************')
            print('\n')
            
            print('############################# STEP 1 - DATA PROCESSING OF ALL TOPICS ############################# \n')
            t1 = time.time()
            if(text_model == False and pre_trained_feat == False):
                dataset_file = path + 'dataset_GRAPH'
            else:
                dataset_file = path + 'dataset_'+str(text_model)+'_'+str(pre_trained_feat)+'_'+str(all_layer_train)+'_'+str(text_aggregator)
            if(type_sample == 0):
                dataset_file += '.pickle'
            else:
                dataset_file = dataset_file+'_'+str(type_sample)+'_'+str(type_user_to_discard)+'.pickle' #aaa
                node_off_results_by_topic = gp.load_pickle(node_off_file)
            print(dataset_file)
            
            # dataset_file = path + 'dataset_'+code_xp+'.pickle'
            if(os.path.isfile(dataset_file) == True):
                    dataset_by_topic = gp.load_pickle(dataset_file)  
                    print(' - Find', len(dataset_by_topic), 'topics pre-processed !')
            else:
                    dataset_by_topic = {}
            cpt_top = 0
            
            for topic in TOPICS_TO_LOOK:
                t2 = time.time()
                t3 = time.time()
                key = topic
                cpt_top+=1            
                if(data_type == ut.mydata):
                    uuid_request = ut.request_to_topic[topic]
                    label = ut.my_data_labels[topic]
                elif(data_type == ut.data_zarate):
                    uuid_request = topic
                    label = ut.data_zarate_labels[topic]
                else:
                    uuid_request = topic
                    label = ut.data_qc_labels[topic]
                
                print('*======================================================================================*')
                print('-------- graph',cpt_top,'/', len(all_topics),':', topic, label, ':', key, '* -------- \n')
                print('*======================================================================================*')
                
                if(key in dataset_by_topic.keys()):
                    print(' > already done !')
                    continue
                
                current_path = data_object_path + str(key) +'/'
                if(os.path.isdir(current_path) == False):
                    os.mkdir(current_path)        
                object_file = current_path + 'data_object.pickle'
                res = gp.load_pickle(object_file)
                if(res == False):
                    ### 1. get user graph information
                    print("\n ### 1. get user graph information")
                    if(data_type == ut.mydata):
                        dict_tweets, dict_nodes, dict_edges, dict_edge_weights = gp.create_user_info_graph(type_graph, uuid_request, period, to_connex, minimum_links)
                    elif(data_type == ut.dataqc_text):
                        dict_tweets, dict_nodes, dict_edges, dict_edge_weights = gp.create_user_QC_TEXT_graph(topic, type_graph, to_connex, minimum_links)
                    elif(data_type == ut.data_zarate):
                        dict_tweets, dict_nodes, dict_edges, dict_edge_weights = gp.create_user_ZARATE_graph(current_path, topic, type_graph, to_connex, minimum_links)
                    else:
                        dict_tweets = None
                        dict_nodes, dict_edges, dict_edge_weights = gp.create_user_QC_graph(topic, type_graph, to_connex, minimum_links)
                    if(dict_nodes is None and dict_edges is None):
                        print(' /!\ /!\ /!\ We did not find the topic ', topic, ' on our database /!\ /!\ /!\ ')
                        continue
                    print('--- 1. ', round(((time.time() - t3)/60), 2), 'min ---') 
                    t3 = time.time()
                      
                    ## 2. get new node id and edge corresponding
                    print("\n ### 2. get new node id and edge corresponding >>>")
                    dict_user_to_id, dict_id_to_user, dict_new_nodes, dict_new_edges, dict_new_edge_weights = gp.create_new_node_id(dict_nodes, dict_edges, dict_edge_weights)
                    print('--- 2. ', round(((time.time() - t3)/60), 2), 'min ---') 
                    t3 = time.time()
                                
                    ### 3. get node features
                    print("\n ### 3. get node features")
                    # dict_node_text, dict_node_tokens, dict_node_degree = (None, None, None)
                    type_model_temp = text_model
                    if(text_model == False):
                        type_model_temp = ut.BASIC
                    dict_node_text, dict_node_tokens, dict_node_degree = gp.get_user_node_features_v2(dict_new_nodes, dict_new_edges, dict_tweets, type_model_temp, to_connex, batch_bert)
        
                    print('%%% Test dict_node_features : ', len(dict_node_text), len(dict_node_tokens), len(dict_node_degree))
                    print(' --- 3. ', round(((time.time() - t3)/60), 2), 'min ---') 
                    t3 = time.time()
                    
                    ### 4. get node label communities
                    print("\n ### 4. get node label communities")
                    print('  #4.a. METIS LABEL')
                    dict_node_label_metis = gp.create_communities(dict_new_edges, method='metis')
                    print('%%% Test dict_node_label_metis : ', len(dict_node_label_metis))     
                    
                    print('  #4.b. LOUVAIN LABEL')
                    dict_node_label_louvain = gp.create_communities(dict_new_edges, method='louvain')
                    print('%%% Test dict_node_label_louvain : ', len(dict_node_label_louvain))         
                    
                    print(' --- 4. ', round(((time.time() - t3)/60), 2), 'min ---') 
                    t3 = time.time()
            
                    ### 5. get node label communities
                    print("\n ### 5. get node label from unsupervised learning [PAUSE]")
                    # dict_node_label_sample = gp.get_user_node_label(label_method, topic, type_graph)
                    dict_node_label_sample = None
                    print(' --- 5. ', round(((time.time() - t3)/60), 2), 'min ---')
                    t3 = time.time()
                    
                    ###  6. Create giphi files for vizu
                    print("\n ### 6. create giphi files label")
                    gp.save_giphi_files_comu(current_path, (dict_node_label_metis, dict_node_label_louvain), dict_new_edges, type_label='metis_louvain')
                    
                    ### 6. Save pickle files
                    print("\n ### 7. Save pickle files")
                    data_to_save = (dict_tweets, dict_nodes, dict_edges, dict_new_nodes, dict_new_edges, dict_user_to_id, dict_id_to_user, dict_new_nodes,
                                    dict_node_label_metis, dict_node_label_louvain, dict_node_label_sample, dict_node_text, dict_node_tokens, dict_node_degree, dict_new_edge_weights)
                    gp.pickle_save(object_file, data_to_save)
                    print(' --- 7. ', round(((time.time() - t3)/60), 2), 'min ---') 
                    t3 = time.time()
                
                else:
                    print('<data object already loaded>')
                    dict_tweets, dict_nodes, dict_edges, dict_new_nodes, dict_new_edges, dict_user_to_id, dict_id_to_user, dict_new_nodes, dict_node_label_metis, dict_node_label_louvain, dict_node_label_sample, dict_node_text, dict_node_tokens, dict_node_degree, dict_new_edge_weights = res
                    print(' --- 0. ', round(((time.time() - t3)/60), 2), 'min ---') 
                    t3 = time.time()
                    
                print('######### STEP 1*- GET BASELINE SCORE ######## \n')
                print("\n ### 8. Get baseline scores")
                if(get_baseline_score == True):
                    result_file = path + 'baseline_score.json'
                    baseline_results = qs.get_baseline_score(key, dict_new_edges, dict_new_edge_weights, dict_node_label_metis, result_file, type_scores=type_score)
                    print(' --- 8. ', round(((time.time() - t3)/60), 2), 'min ---') 
                    t3 = time.time()          
                    
                print('#### STEP 1**- GRAPH & FEATURE PROCESSING #### \n')
                print("\n ### 9.  GET pre-trained features")
                t3 = time.time()
                f = 'DATA_pre_trained_features'+'_'+str(text_aggregator)+'.pickle'
                pre_trained_feat_file = current_path + f
                if(os.path.isfile(pre_trained_feat_file) == False):
                    type_model_temp = text_model
                    if(text_model == False):
                        type_model_temp = ut.BASIC    
                    filename_temp = topic+'_'+str(key)+'_'+str(text_model)+'_'+str(text_aggregator)+'_tweet_embeddings.pickle'
                    dict_node_features_aggregated, dict_node_features_stacked, new_dict_tweets = gp.get_user_node_features(filename_temp, type_model_temp,
                                                                                                                            dict_new_nodes, dict_new_edges, dict_tweets, text_aggregator, device, batch_bert)
                    gp.pickle_save(pre_trained_feat_file, (dict_node_features_aggregated, dict_node_features_stacked, new_dict_tweets))
                else:
                      dict_node_features_aggregated, dict_node_features_stacked, new_dict_tweets = gp.load_pickle(pre_trained_feat_file)  
                default_feature_of_empty_tweet = new_dict_tweets['DEFAULT_TWEET'][1]
                if(os.path.isfile(ut.CURRENT_OTHER_FOLD+'default_embedding.pickle') == False):
                    gp.pickle_save(ut.CURRENT_OTHER_FOLD+'default_embedding.pickle', default_feature_of_empty_tweet)
                
                for k in dict_new_nodes.keys():
                    if(k not in dict_node_features_stacked.keys()):
                        dict_node_features_stacked[k] = None
                dict_node_features_stacked = dict(collections.OrderedDict(sorted(dict_node_features_stacked.items())))
                print(' --- 9.', round(((time.time() - t3)/60), 2), 'min ---')
                t3 = time.time()
                
                        
                print("\n\n ### 10. Create graphs object")                
                t3 = time.time()        
                if(text_model == False):
                    if(pre_trained_feat == False):
                        print(' >>> degree features (1)')
                        dict_node_features = dict_node_degree
                        only_text = False #force it in case !
                    else:
                        print(' >>> pre_trained_features (2)')
                        dict_node_features = dict_node_features_aggregated
                else:
                    if(all_layer_train == True):
                        print(' >>> top layer training (3)')
                        dict_node_features = dict_node_tokens
                    else:
                        print(' >>> all layers training (4)')
                        dict_node_features = dict_node_features_stacked
                
                G = gp.create_dgl_graph(current_path, text_model, 
                                        dict_new_edges, dict_new_edge_weights, label,
                                        device, is_edge_weighted, is_test_set, undirected = is_undirected)
                print(' -- equal ? ', len(G.nodes()), len(dict_node_features), '--')
                print(' --- 10. ', round(((time.time() - t3)/60), 2), 'min ---') 
                t3 = time.time()       
                print('\n\n\n')
                
                #### ADD THE SAMPLE PART
                print("\n\n ### 11. Sample graphs", type_sample)
                t3 = time.time() 
                if(type_sample == 0 or key in ut.data_zarate_split['TRAIN']):
                    print(' >> no_sample (', type_sample, key, ')')
                    OG = copy.deepcopy(G)
                    OG_dict_node_features = copy.deepcopy(dict_node_features)
                else:
                    node_ids, _, _, node_probas, _ = node_off_results_by_topic[key]
                    dict_probs_by_id = {k:v for k,v in zip(node_ids, node_probas)}
                    
                    ####### aaa
                    if(type_user_to_discard == 'RANDOM'):
                        print('> random !')
                        sorted_list = dict_probs_by_id.keys()
                        idx_to_keep = len(sorted_list) - int(len(sorted_list) * (type_sample/100))
                        usr_to_keep = random.sample(sorted_list, idx_to_keep)
                    else:
                        if(type_user_to_discard == 'BEST'):
                            print('> best !')
                            sorted_list = sorted(dict_probs_by_id.items(), key = lambda x:x[1], reverse = True)
                        elif(type_user_to_discard == 'WORST'):
                            print('> worst !')
                            sorted_list = sorted(dict_probs_by_id.items(), key = lambda x:x[1], reverse = False)
                            
                        idx_to_remove = int(len(sorted_list) * (type_sample/100))      
                        print(' > remove users from score', sorted_list[0] ,'to score', sorted_list[idx_to_remove])
                        usr_to_keep = [x[0] for x in sorted_list[idx_to_remove:]]
                    ####### aaa
                    OG = dgl.node_subgraph(G, usr_to_keep)            
                    OG_dict_node_features = {}
                    for id_, old_id_ in zip(OG.nodes().tolist(), OG.ndata[dgl.NID].tolist()):
                        OG_dict_node_features[id_] = dict_node_features[old_id_]
                print(' > from', len(G.nodes()), 'users to', len(OG.nodes()), 'users')
                      
                print("\n\n ### 11. Save ALL in dataset")     
                dataset_by_topic[key] = [OG, OG_dict_node_features]
                gp.pickle_save(dataset_file, dataset_by_topic)
                
                print(' **** TOTAL time for topic : ', round(((time.time() - t2)/60)/60, 2), 'hours ****') 
                print('\n\n')
                
            if(type_sample not in DICT_BY_SAMPLE_BY_DISCARDUSER):
                DICT_BY_SAMPLE_BY_DISCARDUSER[type_sample] = {}
            DICT_BY_SAMPLE_BY_DISCARDUSER[type_sample][type_user_to_discard] = dataset_by_topic
                
            print(' ********* TIME STEP 1: DATA PROCESSING --> ', round(((time.time() - t1)/60)/60, 2), 'hours *********')     
            print('\n\n\n')
            # stop_for_now
 
    
    print('TO SAVE...')
    final_path = path + '/DICT_BY_SAMPLE_BY_DISCARDUSER.pickle'
    gp.pickle_save(final_path, DICT_BY_SAMPLE_BY_DISCARDUSER)
    print('finish !')

#----------------------------------------------------------------------------
    print(' \n\n\n\n >>>>>>> II- Start plot : ')
    final_path = path + '/DICT_BY_SAMPLE_BY_DISCARDUSER.pickle'
    DICT_BY_SAMPLE_BY_DISCARDUSER = gp.load_pickle(final_path)
    
    plot_path = path + 'PLOT/'
    if(os.path.isdir(plot_path) == False):
        os.mkdir(plot_path)
    
    print('xxxxxxxxx  verif  xxxxxxxxxx\n\n')
    
    for k1,v1 in DICT_BY_SAMPLE_BY_DISCARDUSER.items():
        for k2,v2 in v1.items():
            for k3,v3 in v2.items():
                print(' --', k1, k2, k3)
    
    print('xxxxxxxxxxxxxxxxxxxxxxxxxxxx\n\n')
    
    dict_rwc_by_sample_by_discard_by_topic = {}
    for type_sample in list_type_samples:
        if(type_sample == 0):
            type_user_to_discard = 'BEST'
            dataset_by_topic = DICT_BY_SAMPLE_BY_DISCARDUSER[type_sample][type_user_to_discard]
            for topic in TOPICS_TO_LOOK:
                    print('\n\n >>', type_sample, topic, '<<')
                    G, _ = dataset_by_topic[topic]

                    object_file = data_object_path + str(topic) +'/data_object.pickle'  
                    res = gp.load_pickle(object_file)
                    
                    #get object and corresponding values
                    _, _, _, _, dict_edges, _, _, _, dict_node_label, _, _, _, _, _, dict_edge_weights = res

                    #get plot
                    rwc_score = qs.get_rwc_score_for_sampled_graph(dict_edges, dict_edge_weights, dict_node_label)
                    
                    if(type_sample not in dict_rwc_by_sample_by_discard_by_topic):
                        dict_rwc_by_sample_by_discard_by_topic[type_sample] = {}
                    if(type_user_to_discard not in dict_rwc_by_sample_by_discard_by_topic[type_sample]):
                        dict_rwc_by_sample_by_discard_by_topic[type_sample][type_user_to_discard] = {}
                    dict_rwc_by_sample_by_discard_by_topic[type_sample][type_user_to_discard][topic] = rwc_score
                    
                    
                    #get plot
                    to_add = str(type_sample)
                    gp.save_giphi_files(plot_path, topic, dict_node_label, dict_edges, to_add)
                    
        else:
            for type_user_to_discard in list_type_user_to_discard:
                dataset_by_topic = DICT_BY_SAMPLE_BY_DISCARDUSER[type_sample][type_user_to_discard]
                
                for topic in TOPICS_TO_LOOK:
                    print(' >>', type_sample, type_user_to_discard, topic, '<<')
                    G, _ = dataset_by_topic[topic]
                
                    object_file = data_object_path + str(topic) +'/data_object.pickle'  
                    res = gp.load_pickle(object_file)
            
                    #get object and corresponding values
                    _, _, _, _, _, _, _, _, dict_node_label_metis, _, _, _, _, _, dict_new_edge_weights = res
                    
                    dict_node_label = {}
                    for id_, old_id_ in zip(G.nodes().tolist(), G.ndata[dgl.NID].tolist()):
                        dict_node_label[id_] = dict_node_label_metis[old_id_]
                        
                    dict_edges = {}
                    dict_edge_weights = {}
                    cpt = 0
                    all_in, all_out = G.edges()
                    for in_, out_ in zip(all_in, all_out):                        
                        dict_edges[cpt] = (in_.item(), out_.item())
                        dict_edge_weights[(in_.item(), out_.item())] = 1
                        cpt+=1
                    
                    #get plot
                    rwc_score = qs.get_rwc_score_for_sampled_graph(dict_edges, dict_edge_weights, dict_node_label)
                    
                    if(type_sample not in dict_rwc_by_sample_by_discard_by_topic):
                        dict_rwc_by_sample_by_discard_by_topic[type_sample] = {}
                    if(type_user_to_discard not in dict_rwc_by_sample_by_discard_by_topic[type_sample]):
                        dict_rwc_by_sample_by_discard_by_topic[type_sample][type_user_to_discard] = {}
                    dict_rwc_by_sample_by_discard_by_topic[type_sample][type_user_to_discard][topic] = rwc_score
                    
                    
                    #get plot
                    to_add = str(type_sample) + '_' + type_user_to_discard + '_'
                    gp.save_giphi_files(plot_path, topic, dict_node_label_metis, dict_edges, to_add)
    
    gp.write_json(plot_path+'ALL_RWC_SCORE_SAMPLE.json', dict_rwc_by_sample_by_discard_by_topic)
    print('finish !!!!')
            
            
            
            

    