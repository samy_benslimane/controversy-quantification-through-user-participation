#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 27 16:16:16 2023

@author: samy
"""
import graph_processing as gp

import torch.nn.functional as F
import torch.nn as nn
import torch

import numpy as np
import copy
import time
import os

import util as ut
import matplotlib.pyplot as plt

from sklearn.metrics import classification_report, accuracy_score
from transformers import BertTokenizer, BertModel
from transformers import (AdamW, 
                          get_linear_schedule_with_warmup,
                          set_seed)

from torch.utils.data import WeightedRandomSampler
from torch.utils.data import DataLoader

import html
import re
import pandas as pd
import random

from scipy.linalg import logm, expm
import scipy as sp
from scipy.special import logsumexp


early_stop = 3

#-----------------------------------------------------------------------

"""
get min number of samples on one topic
dataset = {topic: [_,_,_,_]}
"""
def get_min_nodes_by_graph(dataset):
    min_nodes = None
    for topic, v in dataset.items():
        list_text, _, _, _ = v
        nbs = len(list_text)
        if(min_nodes is None or nbs < min_nodes):
            min_nodes = nbs
           
    print(' > min_nodes:', min_nodes)
    return min_nodes


def get_random_elements(data, s):
    train_tweets, train_labels, ids, train_tokenized_tweets = data
    indices = random.sample(range(len(train_labels)), s)
    
    tweets_kept = [train_tweets[i] for i in indices]
    label_kept = [train_labels[i] for i in indices]
    ids_kept = [ids[i] for i in indices]
    
    tokens_kept = {}
    for k,v in train_tokenized_tweets.items():
        tokens_kept[k] = v[indices]
    
    return tweets_kept, label_kept, ids_kept, tokens_kept

def concat_tokenized_tweets(t1, t2):
    if(len(t1) == 0):
        new_X_train = t2
    else:
        new_X_train = {}
        for key in t1.keys():
            new_X_train[key] = torch.cat([t1[key], t2[key]], dim=0)
    
    return new_X_train
    


"""
bert_fine_tuning with train & test set only

TRAIN_SET = {topic: list_text, list_label}
"""
def bert_fine_tuning(xp_path, model_name, TRAIN_SET, TEST_SET,
                     device, 
                     bert_epochs = 30,
                     bert_batches = 64,
                     max_length = 256,
                     lr_rate = 2e-5,
                     truncate = True,
                     discr_lr = True):
    
    # bert_epochs = 10
    print('<<<<<<<<<<<<<<<<<<<< TRAIN BERT MODEL FT >>>>>>>>>>>>>>>>>>>>>>>')
    print('******************************')
    print('       INIT PARAMETERS       ')
    print('+ nbs topics train/test : ', len(TRAIN_SET), len(TEST_SET))
    print('+ device : ', device)
    print('+ bert_epochs : ', bert_epochs)
    print('+ bert_batches : ', bert_batches)
    print('+ max_length : ', max_length)
    print('+ lr_rate : ', lr_rate)
    print('+ truncate : ', truncate)
    print('+ discr_lr : ', discr_lr)
    print('+ *early stoping*')
    print('******************************')
    
    
    ##### 1. GET MODEL & TOKENIZER if model already exists #####
    # Loading tokenizer...
    if(os.path.isdir(ut.basic_tokenizer)==False):
        tokenizer = BertTokenizer.from_pretrained(ut.basic_hugging_face)
    else:
        tokenizer = BertTokenizer.from_pretrained(ut.basic_tokenizer)
    #... And load model...
    if(os.path.isdir(ut.BASIC_MODEL)==False):
        pretrained_bert_model = BertModel.from_pretrained(ut.basic_hugging_face)
    else:
        pretrained_bert_model = BertModel.from_pretrained(ut.BASIC_MODEL)        
       
    path_fine_tuned_model = xp_path + model_name
    print(path_fine_tuned_model)
    #delete previous fine-tuned model, because different split train/test
    if(os.path.exists(path_fine_tuned_model) == True):
        print('- /!\ a model fine-tuned was found & KEPT !')
        model = torch.load(path_fine_tuned_model)
        results = gp.load_json(xp_path + 'train_info.json')
        return model, tokenizer, results        
    
    #get model
    model = Tweet_BERTModel(ut.basic_hugging_face, pretrained_bert_model)
    #... and put it on device
    if(torch.cuda.is_available() == False):
        print('- No gpu available !')
        model.to(torch.device("cpu"))
        device_ = 'cpu'
    else:
        first_gpu = 'cuda:'+str(device[0])
        model.to(torch.device(first_gpu)) #put the model on GPU(s) --for the backpropagation part--
        if(len(device) > 1):
            print('- Use of mutliple gpu !')
            model = nn.DataParallel(model, device_ids = device) #--for the forward part--
        device_ = ['cuda:'+d for d in device]
        

    ##### 2. GET OPTIMIZER & LOSS FUNCTION #####
    print(' > add weight decay')
    if(discr_lr == True):
        grouped_parameters = configure_optimizers(model, lr_rate)
        optimizer = AdamW(grouped_parameters, lr = lr_rate, eps = 1e-8, weight_decay=0.05)
    else:
        optimizer = AdamW(model.parameters(), lr = lr_rate, eps = 1e-8, weight_decay=0.05)
    criterion = nn.CrossEntropyLoss()
    
    ##### 3. TOKENIZE ALL TEXTS #####
    tokenize_file = xp_path + 'train_test_tokenized_set.pickle'
    if(os.path.isfile(tokenize_file) == True):
        TRAIN_SET, TEST_SET = gp.load_pickle(tokenize_file)
    else:
        TRAIN_SET, TEST_SET = tokenize_all_set(TRAIN_SET, TEST_SET, tokenizer, max_length)
        gp.pickle_save(tokenize_file, (TRAIN_SET, TEST_SET))
      
    ##### 4. TRAIN LOOP for each epoch #####
    min_nodes_by_graph = get_min_nodes_by_graph(TRAIN_SET)
    
    # set parameters of EPOCHS LOOP
    final_model = copy.deepcopy(model)
    start_epoch = 0
    best_train_epoch = -1
    best_train_loss = None
    list_train_loss = []
    best_train_acc = None
    list_train_acc = []
    cpt_early_stop = 0
    runtime = 0
    set_seed(123)
    
    fold_chkpt = xp_path + ut.checkpoint_folder
    if(os.path.isdir(fold_chkpt) == False):
        os.mkdir(fold_chkpt)
    
    if(len(os.listdir(fold_chkpt)) > 0):
        file = os.listdir(fold_chkpt)[0]
        checkpoint_path = fold_chkpt + file
    
        if(os.path.exists(checkpoint_path)):
            print(' > found checkpoint!')
            chk = torch.load(checkpoint_path)
            model.load_state_dict(chk['model_state_dict'])
            optimizer.load_state_dict(chk['optimizer_state_dict'])
            start_epoch = chk['epoch']+1
            best_train_epoch = chk['best_train_epoch']
            best_train_loss = chk['best_train_loss']
            list_train_loss = chk['list_train_loss']
            best_train_acc = chk['best_train_acc']
            list_train_acc = chk['list_train_acc']
            cpt_early_stop = chk['cpt_early_stop']
            runtime = chk['runtime']
            
            final_model = torch.load(xp_path+'final_TEMP.pth')        
    print(' >> early_stop=', early_stop)
    
    for e in range(start_epoch, bert_epochs):         
        print("------------------", e, "---------------------------")
        t_ep = time.time()
        ################# A. TRAINING  #################
        
        ### 1. GET train set
        X_train = {}
        y_train = []
        cpt_train_labs=[0,0]
        for topic, data in TRAIN_SET.items():
            tweets_kept, labels_kept, ids_kept, tokenized_tweets_kept = get_random_elements(data, min_nodes_by_graph)
            X_train = concat_tokenized_tweets(X_train, tokenized_tweets_kept)
            y_train += labels_kept
            cpt_train_labs[labels_kept[0]]+=len(tokenized_tweets_kept)
            
        ### 2. CREATE dataset & dataloader object 
        train_dataset = TwitterDataset_v2(X_train, y_train)
        train_dataloader = DataLoader(train_dataset,                                      
                                      batch_size=bert_batches,
                                      shuffle=True)
        
        
        ### 3. set parameters
        total_steps = len(train_dataset) * bert_epochs # number of training steps (nbs_batch * nbs_epochs)
        scheduler = get_linear_schedule_with_warmup(optimizer, 
                                                    num_warmup_steps = 0,
                                                    num_training_steps = total_steps) # Create the learning rate scheduler.

        
        ### 4. run model
        train_labels, train_predict, epoch_loss = train(model, train_dataloader,
                                                        criterion, optimizer, scheduler, device_)
        epoch_acc = accuracy_score(train_labels, train_predict)
        
        
        ################# B. SAVE BEST MODEL METRICS  #################
        if(early_stop is None):
            final_model = copy.deepcopy(model)
        else:
            if(best_train_loss is not None and best_train_loss < epoch_loss):
                if(cpt_early_stop >= early_stop):
                    print(' > hop we stop, new_train_loss=', epoch_loss, 'best train loss=', best_train_loss, '==> cpt_early=', cpt_early_stop)
                    break
                else:
                    cpt_early_stop+=1
            else:
                print(' ! best model !')
                best_train_epoch = e
                best_train_loss = epoch_loss
                best_train_acc = epoch_acc
                final_model = copy.deepcopy(model)
                cpt_early_stop = 0        
        train_epoch_runtime = time.time() - t_ep
        runtime += train_epoch_runtime
        
        ################# C. SAVE CHKPT & METADATA  #################        
        #1 SAVE LOSS & ACC train test sets
        list_train_loss.append(epoch_loss)
        list_train_acc.append(epoch_acc)
                
        #2 SAVE FINAL MODEL
        if(best_train_epoch == e):
            save_model(xp_path+'final_TEMP.pth', final_model)
        
        #3 SAVE CHKPT MODEL
        for old_file in os.listdir(fold_chkpt):
              os.remove(fold_chkpt+old_file)
             
        # save current epochs 
        checkpoint_file = fold_chkpt + str(e)+'_'+model_name 
        torch.save({
            'epoch': e,
            'model_state_dict': model.state_dict(),
            'optimizer_state_dict': optimizer.state_dict(),
            'best_train_epoch': best_train_epoch,
            'best_train_loss': best_train_loss,
            'list_train_loss': list_train_loss,
            'best_train_acc': best_train_acc,
            'list_train_acc': list_train_acc,
            'cpt_early_stop': cpt_early_stop,
            'runtime': runtime
            }, checkpoint_file)
        
        
        ####TODO delete
        ################# 5. EVALUATION  #################   
        ### 1. GET test set
        X_test = {}
        y_test = []
        cpt_test_labs=[0,0]
        for topic, data in TEST_SET.items():
            tweets_kept, labels_kept, ids, tokenized_tweets_kept = data
            X_test = concat_tokenized_tweets(X_test, tokenized_tweets_kept)
            y_test += labels_kept
            cpt_test_labs[labels_kept[0]]+=len(tokenized_tweets_kept)
            
        ### 2. CREATE dataset & dataloader object 
        test_dataset = TwitterDataset_v2(X_test, y_test)
        test_dataloader = DataLoader(test_dataset,                                      
                                      batch_size=bert_batches,
                                      shuffle=False)
        ### 3. run model
        test_labels, test_predict, _, test_loss = validation(final_model, test_dataloader, criterion, device_)
        test_acc = accuracy_score(test_labels, test_predict)
        test_report = classification_report(test_labels, test_predict)
        ####
    
        ################# D. PRINT LOG  #################
        if(e%1 == 0):
            print('In epoch {}, train loss: {:.3f} + train acc: {:.3f}'.format(
                e, epoch_loss, epoch_acc))
            print('           > BEST: In epoch {}, train_loss: {:.3f} + train_acc: {:.3f}'.format(
                best_train_epoch, best_train_loss, best_train_acc))
            print(' > len(X_train):', len(X_train), "| balanced ?", cpt_train_labs)
            print(' --> time :', round(((time.time() - t_ep)/60), 2), 'min --- ')
            
            ####TODO delete
            print('*******')
            print('metric:', test_acc, test_loss)
            print(test_report)
            print(' > len(X_train):', len(X_test['input_ids']), "| balanced ?", cpt_test_labs)
            #####
            
        print("------------------------------------------------- \n")


    ################# 5. EVALUATION  #################   
    ### 1. GET test set
    X_test = {}
    y_test = []
    cpt_test_labs=[0,0]
    for topic, data in TEST_SET.items():
        tweets_kept, labels_kept, ids, tokenized_tweets_kept = data
        X_test = concat_tokenized_tweets(X_test, tokenized_tweets_kept)
        y_test += labels_kept
        cpt_test_labs[labels_kept[0]]+=len(tokenized_tweets_kept)
        
    ### 2. CREATE dataset & dataloader object 
    test_dataset = TwitterDataset_v2(X_test, y_test)
    test_dataloader = DataLoader(test_dataset,                                      
                                  batch_size=bert_batches,
                                  shuffle=False)
    
    ### 3. run model
    test_labels, test_predict, _, test_loss = validation(final_model, test_dataloader, criterion, device_)
    test_acc = accuracy_score(test_labels, test_predict)
    test_report = classification_report(test_labels, test_predict)

    
    ### 4. print logs
    print("----------------------- FINAL METRICS TRAIN ---------------------------- \n")
    print(' > best epoch:', best_train_epoch, ' | train_loss: ', best_train_loss, 
          '| train_acc', best_train_acc, '\\ runtime=', runtime)
    print("------------------- ----------------------------- ---------------------------- \n")
    print("----------------------- FINAL METRICS TEST ---------------------------- \n")
    print(' > test_loss: ', test_loss, '| test_acc', test_acc)
    print(' > len(X_test):', len(X_test), "| not balanced ?", cpt_test_labs, '\n')
    print(test_report)
    print("------------------- ----------------------------- ---------------------------- \n")
    
    
    ### 5. plot loss and acc
    plot_path = xp_path + 'plot/'
    if(os.path.isdir(plot_path) == False):
        os.mkdir(plot_path)
    
    p_train_loss = plot_path + "train_loss.png"
    plt.plot(range(len(list_train_loss)), list_train_loss)
    plt.savefig(fname = p_train_loss)
    plt.show()
    
    p_train_acc = plot_path + "train_acc.png"
    plt.plot(range(len(list_train_acc)), list_train_acc)
    plt.savefig(fname = p_train_acc)
    plt.show()
    
    
    ################# 6. SAVE RESULTS #################      
    ### 1. save xp logs (json)
    results = {'train_runtime': round(runtime/60,4),
                  'train_loss': best_train_loss,
                  'test_acc': test_acc}
    gp.write_json(xp_path+'train_info.json', results)
    
    ### 2. save classification_report test (json)
    test_report_dict = classification_report(test_labels, test_predict, output_dict=True)
    gp.write_json(xp_path + 'classification_report_test.json', test_report_dict)
    
    ### 3. save final model temp 
    torch.save(final_model, path_fine_tuned_model)
    print('- model saved! ', path_fine_tuned_model)
   
    ### 4. delete checkpoint
    for old_file in os.listdir(fold_chkpt):
         os.remove(fold_chkpt+old_file)
    
    
    return final_model, tokenizer, results
    



"""
CONTROVERSY
class representing our model composed by a bert model + 1 more dense layer of 64 neurons + 1 softmax layer
Fine-tuning already done, here it is just a forward model
"""
class Tweet_BERTModel(nn.Module):
    """
    init function
    input : 
        - model_name_or_path : name of the pre-trained model
        - type_model : 'sentiment' or 'controversy'
        - pretrained_bert_model : pre-trained model il already loaded (default=None)
    """
    def __init__(self, model_name_or_path, pretrained_bert_model=None):
        super(Tweet_BERTModel, self).__init__()
        
        if(pretrained_bert_model is None):
            self.bert_model = BertModel.from_pretrained(model_name_or_path)
        else:
            self.bert_model = pretrained_bert_model
            
        self.dropout = nn.Dropout(0.2)
    
        self.embedding_layer = nn.Linear(768, 768)
        self.classifier_layer = nn.Linear(768, 2)
        
        
    """
    forward function
    input :
        - bert_ids : bert id of the sentence (from tokenization)
        - bert_mask : bert mask og the sentence (from tokenization)
    output :
        - out_ : prediction
        - out_embedding : embedding representation   
    """
    def forward(self, x, forward_2 = False):
        if(forward_2 == False):
            bert_ids = x[0]
            bert_mask = x[1]
                        
            #sequence_output, pooled_output = self.bert_model(bert_ids, attention_mask=bert_mask)
            out_bert = self.bert_model(bert_ids, attention_mask=bert_mask)
            sequence_output = out_bert['last_hidden_state']
            
            #Layer 1
            # sequence_output has the following shape: (batch_size, sequence_length, 768)
            seq = sequence_output[:,0,:].view(-1,768)
            seq = self.dropout(seq)
            seq = self.embedding_layer(seq) ## extract the 1st token's embeddings
            
            out_embedding = F.relu(seq)
        
            #Layer 2
            out_ = self.classifier_layer(out_embedding)
            out_ = F.softmax(out_, dim=1)
            
            return out_, out_embedding
        
        else:
            bert_ids = x[0]
            bert_mask = x[1]
                        
            #sequence_output, pooled_output = self.bert_model(bert_ids, attention_mask=bert_mask)
            out_bert = self.bert_model(bert_ids, attention_mask=bert_mask)
            sequence_output = out_bert['last_hidden_state']
            
            #Layer 1
            # sequence_output has the following shape: (batch_size, sequence_length, 768)
            seq = sequence_output[:,0,:].view(-1,768)
            seq = self.dropout(seq)
            seq = self.embedding_layer(seq) ## extract the 1st token's embeddings
            
            out_embedding = F.relu(seq)
        
            #Layer 2
            out_no_soft = self.classifier_layer(out_embedding)
            out_ = F.softmax(out_, dim=1)
            
            return out_, out_no_soft, out_embedding
    
        
"""
tokenize both train and test set and clean text
"""
def tokenize_all_set(TRAIN_SET, TEST_SET, tokenizer, max_sequence_len=256):
    for k,v in TRAIN_SET.items():
        tweets, labels, ids = v
        clean_tweets = clean_text_v2(tweets)
        tokenized_tweets = tokenize_texts(tokenizer, clean_tweets, max_sequence_len)
        
        TRAIN_SET[k] = [clean_tweets, labels, ids, tokenized_tweets]
        
    for k,v in TEST_SET.items():
        tweets, labels, ids = v
        clean_tweets = clean_text_v2(tweets)
        tokenized_tweets = tokenize_texts(tokenizer, clean_tweets, max_sequence_len)
        
        TEST_SET[k] = [clean_tweets, labels, ids, tokenized_tweets]
        
    return TRAIN_SET, TEST_SET


"""
tokenize both train and test set and clean text
"""
def tokenize_set(SET, tokenizer, max_sequence_len=256):       
    for k,v in SET.items():
        tweets, labels, ids = v
        clean_tweets = clean_text_v2(tweets)
        tokenized_tweets = tokenize_texts(tokenizer, clean_tweets, max_sequence_len)
        
        SET[k] = [clean_tweets, labels, ids, tokenized_tweets]
        
    return SET


    
"""
tokenize all texts directly
"""
def tokenize_texts(tokenizer, texts, max_sequence_len):
    if(len(texts) == 0):
        return {}
    
    tokenized_texts = tokenizer(text=texts,  # the sentence to be encoded
                                add_special_tokens=True,  # Add [CLS] and [SEP]
                                truncation=True,
                                max_length = max_sequence_len,  # maximum length of a sentence
                                pad_to_max_length=True,  # Add [PAD]s
                                return_attention_mask = True,  # Generate the attention mask
                                return_tensors = 'pt',  # ask the function to return PyTorch tensors
                                )
    
    return tokenized_texts

  
"""
class which will transform (tokenized) the input text, and keep its corresponding label 
"""
class TwitterDataset_v2():
    """
    init function
    input : 
        - y : Dictionary to encode any labels names into numbers. Keys map to labels names and Values map to number associated to those labels.
    """
    def __init__(self, tokenized_X, labels):
        # Number of exmaples.
        self.n_examples = len(labels)
        
        # Use tokenizer on texts. This can take a while.
        if(len(tokenized_X) > 0):
            self.inputs = tokenized_X  
    
            # Get maximum sequence length.
            self.sequence_len = self.inputs['input_ids'].shape[-1]
            print(' > Texts padded or truncated to', self.sequence_len, 'length!')
    
            # Add labels.
            self.inputs.update({'labels':torch.tensor(labels)})

        else:
            print(' > no sample inside!')
            self.inputs = {}

    def __len__(self):
        return self.n_examples

    def __getitem__(self, item):
        return {key: self.inputs[key][item] for key in self.inputs.keys()}
    

    
"""
clean text
"""
def clean_text_v2(inputs, is_list=True):
    if(is_list == True):
        new_inputs = []
        for tweet in inputs:
            #remove retweet
            tweet = re.sub('(?<=^|(?<=[^a-zA-Z0-9-_\.]))@([A-Za-z0-9-_]+[A-Za-z0-9-_]+)', r'USER', tweet)
            
            #remove URL
            tweet = re.sub(r'http\S+', r'URL', tweet)
            
            tweet = html.unescape(tweet)     
            tweet = re.sub(r'\n', r'', tweet)
            new_inputs.append(tweet)
            
        return new_inputs
    
    
"""
get a sampler for having more balanced dataset for the dataloader
input : 
    - labels : list of label of the dataloader
output :
    - sampler : sampler which will sample the data
"""
def get_balanced_sampler(labels):
    lab_unique, counts = np.unique(labels, return_counts=True)
    
    class_w = [sum(counts)/c for c in counts]
    balanced_w = [class_w[e] for e in labels]
    
    sampler = WeightedRandomSampler(balanced_w, len(labels))
    
    return sampler


"""
test training proportion of the dataloader
input : 
    - train_dataloader : dataloader of training set
"""
def test_train_proportion(train_dataloader):
    lab0 = 0
    lab1 = 0
    for step, batch in enumerate(train_dataloader):
        list_lab =  batch['labels']
        for l in list_lab:
            if(l == 0):
                lab0+=1
            else:
                lab1+=1
                
    tot = lab0 + lab1
    print('--- PROPORTION CLASS DATALOADER ---')
    print('lab0 : ', round((lab0/tot)*100, 2), '% (', lab0 ,') / lab1 : ', round((lab1/tot)*100, 2), '% (', lab1 ,') --> TOT : ', tot)
   

################## MODEL FUNCTION ###################

"""
function used to fine-tuned our model
input : 
    - model : model to fine-tuned
    - dataloader : train dataloader
    - criterion : criterion to compute loss
    - optimizer_ : optimizer chosed
    - scheduler_ : scheduler 
    - device_ : device used
output : 
    - true_labels : real labels
    - predictions_labes : predicted labels
    - avg_epoch_loss : average loss by epoch during training
"""
def train(model, dataloader, criterion, optimizer, scheduler, device_):
    predictions_labels = []
    true_labels = []

    total_loss = 0
    model.train() 
    for step, batch in enumerate(dataloader):
        if(device_ != 'cpu'):
            batch = {k:v.type(torch.long).cuda(device_[0]) for k,v in batch.items()}     # move batch to device

        model.zero_grad() #init gradient at 0

        outputs, outputs_embedding = model([batch['input_ids'], batch['attention_mask']])
        loss = criterion(outputs, batch['labels'])
        
        loss.backward() #compute gradient
        torch.nn.utils.clip_grad_norm_(model.parameters(), 1.0)     #  prevent the "exploding gradients" problem.
        optimizer.step() #Apply gradient descent
        scheduler.step() #Update the learning rate.

        total_loss += loss.item() #sum up all loss       
        
        true_labels += batch['labels'].detach().cpu().numpy().flatten().tolist()  
        predictions_labels += outputs.detach().cpu().numpy().argmax(axis=-1).flatten().tolist() 

    avg_epoch_loss = total_loss / len(dataloader)
  
    return true_labels, predictions_labels, avg_epoch_loss


"""
function used to test our model
input : 
    - model : model to fine-tuned
    - dataloader : test dataloader
    - criterion : criterion to compute loss
    - device_ : device used
output : 
    - true_labels : real labels
    - predictions_labes : predicted labels
    - avg_epoch_loss : average loss by epoch during test
"""
def validation(model, dataloader, criterion, device_):
    predictions_labels = []
    probs_labels = []
    true_labels = []

    total_loss = 0
    model.eval()
    for step, batch in enumerate(dataloader):
        if(device_ != 'cpu'):
            batch = {k:v.type(torch.long).cuda((device_[0])) for k,v in batch.items()} #move batch to device

        with torch.no_grad():
            outputs, outputs_embedding = model([batch['input_ids'], batch['attention_mask']])
               
            loss = criterion(outputs, batch['labels'])
            
            total_loss += loss.item()

            true_labels += batch['labels'].detach().cpu().numpy().flatten().tolist() #add original labels
            predictions_labels += outputs.detach().cpu().numpy().argmax(axis=-1).flatten().tolist()
            probs_labels += outputs.detach().cpu().numpy()[:, 1].tolist()
            # embeddings += outputs_embedding.detach().cpu().numpy().tolist()

    avg_loss = total_loss / len(dataloader)

    return true_labels, predictions_labels, probs_labels, avg_loss


"""
Predict QC scores of topics from the model
"""
def predict_model(topics, dataset_by_topic, model, text_model, device, xp_path,
                  bert_batches=64, max_length = 256, type_set='TEST'):
    ########################### Check if results computed ###########################
    ALL_RESULTS_file = xp_path + 'all_results_object.pickle'
    ALL_RESULTS = {}
    QC_SCORES_file = xp_path + 'results.json'
    QC_SCORES_BY_TOPIC = {}
    to_line = True
    
    if(os.path.isfile(ALL_RESULTS_file) == True):
        ALL_RESULTS = gp.load_pickle(ALL_RESULTS_file)     
    if(os.path.isfile(QC_SCORES_file) == True):
        QC_SCORES_BY_TOPIC = gp.load_json(QC_SCORES_file)  
        
    if(type_set in QC_SCORES_BY_TOPIC.keys()):
        print(' > already computed !')
        return QC_SCORES_BY_TOPIC, ALL_RESULTS
    else:
        QC_SCORES_BY_TOPIC[type_set] = {}   
    
    
    ########################### get tokenizer, model ###########################
    #1. Get tokenizer
    if(text_model == ut.BASIC):
        if(os.path.isdir(ut.basic_tokenizer)==False):
            tokenizer = BertTokenizer.from_pretrained(ut.basic_hugging_face)
        else:
            tokenizer = BertTokenizer.from_pretrained(ut.basic_tokenizer) 
    criterion = nn.CrossEntropyLoss()
        
    #2. Put model on device
    if(torch.cuda.is_available() == False):
        print('- No gpu available !')
        model.to(torch.device("cpu"))
        device_ = 'cpu'
    else:
        first_gpu = 'cuda:'+str(device[0])
        model.to(torch.device(first_gpu)) #put the model on GPU(s) --for the backpropagation part--
        if(len(device) > 1):
            print('- Use of mutliple gpu !')
            model = nn.DataParallel(model, device_ids = device) #--for the forward part--
        device_ = ['cuda:'+d for d in device]        

        
    ######################## Predict & Quantify topics #########################
    sorted_topic = {k:ut.data_zarate_labels[k] for k in topics}
    sorted_topic = [k for k, v in sorted(sorted_topic.items(), key=lambda x: x[1])]

    all_test_proba = []
    all_test_labs = []
    res_by_topics = {}
    for topic in sorted_topic:   
        topic_lab = ut.data_zarate_labels[topic]
        
        #create set of data
        d_new_nodes, d_tweets, d_tweets_by_all_new_user = dataset_by_topic[topic]
        text_tweets = []
        label_tweets = []
        id_tweets = []
        
        current_label = ut.data_zarate_labels[topic]
        for t_usr, list_tweets in d_new_nodes.items():
            if(list_tweets is not None):
                for tweet_id in list_tweets:
                    if(tweet_id not in id_tweets):
                        text_tweets.append(d_tweets[tweet_id])
                        label_tweets.append(current_label)
                        id_tweets.append(tweet_id)
                
        set_ = {topic: [text_tweets, label_tweets, id_tweets]}
        tokenized_set = tokenize_set(set_, tokenizer, max_sequence_len=256)
        
        # 1. prepare dataloader
        tweets_kept, y_test, ids, X_test = tokenized_set[topic]

        
        # 2. CREATE dataset & dataloader object 
        test_dataset = TwitterDataset_v2(X_test, y_test)
        test_dataloader = DataLoader(test_dataset,                                      
                                      batch_size=bert_batches,
                                      shuffle=False)
        
        # 3. run model
        t_mod = time.time()
        test_labels, test_predict, probs_predict, test_loss = validation(model, test_dataloader, criterion, device_)
        topic_acc = accuracy_score(test_labels, test_predict)
        runtime = (time.time() - t_mod)/60
        
        
        # 4. Get for each tweet, its label, and proba of C
        cpt=0
        dict_scores_tweets = {}
        print(len(ids), 'vs', len(probs_predict))
        for i in ids:
            dict_scores_tweets[i] = [probs_predict[cpt], test_predict[cpt]]
            cpt+=1
    
        # 5. for each user of the topic, compute scores ++ topic QC score
        QC_score = [[], []]
        dict_score_users = {}
        for usr, tweets in d_tweets_by_all_new_user.items():
            probs = []
            preds = []
            for t in tweets:
                probs.append(dict_scores_tweets[t][0])
                preds.append(dict_scores_tweets[t][1])
            usr_prob = sum(probs) / len(probs)
            usr_pred = sum(preds) / len(preds)
            QC_score[0].append(usr_prob)
            QC_score[1].append(usr_pred)
            dict_score_users[usr] = {'tweets_scores:': [probs, preds],
                                     'user_score': [usr_prob, usr_pred]}
            all_test_proba.append(usr_prob)
            all_test_labs.append(topic_lab)
                                
        # 6. compute the topic score
        naive_bayes = gp.get_naive_score(probs)
        
        topic_score = [round(sum(QC_score[0]) / len(QC_score[0]), 3), round(sum(QC_score[1]) / len(QC_score[1]), 3), round(naive_bayes, 3)]
        
        # 7. save qc_score by topic (json)
        res_by_topics[topic] = {'proba': topic_score[0],
                                'prediction': topic_score[1],
                                'bayes': topic_score[2],
                                'accuracy': topic_acc,
                                'runtime': runtime}
        
        # 8. save ALL results (pickle)
        ALL_result_file = xp_path + type_set + '_results_object.pickle'
        ALL_RESULTS[topic] = [dict_scores_tweets, dict_score_users]
        gp.pickle_save(ALL_result_file, ALL_RESULTS)
                
        ### 9. print logs
        if(current_label == 1 and to_line == True):
            print('\n')
            to_line=False
        print('----> ', topic, ': LAB=,', current_label, 'qc_probas (preds) =', topic_score[0], '(', topic_score[1], ')', '---> nb:', naive_bayes)    
    
    
    #TRUE METRICS
    print('---- TRUE METRICS ----')
    test_loss = F.binary_cross_entropy(torch.tensor(all_test_proba, dtype=torch.float32), torch.tensor(all_test_labs, dtype=torch.float32))
    print(test_loss)
    test_acc = accuracy_score(all_test_labs, [0 if x<0.5 else 1 for x in all_test_proba])

    print('> test_acc=', test_acc)
    print(' > test_loss=',test_loss.item())
    
    #SAVE RESULTS
    QC_SCORES_BY_TOPIC[type_set] = {'qc_score':res_by_topics}  
    QC_SCORES_BY_TOPIC[type_set]['metric'] = {'accuracy': -1}
    gp.write_json(QC_SCORES_file, QC_SCORES_BY_TOPIC)
        
    return QC_SCORES_BY_TOPIC, ALL_RESULTS



"""
Predict QC scores of topics from the model
"""
def predict_model_SUBGRAPHS(topics, dataset_by_topic, model, text_model, device, xp_path,
                  bert_batches=64, max_length = 256, type_set='TEST', type_topic='ALL', iter_=5000):
    ########################### Check if results computed ###########################
    ALL_RESULTS_file = xp_path + 'all_results_object.pickle'
    ALL_RESULTS = {}
    QC_SCORES_file = xp_path + 'results.json'
    QC_SCORES_BY_TOPIC = {}
    to_line = True
    
    if(os.path.isfile(ALL_RESULTS_file) == True):
        ALL_RESULTS = gp.load_pickle(ALL_RESULTS_file)     
    if(os.path.isfile(QC_SCORES_file) == True):
        QC_SCORES_BY_TOPIC = gp.load_json(QC_SCORES_file)  
        
    if(type_set in QC_SCORES_BY_TOPIC.keys()):
        print(' > already computed !')
        return QC_SCORES_BY_TOPIC, ALL_RESULTS
    else:
        QC_SCORES_BY_TOPIC[type_set] = {}   
    
    
    ########################### get tokenizer, model ###########################
    #1. Get tokenizer
    if(text_model == ut.BASIC):
        if(os.path.isdir(ut.basic_tokenizer)==False):
            tokenizer = BertTokenizer.from_pretrained(ut.basic_hugging_face)
        else:
            tokenizer = BertTokenizer.from_pretrained(ut.basic_tokenizer) 
    criterion = nn.CrossEntropyLoss()
        
    #2. Put model on device
    if(torch.cuda.is_available() == False):
        print('- No gpu available !')
        model.to(torch.device("cpu"))
        device_ = 'cpu'
    else:
        first_gpu = 'cuda:'+str(device[0])
        model.to(torch.device(first_gpu)) #put the model on GPU(s) --for the backpropagation part--
        if(len(device) > 1):
            print('- Use of mutliple gpu !')
            model = nn.DataParallel(model, device_ids = device) #--for the forward part--
        device_ = ['cuda:'+d for d in device]        

        
    ######################## Predict & Quantify topics #########################
    sorted_topic = {k:ut.data_zarate_labels[k] for k in topics}
    sorted_topic = [k for k, v in sorted(sorted_topic.items(), key=lambda x: x[1])]

    all_test_proba = []
    all_test_labs = []
    res_by_topics = {}
    proba_by_topic = {}
    for topic in sorted_topic:   
        topic_lab = ut.data_zarate_labels[topic]
        
        #create set of data
        d_new_nodes, d_tweets, d_tweets_by_all_new_user = dataset_by_topic[topic]
        text_tweets = []
        label_tweets = []
        id_tweets = []
        
        current_label = ut.data_zarate_labels[topic]
        for t_usr, list_tweets in d_new_nodes.items():
            if(list_tweets is not None):
                for tweet_id in list_tweets:
                    if(tweet_id not in id_tweets):
                        text_tweets.append(d_tweets[tweet_id])
                        label_tweets.append(current_label)
                        id_tweets.append(tweet_id)
                
        set_ = {topic: [text_tweets, label_tweets, id_tweets]}
        tokenized_set = tokenize_set(set_, tokenizer, max_sequence_len=256)
        
        # 1. prepare dataloader
        tweets_kept, y_test, ids, X_test = tokenized_set[topic]

        
        # 2. CREATE dataset & dataloader object 
        test_dataset = TwitterDataset_v2(X_test, y_test)
        test_dataloader = DataLoader(test_dataset,                                      
                                      batch_size=bert_batches,
                                      shuffle=False)
        
        # 3. run model
        t_mod = time.time()
        test_labels, test_predict, probs_predict, test_loss = validation(model, test_dataloader, criterion, device_)
        topic_acc = accuracy_score(test_labels, test_predict)
        runtime = (time.time() - t_mod)/60
        
        
        # 4. Get for each tweet, its label, and proba of C
        cpt=0
        dict_scores_tweets = {}
        print(len(ids), 'vs', len(probs_predict))
        for i in ids:
            dict_scores_tweets[i] = [probs_predict[cpt], test_predict[cpt]]
            cpt+=1
    
        # 5. for each user of the topic, compute scores ++ topic QC score
        QC_score = [[], []]
        dict_score_users = {}
        list_user_probs = []
        for usr, tweets in d_tweets_by_all_new_user.items():
            probs = []
            preds = []
            for t in tweets:
                probs.append(dict_scores_tweets[t][0])
                preds.append(dict_scores_tweets[t][1])
            usr_prob = sum(probs) / len(probs)
            usr_pred = sum(preds) / len(preds)
            QC_score[0].append(usr_prob)
            QC_score[1].append(usr_pred)
            dict_score_users[usr] = {'tweets_scores:': [probs, preds],
                                     'user_score': [usr_prob, usr_pred]}
            all_test_proba.append(usr_prob)
            all_test_labs.append(topic_lab)
            
            list_user_probs.append(usr_prob)
        
        proba_by_topic[topic] = list_user_probs
        # 6. compute the topic score
        naive_bayes = gp.get_naive_score(probs)
        
        topic_score = [round(sum(QC_score[0]) / len(QC_score[0]), 3), round(sum(QC_score[1]) / len(QC_score[1]), 3), round(naive_bayes, 3)]
        
        # 7. save qc_score by topic (json)
        res_by_topics[topic] = {'proba': topic_score[0],
                                'prediction': topic_score[1],
                                'bayes': topic_score[2],
                                'accuracy': topic_acc,
                                'runtime': runtime}
        
        # 8. save ALL results (pickle)
        ALL_result_file = xp_path + type_set + '_results_object.pickle'
        ALL_RESULTS[topic] = [dict_scores_tweets, dict_score_users]
        gp.pickle_save(ALL_result_file, ALL_RESULTS)
                
        ### 9. print logs
        if(current_label == 1 and to_line == True):
            print('\n')
            to_line=False
        print('----> ', topic, ': LAB=,', current_label, 'qc_probas (preds) =', topic_score[0], '(', topic_score[1], ')', '---> nb:', naive_bayes)    
    
    
    #TRUE METRICS
    print('---- TRUE METRICS ----')
    test_loss = F.binary_cross_entropy(torch.tensor(all_test_proba, dtype=torch.float32), torch.tensor(all_test_labs, dtype=torch.float32))
    print('> test_loss:', test_loss)
    test_acc = accuracy_score(all_test_labs, [0 if x<0.5 else 1 for x in all_test_proba])

    print(' > test_acc=', test_acc)
    print(' > test_loss=',test_loss.item())
    
    
    #SAVE RESULTS
    QC_SCORES_BY_TOPIC[type_set] = {'qc_score':res_by_topics}  
    QC_SCORES_BY_TOPIC[type_set]['metric'] = {'accuracy': -1}
    gp.write_json(QC_SCORES_file, QC_SCORES_BY_TOPIC)
    
    
    #### SAVE FOR SUBGRAPHS
    print('--------------------- SAVE FOR SUBGRAPHS --------------------')
    
    if(type_topic == 'ALL'):
        json_loss = xp_path + type_set+'_loss_prediction.json'
    else:
        json_loss = xp_path + 'TOPIC_loss_prediction.json'
    results_loss_score = {}
    if(os.path.isfile(json_loss) == True):
        results_loss_score = gp.load_json(json_loss)    
    
    k_value = 0
    proba_selected = []
    labs_selected = []
    if(k_value not in results_loss_score.keys()):
        for it in range(iter_):
            current_topic = random.choice(topics)
            list_user_probs = proba_by_topic[current_topic]
                        
            proba_selected.append(random.choice(list_user_probs))
            labs_selected.append(ut.data_zarate_labels[current_topic])
        print(proba_selected[:100])
        print(labs_selected[:100])
        test_loss = F.binary_cross_entropy(torch.tensor(proba_selected, dtype=torch.float32), torch.tensor(labs_selected, dtype=torch.float32))
        print('> test_loss:', test_loss, '(', len(proba_selected), 'elements)')
    
        results_loss_score[str(k_value)] = test_loss.item()
        gp.write_json(json_loss, results_loss_score)
    else:
        print(' > already computed !')
    
        
    return QC_SCORES_BY_TOPIC, ALL_RESULTS


"""
output 
"""
def send_to_train(x, dict_label_by_tweet, dict_text):
    lab = dict_label_by_tweet[x['tweet_id']]
    feat = dict_text[x['tweet_id']]

    return [feat, feat, lab, lab]

"""
input :
    - train_tweets : list of train post
    - test_tweets : list of test post
    - dict_tweet_label : label for each post
    - dict_text : text for each post/comment
"""
def get_train_test_set(train_tweets, test_tweets, dict_tweet_label, dict_text):
    
    print(' -get train_test_set-')
    print('- dict_text (', len(dict_text.keys()),')')

                       
    print(' **sol fati2**')
    print('   *send train')
    df = pd.DataFrame({'tweet_id': train_tweets})
    df['temp_train'] = df.apply(lambda x: send_to_train(x, dict_tweet_label, dict_text), axis=1)
    df[['X', 'X_train', 'y', 'y_train']] = pd.DataFrame(df['temp_train'].tolist(), index= df.index)
    X = df[df["X"].isnull() == False]['X'].tolist()
    X_train = df[df["X_train"].isnull() == False]['X_train'].tolist()
    y = df[df["y"].isnull() == False]['y'].tolist()
    y_train = df[df["y_train"].isnull() == False]['y_train'].tolist()
    
    X_train_idx = df[df["X_train"].isnull() == False]['tweet_id'].tolist()
    
    print('   *send test')
    df = pd.DataFrame({'tweet_id': test_tweets})
    df['temp_test'] = df.apply(lambda x: send_to_train(x, dict_tweet_label, dict_text), axis=1)
    if(len(df) == 0):
        df = df.assign(X='', X_test='', y='', y_test='')
    else:
        df[['X', 'X_test', 'y', 'y_test']] = pd.DataFrame(df['temp_test'].tolist(), index= df.index)
    X += df[df["X"].isnull() == False]['X'].tolist()
    X_test = df[df["X_test"].isnull() == False]['X_test'].tolist()
    y += df[df["y"].isnull() == False]['y'].tolist()
    y_test = df[df["y_test"].isnull() == False]['y_test'].tolist()
    
    X_test_idx = df[df["X_test"].isnull() == False]['tweet_id'].tolist()
    
    pb=0    
    print('---problem in get train_test set (NO MEANING) : ', pb)
    
    print('  -- transform to DF no FN')
    X = pd.DataFrame(X)
    X_train = pd.DataFrame(X_train, index=X_train_idx)
    X_test = pd.DataFrame(X_test, index=X_test_idx)
    y = pd.DataFrame(y, columns=['label'])
    y_train = pd.DataFrame(y_train, columns=['label'], index=X_train_idx)
    y_test = pd.DataFrame(y_test, columns=['label'], index=X_test_idx)
    
    print(len(X), len(X_train), len(X_test), len(y), len(y_train), len(y_test))
    print('vs')
    print(len(dict_tweet_label), len(train_tweets), len(test_tweets))
    print('***')
    print(X_train.head(3))
    print(' <finish>')

    return X, X_train, X_test, y, y_train, y_test


"""
function used for creating discriminative learning rate
input :
    - model : the current model
    - the learning rate for bert layers
output :
    - grouped_parameters : the group parameters for each layers
"""
def configure_optimizers(model, lr):
    params = list(model.named_parameters())
    def is_backbone(n): return 'bert' in n
    grouped_parameters = [
        {"params": [p for n, p in params if is_backbone(n)], 'lr': lr},
        {"params": [p for n, p in params if not is_backbone(n)], 'lr': lr * 100},
    ]
    return grouped_parameters

"""
Save GNN model
"""
def save_model(model_path, model):
    torch.save(model, model_path)
    
    return 1