#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb  8 15:39:02 2023

@author: samy
"""

import graph_processing as gp
import util as ut

from scipy.stats import ks_2samp, wasserstein_distance
from sklearn.metrics import roc_auc_score
import matplotlib.pyplot as plt
from datetime import datetime

import seaborn as sns
import pandas as pd
import numpy as np

from sklearn.metrics import mean_absolute_error


data_path = ut.CURRENT_DATA_FOLD + ut.object_folder


"""
The KS test compares the cumulative distribution functions (CDF) of the two samples 
and computes the maximum difference between them. 
This maximum difference, often denoted as the D-statistic, provides a measure of the overlap between the two distributions.
The overlap score as 1 - D and print it out.

The overlap score ranges from 0 (no overlap) to 1 (complete overlap).
"""
def Kolmogorov_Smirnov(labels, scores):
    list1 = [s for s,l in zip(scores, labels) if(l==0)]
    list2 = [s for s,l in zip(scores, labels) if(l==1)]
    
    D, p = ks_2samp(list1, list2)
    overlap_score = 1 - D
    
    return overlap_score

    

"""
The EMD measures the minimum amount of "work" required to transform one distribution into another,
where the "work" is defined as the amount of mass moved times the distance it is moved.
The overlap score as 1 - EMD and print it out.

The overlap score ranges from 0 (no overlap) to 1 (complete overlap).
"""
def wasserstein_dist(labels, scores):
    list1 = [s for s,l in zip(scores, labels) if(l==0)]
    list2 = [s for s,l in zip(scores, labels) if(l==1)]
    
    overlap_score = 1 - wasserstein_distance(list1, list2)
    
    return overlap_score
    
"""
measure of the distance between the two distributions in terms of their means and variances.
From 0 (no separation between the distributions) to infinity (complete separation between the distributions)
"""

def separation_index(labels, scores):
    list1 = [s for s,l in zip(scores, labels) if(l==0)]
    list2 = [s for s,l in zip(scores, labels) if(l==1)]
    
    mean1 = np.mean(list1)
    mean2 = np.mean(list2)
    var1 = np.var(list1)
    var2 = np.var(list2)
    
    S = abs(mean1 - mean2) / np.sqrt(var1**2 + var2**2)
    
    return S

##-----------------------------------------------------------------------------------------------------------------
##--------------------------------------------- Compare metrics of xp ---------------------------------------------

# ALL TOPICS
TOPICS_TO_USE_ZARATE = ['impeachment-5-10',
                    'menciones-1-10enero',
                    'menciones-20-27marzo',
                    'area51', 
                    'OTDirecto20E',
                    'bolsonaro27',
                    'bolsonaro28', 
                    'VanduMuruganAJITH',
                    'nintendo',
                    'messicumple', 
                    'wrestlemania',
                    'kingjacksonday',
                    'kavanaugh06-08',
                    'bolsonaro30', 
                    'notredam',
                    'Thanksgiving',
                    'halsey', 
                    'feliznatal', 
                    'kavanaugh16',
                    'kavanaugh02-05',
                    'EXODEUX', 
                    'bigil', 
                    'lula_moro_chats',
                    'menciones-05-11abril',
                    'LeadersDebate',
                    'championsasia', 
                    'menciones05-11mayo',
                    'pelosi', 
                    'SeungWooBirthday',
                    'menciones-11-18marzo']

# # TRAIN SET (USR_QC)
# TOPICS_TO_USE_ZARATE = ['menciones-1-10enero',
#                                 'menciones-20-27marzo',
#                                 'menciones-05-11abril',
#                                 'menciones05-11mayo',
#                                 'menciones-11-18marzo',
#                                 'bolsonaro27',
#                                 'bolsonaro28',
#                                 'bolsonaro30',
#                                 'lula_moro_chats',
#                                 'impeachment-5-10',
#                                 'EXODEUX',
#                                 'notredam',
#                                 'bigil',
#                                 'VanduMuruganAJITH',
#                                 'championsasia',
#                                 'SeungWooBirthday',
#                                 'nintendo',
#                                 'halsey',
#                                 'kingjacksonday',
#                                 'OTDirecto20E']

# # TEST SET (USR_QC)
# TOPICS_TO_USE_ZARATE = ['kavanaugh16',
#                         'kavanaugh02-05',
#                         'kavanaugh06-08',
#                         'pelosi',
#                         'LeadersDebate',
#                         'messicumple',
#                         'feliznatal',
#                         'area51',
#                         'Thanksgiving',
#                         'wrestlemania']

SCORE_TO_COMPARE = {'BASELINE': ['rwc_score'], #, 'dipole_GAR'],
                    'XP': ['proba', 'prediction']} #, 'bayes']}
data_type = 'ZARATE/RETWEET'


#------ param----------
TYPE_GRAPH = 'connex'
XP_TO_COMPARE = ['GRAPH', 'TEXT', 'GRAPH_TEXT_B1']
type_sets = ['TEST'] #'TRAIN', 'TEST'


to_print = 'metric' # score / metric / neither
to_plot = False
print_baseline = True


"""
Compare QC scores via metrics
type_topics = (ut.mydata, topic, from_date, to_date, step)

/data/DEV_DATA/TWITTER API/other/data_complete_vaccine_chloro/data/data_objects/USR_QC_connex/ZARATE/RETWEET/

"""
if __name__ == "__main__":
    path = data_path  + 'USR_QC_'+TYPE_GRAPH + '/' + data_type + '/' 
    print( '>>>>>', TYPE_GRAPH)
    if(print_baseline == True):
        print('************************** BASELINE ****************************')
        results = gp.load_json(path + 'baseline_score.json')
    
        for score in SCORE_TO_COMPARE['BASELINE']:
            print('******', score, '******')
            scores = []
            labels = []  
            times = []
            topic = []
            if('ZARATE' in data_type):
                list_topics = TOPICS_TO_USE_ZARATE
                    
            #1. get data
            the_res = {}
            for topic in results.keys():   
                if(topic in list_topics):
                    scores.append(results[topic][score][0])
                    times.append(results[topic][score][1])
                    labels.append(ut.data_zarate_labels[topic])
                    the_res[topic] = [results[topic][score][0],
                                      results[topic][score][1],
                                      ut.data_zarate_labels[topic]] 
            
            #2. Get results  
            if(to_print == 'score'):
                #a. print scores
                for k,v in sorted(the_res.items(), key=lambda e: e[1][0]): #score, time, label
                    print('            +', k, ':', v[0], v[2])
            elif(to_print == 'metric'):
                #b. compute score
                if(len(scores) > 0):
                    roc_auc = round(roc_auc_score(labels, scores), 3)
                    mae = round(mean_absolute_error(labels, scores), 3)
                    ks_score = round(Kolmogorov_Smirnov(labels, scores), 3)
                    wass_dist = round(wasserstein_dist(labels, scores), 3)
                    separation_idx = round(separation_index(labels, scores), 3)
                    avg_runtime = round(sum(times)/len(times) , 2)           
                    
                    print('            + roc_auc =', roc_auc)
                    print('            + mean_abs_error =', mae)
                    print('            + ks_score =', ks_score)
                    # print('            + wass_dist =', wass_dist)
                    # print('            + separation_idx =', separation_idx)
                    print('            --------------------')
                    print('            + avg_runtime =', avg_runtime, 'min')
            print('\n')
        print('\n')
    print('\n')
    
    for xp in XP_TO_COMPARE:
        print('**************************', xp,'****************************')
        xp_path = path + xp + '/'
        results = gp.load_json(xp_path + 'results.json')
        
        for set_ in type_sets:
            print('--------------', set_, '--------------')
            for score in SCORE_TO_COMPARE['XP']:
                print('******', score, '******')
                scores = []
                labels = []  
                times = []
                acc = []
                topic = []
                if('ZARATE' in data_type):
                    list_topics = TOPICS_TO_USE_ZARATE
                        
                #1. get data
                the_res = {}
                for topic in results[set_]['qc_score'].keys():   
                    if(topic in list_topics):
                        scores.append(results[set_]['qc_score'][topic][score])
                        runtime_temp = -1
                        if('runtime' in results[set_]['qc_score'][topic]):
                            runtime_temp = results[set_]['qc_score'][topic]['runtime']
                        times.append(runtime_temp)
                        acc.append(results[set_]['qc_score'][topic]['accuracy'])
                        labels.append(ut.data_zarate_labels[topic])
                        the_res[topic] = [results[set_]['qc_score'][topic][score],
                                          runtime_temp,
                                          ut.data_zarate_labels[topic]] 
                
                #2. Get results  
                if(to_print == 'score'):
                    #a. print scores
                    for k,v in sorted(the_res.items(), key=lambda e: e[1][0]): #score, time, label
                        print('            +', k, ':', v[0], v[2])
                elif(to_print == 'metric'):
                    #b. compute score
                    if(len(scores) > 0):
                        roc_auc = round(roc_auc_score(labels, scores), 3)
                        mae = round(mean_absolute_error(labels, scores), 3)
                        ks_score = round(Kolmogorov_Smirnov(labels, scores), 3)
                        wass_dist = round(wasserstein_dist(labels, scores), 3)
                        separation_idx = round(separation_index(labels, scores), 3)
                        avg_runtime = round(sum(times)/len(times) , 2)   
                        avg_accuracy = round(sum(acc)/len(acc) , 2)   
                        
                        print('            + roc_auc =', roc_auc)
                        print('            + mean_abs_error =', mae)
                        print('            + ks_score =', ks_score)
                        # print('            + wass_dist =', wass_dist)
                        # print('            + separation_idx =', separation_idx)
                        print('            --------------------')
                        print('               + avg_acc =', avg_accuracy)
                        print('               + avg_runtime =', avg_runtime, 'min')
                print('\n')
            print('\n')
        print('\n')
        
        
        results = gp.load_json(xp_path + 'train_info.json')
        
        print('------- TRAINING INFORMATION -------')
        print('train_runtime', ':', results['train_runtime'])
        print('train_loss', ':', results['train_loss'])
        print('test_acc', ':', results['test_acc'])

        print('\n')
        
        
        
        
        
        
        
        
       

    