#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 17 16:34:30 2023

@author: samy
"""

import matplotlib.pyplot as plt
import numpy as np

import pickle
import os

def load_pickle(path_file):
    if(os.path.isfile(path_file) == False):
        print(path_file, ': <not found>')
        return False
    else:     
        with open(path_file, 'rb') as file:
            data = pickle.load(file)
    return data


def plot_prop(topic, proba, bin_size=0.01):
    bins = np.arange(0, 1.01, bin_size)
    counts, _ = np.histogram(proba, bins=bins)
    cumulative_counts = np.cumsum(counts)
    proportions = cumulative_counts / len(proba)
    
    plt.plot(bins[:-1], proportions)
    plt.xlabel('Probability')
    plt.ylabel('Cumulative Proportion')
    plt.title('Cumulative Distribution :'+str(topic))
    plt.grid(True)
    plt.show()
    

path = "/data/DEV_DATA/TWITTER API/other/data_complete_vaccine_chloro/data/data_objects/USR_QC_connex/ZARATE/jerome/"
filename = path + 'TEST_predictions.pickle'
topics = ['pelosi', 'kavanaugh16', 'area51', 'Thanksgiving'] 

data_objects_by_topic = load_pickle(filename)

for topic in topics:
    print('-----', topic, '-----')
    node_ids, _, _, node_probas, _ = data_objects_by_topic[topic]
    plot_prop(topic, node_probas)
    print(' ++ score:', sum(node_probas)/len(node_probas))
    
    
    
    
    
    
