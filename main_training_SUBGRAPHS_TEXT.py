#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 21 09:45:01 2022

@author: samy
"""

import graph_processing as gp
import quantification_score as qs
import text_models as tm
import util as ut

import numpy as np
import collections
import time
import os


code_graph = 'connex' #TODO
code_xp = 'TEXT' #TODO

#----------------------------------------------- VARIABLE --------------------------------------------------
list_reduction_method = [ut.UMAP, ut.TSNE] #[ut.TSNE, ut.UMAP]
device = ['0'] #torch.device('cuda:0') #'cuda:0', 'cpu'
type_graph = ut.RETWEET

type_score = ['rwc_score', 'dipole_GAR'] #, 'dipole_GPT', 'betweeness'

get_baseline_score = True #rwc_score
get_plot = False

#--------------- SHELL VARIABLE ----------------

#---- type_graph of xp ----
is_undirected = True
to_connex = False #TODO
minimum_links = 1

#---- Graph parameters ----
text_model = ut.BASIC #ut.BASIC, ut.SENTIMENT, ut.ARGUMENT, ut.EMOTION, False, ut.HASHTAGS #TODO
is_edge_weighted = True
type_label = 'metis' #louvain

#----FULL Model Parameters ----
epochs = 15 #30

# #---- Model graph Parameters ----
# layer_dimension = [192]
# learning_rate_gnn = 0.001
# weight_decay = 0.05
# batch_gnn = 64

#---- Model bert Parameters ----
text_aggregator = ut.MAX_AGGR
learning_rate_bert = 2e-5
batch_bert = 64 

#------------------------------------- ------------ ---- ------------ ----------------------------- --------

inputs = (ut.data_zarate, 'ZARATE', None, None, 1)

"""
GNN_QC semi-supervised for my data
type_topics = (ut.mydata, topic, from_date, to_date, step)
"""
if __name__ == "__main__":

    print('    >>>>>>>>>>>>>>>>>>>>>>>>     Welcome to USR_QC --TEXT-- training from SB  <<<<<<<<<<<<<<<<<<<<<<<<< \n')
    t0 = time.time()  
    ut.update_data_object_folder('USR_QC'+'_'+code_graph)
    period=(inputs[2], inputs[3])
    data_type = inputs[0]
    general_topic = inputs[1]
    step = inputs[4]
    path = gp.create_xp_folder(general_topic, type_graph)
    data_object_path = path + 'temp_topic_objects/'
    if(os.path.isdir(data_object_path) == False):
        os.mkdir(data_object_path)
    if(general_topic == 'ZARATE'):
        all_topics = ut.topic_qc_zarate

    print('******************', data_type, 'PARAMETERS *******************')
    print('CODE_XP : ', code_xp)
    print('general_topic : ', general_topic)
    print('to_connex : ', to_connex)
    print('is_undirected : ', is_undirected)
    print('minimum_links : ', minimum_links)
    print('epochs : ', epochs)
    print('   + are edges weight :', is_edge_weighted)
    print('text_model : ', text_model)
    print('   + text_aggregator : ', text_aggregator)
    print('   + batch_bert : ', batch_bert)
    print('   + learning_rate_bert : ', learning_rate_bert)
    print('device :', device)
    print('****************   *   ****   *   ******************')
    print('\n')

    print('############################# STEP 1 - DATA PROCESSING OF ALL TOPICS ############################# \n')
    t1 = time.time()
    dataset_file = path + 'dataset_TEXT_'+str(text_model)+'.pickle'
    if(os.path.isfile(dataset_file) == True):
            dataset_by_topic = gp.load_pickle(dataset_file)  
            print(' - Find', len(dataset_by_topic), 'topics pre-processed !')
    else:
            dataset_by_topic = {}
    cpt_top = 0
    
    for topic in all_topics:
        t2 = time.time()
        t3 = time.time()
        key = topic
        cpt_top+=1            
        if(data_type == ut.mydata):
            uuid_request = ut.request_to_topic[topic]
            label = ut.my_data_labels[topic]
        elif(data_type == ut.data_zarate):
            uuid_request = topic
            label = ut.data_zarate_labels[topic]
        else:
            uuid_request = topic
            label = ut.data_qc_labels[topic]
        
        print('*======================================================================================*')
        print('-------- graph',cpt_top,'/', len(all_topics),':', topic, label, ':', key, '* -------- \n')
        print('*======================================================================================*')
        
        if(key in dataset_by_topic.keys()):
            print(' > already done !')
            continue
        
        current_path = data_object_path + str(key) +'/'
        if(os.path.isdir(current_path) == False):
            os.mkdir(current_path)        
        object_file = current_path + 'data_object.pickle'
        res = gp.load_pickle(object_file)
        if(res == False):
            ### 1. get user graph information
            print("\n ### 1. get user graph information")
            if(data_type == ut.mydata):
                dict_tweets, dict_nodes, dict_edges, dict_edge_weights = gp.create_user_info_graph(type_graph, uuid_request, period, to_connex, minimum_links)
            elif(data_type == ut.dataqc_text):
                dict_tweets, dict_nodes, dict_edges, dict_edge_weights = gp.create_user_QC_TEXT_graph(topic, type_graph, to_connex, minimum_links)
            elif(data_type == ut.data_zarate):
                dict_tweets, dict_nodes, dict_edges, dict_edge_weights = gp.create_user_ZARATE_graph(current_path, topic, type_graph, to_connex, minimum_links)
            else:
                dict_tweets = None
                dict_nodes, dict_edges, dict_edge_weights = gp.create_user_QC_graph(topic, type_graph, to_connex, minimum_links)
            if(dict_nodes is None and dict_edges is None):
                print(' /!\ /!\ /!\ We did not find the topic ', topic, ' on our database /!\ /!\ /!\ ')
                continue
            print('--- 1. ', round(((time.time() - t3)/60), 2), 'min ---') 
            t3 = time.time()
              
            ## 2. get new node id and edge corresponding
            print("\n ### 2. get new node id and edge corresponding >>>")
            dict_user_to_id, dict_id_to_user, dict_new_nodes, dict_new_edges, dict_new_edge_weights = gp.create_new_node_id(dict_nodes, dict_edges, dict_edge_weights)
            print('--- 2. ', round(((time.time() - t3)/60), 2), 'min ---') 
            t3 = time.time()
                        
            ### 3. get node features
            print("\n ### 3. get node features")
            # dict_node_text, dict_node_tokens, dict_node_degree = (None, None, None)
            type_model_temp = text_model
            if(text_model == False):
                type_model_temp = ut.BASIC
            dict_node_text, dict_node_tokens, dict_node_degree = gp.get_user_node_features_v2(dict_new_nodes, dict_new_edges, dict_tweets, type_model_temp, batch_bert)

            print('%%% Test dict_node_features : ', len(dict_node_text), len(dict_node_tokens), len(dict_node_degree))
            print(' --- 3. ', round(((time.time() - t3)/60), 2), 'min ---') 
            t3 = time.time()
            
            ### 4. get node label communities
            print("\n ### 4. get node label communities")
            print('  #4.a. METIS LABEL')
            dict_node_label_metis = {}
            # dict_node_label_metis = gp.create_communities(dict_new_edges, method='metis')
            print('%%% Test dict_node_label_metis : ', len(dict_node_label_metis))     
            
            print('  #4.b. LOUVAIN LABEL')
            dict_node_label_louvain = {}
            # dict_node_label_louvain = gp.create_communities(dict_new_edges, method='louvain')
            print('%%% Test dict_node_label_louvain : ', len(dict_node_label_louvain))         
            
            print(' --- 4. ', round(((time.time() - t3)/60), 2), 'min ---') 
            t3 = time.time()
    
            ### 5. get node label communities
            print("\n ### 5. get node label from unsupervised learning [PAUSE]")
            # dict_node_label_sample = gp.get_user_node_label(label_method, topic, type_graph)
            dict_node_label_sample = None
            print(' --- 5. ', round(((time.time() - t3)/60), 2), 'min ---')
            t3 = time.time()
            
            ###  6. Create giphi files for vizu
            print("\n ### 6. create giphi files label")
            # gp.save_giphi_files_comu(current_path, (dict_node_label_metis, dict_node_label_louvain), dict_new_edges, type_label='metis_louvain')
            
            ### 6. Save pickle files
            print("\n ### 7. Save pickle files")
            data_to_save = (dict_tweets, dict_nodes, dict_edges, dict_new_nodes, dict_new_edges, dict_user_to_id, dict_id_to_user, dict_new_nodes,
                            dict_node_label_metis, dict_node_label_louvain, dict_node_label_sample, dict_node_text, dict_node_tokens, dict_node_degree, dict_new_edge_weights)
            gp.pickle_save(object_file, data_to_save)
            print(' --- 7. ', round(((time.time() - t3)/60), 2), 'min ---') 
            t3 = time.time()
        
        else:
            print('<data object already loaded>')
            dict_tweets, dict_nodes, dict_edges, dict_new_nodes, dict_new_edges, dict_user_to_id, dict_id_to_user, dict_new_nodes, dict_node_label_metis, dict_node_label_louvain, dict_node_label_sample, dict_node_text, dict_node_tokens, dict_node_degree, dict_new_edge_weights = res
            print(' --- 0. ', round(((time.time() - t3)/60), 2), 'min ---') 
            t3 = time.time()
            

        #0. get tweets for each user 
        print('\n ### 8. get tweets for every user (even w/ no tweets)')
        t3 = time.time()
        tweets_ev_user_file = current_path + 'DATA_tweets_by_every_user.pickle'
        if(os.path.isfile(tweets_ev_user_file) == False):
            dict_tweets_2, dict_tweets_by_all_user = gp.read_text_zarate_tweet_for_every_user(topic)
            dict_tweets_by_all_new_user = {}
            for user, id_ in dict_user_to_id.items():                
                dict_tweets_by_all_new_user[id_] = dict_tweets_by_all_user[user]               
            
            gp.pickle_save(tweets_ev_user_file, (dict_tweets_by_all_user, dict_tweets_by_all_new_user))
        else:
            dict_tweets_by_all_user, dict_tweets_by_all_new_user = gp.load_pickle(tweets_ev_user_file)
        print(' --- 8. ', round(((time.time() - t3)/60), 2), 'min ---') 
            
    
        print("\n\n ### 8. Save ALL in dataset")     
        dataset_by_topic[key] = [dict_new_nodes, dict_tweets, dict_tweets_by_all_new_user]        
        gp.pickle_save(dataset_file, dataset_by_topic)
        
        print(' **** TOTAL time for topic : ', round(((time.time() - t2)/60)/60, 2), 'hours ****') 
        print('\n\n')
        
        
    print(' ********* TIME STEP 1: DATA PROCESSING --> ', round(((time.time() - t1)/60)/60, 2), 'hours *********')     
    print('\n\n\n') 


    print('############################# STEP 2 - CREATE COMPLETE DATASET ############################# \n')   
    t1 = time.time()
    dataset_file2 = path + 'dataset_TEXT_v2_'+str(text_model)+'.pickle'
    if(os.path.isfile(dataset_file2) == True):
        TRAIN_SET, TEST_SET = gp.load_pickle(dataset_file2)  
        print(' - Find v2', len(dataset_by_topic), 'topics pre-processed !')
    else:
        print(" \n ### 1. create dict by tweets for all topics")
        t2 = time.time()
        TRAIN_SET = {}
        TEST_SET = {}
        for t, v in dataset_by_topic.items():
            text_tweets = []
            label_tweets = []
            id_tweets = []
            
            d_new_nodes, d_tweets, _ = v
            t_lab = ut.data_zarate_labels[t]
            for t_usr, list_tweets in d_new_nodes.items():
                if(list_tweets is not None):
                    for tweet_id in list_tweets:
                        if(tweet_id not in id_tweets):
                            text_tweets.append(d_tweets[tweet_id])
                            label_tweets.append(t_lab)
                            id_tweets.append(tweet_id)
                
            if(t in ut.data_zarate_split['TRAIN']):
                TRAIN_SET[t] = [text_tweets, label_tweets, id_tweets]
            elif(t in ut.data_zarate_split['TEST']):
                 TEST_SET[t] = [text_tweets, label_tweets, id_tweets]
            else:
                print('- wtf problem!')
        gp.pickle_save(dataset_file2, (TRAIN_SET, TEST_SET))
                        
                        
    print(' ********* TIME STEP 2: CREATE COMPLETE DATASET --> ', round(((time.time() - t1)/60)/60, 2), 'hours *********')     
    print('\n\n\n') 
        
        
    print('############################################ STEP 2- TRAIN bert model ############################################ \n')
    xp_path = path + code_xp + '/'
    t1 = time.time()
    if(os.path.isdir(xp_path) == False):
        os.mkdir(xp_path)   
    model_name = type_graph + '_BERT_' + str(text_model) + '.pth'
    model_path = xp_path + model_name
    
    p_model, tokenizer, results = tm.bert_fine_tuning(xp_path, model_name, TRAIN_SET, TEST_SET, device,
                        bert_epochs = 30, bert_batches = 32, max_length = 256, lr_rate = 2e-5,
                        truncate = True, discr_lr = True)

    print(' ********* TIME STEP 2 - TRAINING MODEL --> ', round(((time.time() - t1)/60)/60, 2), 'hours *********')     
    print('\n\n\n')
    
    
    print('############################################ STEP 3- GET QC SCORES ############################################ \n')
    t1 = time.time()
    #3. Get node embeddings & label predicted
    print('\n ### 1. Get TEST prediction, and QC score !')
    t3 = time.time()
    print(' > nbs_topics:', len(TEST_SET))
    test_topics = ut.data_zarate_split['TEST']
    test_results, test_data_objects_by_topic = tm.predict_model_SUBGRAPHS(test_topics,
                                                                dataset_by_topic,
                                                                p_model, 
                                                                text_model,
                                                                device,
                                                                xp_path,
                                                                bert_batches = batch_bert,
                                                                max_length = 256,
                                                                type_set='TEST',
                                                                type_topic='ALL',
                                                                iter_=5000)    
    print(' --- 1', round(((time.time() - t3)/60), 2), 'min ---') 
    
    print(' ********* TIME STEP 3 - GET QC SCORES --> ', round(((time.time() - t1)/60)/60, 2), 'hours *********')     
    
    print('\n\n')
    print('-- -- -- -- -- TOTAL RUNTIME : ', round(((time.time() - t0)/60)/60, 2), 'hours -- -- -- -- --')
        
    
    print('    >>>>>>>>>>>>>>>>>>>>>>>>     See you soon! :)    <<<<<<<<<<<<<<<<<<<<<<<<< \n')    