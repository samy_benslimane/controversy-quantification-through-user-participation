#!/bin/bash

#SBATCH --job-name=job_samy_usr_qc # nom du job

#SBATCH  --cpus-per-task=4
#SBATCH --partition=gpu_p2

#SBATCH --gres=gpu:2  # nombre de GPU à réserver
#SBATCH --time=80:00:00             # (HH:MM:SS)
#SBATCH --output="/gpfswork/rech/kdj/unn89uy/USR_QC/log/test_%j.out"
#SBATCH --error="/gpfswork/rech/kdj/unn89uy/USR_QC/log/test_%j.err"

#SBATCH -A kdj@v100
#SBATCH --qos=qos_gpu-t4

module load pytorch-gpu/py3/2.0.1 metis/5.1.0

# echo des commandes lancées
set -x

# exécution shell for all subreddit #SBATCH  --mem=160G
sh "/gpfswork/rech/kdj/unn89uy/USR_QC/jz_usr_qc.sh"

