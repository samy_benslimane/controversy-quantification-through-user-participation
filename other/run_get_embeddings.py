#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 16 16:13:39 2023

@author: samy
"""

import matplotlib.pyplot as plt
from matplotlib.pyplot import figure

import graph_processing as gp
import util as ut

import time
import os

code_graph = 'connex' #'connex', 'not_connex' #TODO
code_xp = 'GRAPH_TEXT_B1' #TODO
topics = ['pelosi', 'menciones05-11mayo']
reduction_method = [ut.UMAP, ut.TSNE]

type_graph = ut.RETWEET
inputs = (ut.data_zarate, 'ZARATE', None, None, 1)

p = '/data/DEV_DATA/TWITTER API/other/data_complete_vaccine_chloro/data/data_objects/USR_QC_connex/ZARATE/RETWEET/GRAPH_TEXT_B1/plot/reduced_object.pickle'


"""
plot scatter point with matplotlib 
"""
def plot_plt_local(reduced_node_embeddings, node_community, filename=None):   
    figure(figsize=(12, 8), dpi=100)
        
    colormap = ['blue', 'red', 'black']
    colors = [colormap[x] for x in node_community]

    plt.scatter(reduced_node_embeddings[:,0], reduced_node_embeddings[:,1],
                s=5, c=colors) 
    
    if(filename is not None):
        plt.savefig(filename)
    plt.show()
    
    return 1



"""
 	(1) save_plot.py [for C topics]
 			+ get embedding
 			+ reduce (UMAP, T-SNE)
 			+ label = community
"""
if __name__ == "__main__":
    print('    >>>>>>>>>>>>>>>>>>>>>>>>     RUN EMBEDDINGS     <<<<<<<<<<<<<<<<<<<<<<<<< \n')
    t0 = time.time()  
    if(os.path.isfile(p) == False):
        ut.update_data_object_folder('USR_QC'+'_'+code_graph)
        period=(inputs[2], inputs[3])
        data_type = inputs[0]
        general_topic = inputs[1]
        step = inputs[4]
        
        path = gp.create_xp_folder(general_topic, type_graph)
        data_object_path = path + 'temp_topic_objects/'       
        xp_path = path + code_xp +'/'
        
        plot_path = xp_path + 'plot/'
        if(os.path.isdir(plot_path) == False):
            os.mkdir(plot_path)
        
        embedding_file = xp_path + 'TEST_predictions.pickle'
        data_objects_by_topic_test = gp.load_pickle(embedding_file)
        embedding_file = xp_path + 'TRAIN_predictions.pickle'
        data_objects_by_topic_train = gp.load_pickle(embedding_file)   
        dict_data = {}
        for topic in topics:
            object_file = data_object_path + str(topic) +'/data_object.pickle'
            dict_tweets, _, _, dict_new_nodes, _, _, _, _, dict_node_label_metis, _, _, _, _, _, _ = gp.load_pickle(object_file)
            if(topic in data_objects_by_topic_test.keys()):
                node_ids, text_embeddings, node_embeddings, node_probas, node_predictions = data_objects_by_topic_test[topic]
            else:
                node_ids, text_embeddings, node_embeddings, node_probas, node_predictions = data_objects_by_topic_train[topic]
            
            label_metis = [v for k,v in dict_node_label_metis.items()]
            
            dict_data[topic] = {'label_metis':label_metis}
            print('  -', topic,'\n')
            for r_meth in reduction_method:
                print('    -', r_meth, '\n')
                #text_embedding
                print('      - text \n')
                reduced_text_emb = gp.reduce_dimension(r_meth, text_embeddings)
            
                filename = topic+'_'+r_meth+'_metis.png'
                title = topic + ' - ' + str(ut.data_zarate_labels[topic])
                plot_plt_local(reduced_text_emb, label_metis, plot_path+filename)
                  
                #node_embedding
                print('      - noeuds \n')
                reduced_node_emb = gp.reduce_dimension(r_meth, node_embeddings)
                filename = topic+'_'+r_meth+'_metis.png'
                title = topic + ' - ' + str(ut.data_zarate_labels[topic])
                plot_plt_local(reduced_node_emb, label_metis, plot_path+filename)   
                
                dict_data[topic][r_meth] = [reduced_text_emb, reduced_node_emb]
            
        gp.pickle_save(plot_path+'reduced_object.pickle', dict_data)
        
    else:
        print(' in local ')
        dict_data = gp.load_pickle(p)
        for temp_t in topics:
            temp_r_meth = ut.UMAP
            
            node_community = dict_data[temp_t]['label_metis']
            reduced_text_emb, reduced_node_emb = dict_data[temp_t][temp_r_meth]
            
            no_color = True
            if(no_color == True):
                node_community = [2] * len(node_community)
            plot_plt_local(reduced_text_emb, node_community)
            # plot_plt_local(reduced_node_emb, node_community)
        
        
        
        
        
 
        

        
        
        
        
        